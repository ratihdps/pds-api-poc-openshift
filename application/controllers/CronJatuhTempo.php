<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Curl\Curl;
class CronJatuhTempo extends CI_Controller
{
    // Path CSV File for Notification Jatuh Tempo
    private $pathCsv = './files/csv/';
    private $csvReadCount = 100;
    private $dateTimeNow;
    private $emailAdmin = array(
        'sibangprogrammer@gmail.com'
    );
    private $templateTop;
    private $templateBottom;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('User','NotificationModel','PaymentModel','EmasModel','GadaiModel','MikroModel','MpoModel','WalletModel','ConfigModel'));
        $this->load->helper(array('Message'));
        $this->dateTimeNow = date('Y-m-d H:i:s');

        $data['title'] = 'Jatuh Tempo';

        $this->templateTop = $this->load->view('mail/email_template_top', $data, TRUE);
        $this->templateBottom = $this->load->view('mail/email_template_bottom', $data, TRUE);
    }

    function notifikasi() {
        $this->filesNameSave();
        $this->notifikasiJatuhTempoRun();
    }

    /**
     * @param $pathFile
     * @return int
     *
     * Untuk menghitung jumlah row yang ada pada file CSV
     */
    function csvTotalRow($pathFile) {
        $this->load->library('CSVReader');
        $csvData = $this->csvreader->parse_file($pathFile);
        return count($csvData);
    }

    /**
     * Digunakan untuk menyimpan file CSV yang ada di server PDS API
     */
    function filesNameSave() {
        $this->load->helper('directory');
        $files = directory_map($this->pathCsv);
        $data_insert = array();
        if (!empty($files)) {
            foreach($files as $filename) {
                $check = $this->db->where('filename', $filename)->get('files_jatuh_tempo')->num_rows();
                if($check == 0) {
                    $data_insert[] = array(
                        'filename'=>$filename,
                        'row_total'=>$this->csvTotalRow($this->pathCsv.$filename),
                        'success'=>0,
                        'errors'=>0,
                        'last_row'=>0,
                        'keterangan'=>'[]'
                    );
                }
            }
        }

        if(count($data_insert) > 0) {
            $this->db->insert_batch('files_jatuh_tempo', $data_insert);
        }
    }

    /**
     * Digunakan untuk memulai membaca row file CSV
     */
    function notifikasiJatuhTempoRun() {
        $filesDb = $this->db->get('files_jatuh_tempo')->result();
        foreach($filesDb as $r) {
            if($r->row_total != $r->last_row) {
                $this->notifikasiJatuhTempoProses($r->filename, $r->row_total, $r->last_row, $r->id, $r->success, $r->keterangan);
            }
        }
    }

    /**
     * @param $filename
     * @param $totalRow
     * @param $lastRow
     * @param $id
     * @param $successCount
     *
     * Proses untuk mengirimkan notifikasi Jatuh Tempo dalam bentun email dan notifikasi mobile user
     */
    function notifikasiJatuhTempoProses($filename, $totalRow, $lastRow, $csvDataId, $successCount, $keterangan) {
        $this->load->library('CSVReader');

        $pathFile = $this->pathCsv.$filename;
        $csvData = $this->csvreader->parse_file($pathFile);
        $rowTo = $lastRow+$this->csvReadCount;
        $keterangan = json_decode($keterangan, TRUE);
        $success = 0;
        $errors = 0;
        $countSended = 0;
        $errorStatus = FALSE;

        for($i=$lastRow; $i<$rowTo; $i++) {
            if(isset($csvData[$i])) {
                // Check field kodeTemplate
                if(empty($csvData[$i]['kodeTemplate'])) {
                    $message = array(
                        'datetime'=>$this->dateTimeNow,
                        'filename'=>$filename,
                        'row'=>$i+2,
                        'data'=>array(),
                        'message'=>'Field kodeTemplate tidak ada pada File CSV'
                    );
                    $keterangan[count($keterangan)+1] = $message;
                    $errorStatus = TRUE;
                    $errors++;

                    break;
                }

                // Check field CIF
                if(empty($csvData[$i]['CIF'])) {
                    $message = array(
                        'datetime'=>$this->dateTimeNow,
                        'filename'=>$filename,
                        'row'=>$i+2,
                        'data'=>array(),
                        'message'=>'Field CIF tidak ada pada File CSV'
                    );
                    $keterangan[count($keterangan)+1] = $message;
                    $errorStatus = TRUE;
                    $errors++;

                    break;
                }

                // Check ketersediaan kodeTemplate
                $template = $this->db->select('isi, judul, tagline')
                    ->where('kode_template', $csvData[$i]['kodeTemplate'])
                    ->get('ref_template_notifikasi')
                    ->row();

                if(count($template) == 0) {
                    $message = array(
                        'datetime' => $this->dateTimeNow,
                        'filename' => $filename,
                        'row' => $i + 2,
                        'data' => array(
                            'kodeTemplate' => $csvData[$i]['kodeTemplate'],
                        ),
                        'message' => 'Kode Template ' . $csvData[$i]['kodeTemplate'] . ' tidak tersedia di Super Administrator'
                    );
                    $keterangan[count($keterangan)+1] = $message;
                    $errorStatus = TRUE;
                    $errors++;

                    break;
                }

                // Check ketersediaan CIF
                $user = $this->db->select('email, fcm_token, user_AIID, token_web')
                    ->where('cif', $csvData[$i]['CIF'])
                    ->get('user')
                    ->row();

                if(count($user) == 0) {
                    $message = array(
                        'datetime'=>$this->dateTimeNow,
                        'filename'=>$filename,
                        'row'=>$i+2,
                        'data'=>array(
                            'cif'=>$csvData[$i]['CIF'],
                        ),
                        'message'=>'CIF '.$csvData[$i]['CIF'].' tidak tersedia di PDS'
                    );
                    $keterangan[count($keterangan)+1] = $message;
                    $errorStatus = TRUE;
                    $errors++;

                    break;
                }

                $check = $this->db->where('user_AIID', $user->user_AIID)
                        ->where('file_jatuh_tempo_id', $csvDataId)
                        ->get('notifikasi')
                        ->row();

                if(count($check) == 0){
                    $success++;
                    $countSended++;
                    $this->send($user, $template, $csvData[$i], $csvDataId);
                }
            }
        }

        $this->db->where('filename', $filename)->update('files_jatuh_tempo', array(
            'last_row'=>$lastRow+$countSended,
            'success'=>$success,
            'errors'=>$errors,
            'keterangan'=>json_encode($keterangan)
        ));

        if($errorStatus) {
            foreach($this->emailAdmin as $email) {
                Message::sendEmail($email, 'Notifikasi Jatuh Tempo - Troubles', json_encode($keterangan));
            }
        }
    }

    /**
     * @param $user
     * @param $template
     * @param $csvDataRow
     * @param $csvDataId
     *
     * Proses pengiriman notifikasi email dan mobile
     */
    function send($user, $template, $csvDataRow, $csvDataId) {
        $user_AIID = $user->user_AIID;
        $email = $user->email;
        $fcm_token = $user->fcm_token;
        $token_web = $user->token_web;
        $template_isi = html_entity_decode($template->isi);
        $template_judul = $template->judul;
        $template_tagline = $template->tagline;

        $find = array();
        $repl = array();

        foreach($csvDataRow as $field=>$value) {
            $find[] = "{{".$field."}}";
            $repl[] = $value;
        }

        $template_isi = str_replace($find, $repl, $template_isi);
        $template_html = $this->templateTop.$template_isi.$this->templateBottom;

        $data_notifikasi = array(
            'user_AIID'=>$user_AIID,
            'type'=>'jatuh_tempo',
            'jenisTransaksi'=>'NJT',
            'type'=>'jatuh_tempo',
            'content_type'=>'html',
            'judul'=>$template_judul,
            'tagline'=>$template_tagline,
            'isi'=>$template_html,
            'isi_minimal'=>$template_isi,
            'status'=>'1',
            'status_send'=>'1',
            'file_jatuh_tempo_id'=>$csvDataId
        );

        $notifikasi_AIID = $this->db->insert('notifikasi', $data_notifikasi);

        Message::sendEmail($email, $template_judul, $template_html);
        Message::sendFCMNotif($fcm_token, array(
            'id'=>$notifikasi_AIID,
            'title'=>$template_judul,
            'tagline'=>$template_tagline,
            'token'=>$token_web
        ));
    }
}
