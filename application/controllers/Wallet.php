<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Wallet extends CorePegadaian {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('User', 'PaymentModel', 'NotificationModel','ConfigModel'));
        $this->load->library('form_validation');
        $this->load->helper('message');
    }

    /**
     * Endpoint untuk melakukan aktifasi wallet pegadaian
     */
    function index_get() {
        $token = $this->getToken();
        if ($token) {
            
            //Check apakah user sudah link cif atau belum            
            $user = $this->User->getUser($token->id);
            if ($user->cif != "") {

                //Langsung lakukan aktifasi wallet
                $paramsWallet = array(
                    'channelId' => $token->channelId,
                    'cif' => $user->cif,
                    'noHp' => $user->no_hp,
                    'tipe' => 1
                );

                $response = $this->aktivasiWallet($paramsWallet);

                if ($response->responseCode == '00') {
                    $this->load->model('user');
                    $response_data = json_decode($response->data);
                    $data_update = array(
                        'saldo' => $response_data->saldo,
                        'norek' => $response_data->norek
                    );
                    $this->user->updateUser($user->user_AIID, $data_update);
                    $return = array(
                        'status' => 'success',
                        'message' => $response->responseDesc,
                        'data' => array(
                            'saldo' => $response_data->saldo,
                            'norek' => $response_data->norek,
                        ),
                        'reason' =>$response
                    );
                } else {
                    $return = array(
                        'status' => 'error',
                        'code' => 103,
                        'message' => '',
                        'reason' => $response
                    );
                }

                $this->set_response($return, 200);
            } else {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Lakukan link CIF dan KYC untuk mengaktifkan Wallet',
                    'code' => 102
                ), 200);
            }
            
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Endpoint untuk melakukan topup saldo wallet Pegadaian
     * @return type
     */
    function topup_post() {
        $token = $this->getToken();
        if ($token) {
            
            //Check apakah user sudah link cif atau belum
            $user = $this->User->getUser($token->id);
            
            if($user->norek == ''){
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Anda belum mempunyai rekening Wallet Pegadaian',
                    'errros' => ''
                ), 200);
                return;
            }

            $setData = array(
                'amount' => $this->post('amount'),
                'payment' => $this->post('payment'),
                'booking_code' => $this->post('booking_code'),
                'card_number' => $this->post('card_number'),
                'token_response' => $this->post('token_response')
            );
            
            $paymentMethod = $this->post('payment');
            
            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('amount', 'amount', 'required|integer');
            $this->form_validation->set_rules('payment','payment_method','required');
            
            if($paymentMethod == 'MANDIRI'){
                $this->form_validation->set_rules('booking_code','booking_code','required');            
                $this->form_validation->set_rules('card_number','card_number','required');            
                $this->form_validation->set_rules('token_response','token_response','required');            
            }
                       
            if ($this->form_validation->run() == FALSE) {
                
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => '',
                    'errros' => $this->form_validation->error_array()
                ), 200);
                
            } else {
                                
                //Validasi paymentMethod
                if($paymentMethod != 'BNI' && $paymentMethod != 'MANDIRI'){
                    $this->set_response(array(
                        'status' => 'error',                        
                        'message' => 'Invalid payment method',
                        'code' => '101'                        
                    ));
                    return;
                }                              
                
                $amount = $this->post('amount');                
                $channelId = $token->channelId;
                $noHp = $user->no_hp;
                $tipe = 1;
                $jenisTransaksi = 'TU';

                //Langsung lakukan aktifasi wallet
                $paramsWallet = array(
                    'channelId' => $channelId,
                    'amount' => $amount,
                    'flag' => 'K',
                    'jenisTransaksi' => $jenisTransaksi,
                    'noHp' => $noHp,
                    'tipe' => $tipe
                );

                $response = $this->inquiryWallet($paramsWallet);
                
                $this->load->model('WalletModel');

                if ($response->responseCode == '00') {
                                        
                    $response_data = json_decode($response->data, true);                    
                    $totalKewajiban = $response_data['totalKewajiban'];
                    $reffSwithcing = $response_data['reffSwitching'];
                    
                    if($paymentMethod == 'BNI'){
                        
                        log_message('debug', 'Payment Method BNI');
                        
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('TU','BANK','63','009');                        
                        $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('TU','BANK','63','009');

                        //Create billing BNI
                        $billingBNI = $this->createBillingVABNI(
                                $totalKewajiban + $biayaTransaksi,
                                $user->email,
                                $user->nama,
                                $user->no_hp,
                                'K',
                                'TU',
                                'Top Up Wallet', //keterangan pembayaran emas
                                $response_data['norek'],
                                '63',
                                $reffSwithcing,
                                $token->channelId
                        );
                        
                        $response_data['user_AIID'] = $user->user_AIID;
                        $response_data['amount'] = $amount;

                        if($billingBNI->responseCode == '00'){

                            
                            $billingData = json_decode($billingBNI->data);
                            
                            $response_data['virtualAccount'] = $billingData->virtualAccount;
                            $response_data['tglExpired'] = $billingData->tglExpired;
                            $response_data['biayaTransaksi'] = $biayaTransaksiDisplay;

                            $this->WalletModel->insert($response_data);

                            $return = array(
                                'status' => 'success',
                                'message' => '',
                                'data' => $response_data
                            );
                            $this->set_response($return);

                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'code' => 103,
                                'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                'reason' => $billingBNI
                            ));
                        }
                        
                    }else if($paymentMethod == 'MANDIRI'){
                        
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('TU','BANK','63','008');
                        $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('TU','BANK','63','008');

                        log_message('debug', 'Mandiri Click Pay');
                        
                        $bookingCode = sprintf("%06d", mt_rand(1, 999999));
                        $cardNumber = $this->post('card_number');
                        $tokenResponse = $this->post('token_response');
                                              
                        
                        $clickPay = $this->mandiriClickPay(
                                $totalKewajiban + $biayaTransaksi, 
                                $bookingCode, 
                                $cardNumber, 
                                $jenisTransaksi, 
                                'Top Up Wallet', 
                                $user->no_hp, 
                                $response_data['norek'], 
                                '63', 
                                $tokenResponse, 
                                $reffSwithcing, 
                                $token->channelId);
                        
                        if($clickPay->responseCode == '00'){
                            
                            $clickpayData = json_decode($clickPay->data);
                            
                            $response_data['bookingCode'] = $bookingCode;
                            $response_data['cardNumber'] = $cardNumber;
                            $response_data['tokenResponse'] = $tokenResponse;
                            $response_data['reffBiller'] = $clickpayData->reffBiller;
                            $response_data['user_AIID'] = $user->user_AIID;
                            $response_data['amount'] = $amount;
                            $response_data['biayaTransaksi'] = $biayaTransaksiDisplay;
                            
                            $this->WalletModel->insert($response_data);
                            
                            $this->set_response(array(
                                'status' => 'success',
                                'message' => '',
                                'data' => $response_data
                            ));
                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'code' => 103,
                                'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                'reason' => $clickPay
                            ));
                        }                       
                        
                    }
                    
                } else {
                    $return = array(
                        'status' => 'error',
                        'code' => 103,
                        'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                        'reason' => $response
                    );
                    $this->set_response($return, 200);                    
                }
                
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function payment_post() {
        $token = $this->getToken();
        if ($token) {

            $setData = array(
                'inquiryId' => $this->post('inquiryId')
            );
            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('inquiryId', 'Inquiry ID', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => '',
                    'errros' => $this->form_validation->error_array()
                        ), 200);
            } else {
                $user = $this->User->getUser($token->id);
                $this->load->model('WalletModel');
                $inquiryId = $this->post('inquiryId');
                $check = $this->WalletModel->checkInquiryId($inquiryId, $user->user_AIID);

                if ($check) {
                    $inquiry = $this->WalletModel->row(array('id' => $inquiryId));


                    $paramsWallet = array(
                        'administrasi' => $inquiry->administrasi,
                        'amount' => $inquiry->amount,
                        'channelId' => $token->channelId,
                        'jenisTransaksi' => $inquiry->jenisTransaksi,
                        'noHp' => $user->no_hp,
                        'norek' => $inquiry->norek,
                        'reffSwitching' => $inquiry->reffSwitching,
                        'surcharge' => $inquiry->surcharge,
                        'totalKewajiban' => $inquiry->totalKewajiban
                    );

                    $response = $this->paymentWallet($paramsWallet);
                    $response_data = json_decode($response->data);

                    if ($response->responseCode == '00') {
                        $this->user->updateSaldo($user->user_AIID, $response_data->saldo);
                        $this->WalletModel->payment_success($inquiryId);
                        $return = array(
                            'status' => 'success',
                            'message' => $response->responseDesc
                        );
                    } else {
                        $return = array(
                            'status' => 'error',
                            'message' => $response->responseDesc,
                            'params' => $paramsWallet
                        );
                    }
                } else {
                    $return = array(
                        'status' => 'error',
                        'message' => 'InquiryId tidak tersedia'
                    );
                }

                $this->set_response($return, 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    function saldo_get(){
        $token = $this->getToken();
        if($token){
            
            $user = $this->User->getUser($token->id); 
                    
            $checkData = array(
                'channelId' => $token->channelId,
                'flag' => 'K',
                'noHp' => $user->no_hp,
                'tipe' => '1'
            );
            
            $checkSaldo = $this->saldoWallet($checkData);
            
            if($checkSaldo->responseCode == '00'){
                
                if(!isset($checkSaldo->data)){
                    $this->response(array(
                        'status' => 'error',
                        'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                        'code' => 103,
                        'reason' => $checkSaldo
                    ),200);
                }
                
                $saldoData = json_decode($checkSaldo->data);
                
                //Update saldo wallet user
                $this->User->updateUser($token->id, array(
                    'norek' => $saldoData->norek,
                    'saldo' => $saldoData->saldo
                ));
                
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $saldoData
                ),200);
                
                
            }else{
                $this->response(array(
                    'status' => 'error',
                    'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                    'code' => 103,
                    'reason' => $checkSaldo
                ),200);
            }
            
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function portofolio_get()
    {
        $token = $this->getToken();
        if($token){
            $user = $this->User->getUser($token->id);
            
            $dataReq = array(
                'channelId' => $token->channelId,
                'noHp' => $user->no_hp,
                'tipe' => '1',
                'flag' => 'K'
            );
            
            $inquiry = $this->portofolioWallet($dataReq);
            
            if($inquiry->responseCode == '00' && isset($inquiry->data)){
                $resData = json_decode($inquiry->data, true);
                //$portofolio = json_decode($resData->dataMutasi);
                
                $portofolio = $resData['dataMutasi'];
                $this->sksort($portofolio,'ke');
                
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $portofolio
                ),200);
                
            }else{
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                    'code' => 103,
                    'reason' => $inquiry
                ),200);
            }
            
            
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function sksort(&$array, $subkey = "id", $sort_ascending = false) {

        if (count($array))
            $temp_array[key($array)] = array_shift($array);

        foreach ($array as $key => $val) {
            $offset = 0;
            $found = false;
            foreach ($temp_array as $tmp_key => $tmp_val) {
                if (!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey])) {
                    $temp_array = array_merge((array) array_slice($temp_array, 0, $offset), array($key => $val), array_slice($temp_array, $offset)
                    );
                    $found = true;
                }
                $offset++;
            }
            if (!$found)
                $temp_array = array_merge($temp_array, array($key => $val));
        }

        if ($sort_ascending)
            $array = array_reverse($temp_array);
        else
            $array = $temp_array;
    }

   

}
