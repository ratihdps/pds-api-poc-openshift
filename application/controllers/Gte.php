<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Gte extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $models = [
            'User',
            'Otp',
            'BankModel',
            'PaymentModel',
            'NotificationModel',
            'EmasModel',
            'ConfigModel',
            'GteModel',
            'ProductMasterModel'
        ];
        
        $this->load->model($models);
        $this->load->library('form_validation');
        $this->load->helper('message');
        
        //Log request and post
        $CI =& get_instance();
        $uri = $CI->uri->uri_string();
        
        $postParam = $CI->input->post();
        $getParam = $CI->input->get();
        
        log_message('debug', 'REQUEST INFO');
        log_message('debug', 'REQUEST INFO URI: ' . $uri);
        log_message('debug', 'REQUEST INFO POST PARAM:'. json_encode($postParam));
        log_message('debug', 'REQUEST INFO GET PARAM:'. json_encode($getParam));
        
    }

    function check_promo_get(){
        $token = $this->getToken();
        if($token) {
            $this->form_validation->set_data([
                'promo' => $this->query('promo')
            ]);
            
            $this->form_validation->set_rules('promo','promo','required');
            
            if($this->form_validation->run() == false) {
                $this->set_response([
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Input error',
                    'errors' => $this->form_validation->error_array() 
                ]);
            } else {
                $promo = $this->query('promo');
                $data = $this->GteModel->checkPromo($promo);
                if(count($data) == 0){
                    $this->set_response([
                        'status' => 'error',
                        'code' => 102,
                        'message' => 'Promo tidak ditemukan'
                    ]);                    
                }else{
                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $data
                    ]);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    public function simulasi_post(){

        $this->form_validation->set_rules('amount', 'amount', 'required');
        $this->form_validation->set_rules('gram', 'gram', 'required');
//        $this->form_validation->set_rules('jenisTransaksi', 'jenisTransaksi', 'required');
        $this->form_validation->set_rules('norek', 'norek', 'required');
        $this->form_validation->set_rules('tenor', 'tenor', 'required');
//        $this->form_validation->set_rules('tipe', 'tipe', 'required');


        if ($this->form_validation->run()){

            $data = $this->post();

            //get productCode
            $productCode = $this->ProductMasterModel->get_id_product('GADAI TABUNGAN EMAS');
            $data['productCode'] = $productCode->productCode;

            //getCif
            $user = $this->getToken();
            $data['cif'] = $this->User->getUserCif($user->id);

            $data['clientId'] =  $this->config->item('core_post_username');
            $data['channelId'] = $this->config->item('core_client_id');

            $data['jenisTransaksi'] = "OP";

            if ($data['gram'] != "0")
                $data['tipe'] = "1";
            else if($data['amount'] != "0")
                $data['tipe'] = "2";


            $path = '/simulasi/gte';
            $req = $this->coreRequest($path, $data, "Simulasi GTE");

            if ($req->responseCode == "00") {
                $this->set_response([
                    'response_code' => $req->responseCode,
                    'status' => 'success',
                    'message' => $req->responseDesc,
                    'data' => json_decode($req->data)
                ], 200);
            } else {
                $this->set_response([
                    'response_code' => $req->responseCode,
                    'status' => 'error',
                    'message' => $req->responseDesc,
                    'data' => json_decode($req->data)
                ], 201);
            }
        }else{
            $error = "";
            foreach ($this->form_validation->error_array() as $value){
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101
            ]);
        }
    }
}
