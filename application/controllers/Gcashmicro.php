<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';
require_once APPPATH . 'helpers/message_helper.php';

class Gcashmicro extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('GcashModel', 'User', 'NotificationModel','ConfigModel'));
        $this->load->model('MasterModel');
        $this->load->library('form_validation');
    }

    function bank_get(){
        $response = $this->msGcashBank();
        if($response->responseCode == '00'){
            $data = json_decode($response->data);
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $data->bank
            ]);    
        }
        
    }

    function index_get(){
        $token = $this->getToken();
        if($token){
            $user = $this->User->getUser($token->id);

            // Sinkronisasi data dengan CORE
            $inquiryData = [
                'channelId' => $token->channelId,
                'cif' => $user->cif,
                'flag' => 'K'
            ];

            $inquiry = $this->msGcashRekening($inquiryData);
            if($inquiry->responseCode == '00'){
                $vaCoreData = json_decode($inquiry->data, true);

                $coreListRekening = $vaCoreData['listRekening'];

                // Clear VA user yang tersisa
                $this->GcashModel->clearVA($token->id);
                
                // Insert setiap data baru
                foreach($coreListRekening as $clr){
                    $insertData = [
                        'user_AIID' => $token->id,
                        'amount' => $clr['saldoAkhir'],
                        'tglExpired' => $clr['tglExpired'],
                        'virtualAccount' => $clr['vaNumber'],
                        'kodeBank' => $clr['kodeBank'] 
                    ];

                    $this->GcashModel->addVA($insertData);    
                }

                // Fetch ulang data
                $rList = $this->GcashModel->getVA($token->id);
                
                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $rList
                ]);
            } else {
                $this->set_response([
                    'status' => 'error',
                    'message' => $inquiry->responseCode.':'.$inquiry->responseDesc,
                    'code' => 103,
                    'reason' => $inquiry
                ]);
            }
        } else {
            $this->errorUnAuthorized();
        }
        // $this->set_response(array (
        //   'status' => 'success',
        //   'message' => '',
        //   'data' => 
        //   array (
             
        //     array (
        //       'id' => '29',
        //       'user_AIID' => '1310',
        //       'amount' => '0.00',
        //       'kodeBank' => '002',
        //       'trxId' => '',
        //       'tglExpired' => '2029-03-13 00:00:00',
        //       'virtualAccount' => '84978878631656002',
        //       'vaNumber' => NULL,
        //       'createdAt' => '2019-03-23 12:55:59',
        //       'lastUpdate' => '2019-03-23 12:55:59',
        //       'namaBank' => 'BANK BRI',
        //       'thumbnail' => 'http://pds-api.hookiarisan.com/uploaded/bank/bri.jpg',
        //     ),
        //     array (
        //       'id' => '30',
        //       'user_AIID' => '1310',
        //       'amount' => '0.00',
        //       'kodeBank' => '009',
        //       'trxId' => '',
        //       'tglExpired' => '2029-03-13 00:00:00',
        //       'virtualAccount' => '84978878631656002',
        //       'vaNumber' => NULL,
        //       'createdAt' => '2019-03-23 12:55:59',
        //       'lastUpdate' => '2019-03-23 12:55:59',
        //       'namaBank' => 'BANK BRI',
        //       'thumbnail' => 'http://pds-api.hookiarisan.com/uploaded/bank/bni.jpg',
        //     ),
        //   ),

        // ));
    }

    function open_post(){
        $token = $this->getToken();
        if($token)
        {
            // Check user identity
            if(!$this->User->isIdentityOK($token->id)){
                return $this->set_response([
                    'status' => 'error',
                    'code' => 102,
                    'message' => 'Mohon lengkapi identitas Anda terlebih dahulu'
                ]);
            }

            $this->form_validation->set_rules('kode_bank','kode_bank','required');
            $this->form_validation->set_rules('pin','pin','required');

            if($this->form_validation->run() == FALSE){
                return $this->set_response([
                   'status' => 'error',
                   'message' => 'Invalid input',
                   'code' => 102,
                   'errors' => $this->form_validation->error_array()
                ]);
            }
            else
            {
                $kodeBank = $this->post('kode_bank');
                $pin = $this->post('pin');

                $user = $this->User->getUser($token->id);

                // Check aktivasi finansial
                if($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2' ){
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Anda belum melakukan aktifasi finansial/status blokir.',
                        'code' => 201
                    ), 200);   
                    return;
                }

                // check user pin
                if(!$this->User->isValidPIN2($token->id, $pin)){
                    return $this->set_response(array(
                        'status' => 'error',
                        'message' => 'PIN tidak valid',
                        'code' => 102
                    ),200);
                }

                $openData = [
                    'channelId' => $token->channelId,
                    'customerEmail' => $user->email,
                    'customerName' => $user->nama,
                    'customerPhone' => $user->no_hp,
                    'description' => 'Register Wallet',
                    'bankCode' => $kodeBank,
                    'flag' => 'K',
                    'trxId' => rand(1,100000000),
                    'cif' => $user->cif,
                    'description' => 'Register VA'
                ];

                $openGcash = $this->msGcashOpen($openData);

                if($openGcash->responseCode == '00'){
                    //$openResponse = json_decode($openGcash->data, true);
                    $openResponse = json_decode($openGcash->data, true);

                    $openResponse['user_AIID'] = $token->id;
                    $openResponse['kodeBank'] = $kodeBank;

                    $saveData = [
                        'user_AIID' => $token->id,
                        'amount' => $openResponse['amount'],
                        'virtualAccount' => $openResponse['virtualAccount'],
                        'trxId' => $openResponse['trxId'],
                        'tglExpired' => $openResponse['tglExpired'],
                        'kodeBank' => $kodeBank
                    ];

                    $this->GcashModel->addVA($saveData);

                    $openResponse['nama'] = $user->nama;

                    // Send notification
                    $template = $this->generateNotif($openResponse, 'open');

                    $mobileTemplate = $template['mobile'];
                    $emailTemplate = $template['email'];
                    $minimalTemplate = $template['minimal'];

                    //Simpan notifikasi baru
                    $notifId = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_GCASH,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "G-Cash",
                        "Registrasi Virtual Account",
                        $mobileTemplate,
                        $minimalTemplate,
                        "GC"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id)
                        , [
                        "id" => $notifId,
                        "tipe" => "GC",
                        "title" => "G-Cash",
                        "tagline" => "Registrasi Virtual Account",
                        "content" => "Registrasi Berhasil",
                        "token" => $token->no_hp
                    ]);

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmail($token->email, "Registrasi Virtual Account Pegadaian ", $emailTemplate);

                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Registrasi Virtual Account G-Cash Pegadaian Berhasil',
                        'data' => $openResponse
                    ], 200);

                } else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 102,
                        'message' => $openGcash->responseCode. ''.$openGcash->responseDesc,
                        'reason' => $openGcash
                    ]);
                }

//                $this->set_response(array (
//                    'status' => 'success',
//                    'message' => 'Registrasi Virtual Account G-Cash Pegadaian Berhasil',
//                    'data' =>
//                        array (
//                            'amount' => '0',
//                            'trxId' => '087863165600',
//                            'tglExpired' => '2020-01-01 10:10:10',
//                            'virtualAccount' => '123456789',
//                            'user_AIID' => '1310',
//                            'kodeBank' => '009',
//                            'nama' => 'BUDA',
//                        ),
//                ));
            }
        }else{
            $this->errorUnAuthorized();
        }
    }

    function inquiry_post(){
        $token = $this->getToken();
        if($token){

            $user = $this->User->getUser($token->id);
            $this->form_validation->set_rules('kode_bank','kode_bank','required');
            $this->form_validation->set_rules('va','va','required');

            if($this->form_validation->run() == false){
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 102,
                    'errors' => $this->form_validation->error_array()
                ]);
            }
            else
            {
                $kodeBank = $this->post('kode_bank');
                $va = $this->post('va');

                $inquiryData = [
                    'channelId' => $token->channelId,
                    'bankCode' => $kodeBank,
                    'flag' => 'K',
                    'vaNumber' => $va,
                    'cif' => $user->cif
                ];

                $inquiry = $this->msGcashInquiry($inquiryData);

                if($inquiry->responseCode == '00'){
                    $responseData = json_decode($inquiry->data);
                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $responseData
                    ]);
                }else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 103,
                        'message' => $inquiry->responseCode.' '. $inquiry->responseDesc
                    ]);
                }
            }
        }else{
            $this->errorUnAuthorized();
        }
    }

    function transfer_post($method = "inquiry")
    {
        $token = $this->getToken();
        if($token)
        {
            if($method == 'inquiry'){
                $this->_transferInquiry($token);
            } else if($method == 'payment'){
                $this->_transferPayment($token);
            }else{
                $this->errorForbbiden();
            }
            
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    function _transferInquiry($token)
    {
        $user = $this->User->getUser($token->id);
        $this->form_validation->set_rules('va_id','va_id','required|numeric');
        $this->form_validation->set_rules('dest_va','dest_va','required|numeric');
        $this->form_validation->set_rules('dest_va_kode_bank','dest_va_kode_bank','required|numeric');
        $this->form_validation->set_rules('amount','amount','required|integer');

        if($this->form_validation->run() == false){
            return $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 102,
                'errors' => $this->form_validation->error_array()
            ]);
        }

        $vaId = $this->post('va_id');

        $userVa = $this->GcashModel->getVaDetails($vaId);
        if(!$userVa){
            return $this->set_response([
                'status' => 'errror',
                'message' => 'VA tidak ditemukan',
                'code' => 102
            ]);
        }

        $vaNumber = $userVa->virtualAccount;
        $destinationAccountNumber = $this->post('dest_va');
        $destinationBankCode = $this->post('dest_va_kode_bank');
        $bankCode = $userVa->kodeBank;
        $amount = $this->post('amount');

        $inquiryData = [
            'channelId' => $token->channelId,
            'flag' => 'K',
            'amount' => $amount,
            'vaNumber' => $vaNumber,
            'bankCode' => $bankCode,
            'destinationBankCode' => $destinationBankCode,
            'destinationAccountNumber' => $destinationAccountNumber,            
            'cif' => $user->cif
        ];

        $inquiry = $this->gcashInquiryTransfer($inquiryData);

        if($inquiry->responseCode == '00'){
            $inquiryData = json_decode($inquiry->data);
            $inquiryData->id_transaksi = $inquiryData->reffSwitching;
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $inquiryData
            ]);

        }else{
            $this->set_response([
                'status' => 'error',
                'message' => $inquiry->responseCode.':'.$inquiry->responseDesc,
                'code' => 103,
                'reason' => $inquiry
            ]);
        }
        // $this->set_response(array (
        //   'status' => 'success',
        //   'message' => '',
        //   'data' => 
        //   array (
        //     'bankCode' => '009',
        //     'destinationAccountNumber' => '0115476151',
        //     'destinationAccountName' => 'ROBERT NARO ROBERT NARO',
        //     'destinationAccountBankCode' => '009',
        //     'reffSwitching' => '15531756088762257',
        //     'amount' => '100000',
        //     'administrasi' => '2500',
        //     'id_transaksi' => '15531756088762257',
        //   ),
        // ));        
    }

    function _transferPayment($token)
    {
        $user = $this->User->getToken($token);

        $this->form_validation->set_rules('va_id','va_id','required|numeric');
        $this->form_validation->set_rules('dest_va','dest_va','required|numeric');
        $this->form_validation->set_rules('dest_va_nama','dest_va_nama','required');
        $this->form_validation->set_rules('dest_va_kode_bank','dest_va_kode_bank','required|numeric');
        $this->form_validation->set_rules('dest_va_nama_bank','dest_va_nama_bank','required');
        $this->form_validation->set_rules('amount','amount','required|integer');
        $this->form_validation->set_rules('administrasi','administrasi','required');
        $this->form_validation->set_rules('id_transaksi','id_transaksi','required');
        $this->form_validation->set_rules('pin','pin','required|numeric');

        if($this->form_validation->run() == false)
        {
            log_message('debug', json_encode($this->form_validation->error_array()));
            $this->set_response([
                'status' => 'error',
                'message' => 'invalid input',
                'code' => 101,
                'errors' => $this->form_validation->error_array()
            ]);
        }
        else
        {
            $vaId = $this->post('va_id');
            $destVa = $this->post('dest_va');
            $destVaName = $this->post('dest_va_nama');
            $destVaBankCode = $this->post('dest_va_kode_bank');
            $destVaBankName = $this->post('dest_va_nama_bank');
            $amount = $this->post('amount');
            $reffSwitching = $this->post('id_transaksi');
            $administrasi = $this->post('administrasi');
            $pin = $this->post('pin');

            if(!$this->User->isValidPIN2($token->id, $pin)){
                return $this->set_response([
                    'status' => 'errror',
                    'message' => 'PIN tidak valid',
                    'code' => 102
                ]);
            }

            // Get user gcash info by id
            $userVa = $this->GcashModel->getVaDetails($vaId);
            if(!$userVa){
                return $this->set_response([
                    'status' => 'errror',
                    'message' => 'VA tidak ditemukan',
                    'code' => 102
                ]);
            }

            $transferData = [
                'channelId' => $token->channelId,
                'flag' => 'K',
                'bankCode' => $userVa->kodeBank,
                'vaNumber' => $userVa->virtualAccount,
                'bankCode' => $userVa->kodeBank,
                'amount' => $amount,
                'destinationAccountNumber' => $destVa,
                'destinationAccountName' => $destVaName,
                'destinationBankCode' => $destVaBankCode,
                'destinationBankName' => $destVaBankName,
                'reffSwitching' => $reffSwitching,
                'administrasi' => $administrasi,
                'surcharge' => '0',
                'cif' => $user->cif
            ];

            $gcashTransfer = $this->gcashTransfer($transferData);

            if($gcashTransfer->responseCode == '00')
            {
                $resData = json_decode($gcashTransfer->data);

                $saveData = [
                    'user_AIID' => $token->id,
                    'tipeTransaksi' => 'TR',
                    'tglTransaksi' => date('Y-m-d H:i:s'),
                    'virtualAccount' => $userVa->virtualAccount,
                    'kodeBank' => $userVa->kodeBank,
                    'amount' => $amount,
                    'destVa' => $destVa,
                    'destVaNama' => $destVaName,
                    'destVaKodeBank' => $destVaBankCode,
                    'destVaNamaBank' => $destVaBankName,
                    'reffBiller' => $resData->reffBiller,
                    'reffSwitching' => $reffSwitching,
                    'tglPayment' => date('Y-m-d'),
                    'administrasi' => $administrasi,
                ];

                $this->GcashModel->addHistory($saveData);

                // Send notification
                // Send notification
                $saveData['nama'] = $token->nama;
                $template = $this->generateNotif($saveData, 'transfer');

                $mobileTemplate = $template['mobile'];
                $emailTemplate = $template['email'];
                $minimalTemplate = $template['minimal'];


                $resData->tgl = date('Y-m-d H:i:s'); 

                //Simpan notifikasi baru
                $notifId = $this->NotificationModel->add(
                    $token->id,
                    NotificationModel::TYPE_GCASH,
                    NotificationModel::CONTENT_TYPE_HTML,
                    "G-Cash",
                    "Transfer G-Cash",
                    $mobileTemplate,
                    $minimalTemplate,
                    "GC"
                );

                //Kirim notifikasi pembayaran ke device user
                Message::sendFCMNotif(
                    $this->User->getFCMToken($token->id)
                    , [
                    "id" => $notifId,
                    "tipe" => "GC",
                    "title" => "G-Cash",
                    "tagline" => "Transfer G-Cash",
                    "content" => "Transfer G-Cash",
                    "token" => $token->no_hp
                ]);

                //Kirim Email Notifikasi
                $this->load->helper('message');
                Message::sendEmail($token->email, "Transfer G-Cash", $emailTemplate);

                //Set response
                $this->set_response([
                    'status' => 'success',
                    'message' => 'Transfer Gcash Sukses',
                    'data' => $resData
                ], 200);
            } 
            else 
            {
                $this->set_response([
                    'status' => 'error',
                    'message' => $gcashTransfer->responseCode.' '.$gcashTransfer->responseDesc,
                    'code' => 103,
                    'reason' => $gcashTransfer
                ]);
            }
        }
        // $this->set_response(array (
        //   'status' => 'success',
        //   'message' => 'Transfer Gcash Sukses',
        //   'data' => 
        //   array (
        //     'bankCode' => '009',
        //     'vaNumber' => '8364008127511590',
        //     'amount' => '200000',
        //     'destinationAccountNumber' => '0115476151',
        //     'destinationAccountName' => 'ROBERT NARO ROBERT NARO',
        //     'destinationBankCode' => '009',
        //     'destinationBankName' => 'BNI',
        //     'reffBiller' => '0091903212054355',
        //     'tgl' => '2019-03-25 10:10:10',
        //   ),
        // ));
    }

    function history_get(){
        $token = $this->getToken();
        if($token){
            $va = $this->query('va');
            $history = $this->GcashModel->getHistory($token->id, $va);
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $history
            ]);
        }else{
            $this->errorUnAuthorized();
        }
    }

    function otp_bri_post()
    {
        $token = $this->getToken();
        if($token){
            $this->form_validation->set_rules('id_va','id_va','required');
            $this->form_validation->set_rules('amount','amount','required');
            $this->form_validation->set_rules('pin','pin','required|numeric');
            if($this->form_validation->run() == FALSE){
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 103,
                    'errors' => $this->form_validation->error_array()
                ]);
            }

            $vaId = $this->post('id_va');
            $amount = $this->post('amount');
            $pin = $this->post('pin');

            if(!$this->User->isValidPIN2($token->id, $pin)){
                return $this->set_response(array(
                    'status' => 'error',
                    'message' => 'PIN tidak valid',
                    'code' => 102
                ),200);
            }

            $userVa = $this->GcashModel->getVaDetails($vaId);
            if(!$userVa){
                return $this->set_response([
                    'status' => 'errror',
                    'message' => 'VA tidak ditemukan',
                    'code' => 102
                ]);
            }

            $otpData = [
                'channelId' => $token->channelId,
                'bankCode' => $userVa->kodeBank,
                'vaNumber' => $userVa->virtualAccount,
                'amount' => $amount,
                'requestType' => '1'
            ];

            $sendOTP = $this->gcashOtpBri($otpData);

            if($sendOTP->responseCode == '00'){
                $this->set_response([
                    'status' => 'success',
                    'message' => 'OTP tarik tunai akan dikirimkan ke no telepon Anda.',
                    'data' => null
                ]);
            }else{
                $this->set_response([
                    'status' => 'error',
                    'message' => $sendOTP->responseCode.':'.$sendOTP->responseDesc,
                    'code' => 103,
                    'reason' => $sendOTP
                ]);
            }
        }else{
            $this->errorUnAuthorized();
        }
//         $this->set_response([
//             'status' => 'success',
//             'message' => 'OTP tarik tunai akan dikirimkan ke no telepon Anda.',
//             'data' => null
//         ]);
    }

    function bankparams_get(){
        $token = $this->getToken();
        if($token){
            $list = $this->GcashModel->getBankParams();
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $list
            ]);
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    public function generateNotif($data, $type = 'open')
    {
        $subject = "Registrasi Virtual Account Gcash";
        $content = '';

        if($type == 'open'){
            $content = $this->load->view('mail/gcash/open', $data, true);    
        } else if($type == 'transfer'){
            $content = $this->load->view('mail/gcash/transfer', $data, true);
        }

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }



}
