<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Emas extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $models = [
            'User',
            'Otp',
            'BankModel',
            'PaymentModel',
            'NotificationModel',
            'EmasModel',
            'ConfigModel',
            'GcashModel',
            'GpoinModel',
            'ProductMasterModel'
        ];
        
        $this->load->model($models);
        $this->load->library('form_validation');
        $this->load->helper('message');
        
        //Log request and post
        $CI =& get_instance();
        $uri = $CI->uri->uri_string();
        
        $postParam = $CI->input->post();
        $getParam = $CI->input->get();
        
        log_message('debug', 'REQUEST INFO');
        log_message('debug', 'REQUEST INFO URI: ' . $uri);
        log_message('debug', 'REQUEST INFO POST PARAM:'. json_encode($postParam));
        log_message('debug', 'REQUEST INFO GET PARAM:'. json_encode($getParam));
        
    }

    function buka_tabungan_post()
    {
        $token = $this->getToken();
        if ($token) {

            $user = $this->User->profile($token->id);
            
            $setData = array(
                'amount' => $this->post('amount'),
                'domisili' => $this->post('domisili'),
                'nama' => $this->post('nama'),
                'jenis_identitas' => $this->post('jenis_identitas'),
                'no_identitas' => $this->post('no_identitas'),
                'tanggal_expired_identitas' => $this->post('tanggal_expired_identitas'),
                'tempat_lahir' => $this->post('tempat_lahir'),
                'tanggal_lahir' => $this->post('tanggal_lahir'),
                'no_hp' => $this->post('no_hp'),
                'jenis_kelamin' => $this->post('jenis_kelamin'),
                'status_kawin' => $this->post("status_kawin"),
                'kode_kelurahan' => $this->post("kode_kelurahan"),
                'jalan' => $this->post("jalan"),
                'ibu_kandung' => $this->post("ibu_kandung"),
                'kewarganegaraan' => $this->post("kewarganegaraan"),
                'kode_cabang' => $this->post("kode_cabang"),
                'flag' => $this->post("flag"),
                'payment' => $this->post("payment"),
                'booking_code' => $this->post('booking_code'),
                'card_number' => $this->post('card_number'),
                'token_response' => $this->post('token_response'),  
                'va' => $this->post('va'),
                'pin' => $this->post('pin')  
            );

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('amount', 'Amount', 'required');
            $this->form_validation->set_rules('domisili', 'Domisili', 'required');
            $this->form_validation->set_rules('nama', 'Nama Nasabah', 'required');

            $this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required');
            $this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|exact_length[16]');
            
            if($this->post('jenis_identitas')=='12'){
                $this->form_validation->set_rules('tanggal_expired_identitas', 'tanggal_expired_identitas', 'required');
            }            

            $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
            $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');

            $this->form_validation->set_rules('no_hp', 'No HP', 'required');

            $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
            $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required');

            if ($this->post('domisili') == '1') {
                $this->form_validation->set_rules('kode_kelurahan', 'Kode kelurahan', 'required');
            } else if ($this->post('domisili') == '2') {
                $this->form_validation->set_rules('jalan', 'Jalan', 'required');
            }

            $this->form_validation->set_rules('ibu_kandung', 'Ibu Kandung', 'required');
            $this->form_validation->set_rules('kewarganegaraan', 'Jenis Kelamin', 'required');

            $this->form_validation->set_rules('kode_cabang', 'Kode Cabang', 'required');
            $this->form_validation->set_rules('flag', 'Flag', 'required');
            $this->form_validation->set_rules('payment', 'Payment', 'required');
            
            $payment = $this->post('payment');
            
            if($payment == 'MANDIRI'){
                $this->form_validation->set_rules('booking_code','booking_code','required');            
                $this->form_validation->set_rules('card_number','card_number','required');            
                $this->form_validation->set_rules('token_response','token_response','required'); 
            }
            
            if($payment == 'WALLET' && $user->norek == ''){
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Anda belum melakukan aktifasi wallet',
                ), 200);
                return;
            }

            if($payment == 'WALLET' || $payment == 'GCASH'){
                $this->form_validation->set_rules('pin','pin','required|exact_length[6]|numeric');
            }
            

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => '',
                    'errros' => $this->form_validation->error_array()
                ), 200);
            } else {
                $amount = $this->post('amount');
                $domisili = $this->post('domisili');
                $nama = $this->post('nama');
                $jenis_identitas = $this->post('jenis_identitas');
                $no_identitas = $this->post('no_identitas');
                $tanggal_expired_identitas = $this->post('tanggal_expired_identitas');
                $tempat_lahir = $this->post('tempat_lahir');
                $tanggal_lahir = $this->post('tanggal_lahir');
                $no_hp = $this->post('no_hp');
                $jenis_kelamin = $this->post('jenis_kelamin');
                $status_kawin = $this->post("status_kawin");

                $kode_kelurahan = $this->post("kode_kelurahan");
                $jalan = $this->post("jalan");

                $ibu_kandung = $this->post("ibu_kandung");
                $kewarganegaraan = $this->post("kewarganegaraan");
                $kode_cabang = $this->post("kode_cabang");
                $flag = $this->post("flag");               
                
                $pin = $this->post('pin');
                $va = $this->post('va');

                $uploadDir = $this->config->item('upload_dir');


                $namaFoto = '';
                if (!isset($_FILES['userfile']['name'])) {
                    // get foto name from UserModel
                    $namaFoto = $this->User->profile($token->id)->fotoKTP;
                    if (!$namaFoto) {
                        $this->response([
                            'code' => 101,
                            'status' => 'error',
                            'message' => 'Foto KTP tidak boleh kosong',
                            'errors' => 'Foto KTP Tidak boleh kosong'
                        ], 200);
                        return;
                    }
                } else {
                    // Upload Foto
                    // Upload file
                    // Update foto KTP User
                    $config['upload_path'] = $uploadDir . '/user/ktp';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                    $config['max_size'] = 0;
                    $config['max_width'] = 0;
                    $config['max_height'] = 0;
                    $config['encrypt_name'] = true;

                    $this->load->library('upload', $config);

                    $uploadData = null;
                    if (!$this->upload->do_upload('userfile')) {
                        $this->response([
                            'code' => 101,
                            'status' => 'error',
                            'message' => '',
                            'errors' => $this->upload->display_errors()
                        ], 200);
                        return;
                    } else {
                        //Delete previous user file
                        $prevFile = $this->User->profile($token->id)->fotoKTP;

                        if ($prevFile != null && file_exists( $uploadDir . '/user/ktp/' . $prevFile)) {
                            unlink( $uploadDir . '/user/ktp/' . $prevFile);
                        }

                        $uploadData = $this->upload->data();
                        $namaFoto = $uploadData['file_name'];
                    }
                }

                $this->User->updateUser($token->id, array(
                    'foto_ktp_url' => $namaFoto
                ));

                /*
                 * Lakukan inquiry tabungan.
                 * Response data:
                 * administrasi
                 */
                $inquiryTabunganEmasData = [
                    'channelId' => $token->channelId,
                    'clientId' => $this->config->item('core_post_username'),
                    'jenisTransaksi' => 'OP',
                    'flag' => $flag,
                    'amount' => $amount,
                    'norek' => NULL,
                    'kodeCabang' => $kode_cabang,
                    'cif' => $user->cif,
                ];

                $inquiry = $this->inquiryTabunganEmas($inquiryTabunganEmasData);
                if ($inquiry->responseCode == '00') {
                    $dataInquiry = json_decode($inquiry->data);

                    //Dapatkan id transaksi sesuai dengan reffSwitching
                    $administrasi = $dataInquiry->administrasi;
                    $satuan = $dataInquiry->satuan;
                    $harga = $dataInquiry->harga;
                    $totalKewajiban = $dataInquiry->totalKewajiban;
                    $hargaEmas = $dataInquiry->harga;
                    $nilaiTransaksi = $dataInquiry->nilaiTransaksi;
                    $hakNasabah = $dataInquiry->hakNasabah;
                    $surcharge = $dataInquiry->surcharge;
                    $reffCore = $dataInquiry->reffCore;
                    $saldoEmas = $dataInquiry->saldoEmas;
                    $saldoNominal = $dataInquiry->saldoNominal;
                    $biayaTitip = $dataInquiry->biayaTitip;
                    $namaProduk = $dataInquiry->namaProduk; 
                    $tglTransaksi = $dataInquiry->tglTransaksi;
                    $trxId =$dataInquiry->reffSwitching;
                    $gram = $dataInquiry->gram;

                    $jenisTransaksi = 'OP';
                    $kodeProduct = '62';
                    $keterangan = "PDSOP ".$trxId;
                    
                    
                    
                    //Buat billing melalui VA, Bisa BNI atau Mandiri
                    if ($payment == 'BNI') {
                        
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','BANK','62','009');
                        $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP','BANK','62','009');
                        
                        $billingBNI = $this->createBillingVABNI(
                            (string) ($totalKewajiban + $biayaTransaksi),
                            $user->email,
                            $nama,
                            $no_hp,
                            $flag,
                            $jenisTransaksi,
                            $keterangan, //keterangan pembayaran emas
                            '',
                            $kodeProduct,
                            $trxId,
                            $token->channelId
                                );

                        if ($billingBNI->responseCode == '00') {

                            if(!isset($billingBNI->data)){
                                $this->set_response(array(
                                    'status' =>'error',
                                    'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                                    'code' => 103
                                ),200);
                                return;
                            }
                            
                            $billingData = json_decode($billingBNI->data);
                            
                            
                            $virtualAccount = $billingData->virtualAccount;
                            $tglExpired = $billingData->tglExpired;

                            $this->db->trans_start();

                            //Save tabungan emas dan update data user
                            $this->User->saveTabunganEmas(
                                $token->id,
                                $domisili,
                                $nama,
                                $jenis_identitas,
                                $no_identitas,
                                $tanggal_expired_identitas,
                                $tempat_lahir,
                                $tanggal_lahir,
                                $no_hp,
                                $jenis_kelamin,
                                $status_kawin,
                                $kode_kelurahan,
                                $jalan,
                                $ibu_kandung,
                                $kewarganegaraan,
                                $kode_cabang,
                                $flag,
                                $namaFoto,
                                $amount,
                                $trxId
                            );
                            
                            $paymentData = array(
                                'id_transaksi' => $trxId,
                                'jenis_transaksi' => 'OP',
                                'amount' => $amount,
                                'gram' => $gram,
                                'administrasi' => $administrasi,
                                'kode_produk' => '62',
                                'harga' => $harga,
                                'satuan' => $satuan,
                                'saldoEmas' => $saldoEmas,
                                'total_kewajiban' => $totalKewajiban,
                                'reffCore' =>$reffCore,
                                'user_AIID' => $token->id,
                                'keterangan' => 'Pembelian Emas',
                                'surcharge' => $surcharge,
                                'reffCore' => $reffCore,
                                'nilaiTransaksi' => $nilaiTransaksi,
                                'hakNasabah' => $hakNasabah,
                                'saldoNominal' => $saldoNominal,
                                'biayaTitip' => $biayaTitip,
                                'namaProduk' => $namaProduk,
                                'tglTransaksi' => $tglTransaksi,
                                'payment' => $payment,
                                'kodeBankPembayar' => '009',
                                'payment' => 'BANK'
                                
                            );

                            //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                            $this->PaymentModel->add($paymentData);

                            $this->db->trans_complete();            
                            if ($this->db->trans_status() === FALSE){
                                $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');                                
                            }

                            $template = $this->generateOpenTabemasNotif($nama, $payment, $totalKewajiban + $biayaTransaksiDisplay - $discountAmount, $virtualAccount, $tglExpired, $trxId);
                            
                            $emailTemplate= $template['email'];
                            $mobileTemplate = $template['mobile'];
                            $minimalTemplate = $template['minimal'];
                            
                            //Simpan notifikasi baru
                            $idNotif = $this->NotificationModel->add(
                                $token->id,
                                NotificationModel::TYPE_EMAS,
                                NotificationModel::CONTENT_TYPE_HTML,
                                $this->ConfigModel->getNamaProduk('OP','62'),
                                "Segera Bayar Rp". number_format($totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                                $mobileTemplate,
                                $minimalTemplate,
                                "OP"
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                "id" => $idNotif,
                                "tipe" => "OP",
                                "title" => $this->ConfigModel->getNamaProduk('OP','62'),
                                "tagline" => "Segera Bayar Rp". number_format($totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                                "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                "paymentType" => $payment,
                                "token" => $token->no_hp    
                            ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                'data' => array(
                                    'idTransaksi' => $trxId,
                                    'virtualAccount' => $virtualAccount,
                                    'expired' => $tglExpired,
                                    'now' => date('Y-m-d H:i:s')  
                                )
                            ], 200);

                        } else {
                            $this->set_response([
                                'status' => 'error',
                                'message' => 'Something went wrong. Please try again later.',
                                'code' => 103,
                                'reason' => $billingBNI
                            ], 200);
                        }   
                    }
                    else if($payment == 'MANDIRI')
                    {
                        
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','BANK','62','008');
                        
                        $bookingCode = $this->post('booking_code');
                        $cardNumber = $this->post('card_number');
                        $tokenResponse = $this->post('token_response');
                        
                        //Bayar dengan mandiri click pay
                        $clickPay = $this->mandiriClickPay(
                            (string) ($totalKewajiban + $biayaTransaksi) ,
                            $bookingCode, 
                            $cardNumber, 
                            'OP', 
                            "PDSOP " . $trxId . " ", 
                            $user->noHP, 
                            '', 
                            '62', 
                            $tokenResponse, 
                            $trxId, 
                            $token->channelId);

                        if($clickPay->responseCode == '00'){

                            $clickpayData = json_decode($clickPay->data);
                            
                            $this->db->trans_start();

                            $paymentData = array(
                                'id_transaksi' => $trxId,
                                'jenis_transaksi' => 'OP',
                                'amount' => $amount,
                                'gram' => $gram,
                                'administrasi' => $administrasi,
                                'kode_produk' => '62',
                                'harga' => $harga,
                                'satuan' => $satuan,
                                'saldoEmas' => $saldoEmas,
                                'total_kewajiban' => $totalKewajiban,
                                'reffCore' =>$reffCore,
                                'user_AIID' => $token->id,
                                'keterangan' => 'Pembelian Emas',
                                'surcharge' => $surcharge,
                                'reffCore' => $reffCore,
                                'nilaiTransaksi' => $nilaiTransaksi,
                                'hakNasabah' => $hakNasabah,
                                'saldoNominal' => $saldoNominal,
                                'biayaTitip' => $biayaTitip,
                                'namaProduk' => $namaProduk,
                                'tglTransaksi' => $tglTransaksi,
                                'payment' => $payment,
                                'kodeBankPembayar' => '008',
                                'payment' => 'BANK',
                                'bookingCode' => $bookingCode,
                                'cardNumber' => $cardNumber,
                                'tokenResponse' => $tokenResponse,
                                'reffBiller' => $clickpayData->reffBiller
                                
                            );
                            
                            //Save tabungan emas dan update data user
                            $this->User->saveTabunganEmas(
                                $token->id,
                                $domisili,
                                $nama,
                                $jenis_identitas,
                                $no_identitas,
                                $tanggal_expired_identitas,
                                $tempat_lahir,
                                $tanggal_lahir,
                                $no_hp,
                                $jenis_kelamin,
                                $status_kawin,
                                $kode_kelurahan,
                                $jalan,
                                $ibu_kandung,
                                $kewarganegaraan,
                                $kode_cabang,
                                $flag,
                                $namaFoto,
                                $amount,
                                $trxId
                            );

                            //update data pembayaran
                            $this->PaymentModel->add($paymentData);

                            $this->db->trans_complete();            
                            if ($this->db->trans_status() === FALSE){
                                $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS MANDIRI');                                
                            }

                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Pembayaran berhasil',
                                'data' => array(
                                    'reffBiller' => $clickpayData->reffBiller 
                                )
                            ], 200);                                

                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'code' => 103,
                                'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                'reason' => $clickPay
                            ));
                        }
                    }
                    else if($payment == 'WALLET')
                    {
                        //Check user pin                                 
                        if(!$this->User->isValidPIN2($token->id, $pin)){
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'PIN tidak valid',
                                'code' => 102
                            ),200);
                            return;
                        }
                            
                        log_message('debug', 'PAYMENT EMAS WALLET');
                        
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','WALLET','62','');

                        $openData = array(                            
                            'amount' => (string) ($amount + $biayaTransaksi),
                            'cif' => $user->cif,
                            'flag' => $flag,
                            'ibuKandung' => $ibu_kandung,
                            'idKelurahan' => $kode_kelurahan,
                            'jalan' => $jalan,
                            'jenisKelamin' => $jenis_kelamin,
                            'kewarganegaraan' => $kewarganegaraan,
                            'kodeCabang' => $kode_cabang,
                            'namaNasabah' => $nama,
                            'noHp' => $no_hp,
                            'noIdentitas' => $no_identitas,
                            'reffSwitching' => $trxId,
                            'statusKawin' => $status_kawin,
                            'tanggalExpiredId' => $tanggal_expired_identitas,
                            'tanggalLahir' => $tanggal_lahir,
                            'tempatLahir' => $tempat_lahir,
                            'tipeIdentitas' => $jenis_identitas,                            
                            'channelId' => $token->channelId,
                        );

                        if($payment == 'WALLET'){
                            $openData['paymentMethod'] = 'WALLET';                            
                            $openData['norekWallet'] = $user->norek;
                            $openData['walletId'] = $user->noHP;
                        } else {
                            $openData['gcashId'] = $va;
                            $openData['paymentMethod'] = 'GCASH';
                            $openData['productCode'] = '62';
                            $openData['jenisTransaksi'] = 'OP';
                        }
                        
                        $openTabungan = $this->bukaTabunganEmas2($openData);

                        if ($openTabungan->responseCode == '00') {
                            $resData = json_decode($openTabungan->data);

                            $noRekening = isset($resData->norek) ? $resData->norek : '';
                            $saldoEmas = isset($resData->saldoEmas) ? $resData->saldoEmas : '';
                            $saldoNominal = isset($resData->saldoNominal) ? $resData->saldoNominal : '';
                            $tglBuka = isset($resData->tglBuka) ? $resData->tglBuka : '';
                            $administrasi = isset($resData->administrasi) ? $resData->administrasi : '';
                            $gram = isset($resData->gram) ? $resData->gram : '';
                            $harga = isset($resData->harga) ? $resData->harga : '';
                            $nilaiTransaksi = isset($resData->nilaiTransaksi) ? $resData->nilaiTransaksi : '';
                            $surcharge = isset($resData->surcharge) ? $resData->surcharge : '';
                            $totalKewajiban = isset($resData->totalKewajiban) ? $resData->totalKewajiban : '';
                            $namaNasabah = isset($resData->namaNasabah) ? $resData->namaNasabah : '';

                            //Lakukan pengecekan CIF customer dan update data rekening tabungan
                            if($payment == 'WALLET'){
                                $checkCustomer = $this->detailTabunganEmas($noRekening);
                            }
                            
                            
                            if ($checkCustomer->responseCode == '00') {
                                $checkCustomerData = json_decode($checkCustomer->data);

                                $tanggalBuka = $checkCustomerData->tglBuka;
                                $namaNasabah = $checkCustomerData->namaNasabah;
                                $noRek = $checkCustomerData->norek;
                                $saldo = $checkCustomerData->saldo;
                                $cif = $checkCustomerData->cif;
                                
                                $this->db->trans_start();

                                //Simpan tabungan emas
                                $this->User->saveTabunganEmas(
                                    $token->id,
                                    $domisili,
                                    $nama,
                                    $jenis_identitas,
                                    $no_identitas,
                                    $tanggal_expired_identitas,
                                    $tempat_lahir,
                                    $tanggal_lahir,
                                    $no_hp,
                                    $jenis_kelamin,
                                    $status_kawin,
                                    $kode_kelurahan,
                                    $jalan,
                                    $ibu_kandung,
                                    $kewarganegaraan,
                                    $kode_cabang,
                                    $flag,
                                    $namaFoto,
                                    $amount,
                                    $trxId
                                );
                                
                                
                                $paymentData = array(
                                    'id_transaksi' => $trxId,
                                    'jenis_transaksi' => 'OP',
                                    'amount' => $amount,
                                    'gram' => $gram,
                                    'administrasi' => $administrasi,
                                    'kode_produk' => '62',
                                    'harga' => $harga,
                                    'satuan' => $satuan,
                                    'saldoEmas' => $saldoEmas,
                                    'total_kewajiban' => $totalKewajiban,
                                    'reffCore' =>$reffCore,
                                    'user_AIID' => $token->id,
                                    'keterangan' => 'PDSOP '.$noRekening,
                                    'no_rekening' => $noRekening,
                                    'surcharge' => $surcharge,
                                    'reffCore' => $reffCore,
                                    'reffSwitching' => $trxId,
                                    'nilaiTransaksi' => $nilaiTransaksi,
                                    'hakNasabah' => $hakNasabah,
                                    'saldoNominal' => $saldoNominal,
                                    'biayaTitip' => $biayaTitip,
                                    'namaProduk' => $namaProduk,
                                    'tglTransaksi' => $tglTransaksi,
                                    'payment' => $payment,
                                    'is_paid' => '1',
                                    'tipe' => $payment,
                                    'namaNasabah' => $namaNasabah,
                                    'norek' => $noRekening,
                                    'totalKewajiban' => $totalKewajiban
                                );

                                //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                                $this->PaymentModel->add($paymentData);


                                $this->User->activeTabunganEmas($trxId, $noRekening, $saldoEmas, $saldoNominal, $tglBuka, $checkCustomerData->cif);

                                //Update CIF user
                                $this->User->updateUser($token->id, array('cif' => $cif));

                                //Masukan saldo emas ke dalam transaksi (saldo dan saldo akhir jumlahnya sama)
                                $this->EmasModel->addHistoryIn($noRek, $saldo, $saldo, '1', 'OP');
                                
                                $dataTabunganEmas = $this->User->getTabunganEmas($trxId);

                                $namaOutlet = $dataTabunganEmas->namaOutlet;
                                $alamatOutlet = $dataTabunganEmas->alamatOutlet;
                                $teleponOutlet = $dataTabunganEmas->teleponOutlet;

                                $body = $this->generateOpenTabemasNotifSuccess(
                                                        $namaNasabah, 
                                                        $noRek, 
                                                        $cif,
                                                        $tanggalBuka,
                                                        $saldo,
                                                        $namaOutlet,
                                                        $alamatOutlet,
                                                        $teleponOutlet,
                                                        $trxId);

                                log_message('debug', "BODY HTML :". $body['mobile']);

                                log_message('debug', 'Generate Body Message OP: '.$user->email.' SUCCESS');


                                //Tambahkan ke notifikasi
                                $idNotif = $this->NotificationModel->add(
                                        $token->id, 
                                        NotificationModel::TYPE_EMAS, 
                                        NotificationModel::CONTENT_TYPE_HTML, 
                                        "Buka Tabungan Emas", 
                                        "Tabungan Emas Anda Sudah Aktif", 
                                        $body['mobile'],
                                        "OP"
                                );

                                log_message('debug', 'Save Notif Message OP: '.$user->email.' SUCCESS');
                                
                                $user2 = $this->User->getUser($token->id);

                                //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                                Message::sendFCMNotif(
                                        $user2->fcm_token
                                        , [
                                    "id" => $idNotif,
                                    "tipe" => "OP_SUCCESS",
                                    "namaNasabah" => $namaNasabah,        
                                    "noRekening" => $noRek,
                                    "idTransaksi" => $trxId,
                                    "saldoEmas" => $saldo,
                                    "tanggal_buka" => $tanggalBuka,
                                    "cif" => $cif,
                                    "paymentType" => $payment,
                                    "token" => $token->no_hp        
                                ]);

                                log_message('debug', 'SEND FCM Message OP: '.$user->email.' SUCCESS');

                                //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                                $this->load->helper('message');
                                Message::sendEmailOpenTabungaEmasSuccess($user->email, $body['email']);

                                log_message('debug', 'Send Email Message OP: '.$user->email.' SUCCESS');
                                
                                //Update Saldo Wallet
                                $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                $resData['wallet'] = $saldoWallet;

                                $this->db->trans_complete();
                                if ($this->db->trans_status() === FALSE){
                                    $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS WALLET');                                
                                }

                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Buka Tabungan Emas Sukses',
                                    'data' => $resData
                                        ], 200);


                            } else {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'code' => 103,
                                    'message' => 'Check Customer Error',
                                    'reason' => $checkCustomer
                                        ), 200);
                            }
                        } else {
                            $this->set_response(array(
                                'status' => 'error',
                                'code' => 103,
                                'message' => 'Open Tabungan Error',
                                'reason' => $openTabungan
                                    ), 200);
                        }
                    }
                    else if($payment == 'GCASH')
                    {
                        //Check user pin                                 
                        if(!$this->User->isValidPIN2($token->id, $pin)){
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'PIN tidak valid',
                                'code' => 102
                            ),200);
                            return;
                        }
                            
                        log_message('debug', 'PAYMENT EMAS WALLET');
                        
                        $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP','WALLET','62','');
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','WALLET','62','');

                        $openData = array(                            
                            'amount' => (string) ($amount + $biayaTransaksi),
                            'cif' => $user->cif,
                            'flag' => $flag,
                            'ibuKandung' => $ibu_kandung,
                            'idKelurahan' => $kode_kelurahan,
                            'jalan' => $jalan,
                            'jenisKelamin' => $jenis_kelamin,
                            'kewarganegaraan' => $kewarganegaraan,
                            'kodeCabang' => $kode_cabang,
                            'namaNasabah' => $nama,
                            'noHp' => $no_hp,
                            'noIdentitas' => $no_identitas,
                            'reffSwitching' => $trxId,
                            'statusKawin' => $status_kawin,
                            'tanggalExpiredId' => $tanggal_expired_identitas,
                            'tanggalLahir' => $tanggal_lahir,
                            'tempatLahir' => $tempat_lahir,
                            'tipeIdentitas' => $jenis_identitas,                            
                            'channelId' => $token->channelId,
                        );

                        $openData['gcashId'] = $va;
                        $openData['paymentMethod'] = 'GCASH';
                        $openData['productCode'] = '62';
                        $openData['jenisTransaksi'] = 'OP';
                        
                        
                        $openTabungan = $this->bukaTabunganEmas2($openData);

                        if ($openTabungan->responseCode == '00') {
                            $resData = json_decode($openTabungan->data);

                            $noRekening = isset($resData->norek) ? $resData->norek : '';
                            $saldoEmas = isset($resData->saldoEmas) ? $resData->saldoEmas : '';
                            $saldoNominal = isset($resData->saldoNominal) ? $resData->saldoNominal : '';
                            $tglBuka = isset($resData->tglBuka) ? $resData->tglBuka : '';
                            $administrasi = isset($resData->administrasi) ? $resData->administrasi : '';
                            $gram = isset($resData->gram) ? $resData->gram : '';
                            $harga = isset($resData->harga) ? $resData->harga : '';
                            $nilaiTransaksi = isset($resData->nilaiTransaksi) ? $resData->nilaiTransaksi : '';
                            $surcharge = isset($resData->surcharge) ? $resData->surcharge : '';
                            $totalKewajiban = isset($resData->totalKewajiban) ? $resData->totalKewajiban : '';
                            $namaNasabah = isset($resData->namaNasabah) ? $resData->namaNasabah : '';

                            $this->db->trans_start();

                            //Simpan tabungan emas
                            $this->User->saveTabunganEmas(
                                $token->id,
                                $domisili,
                                $nama,
                                $jenis_identitas,
                                $no_identitas,
                                $tanggal_expired_identitas,
                                $tempat_lahir,
                                $tanggal_lahir,
                                $no_hp,
                                $jenis_kelamin,
                                $status_kawin,
                                $kode_kelurahan,
                                $jalan,
                                $ibu_kandung,
                                $kewarganegaraan,
                                $kode_cabang,
                                $flag,
                                $namaFoto,
                                $amount,
                                $trxId
                            );

                            $paymentData = array(
                                'id_transaksi' => $trxId,
                                'jenis_transaksi' => 'OP',
                                'amount' => $amount,
                                'gram' => $gram,
                                'administrasi' => $administrasi,
                                'kode_produk' => '62',
                                'harga' => $harga,
                                'saldoEmas' => $saldoEmas,
                                'total_kewajiban' => $totalKewajiban,
                                'reffCore' =>$reffCore,
                                'user_AIID' => $token->id,
                                'keterangan' => 'Pembelian Emas',
                                'surcharge' => $surcharge,
                                'reffCore' => $reffCore,
                                'nilaiTransaksi' => $nilaiTransaksi,
                                'hakNasabah' => $hakNasabah,
                                'saldoNominal' => $saldoNominal,
                                'biayaTitip' => $biayaTitip,
                                'namaProduk' => $namaProduk,
                                'tglTransaksi' => $tglTransaksi,
                                'payment' => $payment,
                                'gcashIdTujuan' => $va,
                                'gcashId' => $va
                                
                            );

                            // Get GCASH Detail
                            $vaDetails = $this->GcashModel->getVaDetailsByVa($va);

                            //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                            $this->PaymentModel->add($paymentData);

                            $template = $this->generateOpenTabemasNotif($nama, $payment, $totalKewajiban + $biayaTransaksiDisplay, NULL, NULL, $trxId, $vaDetails->namaBank);
                            
                            $emailTemplate= $template['email'];
                            $mobileTemplate = $template['mobile'];
                            $minimalTemplate = $template['minimal'];
                            
                            //Simpan notifikasi baru
                            $idNotif = $this->NotificationModel->add(
                                $token->id,
                                NotificationModel::TYPE_EMAS,
                                NotificationModel::CONTENT_TYPE_HTML,
                                $this->ConfigModel->getNamaProduk('OP','62'),
                                "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                                $mobileTemplate,
                                $minimalTemplate,
                                "OP"
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                "id" => $idNotif,
                                "tipe" => "OP",
                                "title" => $this->ConfigModel->getNamaProduk('OP','62'),
                                "tagline" => "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                                "content" => "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                                "paymentType" => $payment,
                                "token" => $token->no_hp    
                            ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE){
                                $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS WALLET');                                
                            }

                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Buka Tabungan Emas Sukses',
                                'data' => $resData
                                    ], 200);   

                        } else {
                            $this->set_response(array(
                                'status' => 'error',
                                'code' => 103,
                                'message' => 'Open Tabungan Error',
                                'reason' => $openTabungan
                                    ), 200);
                        }    
                    }
                    else if ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI') 
                    {
                        
                        $kodeBank = '';
                        if($payment == 'VA_BCA'){
                            $kodeBank = '014';
                        } else if($payment=='VA_MANDIRI') {
                            $kodeBank = '008';
                        } else if($payment=='VA_BRI') {
                            $kodeBank = '002';
                        }
                        
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','BANK','62',$kodeBank);
                        $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP','BANK','62',$kodeBank);

                        $createBillingData = [
                            'channelId' => $token->channelId,
                            'amount' => (string) ($totalKewajiban + $biayaTransaksi),
                            'customerEmail' => $user->email,
                            'customerName' => $nama,
                            'customerPhone' => $no_hp,
                            'flag' => $flag,
                            'jenisTransaksi' => $jenisTransaksi,
                            'kodeProduk' => $kodeProduct,                            
                            'keterangan' => $keterangan,
                            'reffSwitching' => $trxId,
                            'kodeBank' => $kodeBank
                        ];
                        $billing = $this->createBillingPegadaian($createBillingData);

                        if ($billing->responseCode == '00') {

                            if(!isset($billing->data)){
                                $this->set_response(array(
                                    'status' =>'error',
                                    'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                                    'code' => 103
                                ),200);
                                return;
                            }
                            
                            $billingData = json_decode($billing->data);

                            $virtualAccount = $billingData->vaNumber;
                            $tglExpired = $billingData->tglExpired;

                            $this->db->trans_start();

                            //Save tabungan emas dan update data user
                            $this->User->saveTabunganEmas(
                                $token->id,
                                $domisili,
                                $nama,
                                $jenis_identitas,
                                $no_identitas,
                                $tanggal_expired_identitas,
                                $tempat_lahir,
                                $tanggal_lahir,
                                $no_hp,
                                $jenis_kelamin,
                                $status_kawin,
                                $kode_kelurahan,
                                $jalan,
                                $ibu_kandung,
                                $kewarganegaraan,
                                $kode_cabang,
                                $flag,
                                $namaFoto,
                                $amount,
                                $trxId
                            );
                            
                            $paymentData = array(
                                'id_transaksi' => $trxId,
                                'jenis_transaksi' => 'OP',
                                'amount' => $amount,
                                'gram' => $gram,
                                'administrasi' => $administrasi,
                                'kode_produk' => '62',
                                'harga' => $harga,
                                'satuan' => $satuan,
                                'saldoEmas' => $saldoEmas,
                                'total_kewajiban' => $totalKewajiban,
                                'reffCore' =>$reffCore,
                                'user_AIID' => $token->id,
                                'keterangan' => 'Pembelian Emas',
                                'surcharge' => $surcharge,
                                'reffCore' => $reffCore,
                                'nilaiTransaksi' => $nilaiTransaksi,
                                'hakNasabah' => $hakNasabah,
                                'saldoNominal' => $saldoNominal,
                                'biayaTitip' => $biayaTitip,
                                'namaProduk' => $namaProduk,
                                'tglTransaksi' => $tglTransaksi,
                                'payment' => $payment,
                                'kodeBankPembayar' => $kodeBank,                                
                            );

                            //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                            $this->PaymentModel->add($paymentData);

                            $this->db->trans_complete();            
                            if ($this->db->trans_status() === FALSE){
                                $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');                                
                            }

                                
                            $template = $this->generateOpenTabemasNotif($nama, $payment, $totalKewajiban + $biayaTransaksiDisplay, $virtualAccount, $tglExpired, $trxId);
                            
                            $emailTemplate= $template['email'];
                            $mobileTemplate = $template['mobile'];
                            $minimalTemplate = $template['minimal'];
                            
                            //Simpan notifikasi baru
                            $idNotif = $this->NotificationModel->add(
                                $token->id,
                                NotificationModel::TYPE_EMAS,
                                NotificationModel::CONTENT_TYPE_HTML,
                                $this->ConfigModel->getNamaProduk('OP','62'),
                                "Segera Bayar Rp". number_format($totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                                $mobileTemplate,
                                $minimalTemplate,
                                "OP"
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                "id" => $idNotif,
                                "tipe" => "OP",
                                "title" => $this->ConfigModel->getNamaProduk('OP','62'),
                                "tagline" => "Segera Bayar Rp". number_format($totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                                "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                "paymentType" => $payment,
                                "token" => $token->no_hp    
                            ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                'data' => array(
                                    'idTransaksi' => $trxId,
                                    'virtualAccount' => $virtualAccount,
                                    'expired' => $tglExpired,
                                    'now' => date('Y-m-d H:i:s')  
                                )
                            ], 200);

                        } else {
                            $this->set_response([
                                'status' => 'error',
                                'message' => 'Something went wrong. Please try again later.',
                                'code' => 103,
                                'reason' => $billing
                            ], 200);
                        }   
                    }
                    else if ($payment == 'VA_PERMATA') 
                    {
                        
                        $kodeBank = '013';                        
                        $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','BANK','62',$kodeBank);
                        $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP','BANK','62',$kodeBank);

                        $createBillingData = [
                            'channelId' => $token->channelId,
                            'amount' => (string) ($totalKewajiban + $biayaTransaksi),
                            'customerEmail' => $user->email,
                            'customerName' => $nama,
                            'customerPhone' => $no_hp,
                            'flag' => $flag,
                            'jenisTransaksi' => $jenisTransaksi,
                            'productCode' => $kodeProduct,
                            'norek' => '',
                            'keterangan' => $keterangan,
                            'trxId' => $trxId,
                        ];
                        $billing = $this->createBillingPermata($createBillingData);

                        if ($billing->responseCode == '00') {

                            if(!isset($billing->data)){
                                $this->set_response(array(
                                    'status' =>'error',
                                    'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                                    'code' => 103
                                ),200);
                                return;
                            }
                            
                            $billingData = json_decode($billing->data);
                            
                            
                            $virtualAccount = $billingData->virtualAccount;
                            $tglExpired = $billingData->tglExpired;

                            $this->db->trans_start();

                            //Save tabungan emas dan update data user
                            $this->User->saveTabunganEmas(
                                $token->id,
                                $domisili,
                                $nama,
                                $jenis_identitas,
                                $no_identitas,
                                $tanggal_expired_identitas,
                                $tempat_lahir,
                                $tanggal_lahir,
                                $no_hp,
                                $jenis_kelamin,
                                $status_kawin,
                                $kode_kelurahan,
                                $jalan,
                                $ibu_kandung,
                                $kewarganegaraan,
                                $kode_cabang,
                                $flag,
                                $namaFoto,
                                $amount,
                                $trxId
                            );
                            
                            $paymentData = array(
                                'id_transaksi' => $trxId,
                                'jenis_transaksi' => 'OP',
                                'amount' => $amount,
                                'gram' => $gram,
                                'administrasi' => $administrasi,
                                'kode_produk' => '62',
                                'harga' => $harga,
                                'satuan' => $satuan,
                                'saldoEmas' => $saldoEmas,
                                'total_kewajiban' => $totalKewajiban,
                                'reffCore' =>$reffCore,
                                'user_AIID' => $token->id,
                                'keterangan' => 'Pembelian Emas',
                                'surcharge' => $surcharge,
                                'reffCore' => $reffCore,
                                'nilaiTransaksi' => $nilaiTransaksi,
                                'hakNasabah' => $hakNasabah,
                                'saldoNominal' => $saldoNominal,
                                'biayaTitip' => $biayaTitip,
                                'namaProduk' => $namaProduk,
                                'tglTransaksi' => $tglTransaksi,
                                'payment' => $payment,
                                'kodeBankPembayar' => $kodeBank,
                                'payment' => 'BANK'
                                
                            );

                            //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                            $this->PaymentModel->add($paymentData);

                            $this->db->trans_complete();            
                            if ($this->db->trans_status() === FALSE){
                                $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');                                
                            }

                                
                            $template = $this->generateOpenTabemasNotif($nama, $payment, $totalKewajiban + $biayaTransaksiDisplay, $virtualAccount, $tglExpired, $trxId);
                            
                            $emailTemplate= $template['email'];
                            $mobileTemplate = $template['mobile'];
                            $minimalTemplate = $template['minimal'];
                            
                            //Simpan notifikasi baru
                            $idNotif = $this->NotificationModel->add(
                                $token->id,
                                NotificationModel::TYPE_EMAS,
                                NotificationModel::CONTENT_TYPE_HTML,
                                $this->ConfigModel->getNamaProduk('OP','62'),
                                "Segera Bayar Rp". number_format($totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                                $mobileTemplate,
                                $minimalTemplate,
                                "OP"
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                "id" => $idNotif,
                                "tipe" => "OP",
                                "title" => $this->ConfigModel->getNamaProduk('OP','62'),
                                "tagline" => "Segera Bayar Rp". number_format($totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                                "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                "paymentType" => $payment,
                                "token" => $token->no_hp    
                            ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                'data' => array(
                                    'idTransaksi' => $trxId,
                                    'virtualAccount' => $virtualAccount,
                                    'expired' => $tglExpired,
                                    'now' => date('Y-m-d H:i:s')  
                                )
                            ], 200);

                        } else {
                            $this->set_response([
                                'status' => 'error',
                                'message' => 'Something went wrong. Please try again later.',
                                'code' => 103,
                                'reason' => $billing
                            ], 200);
                        }
                    }
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Error Inquiry',
                        'code' => 103,
                        'reason' => $inquiry
                    ], 200);
                }
            }

        } else {
            $this->errorUnAuthorized();
        }
    }

    function open_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if($token)
        {
            $user = $this->User->getUser($token->id);

            if($method == 'inquiry'){
                $this->_openInquiry($token, $user);
            }
            else if($method == 'payment')
            {
                $this->_openPayment($token, $user);
            }
            else
            {
                $this->set_response([
                    'status' => 'error',
                    'message' => 'Path harus inquiry atau payment',
                    'code' => 101
                ]);
            }
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    function _openInquiry($token, $user)
    {
        $setData = array(
            'amount' => $this->post('amount'),
            'domisili' => $this->post('domisili'),
            'nama' => $this->post('nama'),
            'jenis_identitas' => $this->post('jenis_identitas'),
            'no_identitas' => $this->post('no_identitas'),
            'tanggal_expired_identitas' => $this->post('tanggal_expired_identitas'),
            'tempat_lahir' => $this->post('tempat_lahir'),
            'tanggal_lahir' => $this->post('tanggal_lahir'),
            'no_hp' => $this->post('no_hp'),
            'jenis_kelamin' => $this->post('jenis_kelamin'),
            'status_kawin' => $this->post("status_kawin"),
            'kode_kelurahan' => $this->post("kode_kelurahan"),
            'jalan' => $this->post("jalan"),
            'ibu_kandung' => $this->post("ibu_kandung"),
            'kewarganegaraan' => $this->post("kewarganegaraan"),
            'kode_cabang' => $this->post("kode_cabang"),
            'promo_code' => $this->post('promo_code'),
            'voucher_id' => $this->post('voucher_id')  
        );

        $this->form_validation->set_data($setData);

        $this->form_validation->set_rules('amount', 'Amount', 'required');
        $this->form_validation->set_rules('domisili', 'Domisili', 'required');
        $this->form_validation->set_rules('nama', 'Nama Nasabah', 'required');
        $this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required');
        $this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|exact_length[16]');
        
        if($this->post('jenis_identitas')=='12'){
            $this->form_validation->set_rules('tanggal_expired_identitas', 'tanggal_expired_identitas', 'required');
        }            

        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required');

        if ($this->post('domisili') == '1') {
            $this->form_validation->set_rules('kode_kelurahan', 'Kode kelurahan', 'required');
        } else if ($this->post('domisili') == '2') {
            $this->form_validation->set_rules('jalan', 'Jalan', 'required');
        }

        $this->form_validation->set_rules('ibu_kandung', 'Ibu Kandung', 'required');
        $this->form_validation->set_rules('kewarganegaraan', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('kode_cabang', 'Kode Cabang', 'required');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $this->set_response(array(
                'status' => 'error',
                'code' => 201,
                'message' => '',
                'errros' => $this->form_validation->error_array()
            ), 200);
        } 
        else
        {
            $amount = $this->post('amount');
            $domisili = $this->post('domisili');
            $nama = $this->post('nama');
            $jenis_identitas = $this->post('jenis_identitas');
            $no_identitas = $this->post('no_identitas');
            $tanggal_expired_identitas = $this->post('tanggal_expired_identitas');
            $tempat_lahir = $this->post('tempat_lahir');
            $tanggal_lahir = $this->post('tanggal_lahir');
            $no_hp = $this->post('no_hp');
            $jenis_kelamin = $this->post('jenis_kelamin');
            $status_kawin = $this->post("status_kawin");

            $kode_kelurahan = $this->post("kode_kelurahan");
            $jalan = $this->post("jalan");

            $ibu_kandung = $this->post("ibu_kandung");
            $kewarganegaraan = $this->post("kewarganegaraan");
            $kode_cabang = $this->post("kode_cabang");
            $flag = 'K';              
            
            $promoCode = $this->post('promo_code');
            $voucherId = $this->post('voucher_id');

            $uploadDir = $this->config->item('upload_dir');

            $namaFoto = '';
            if (!isset($_FILES['userfile']['name']))
            {
                // get foto name from UserModel
                $namaFoto = $this->User->profile($token->id)->fotoKTP;
                if (!$namaFoto) {
                    $this->response([
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Foto KTP tidak boleh kosong',
                        'errors' => 'Foto KTP Tidak boleh kosong'
                    ], 200);
                    return;
                }
            }
            else
            {
                $config['upload_path'] = $uploadDir . '/user/ktp';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['max_size'] = 0;
                $config['max_width'] = 0;
                $config['max_height'] = 0;
                $config['encrypt_name'] = true;

                $this->load->library('upload', $config);

                $uploadData = null;
                if (!$this->upload->do_upload('userfile'))
                {
                    $this->response([
                        'code' => 101,
                        'status' => 'error',
                        'message' => '',
                        'errors' => $this->upload->display_errors()
                    ], 200);
                    return;
                }
                else 
                {
                    //Delete previous user file
                    $prevFile = $this->User->profile($token->id)->fotoKTP;

                    if ($prevFile != null && file_exists( $uploadDir . '/user/ktp/' . $prevFile)) {
                        unlink( $uploadDir . '/user/ktp/' . $prevFile);
                    }

                    $uploadData = $this->upload->data();
                    $namaFoto = $uploadData['file_name'];
                }
            }

            /*
             * Lakukan inquiry tabungan.
             * Response data:
             * administrasi
             */
            
            $inquiryTabunganEmasData = [
                'channelId' => $token->channelId,
                'clientId' => $this->config->item('core_post_username'),
                'jenisTransaksi' => 'OP',
                'flag' => $flag,
                'amount' => $amount,
                'norek' => NULL,
                'kodeCabang' => $kode_cabang,
                'cif' => $user->cif,
            ];

            $jurnalAccount = null;

            // Lakukan validasi promo/voucher gpoint jika ada
            if($promoCode != null)
            {
                if($user->cif == null){
                    return $this->set_response([
                        'status' => 'error',
                        'message' => 'Anda harus memiliki CIF untuk menggunakan fitur voucher',
                        'code' => 102
                    ]);
                }

                $validateData = [
                    'promoCode' => $promoCode,
                    'voucherId' => $voucherId,
                    'userId' => $user->cif,
                    'transactionAmount' => (int) $amount,
                    'validators' => [
                        'channel' => $this->config->item('core_post_username'),
                        'product' => '62',
                        'transactionType' => 'OP',
                        'unit' => 'rupiah',
                    ]
                ];

                $validateVoucher = $this->gPointValidateVoucher($validateData);

                if($validateVoucher->status == 'Error'){
                    return $this->set_response([
                        'status' => 'error',
                        'message' => $validateVoucher->message,
                        'code' => 103,                                
                    ]);
                } else if($validateVoucher->status == 'Success') {
                    $inquiryTabunganEmasData['discountAmount'] = $validateVoucher->data->discount;
                    $inquiryTabunganEmasData['idPromosi'] = $validateVoucher->data->journalAccount.';'.$promoCode;
                }
            }

            $inquiry = $this->inquiryTabunganEmas($inquiryTabunganEmasData);
            if ($inquiry->responseCode == '00')
            {
                $dataInquiry = json_decode($inquiry->data);

                //Dapatkan id transaksi sesuai dengan reffSwitching
                $administrasi = $dataInquiry->administrasi;
                $satuan = $dataInquiry->satuan;
                $harga = $dataInquiry->harga;
                $totalKewajiban = $dataInquiry->totalKewajiban;
                $hargaEmas = $dataInquiry->harga;
                $nilaiTransaksi = $dataInquiry->nilaiTransaksi;
                $hakNasabah = $dataInquiry->hakNasabah;
                $surcharge = $dataInquiry->surcharge;
                $reffCore = $dataInquiry->reffCore;
                $saldoEmas = $dataInquiry->saldoEmas;
                $saldoNominal = $dataInquiry->saldoNominal;
                $biayaTitip = $dataInquiry->biayaTitip;
                $namaProduk = $dataInquiry->namaProduk; 
                $tglTransaksi = $dataInquiry->tglTransaksi;
                $trxId =$dataInquiry->reffSwitching;
                $gram = $dataInquiry->gram;

                $paymentData = array(
                    'id_transaksi' => $trxId,
                    'jenis_transaksi' => 'OP',
                    'amount' => $amount,
                    'gram' => $gram,
                    'administrasi' => $administrasi,
                    'kode_produk' => '62',
                    'harga' => $harga,
                    'satuan' => $satuan,
                    'saldoEmas' => $saldoEmas,
                    'total_kewajiban' => $totalKewajiban,
                    'reffCore' =>$reffCore,
                    'user_AIID' => $token->id,
                    'keterangan' => 'Pembelian Emas',
                    'surcharge' => $surcharge,
                    'reffCore' => $reffCore,
                    'nilaiTransaksi' => $nilaiTransaksi,
                    'hakNasabah' => $hakNasabah,
                    'saldoNominal' => $saldoNominal,
                    'biayaTitip' => $biayaTitip,
                    'namaProduk' => $namaProduk,
                    'tglTransaksi' => $tglTransaksi,
                    'reffSwitching' => $trxId
                );

                if(isset($inquiryTabunganEmasData['discountAmount'], $inquiryTabunganEmasData['idPromosi']))
                {
                    $paymentData['discountAmount'] = $inquiryTabunganEmasData['discountAmount'];
                    $paymentData['idPromosi'] = $inquiryTabunganEmasData['idPromosi'];    
                }

                $this->PaymentModel->add($paymentData);

                $this->User->updateUser($token->id, [
                    'jenis_identitas' => $jenis_identitas,
                    'no_ktp' => $no_identitas,
                    'tanggal_expired_identitas' => $tanggal_expired_identitas,
                    'tempat_lahir' => $tempat_lahir,
                    'tgl_lahir' => $tanggal_lahir,
                    'jenis_kelamin' => $jenis_kelamin,
                    'status_kawin' => $status_kawin,
                    'id_kelurahan' => $kode_kelurahan,
                    'alamat' => $jalan,
                    'nama_ibu' => $ibu_kandung,
                    'kewarganegaraan' => $kewarganegaraan,
                    'kode_cabang' => $kode_cabang,
                    'foto_ktp_url' => $namaFoto,
                    'foto_url' => $namaFoto,
                    'domisili' => $domisili
                ]);

                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $paymentData
                ]);

            }
            else
            {
                $this->set_response([
                    'status' => 'error',
                    'message' => $inquiry->responseCode.':'.$inquiry->responseDesc,
                    'code' => 103,
                    'reason' => $inquiry
                ]);
            }
        }
    }

    function _openPayment($token, $user)
    {
        $setData = array(
            'id_transaksi' => $this->post('id_transaksi'),
            'payment' => $this->post("payment"),
            'booking_code' => $this->post('booking_code'),
            'card_number' => $this->post('card_number'),
            'token_response' => $this->post('token_response'),  
            'va' => $this->post('va'),
            'pin' => $this->post('pin'),
        );

        $this->form_validation->set_rules('id_transaksi','id_transaksi','required');
        $this->form_validation->set_rules('payment', 'Payment', 'required');
            
        $payment = $this->post('payment');
        
        if($payment == 'MANDIRI')
        {
            $this->form_validation->set_rules('booking_code','booking_code','required');            
            $this->form_validation->set_rules('card_number','card_number','required');            
            $this->form_validation->set_rules('token_response','token_response','required'); 
        }
        
        if($payment == 'WALLET' && $user->norek == '')
        {
            $this->set_response(array(
                'status' => 'error',
                'code' => 201,
                'message' => 'Anda belum melakukan aktifasi wallet',
            ), 200);
            return;
        }

        if($payment == 'WALLET' || $payment == 'GCASH')
        {
            $this->form_validation->set_rules('pin','pin','required|exact_length[6]|numeric');
        }

        $this->form_validation->set_data($setData);

        if ($this->form_validation->run() == FALSE)
        {
            $this->set_response(array(
                'status' => 'error',
                'code' => 201,
                'message' => '',
                'errros' => $this->form_validation->error_array()
            ), 200);
        }
        else
        {
            $trxId = $this->post('id_transaksi');
            $pin = $this->post('pin');
            $va = $this->post('va');

            $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId);

            // Mendapatkan promo berdasarkan id transaksi
            $gpoin = $this->GpoinModel->get_promo($trxId);
            $idPromosi = '';
            $promoCode = '';
            $discountAmount = 0;
            $promoAmount = 0;
            if ($gpoin != '') {
                if ($gpoin->type == 'discount') {
                    $discountAmount = $gpoin->value;
                }
                $promoAmount = $gpoin->value;
                $idPromosi = $gpoin->idPromosi;
                $promoCode = $gpoin->promoCode;
            };
            
            if(!$checkPayment)
            {
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Transaksi tidak ditemukan',
                    'code' => 102
                ]); 
            }
            $amount = $checkPayment->amount;
            $administrasi = $checkPayment->administrasi;
            $satuan = $checkPayment->satuan;
            $harga = $checkPayment->harga;
            $totalKewajiban = $checkPayment->totalKewajiban;
            $hargaEmas = $checkPayment->harga;
            $nilaiTransaksi = $checkPayment->nilaiTransaksi;
            $hakNasabah = $checkPayment->hakNasabah;
            $surcharge = $checkPayment->surcharge;
            $reffCore = $checkPayment->reffCore;
            $saldoEmas = $checkPayment->saldoEmas;
            $saldoNominal = $checkPayment->saldoNominal;
            $biayaTitip = $checkPayment->biayaTitip;
            $namaProduk = $checkPayment->namaProduk; 
            $tglTransaksi = $checkPayment->tglTransaksi;
            $trxId =$checkPayment->reffSwitching;
            $gram = $checkPayment->gram;
            // $discountAmount = $checkPayment->discountAmount;
            // $idPromosi = $checkPayment->idPromosi;

            $jenisTransaksi = 'OP';
            $kodeProduct = '62';
            $keterangan = "PDSOP ".$trxId;

            //Buat billing melalui VA, Bisa BNI atau Mandiri
            if ($payment == 'BNI') 
            { 
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','BANK','62','009');
                $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP','BANK','62','009');
                
                $billingBNI = $this->createBillingVABNI(
                    (string) ($amount + $biayaTransaksi - $discountAmount),
                    $user->email,
                    $user->nama,
                    $user->no_hp,
                    'K',
                    'OP',
                    $keterangan, //keterangan pembayaran emas
                    '',
                    $kodeProduct,
                    $trxId,
                    $token->channelId,
                    $idPromosi,
                    $promoAmount
                );

                if ($billingBNI->responseCode == '00') {

                    if(!isset($billingBNI->data)){
                        $this->set_response(array(
                            'status' =>'error',
                            'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                            'code' => 103
                        ),200);
                        return;
                    }
                    
                    $billingData = json_decode($billingBNI->data);
                    
                    
                    $virtualAccount = $billingData->virtualAccount;
                    $tglExpired = $billingData->tglExpired;

                    $this->db->trans_start();

                    //Save tabungan emas dan update data user
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );
                    
                    $paymentData = array(
                        'tipe' => $payment,
                        'payment' => $payment,
                        'kodeBankPembayar' => '009',
                        'virtual_account' => $virtualAccount                        
                    );

                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->update($trxId, $paymentData);

                    $this->db->trans_complete();            
                    if ($this->db->trans_status() === FALSE){
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');                                
                    }

                    $template = $this->generateOpenTabemasNotif($user->nama, $payment, $amount + $biayaTransaksiDisplay - $discountAmount, $virtualAccount, $tglExpired, $trxId, $promoCode, $promoAmount);
                    
                    $emailTemplate= $template['email'];
                    $mobileTemplate = $template['mobile'];
                    $minimalTemplate = $template['minimal'];
                    
                    //Simpan notifikasi baru
                    $idNotif = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP','62'),
                        "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".")." ke ".$virtualAccount,
                        $mobileTemplate,
                        $minimalTemplate,
                        "OP"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id)
                        , [
                        "id" => $idNotif,
                        "tipe" => "OP",
                        "title" => $this->ConfigModel->getNamaProduk('OP','62'),
                        "tagline" => "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".")." ke ".$virtualAccount,
                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                        "paymentType" => $payment,
                        "token" => $token->no_hp    
                    ]);

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                        'data' => array(
                            'idTransaksi' => $trxId,
                            'virtualAccount' => $virtualAccount,
                            'expired' => $tglExpired,
                            'now' => date('Y-m-d H:i:s')  
                        )
                    ], 200);

                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Something went wrong. Please try again later.',
                        'code' => 103,
                        'reason' => $billingBNI
                    ], 200);
                }
            }
            else if($payment == 'MANDIRI')
            {

                
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','BANK','62','008');
                
                $bookingCode = $this->post('booking_code');
                $cardNumber = $this->post('card_number');
                $tokenResponse = $this->post('token_response');
                
                //Bayar dengan mandiri click pay
                $clickPay = $this->mandiriClickPay(
                    (string) ($amount + $biayaTransaksi - $discountAmount) ,
                    $bookingCode, 
                    $cardNumber, 
                    'OP', 
                    "PDSOP " . $trxId . " ", 
                    $user->no_hp, 
                    '', 
                    '62', 
                    $tokenResponse, 
                    $trxId, 
                    $token->channelId);

                if($clickPay->responseCode == '00'){

                    $clickpayData = json_decode($clickPay->data);
                    
                    $this->db->trans_start();

                    $paymentData = array(
                        'payment' => $payment,
                        'kodeBankPembayar' => '008',
                        'payment' => 'BANK',
                        'bookingCode' => $bookingCode,
                        'cardNumber' => $cardNumber,
                        'tokenResponse' => $tokenResponse,
                        'reffBiller' => $clickpayData->reffBiller
                    );
                    
                    //Save tabungan emas dan update data user
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );

                    //update data pembayaran
                    $this->PaymentModel->update($trxId, $paymentData);

                    $this->db->trans_complete();            
                    if ($this->db->trans_status() === FALSE){
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS MANDIRI');                                
                    }

                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Pembayaran berhasil',
                        'data' => array(
                            'reffBiller' => $clickpayData->reffBiller 
                        )
                    ], 200);                                

                }else{
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 103,
                        'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                        'reason' => $clickPay
                    ));
                }
            }
            else if($payment == 'WALLET')
            {

                //Check user pin                                 
                if(!$this->User->isValidPIN2($token->id, $pin)){
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'PIN tidak valid',
                        'code' => 102
                    ),200);
                    return;
                }
                    
                log_message('debug', 'PAYMENT EMAS WALLET');
                
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','WALLET','62','');

                $openData = array(                            
                    'amount' => (string) ($amount + $biayaTransaksi - $discountAmount),
                    'cif' => $user->cif,
                    'flag' => 'K',
                    'ibuKandung' => $user->nama_ibu,
                    'idKelurahan' => $user->id_kelurahan,
                    'jalan' => $user->alamat,
                    'jenisKelamin' => $user->jenis_kelamin,
                    'kewarganegaraan' => $user->kewarganegaraan,
                    'kodeCabang' => $user->kode_cabang,
                    'namaNasabah' => $user->nama,
                    'noHp' => $user->no_hp,
                    'noIdentitas' => $user->no_ktp,
                    'reffSwitching' => $trxId,
                    'statusKawin' => $user->status_kawin,
                    'tanggalExpiredId' => $user->tanggal_expired_identitas,
                    'tanggalLahir' => $user->tgl_lahir,
                    'tempatLahir' => $user->tempat_lahir,
                    'tipeIdentitas' => $user->jenis_identitas,                            
                    'channelId' => $token->channelId,
                );

                if($payment == 'WALLET'){
                    $openData['paymentMethod'] = 'WALLET';                            
                    $openData['norekWallet'] = $user->norek;
                    $openData['walletId'] = $user->noHP;
                } else {
                    $openData['gcashId'] = $va;
                    $openData['paymentMethod'] = 'GCASH';
                    $openData['productCode'] = '62';
                    $openData['jenisTransaksi'] = 'OP';
                }
                
                $openTabungan = $this->bukaTabunganEmas2($openData);

                if ($openTabungan->responseCode == '00') {
                    $resData = json_decode($openTabungan->data);

                    $noRekening = isset($resData->norek) ? $resData->norek : '';
                    $saldoEmas = isset($resData->saldoEmas) ? $resData->saldoEmas : '';
                    $saldoNominal = isset($resData->saldoNominal) ? $resData->saldoNominal : '';
                    $tglBuka = isset($resData->tglBuka) ? $resData->tglBuka : '';
                    $administrasi = isset($resData->administrasi) ? $resData->administrasi : '';
                    $gram = isset($resData->gram) ? $resData->gram : '';
                    $harga = isset($resData->harga) ? $resData->harga : '';
                    $nilaiTransaksi = isset($resData->nilaiTransaksi) ? $resData->nilaiTransaksi : '';
                    $surcharge = isset($resData->surcharge) ? $resData->surcharge : '';
                    $totalKewajiban = isset($resData->totalKewajiban) ? $resData->totalKewajiban : '';
                    $namaNasabah = isset($resData->namaNasabah) ? $resData->namaNasabah : '';

                    //Lakukan pengecekan CIF customer dan update data rekening tabungan
                    if($payment == 'WALLET'){
                        $checkCustomer = $this->detailTabunganEmas($noRekening);
                    }
                    
                    
                    if ($checkCustomer->responseCode == '00') {
                        $checkCustomerData = json_decode($checkCustomer->data);

                        $tanggalBuka = $checkCustomerData->tglBuka;
                        $namaNasabah = $checkCustomerData->namaNasabah;
                        $noRek = $checkCustomerData->norek;
                        $saldo = $checkCustomerData->saldo;
                        $cif = $checkCustomerData->cif;
                        
                        $this->db->trans_start();

                        //Simpan tabungan emas
                        $this->User->saveTabunganEmas(
                            $token->id,
                            $user->domisili,
                            $user->nama,
                            $user->jenis_identitas,
                            $user->no_ktp,
                            $user->tanggal_expired_identitas,
                            $user->tempat_lahir,
                            $user->tgl_lahir,
                            $user->no_hp,
                            $user->jenis_kelamin,
                            $user->status_kawin,
                            $user->id_kelurahan,
                            $user->alamat,
                            $user->nama_ibu,
                            $user->kewarganegaraan,
                            $user->kode_cabang,
                            'K',
                            $user->foto_url,
                            $checkPayment->amount,
                            $trxId
                        );
                        
                        
                        $paymentData = array(
                            'payment' => $payment,
                            'is_paid' => '1',
                            'tipe' => $payment,
                            'namaNasabah' => $user->nama,
                            'norek' => $noRekening,
                            'totalKewajiban' => $totalKewajiban
                        );

                        //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                        $this->PaymentModel->update($trxId, $paymentData);


                        $this->User->activeTabunganEmas($trxId, $noRekening, $saldoEmas, $saldoNominal, $tglBuka, $checkCustomerData->cif);

                        //Update CIF user
                        $this->User->updateUser($token->id, array('cif' => $cif));

                        //Masukan saldo emas ke dalam transaksi (saldo dan saldo akhir jumlahnya sama)
                        $this->EmasModel->addHistoryIn($noRek, $saldo, $saldo, '1', 'OP');
                        
                        $dataTabunganEmas = $this->User->getTabunganEmas($trxId);

                        $namaOutlet = $dataTabunganEmas->namaOutlet;
                        $alamatOutlet = $dataTabunganEmas->alamatOutlet;
                        $teleponOutlet = $dataTabunganEmas->teleponOutlet;

                        $body = $this->generateOpenTabemasNotifSuccess(
                                                $namaNasabah, 
                                                $noRek, 
                                                $cif,
                                                $tanggalBuka,
                                                $saldo,
                                                $namaOutlet,
                                                $alamatOutlet,
                                                $teleponOutlet,
                                                $trxId);

                        log_message('debug', "BODY HTML :". $body['mobile']);

                        log_message('debug', 'Generate Body Message OP: '.$user->email.' SUCCESS');


                        //Tambahkan ke notifikasi
                        $idNotif = $this->NotificationModel->add(
                                $token->id, 
                                NotificationModel::TYPE_EMAS, 
                                NotificationModel::CONTENT_TYPE_HTML, 
                                "Buka Tabungan Emas", 
                                "Tabungan Emas Anda Sudah Aktif", 
                                $body['mobile'],
                                "OP"
                        );

                        log_message('debug', 'Save Notif Message OP: '.$user->email.' SUCCESS');
                        
                        $user2 = $this->User->getUser($token->id);

                        //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                        Message::sendFCMNotif(
                                $user2->fcm_token
                                , [
                            "id" => $idNotif,
                            "tipe" => "OP_SUCCESS",
                            "namaNasabah" => $namaNasabah,        
                            "noRekening" => $noRek,
                            "idTransaksi" => $trxId,
                            "saldoEmas" => $saldo,
                            "tanggal_buka" => $tanggalBuka,
                            "cif" => $cif,
                            "paymentType" => $payment,
                            "token" => $token->no_hp        
                        ]);

                        log_message('debug', 'SEND FCM Message OP: '.$user->email.' SUCCESS');

                        //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                        $this->load->helper('message');
                        Message::sendEmailOpenTabungaEmasSuccess($user->email, $body['email']);

                        log_message('debug', 'Send Email Message OP: '.$user->email.' SUCCESS');
                        
                        //Update Saldo Wallet
                        $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                        $resData['wallet'] = $saldoWallet;

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE){
                            $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS WALLET');                                
                        }

                        $this->set_response([
                            'status' => 'success',
                            'message' => 'Buka Tabungan Emas Sukses',
                            'data' => $resData
                                ], 200);


                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'code' => 103,
                            'message' => 'Check Customer Error',
                            'reason' => $checkCustomer
                                ), 200);
                    }
                } else {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 103,
                        'message' => 'Open Tabungan Error',
                        'reason' => $openTabungan
                            ), 200);
                }
            }
            else if($payment == 'GCASH')
            {
                

                //Check user pin                                 
                if(!$this->User->isValidPIN2($token->id, $pin)){
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'PIN tidak valid',
                        'code' => 102
                    ),200);
                    return;
                }
                
                log_message('debug', 'PAYMENT EMAS WALLET');
                
                $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP','WALLET','62','');
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','WALLET','62','');
                
                $openData = array(                            
                    'amount' => (string) ($amount + $biayaTransaksi - $discountAmount),
                    'cif' => $user->cif,
                    'flag' => 'K',
                    'ibuKandung' => $user->nama_ibu,
                    'idKelurahan' => $user->id_kelurahan,
                    'jalan' => $user->alamat,
                    'jenisKelamin' => $user->jenis_kelamin,
                    'kewarganegaraan' => $user->kewarganegaraan,
                    'kodeCabang' => $user->kode_cabang,
                    'namaNasabah' => $user->nama,
                    'noHp' => $user->no_hp,
                    'noIdentitas' => $user->no_ktp,
                    'reffSwitching' => $trxId,
                    'statusKawin' => $user->status_kawin,
                    'tanggalExpiredId' => $user->tanggal_expired_identitas,
                    'tanggalLahir' => $user->tgl_lahir,
                    'tempatLahir' => $user->tempat_lahir,
                    'tipeIdentitas' => $user->jenis_identitas,                            
                    'channelId' => $token->channelId,
                );

                $openData['gcashId'] = $va;
                $openData['paymentMethod'] = 'GCASH';
                $openData['productCode'] = '62';
                $openData['jenisTransaksi'] = 'OP';
                
                
                $openTabungan = $this->bukaTabunganEmas2($openData);
                
                if ($openTabungan->responseCode == '00') {
                    $resData = json_decode($openTabungan->data);

                    $noRekening = isset($resData->norek) ? $resData->norek : '';
                    $saldoEmas = isset($resData->saldoEmas) ? $resData->saldoEmas : '';
                    $saldoNominal = isset($resData->saldoNominal) ? $resData->saldoNominal : '';
                    $tglBuka = isset($resData->tglBuka) ? $resData->tglBuka : '';
                    $administrasi = isset($resData->administrasi) ? $resData->administrasi : '';
                    $gram = isset($resData->gram) ? $resData->gram : '';
                    $harga = isset($resData->harga) ? $resData->harga : '';
                    $nilaiTransaksi = isset($resData->nilaiTransaksi) ? $resData->nilaiTransaksi : '';
                    $surcharge = isset($resData->surcharge) ? $resData->surcharge : '';
                    $totalKewajiban = isset($resData->totalKewajiban) ? $resData->totalKewajiban : '';
                    $namaNasabah = isset($resData->namaNasabah) ? $resData->namaNasabah : '';

                    $this->db->trans_start();

                    //Simpan tabungan emas
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );
  
                    
                    
                    $paymentData = array(
                        'payment' => $payment,
                        'gcashIdTujuan' => $va,
                        'gcashId' => $va
                    );

                    // Get GCASH Detail
                    $vaDetails = $this->GcashModel->getVaDetailsByVa($va);

                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->update($trxId, $paymentData);
                    
                    $template = $this->generateOpenTabemasNotif($user->nama, $payment, $amount + $biayaTransaksiDisplay - $discountAmount, NULL, NULL, $trxId, $vaDetails->namaBank, $promoCode, $promoAmount);
                    
                    $emailTemplate= $template['email'];
                    $mobileTemplate = $template['mobile'];
                    $minimalTemplate = $template['minimal'];
                    
                    //Simpan notifikasi baru
                    $idNotif = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP','62'),
                        "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                        $mobileTemplate,
                        $minimalTemplate,
                        "OP"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id)
                        , [
                        "id" => $idNotif,
                        "tipe" => "OP",
                        "title" => $this->ConfigModel->getNamaProduk('OP','62'),
                        "tagline" => "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                        "content" => "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                        "paymentType" => $payment,
                        "token" => $token->no_hp    
                    ]);

                    // Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE){
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS WALLET');                                
                    }

                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Buka Tabungan Emas Sukses',
                        'data' => $resData
                            ], 200);   

                } else {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 103,
                        'message' => 'Open Tabungan Error',
                        'reason' => $openTabungan
                            ), 200);
                }    
            }
            else if ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI') 
            {

                
                $kodeBank = '';
                if($payment == 'VA_BCA'){
                    $kodeBank = '014';
                } else if($payment=='VA_MANDIRI') {
                    $kodeBank = '008';
                } else if($payment=='VA_BRI') {
                    $kodeBank = '002';
                }
                
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','BANK','62',$kodeBank);
                $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP','BANK','62',$kodeBank);

                $createBillingData = [
                    'channelId' => $token->channelId,
                    'amount' => (string) ($amount + $biayaTransaksi - $discountAmount),
                    'customerEmail' => $user->email,
                    'customerName' => $user->nama,
                    'customerPhone' => $user->no_hp,
                    'flag' => 'K',
                    'jenisTransaksi' => 'OP',
                    'kodeProduk' => '62',                            
                    'keterangan' => $keterangan,
                    'reffSwitching' => $trxId,
                    'kodeBank' => $kodeBank
                ];
                $billing = $this->createBillingPegadaian($createBillingData);

                if ($billing->responseCode == '00') {

                    if(!isset($billing->data)){
                        $this->set_response(array(
                            'status' =>'error',
                            'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                            'code' => 103
                        ),200);
                        return;
                    }
                    
                    $billingData = json_decode($billing->data);

                    $virtualAccount = $billingData->vaNumber;
                    $tglExpired = $billingData->tglExpired;

                    $this->db->trans_start();

                    //Save tabungan emas dan update data user
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );
                    
                    $paymentData = array(
                        'payment' => $payment,
                        'kodeBankPembayar' => $kodeBank,
                        'virtual_account' =>  $virtualAccount                                
                    );

                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->update($trxId, $paymentData);

                    $this->db->trans_complete();            
                    if ($this->db->trans_status() === FALSE){
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');                                
                    }

                        
                    $template = $this->generateOpenTabemasNotif($user->nama, $payment, $amount + $biayaTransaksiDisplay - $discountAmount, $virtualAccount, $tglExpired, $trxId, $promoCode, $promoAmount);
                    
                    $emailTemplate= $template['email'];
                    $mobileTemplate = $template['mobile'];
                    $minimalTemplate = $template['minimal'];
                    
                    //Simpan notifikasi baru
                    $idNotif = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP','62'),
                        "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                        $mobileTemplate,
                        $minimalTemplate,
                        "OP"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id)
                        , [
                        "id" => $idNotif,
                        "tipe" => "OP",
                        "title" => $this->ConfigModel->getNamaProduk('OP','62'),
                        "tagline" => "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                        "paymentType" => $payment,
                        "token" => $token->no_hp    
                    ]);

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                        'data' => array(
                            'idTransaksi' => $trxId,
                            'virtualAccount' => $virtualAccount,
                            'expired' => $tglExpired,
                            'now' => date('Y-m-d H:i:s')  
                        )
                    ], 200);

                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Something went wrong. Please try again later.',
                        'code' => 103,
                        'reason' => $billing
                    ], 200);
                } 
            }
            else if ($payment == 'VA_PERMATA') 
            {
                $kodeBank = '013';                        
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP','BANK','62',$kodeBank);
                $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP','BANK','62',$kodeBank);

                $createBillingData = [
                    'channelId' => $token->channelId,
                    'amount' => (string) ($amount + $biayaTransaksi - $discountAmount),
                    'customerEmail' => $user->email,
                    'customerName' => $user->nama,
                    'customerPhone' => $user->no_hp,
                    'flag' => 'K',
                    'jenisTransaksi' => 'OP',
                    'productCode' => '62',
                    'norek' => '',
                    'keterangan' => $keterangan,
                    'trxId' => $trxId,
                ];
                $billing = $this->createBillingPermata($createBillingData);

                if ($billing->responseCode == '00') {

                    if(!isset($billing->data)){
                        $this->set_response(array(
                            'status' =>'error',
                            'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                            'code' => 103
                        ),200);
                        return;
                    }
                    
                    $billingData = json_decode($billing->data);
                    
                    
                    $virtualAccount = $billingData->virtualAccount;
                    $tglExpired = $billingData->tglExpired;

                    $this->db->trans_start();

                    //Save tabungan emas dan update data user
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );
                    
                    $paymentData = array(
                        'payment' => $payment,
                        'kodeBankPembayar' => $kodeBank,
                        'payment' => 'BANK',
                        'virtual_account' => $virtualAccount
                    );

                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->update($trxId, $paymentData);

                    $this->db->trans_complete();            
                    if ($this->db->trans_status() === FALSE){
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');                                
                    }

                        
                    $template = $this->generateOpenTabemasNotif($user->nama, $payment, $amount + $biayaTransaksiDisplay - $discountAmount, $virtualAccount, $tglExpired, $trxId, $promoCode, $promoAmount);
                    
                    $emailTemplate= $template['email'];
                    $mobileTemplate = $template['mobile'];
                    $minimalTemplate = $template['minimal'];
                    
                    //Simpan notifikasi baru
                    $idNotif = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP','62'),
                        "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".")." ke ".$virtualAccount,
                        $mobileTemplate,
                        $minimalTemplate,
                        "OP"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id)
                        , [
                        "id" => $idNotif,
                        "tipe" => "OP",
                        "title" => $this->ConfigModel->getNamaProduk('OP','62'),
                        "tagline" => "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".")." ke ".$virtualAccount,
                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                        "paymentType" => $payment,
                        "token" => $token->no_hp    
                    ]);

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                        'data' => array(
                            'idTransaksi' => $trxId,
                            'virtualAccount' => $virtualAccount,
                            'expired' => $tglExpired,
                            'now' => date('Y-m-d H:i:s')  
                        )
                    ], 200);

                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Something went wrong. Please try again later.',
                        'code' => 103,
                        'reason' => $billing
                    ], 200);
                }
            }

        }

    }


    /**
     * Method untuk menggenerate HTML Konten notifikasi sukses buka tabungan emas
     * @param type $nama
     * @param type $noRekening
     * @param type $cif
     * @param type $tanggalBuka
     * @param type $saldo
     * @param type $namaOutlet
     * @param type $alamatOutlet
     * @param type $teleponOutlet
     * @return String $message HTML Content yang sudah tergenerate
     */
    function generateOpenTabemasNotifSuccess($nama, $noRekening, $cif, $tanggalBuka, $saldo, $namaOutlet, $alamatOutlet, $teleponOutlet, $trxId)
    {
        $subject = "Selamat Rekening Tabungan Emas Anda Sudah Aktif";
        
        //Batas waktu KYC = tanggalBuka + 6 bulan
        $batasWaktu = new DateTime($tanggalBuka);
        $interval = new DateInterval('P6M');
        $batasWaktu->add($interval);
        
        //Format tanggal buka
        $fTanggalBuka = new DateTime($tanggalBuka);
        
        
        $viewData = array(
            'nama' => $nama,
            'noRekening' => $noRekening,
            'cif' => $cif,
            'tanggalBuka' => $fTanggalBuka->format('d/m/Y'),
            'saldo' => $saldo,
            'namaOutlet' =>$namaOutlet,
            'alamatOutlet' => $alamatOutlet,
            'teleponOutlet' => $teleponOutlet,
            'batasWaktu' => $batasWaktu->format('d/m/Y'),
            'trxId' => $trxId
        );
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$this->load->view('mail/notif_opentabemas_success', $viewData, true);
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$this->load->view('mail/notif_opentabemas_success', $viewData, true);
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile
        );
    }
    
    /**
     * Method untuk menggenerate content HTML notif buka tabungan emas
     * @param String $payment
     * @param String $jumlah
     * @param String $nama
     * @param String $rekeningEmas
     * @param String $gram
     * @param String $noRekening
     * @param String $tanggalExpired
     * @return String generated content
     */
    function generateOpenTabemasNotif($nama, $payment, $amount, $va, $tglExpired, $trxId, $promoCode = '', $promoAmount = 0,$bankName = null)
    {
        $subject = "Pengajuan Pembukaan Rekening Tabungan Emas Pegadaian";
                
        //Convert tanggal expired ke dd-mm-yyy H:i:s
        $date = new DateTime($tglExpired);
        
        $viewData = array(
            
            'nama' => $nama,
            'payment' => $payment,
            'va' => $va,
            'amount' => $amount,
            'tglExpired' => $date->format('d/m/Y H:i:s'),
            'trxId' => $trxId,
            'bankName' => $bankName,
            'promoCode' => $promoCode,
            'promoAmount' => $promoAmount
        );
        
        $content = $this->load->view('mail/notif_opentabemas', $viewData, true);
        
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$content;
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);  
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    
    /**
     * Method untuk menggenerate content html notif beli emas
     * @param String $payment
     * @param String $jumlah
     * @param String $nama
     * @param String $rekeningEmas
     * @param String $gram
     * @param String $noRekening
     * @param String $tanggalExpired
     * @param String $trxId
     * @return String generated html content
     */
    function generateBeliEmasNotif($trxId, $cif)
    {
        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        
        // Get promo (if Any)
        $gpoin = $this->GpoinModel->get_promo($trxId);
        $promoCode = '';
        $idPromosi = '';
        $discountAmount = 0;
        $promoAmount = 0;
        if ($gpoin != '') {
            if ($gpoin->type == 'discount') {
                $discountAmount = $gpoin->value;
            }
            $promoCode = $gpoin->promoCode;
            $promoAmount = $gpoin->value;
            $idPromosi = $gpoin->idPromosi;
        };
        $checkPayment['promoCode'] = $promoCode;
        $checkPayment['promoAmount'] = $promoAmount;
        $checkPayment['discountAmount'] = $discountAmount;

        $user = $this->User->getUser($checkPayment['user_AIID']);
        $checkPayment['namaNasabahUser'] = $user->nama;
        
        // log_message('debug', 'GENERATENOTIFBELIEMAS1: '.json_encode($checkPayment));
        // error_log("\n DEBUG NOTIF BELI EMAS: ".json_encode($checkPayment)."\n", 3, "/data/log/beliEmas.log");
        
        $checkPayment['cif'] = $cif;
        $subject = "";
        
        if($checkPayment['payment'] == 'BNI' || 
                $checkPayment['payment'] == 'VA_MANDIRI' ||
                $checkPayment['payment'] == 'VA_BCA' ||
                $checkPayment['payment'] == 'VA_BRI' ||
                $checkPayment['payment'] == 'VA_PERMATA'
            ){
            $subject = "Konfirmasi Pembelian Tabungan Emas Pegadaian ".$checkPayment['norek'];
        }else if($checkPayment['payment'] == 'WALLET' || $checkPayment['payment'] == 'GCASH'){
            $subject = "Selamat Top Up Tabungan Emas Berhasil";
        }
        
        log_message('debug', 'GENERATENOTIFBELIEMAS: '.json_encode($checkPayment));
                
        $content = $this->load->view('mail/notif_beliemas', $checkPayment, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
  
    /**
     * Endpoint alternatif untuk beli emas.
     * Proses inquiry tabungan dan VA dipisah
     */
    function beli_v2_post()
    {
        $token = $this->getToken();
        if ($token) {
            
            $user = $this->User->getUser($token->id);

            $setData = array(
                'no_rekening' => $this->post('no_rekening'),
                'jumlah' => $this->post('jumlah'),
                'promo_code' => $this->post('promo_code'),

            );

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('no_rekening', 'No Rekening', 'required|exact_length[16]');
            $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|integer');

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'code' => 201,
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {

                $no_rekening = $this->post('no_rekening');
                $jumlah = $this->post('jumlah');
                $promoCode = $this->post('promo_code');

                //Dapatkan detail user dan rekening berdasarkan nomo rekening
                $rekeningTabungan = $this->User->getUserByNoRek($no_rekening);
                //Lakukan inquiry tabungan emas
                
                $inquiryTabunganEmasData = [
                    'channelId' => $token->channelId,
                    'clientId' => $this->config->item('core_post_username'),
                    'jenisTransaksi' => 'SL',
                    'flag' => 'K',
                    'amount' => $jumlah,
                    'norek' => $no_rekening,
                    'kodeCabang' => $user->kode_cabang,
                    'cif' => $user->cif,
                ];

                // Lakukan validasi promo/voucher gpoint jika ada
                if($promoCode != null)
                {
                    if($user->cif == null){
                        return $this->set_response([
                            'status' => 'error',
                            'message' => 'Anda harus memiliki CIF untuk menggunakan fitur voucher',
                            'code' => 102
                        ]);
                    }

                    $validateData = [
                        'promoCode' => $promoCode,
                        'voucherId' => null,
                        'userId' => $user->cif,
                        'transactionAmount' => (int) $jumlah,
                        'validators' => [
                            'channel' => $this->config->item('core_post_username'),
                            'product' => '62',
                            'transactionType' => 'SL',
                            'unit' => 'rupiah',
                        ]
                    ];

                    $validateVoucher = $this->gPointValidateVoucher($validateData);

                    if($validateVoucher->status == 'Error'){
                        return $this->set_response([
                            'status' => 'error',
                            'message' => $validateVoucher->message,
                            'code' => 103,                                
                        ]);
                    } else if($validateVoucher->status == 'Success') {
                        $inquiryTabunganEmasData['discountAmount'] = $validateVoucher->data->discount;
                        $inquiryTabunganEmasData['idPromosi'] = $validateVoucher->data->journalAccount.';'.$promoCode;
                    }
                }

                $inquiryTabungan = $this->inquiryTabunganEmas($inquiryTabunganEmasData);

                if ($inquiryTabungan->responseCode == '00') {
                    
                    //Fallback jika tidak bisa connect ke core
                    if(!isset($inquiryTabungan->data)){
                        $this->set_response(array(
                            'status' => 'error',
                            'message'=> 'Kesalahan jaringan mohon coba beberapa saat lagi',
                            'code' => 103,
                            'reason' => $inquiryTabungan
                        ),200);
                        return;
                    }

                    $inquiryData = json_decode($inquiryTabungan->data);

                    $trxId = $inquiryData->reffSwitching;
                    $gram = $inquiryData->gram;
                    $administrasi = $inquiryData->administrasi;
                    $satuan = $inquiryData->satuan;
                    $harga = $inquiryData->harga;
                    $totalKewajiban = $inquiryData->totalKewajiban;
                    $biaya = $inquiryData->surcharge;
                    $namaNasabah = $inquiryData->namaNasabah;
                    $hargaEmas = $inquiryData->harga;
                    $nilaiTransaksi = $inquiryData->nilaiTransaksi;
                    $hakNasabah = $inquiryData->hakNasabah;
                    $surcharge = $inquiryData->surcharge;
                    $reffCore = $inquiryData->reffCore;
                    $saldoEmas = $inquiryData->saldoEmas;
                    $norek = $inquiryData->norek;
                    $saldoNominal = $inquiryData->saldoNominal;
                    $biayaTitip = $inquiryData->biayaTitip;
                    $namaProduk = $inquiryData->namaProduk; 
                    $tglTransaksi = $inquiryData->tglTransaksi;
                    $paymentData = array(
                        'id_transaksi' => $trxId,
                        'jenis_transaksi' => 'SL',
                        'amount' => $jumlah,
                        'gram' => $gram,
                        'administrasi' => $administrasi,
                        'kode_produk' => '62',
                        'harga' => $harga,
                        'satuan' => $satuan,
                        'saldoEmas' => $saldoEmas,
                        'total_kewajiban' => $totalKewajiban,
                        'reffCore' => $inquiryData->reffCore,
                        'user_AIID' => $token->id,
                        'keterangan' => 'Pembelian Emas',
                        'no_rekening' => $norek,
                        'surcharge' => $surcharge,
                        'reffCore' => $reffCore,
                        'nilaiTransaksi' => $nilaiTransaksi,
                        'hakNasabah' => $hakNasabah,
                        'saldoNominal' => $saldoNominal,
                        'biayaTitip' => $biayaTitip,
                        'namaProduk' => $namaProduk,
                        'tglTransaksi' => $tglTransaksi,
                        'totalKewajiban' => $totalKewajiban,
                        'namaNasabah' => $namaNasabah,
                        'jenisTransaksi' => 'SL',
                        'norek' => $no_rekening,
                        'reffSwitching' => $trxId
                        
                    );

                    if(isset($inquiryTabunganEmasData['discountAmount'], $inquiryTabunganEmasData['idPromosi']))
                    {
                        $paymentData['discountAmount'] = $inquiryTabunganEmasData['discountAmount'];
                        $paymentData['idPromosi'] = $inquiryTabunganEmasData['idPromosi'];    
                    }
                    
                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->add($paymentData);
                    
                    //Get biaya channel untuk masing-masing metode pembayaran
                    $biayaChannel = array(
                        'BNI' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','009'),
                        'MANDIRI' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','008'),
                        'WALLET' => $this->ConfigModel->getBiayaPayment('SL','WALLET','62',''),
                        'VA_MANDIRI' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','008'),
                        'VA_BCA' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','014'),
                        'VA_PERMATA' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','013'),
                        'VA_BRI' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','002'),
                    );
                    
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Inquiry success',
                        'data' => array(
                            'idTransaksi' => $trxId,
                            'amount' => $jumlah,
                            'gram' => $gram,
                            'administrasi' => $administrasi,
                            'biaya' => $biaya,
                            'satuan' => $satuan,
                            'totalKewajiban' => $totalKewajiban,
                            'noRekening' => $no_rekening,
                            'namaNasabah' => $namaNasabah,
                            'hargaEmas' => $hargaEmas,
                            'biayaChannel' => $biayaChannel
                        )
                    ),200);
                    

                } else {
                    $this->set_response(array(
                        'status' => 'errror',
                        'message' => 'Inquiry Tabungan Error',
                        'code' => 103,
                        'reason' => $inquiryTabungan
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    function payment_post()
    {
        $token = $this->getToken();
        if($token){
           $user = $this->User->getUser($token->id);
           
            $setData = array(
               'id_transaksi' => $this->post('id_transaksi'),
               'payment' => $this->post('payment'),
               'booking_code' => $this->post('booking_code'),
               'card_number' => $this->post('card_number'),
               'token_response' => $this->post('token_response'),
               'pin' => $this->post('pin'),
               'va' => $this->post('va')
           ); 
           
           $this->form_validation->set_data($setData);
           
           $this->form_validation->set_rules('id_transaksi','id_transaksi','required');
           $this->form_validation->set_rules('payment','payment','required');
           
           $payment = $this->post('payment');
           
            if($payment == 'MANDIRI'){
                $this->form_validation->set_rules('booking_code','booking_code','required');            
                $this->form_validation->set_rules('card_number','card_number','required');            
                $this->form_validation->set_rules('token_response','token_response','required');  
            }
            
            if($payment == 'WALLET' || $payment == 'GCASH'){
                $this->form_validation->set_rules('pin','pin','required|exact_length[6]|numeric');
            }
            
            //Jika payment adalah wallet dan user belum akttifasi wallet
            if($payment == 'WALLET' && $user->norek == ''){
                $this->set_response(array(
                   'status' => 'error',
                   'message' => 'Anda belum melakukan aktifasi wallet',
                   'code' => 101                 
               ),200);
               return;
            }

            if($payment == 'GCASH'){
                $this->form_validation->set_rules('va', 'va', 'required|numeric');
            }
           
           if($this->form_validation->run() == FALSE){
               $this->set_response(array(
                   'status' => 'error',
                   'message' => 'Invalid input',
                   'code' => 101,
                   'errors' => $this->form_validation->error_array()
               ),200);
           }else{
               $idTransaksi = $this->post('id_transaksi');
               
               
               //Mendapatkan payment berdasarkan id transaksi
               $checkPayment = $this->PaymentModel->getPaymentByTrxId($idTransaksi);
               
               if($checkPayment){                   
               
                    $checkCustomer = $this->detailTabunganEmas($checkPayment->no_rekening, $token->channelId);                    
                    
                    if ($checkCustomer->responseCode == '00') {
                        
                        if (!isset($checkCustomer->data)) {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                'code' => 103,
                                'reason' => $checkCustomer
                                    ), 200);
                            return;
                        }
                                
                        $customerData = json_decode($checkCustomer->data);

                        // Mendapatkan promo berdasarkan id transaksi
                        $gpoin = $this->GpoinModel->get_promo($idTransaksi);
                        $idPromosi = '';
                        $discountAmount = 0;
                        $promoAmount = 0;
                        if ($gpoin != '') {
                            if ($gpoin->type == 'discount') {
                                $discountAmount = $gpoin->value;
                            }
                            $promoAmount = $gpoin->value;
                            $idPromosi = $gpoin->idPromosi;
                        };
                        // $discountAmount = $checkPayment->discountAmount;
                        // $idPromosi = $checkPayment->idPromosi;
                        
                        if ($payment == 'BNI') 
                        {
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL','BANK','62','009');
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL','BANK','62','009');
                            error_log("\n MULIA PAYMENT ".(string)($checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount)."\n", 3, "/data/log/Mulia.log");
                            $createBillingBNI = $this->createBillingVABNI(
                                    $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount, 
                                    $user->email, 
                                    $token->nama, 
                                    $token->no_hp, 
                                    'K', 
                                    'SL',
                                    "PDSSL " . $idTransaksi . " " . $checkPayment->no_rekening, 
                                    $checkPayment->no_rekening, 
                                    '62', 
                                    $idTransaksi, 
                                    $token->channelId,
                                    $idPromosi,
                                    $promoAmount
                                );

                            if ($createBillingBNI->responseCode == '00') {
                                log_message('debug', 'RESPONSE CREATE BILLING BNI' . json_encode($createBillingBNI));
                                if (!isset($createBillingBNI->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBillingBNI
                                            ), 200);
                                    return;
                                }
                                // error_log("\n DEBUG  BNI: ".json_encode($createBillingBNI)."\n", 3, "/data/log/beliEmas.log");
                                $billingData = json_decode($createBillingBNI->data);


                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay
                                );

                                //Buat payment baru
                                //Simpan payment                                
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $lblNotifNama = isset($checkPayment->namaNasabah) ? $checkPayment->namaNasabah : $token->nama;
                                $cif = $this->User->getUserCif($token->id);

                                $template = $this->generateBeliEmasNotif($idTransaksi, $customerData->cif);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                        $token->id, 
                                        NotificationModel::TYPE_EMAS, NotificationModel::CONTENT_TYPE_HTML, 
                                        $this->ConfigModel->getNamaProduk('SL','62'), 
                                        "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                        $mobileTemplate, 
                                        $minimalTemplate,
                                        "SL"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                        $this->User->getFCMToken($token->id)
                                        , [
                                    "id" => $notifId,
                                    "tipe" => "SL",
                                    "title" => $this->ConfigModel->getNamaProduk('SL','62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                    "token" => $token->no_hp        
                                ]);

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailBeliEmas(
                                        $token->email, $checkPayment->no_rekening, $emailTemplate
                                );

                                //Mendapatkan detail tabungan emas user
                                $saldo = $this->getSaldo($checkPayment->no_rekening);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir']
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:' . json_encode($createBillingBNI));
                                // error_log("\n DEBUG ERROR BNI: ".json_encode($createBillingBNI)."\n", 3, "/data/log/beliEmas.log");
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBillingBNI
                                        ), 200);
                            }
                        }
                        else if($payment == 'MANDIRI')
                        {
                            
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL','BANK','62','008');
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL','BANK','62','008');

                            $bookingCode = $this->post('booking_code');
                            $cardNumber = $this->post('card_number');
                            $tokenResponse = $this->post('token_response');
                            
                            //Bayar dengan mandiri click pay
                            $clickPay = $this->mandiriClickPay(
                                $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount, 
                                $bookingCode, 
                                $cardNumber, 
                                'SL', 
                                "PDSSL " . $idTransaksi . " " . $checkPayment->no_rekening, 
                                $user->no_hp, 
                                $checkPayment->no_rekening, 
                                '62', 
                                $tokenResponse, 
                                $idTransaksi, 
                                $token->channelId,
                                $idPromosi,
                                $promoAmount
                            );
                             
                            if($clickPay->responseCode == '00'){
                                log_message('debug', 'RESPONSE CREATE BILLING MANDIRI' . json_encode($clickPay));
                                $clickpayData = json_decode($clickPay->data);

                                $updateData['bookingCode'] = $bookingCode;
                                $updateData['cardNumber'] = $cardNumber;
                                $updateData['tokenResponse'] = $tokenResponse;
                                $updateData['reffBiller'] = $clickpayData->reffBiller;
                                $updateData['kodeBankPembayar'] ='008';
                                $updateData['payment'] = $payment;
                                $updateData['biayaTransaksi'] = $biayaTransaksiDisplay;

                                //update data pembayaran
                                $this->PaymentModel->update($idTransaksi, $updateData);

                                //Mendapatkan saldo rekening
                                $saldo = $this->getSaldo($checkPayment->no_rekening);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Pembayaran berhasil',
                                    'data' => array(
                                        'reffBiller' => $clickpayData->reffBiller,
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir'] 
                                    )
                                ], 200);                                

                            }else{
                                $this->set_response(array(
                                    'status' => 'error',
                                    'code' => 103,
                                    'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                    'reason' => $clickPay
                                ));
                            }
                        }
                        else if($payment == 'WALLET' || $payment == 'GCASH')
                        {
                            
                            $pin = $this->post('pin');
                            $va = $this->post('va');                                 
                            //Check user pin                                 
                            if(!$this->User->isValidPIN2($token->id, $pin)){
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'PIN tidak valid',
                                    'code' => 102
                                ),200);
                                return;
                            }
                            
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL','WALLET','62','');
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL','WALLET','62','');

                            log_message('debug', 'PAYMENT EMAS WALLET');
                            $paymentData = array(
                                'administrasi' => $checkPayment->administrasi,
                                'gram' => $checkPayment->gram,
                                'hakNasabah' => $checkPayment->hakNasabah,
                                'harga' => $checkPayment->harga,
                                'jenisTransaksi' => $checkPayment->jenis_transaksi,
                                'nilaiTransaksi' => $checkPayment->nilaiTransaksi,
                                'norek' => $checkPayment->no_rekening,
                                'reffSwitching' => $checkPayment->id_transaksi,
                                'surcharge' => $checkPayment->surcharge,
                                'totalKewajiban' => $checkPayment->total_kewajiban - $discountAmount,                               
                                'productCode' => '62',
                                'flag' => 'K',
                                'channelId' => $token->channelId,
                                'idPromosi' => $idPromosi,
                                'discountAmount' => $promoAmount                                                                    
                            );

                            if($payment == 'WALLET'){
                                $paymentData['paymentMethod'] = 'WALLET';
                                $paymentData['norekWallet'] = $user->norek;
                                $paymentData['walletId'] = $user->no_hp;    
                            } else if($payment == 'GCASH'){
                                $paymentData['amount'] = $checkPayment->totalKewajiban;
                                $paymentData['paymentMethod'] = 'GCASH';
                                $paymentData['gcashId'] = $va;
                            }

                        
                            $walletPayment = $this->paymentTabungan($paymentData);

                            if($walletPayment->responseCode == '00'){
                                log_message('debug', 'RESPONSE CREATE BILLING GCASH AND WALLET' . json_encode($walletPayment));
                                $walletData = json_decode($walletPayment->data, TRUE);

                                $walletData['payment'] = $payment;
                                $walletData['gcashId'] = $va;
                                $walletData['is_paid'] = '1';
                                $walletData['biayaTransaksi'] = $biayaTransaksiDisplay;

                                //Update wallet data
                                $this->PaymentModel->update($checkPayment->id_transaksi, $walletData);

                                $lblNotifNama = isset($checkPayment->namaNasabah) ? $checkPayment->namaNasabah : $token->nama;
                                $cif = $this->User->getUserCif($token->id);

                                $template = $this->generateBeliEmasNotif($idTransaksi, $cif);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                        $token->id, NotificationModel::TYPE_EMAS, 
                                        NotificationModel::CONTENT_TYPE_HTML, 
                                        $this->ConfigModel->getNamaProduk('SL','62'),
                                        "Top Up Emas Berhasil",
                                        $mobileTemplate,
                                        $minimalTemplate,
                                        "SL"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                        $this->User->getFCMToken($token->id)
                                        , [
                                    "id" => $notifId,
                                    "tipe" => "SL",
                                    "title" => $this->ConfigModel->getNamaProduk('SL','62'),
                                    "tagline" => "Top Up Emas Berhasil",
                                    "content" => "Top Up Emas Berhasil",
                                    "paymentType" => $payment,
                                    "token" => $token->no_hp
                                ]);

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailBeliEmas(
                                        $token->email, $checkPayment->no_rekening, $emailTemplate
                                );

                                //Update Saldo Wallet
                                $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                $walletData['wallet'] = $saldoWallet;

                                $saldo = $this->getSaldo($checkPayment->no_rekening);
                                $walletData['saldo'] = $saldo['saldo'];
                                $walletData['saldoAkhir'] = $saldo['saldoAkhir'];
                                $walletData['saldoBlokir'] = $saldo['saldoBlokir']; 

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Pembelian Pembelian Emas Berhasil',
                                    'data' => $walletData
                                ], 200);

                            }else{
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => '',
                                    'code' => 103,
                                    'reason' => $walletPayment                                    
                                ),200);
                            }
                        } 
                        else if($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI')
                        {
                            $kodeBank = '';
                            if($payment == 'VA_BCA'){
                                $kodeBank = '014';
                            } else if($payment == 'VA_MANDIRI') {
                                $kodeBank = '008';
                            } else if($payment == 'VA_BRI') {
                                $kodeBank = '002';
                            }
                            
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL','BANK','62', $kodeBank);
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL','BANK','62', $kodeBank);

                            $createBillingData = [
                                'channelId' => $token->channelId,
                                'amount' => $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount,
                                'customerEmail' => $user->email,
                                'customerName' => $token->nama,
                                'customerPhone' => $token->no_hp,
                                'flag' => 'K',
                                'jenisTransaksi' => 'SL',
                                'kodeProduk' => '62',
                                'norek' => $checkPayment->no_rekening,
                                'keterangan' => "PDSSL " . $idTransaksi . " " . $checkPayment->no_rekening,
                                'reffSwitching' => $idTransaksi,
                                'kodeBank' => $kodeBank,
                                'idPromosi' => $idPromosi,
                                'discountAmount' => $promoAmount
                            ];
                            $createBilling = $this->createBillingPegadaian($createBillingData);

                            if ($createBilling->responseCode == '00') {
                                log_message('debug', 'RESPONSE CREATE BILLING VA' . json_encode($createBilling));
                                if (!isset($createBilling->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBilling
                                            ), 200);
                                    return;
                                }

                                $billingData = json_decode($createBilling->data);


                                $virtualAccount = $billingData->vaNumber;
                                $tglExpired = $billingData->tglExpired;

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay
                                );

                                //Buat payment baru
                                //Simpan payment                                
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $lblNotifNama = isset($checkPayment->namaNasabah) ? $checkPayment->namaNasabah : $token->nama;
                                $cif = $this->User->getUserCif($token->id);

                                $template = $this->generateBeliEmasNotif($idTransaksi, $customerData->cif);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                        $token->id, 
                                        NotificationModel::TYPE_EMAS, NotificationModel::CONTENT_TYPE_HTML, 
                                        $this->ConfigModel->getNamaProduk('SL','62'), 
                                        "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                        $mobileTemplate, 
                                        $minimalTemplate,
                                        "SL"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                        $this->User->getFCMToken($token->id)
                                        , [
                                    "id" => $notifId,
                                    "tipe" => "SL",
                                    "title" => $this->ConfigModel->getNamaProduk('SL','62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                    "token" => $token->no_hp        
                                ]);

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailBeliEmas(
                                        $token->email, $checkPayment->no_rekening, $emailTemplate
                                );

                                //Mendapatkan detail tabungan emas user
                                $saldo = $this->getSaldo($checkPayment->no_rekening);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir']
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:' . json_encode($createBilling));
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                        ), 200);
                            }
                        }
                        else if($payment == 'VA_PERMATA')
                        {
                            $kodeBank = '013';                            
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL','BANK','62', $kodeBank);
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL','BANK','62', $kodeBank);

                            $createBillingData = [
                                'channelId' => $token->channelId,
                                'amount' => $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount,
                                'customerEmail' => $user->email,
                                'customerName' => $token->nama,
                                'customerPhone' => $token->no_hp,
                                'flag' => 'K',
                                'jenisTransaksi' => 'SL',
                                'productCode' => '62',
                                'norek' => $checkPayment->no_rekening,
                                'keterangan' => "PDSSL " . $idTransaksi . " " . $checkPayment->no_rekening,
                                'trxId' => $idTransaksi,
                                'idPromosi' => $idPromosi,
                                'discountAmount' => $promoAmount
                            ];
                            $createBilling = $this->createBillingPermata($createBillingData);

                            if ($createBilling->responseCode == '00') {
                                log_message('debug', 'RESPONSE CREATE BILLING VA_PERMATA' . json_encode($createBilling));
                                if (!isset($createBilling->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBilling
                                            ), 200);
                                    return;
                                }

                                $billingData = json_decode($createBilling->data);


                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay
                                );

                                //Buat payment baru
                                //Simpan payment                                
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $lblNotifNama = isset($checkPayment->namaNasabah) ? $checkPayment->namaNasabah : $token->nama;
                                $cif = $this->User->getUserCif($token->id);

                                $template = $this->generateBeliEmasNotif($idTransaksi, $customerData->cif);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                        $token->id, 
                                        NotificationModel::TYPE_EMAS, NotificationModel::CONTENT_TYPE_HTML, 
                                        $this->ConfigModel->getNamaProduk('SL','62'), 
                                        "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                        $mobileTemplate, 
                                        $minimalTemplate,
                                        "SL"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                        $this->User->getFCMToken($token->id)
                                        , [
                                    "id" => $notifId,
                                    "tipe" => "SL",
                                    "title" => $this->ConfigModel->getNamaProduk('SL','62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                    "token" => $token->no_hp        
                                ]);

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailBeliEmas(
                                        $token->email, $checkPayment->no_rekening, $emailTemplate
                                );

                                //Mendapatkan detail tabungan emas user
                                $saldo = $this->getSaldo($checkPayment->no_rekening);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir']
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:' . json_encode($createBilling));
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                        ), 200);
                            }
                        }
                    }else{
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan',
                            'code' => 103,
                            'reason' => $checkCustomer
                        ),200);
                    }
               }else{
                   log_message('debug', 'Payment not found'.$this->post('id_transaksi'));
                   $this->set_response(array(
                       'status' => 'success',
                       'message' => 'Payment not found',
                       'code' => 102
                   ),200);
               }
           }
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function history_get()
    {
        $token = $this->getToken();
        if ($token) {
            $setData = array(
                'no_rekening' => $this->query('no_rekening')
            );
            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('no_rekening', 'No Rekening', 'exact_length[16]|numeric|required');

            //Mendapatkan daftar history tabungan bulan ini
            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'code' => 102,
                    'errors' => $this->form_validation->error_array()
                ));
            } else {
                $noRekening = $this->query('no_rekening');
                $history = $this->EmasModel->getHistory($noRekening);
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $history
                ));
            }

        } else {
            $this->errorUnAuthorized();
        }
    }
    
    
    /**
     * Endpoint untuk melakukan pengecekan apakah sebelumnya user sudah  menyelesaikan payment atau belum
     */
    function check_unpaid_payment_get()
    {
        $token = $this->getToken();
        if($token){
                        
            //Check apakah payment dengan status belum bayar dan belum expired
            $unpaid = $this->EmasModel->getUnpaidPayment($token->id);            
            
            //Check ke core apakah status sudah terbayarkan atau belum. Status 1 = billing aktif
            if($unpaid){
                $checkBilling = $this->checkBillingVABNI($unpaid->id_transaksi, $token->channelId);
                if($checkBilling->responseCode == '00'){
                    $billingData = json_decode($checkBilling->data);
                    if($billingData->status == '1'){
                        $billingData->unpaid = true;
                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'Anda memiliki tagihan yang belum terbayarkan',
                            'data' => $billingData
                        ), 200);
                    }else{
                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'Tidak ada payment aktif',
                            'data' => array(
                                'unpaid' => false,
                            )
                        ), 200);
                    }                    
                }else{
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Tidak ada payment aktif',
                        'data' => array(
                            'unpaid' => false,
                        )
                    ), 200);
                }
            }else{
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Tidak ada payment aktif',
                    'data' => array(
                        'unpaid' => false
                    )
                ), 200);
            }
            
            
        }else{
            $this->errorUnAuthorized();
        }
    }

    public function portofolio_post() {
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_rules('cif', 'CIF', 'numeric|required');

            if (!$this->form_validation->run()) {
                $this->response(array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $cif = $this->post('cif');
                $iReq = $this->inquiryTabEmas($cif, $token->channelId);

                if ($iReq->responseCode == '00') {
                    $data = json_decode($iReq->data);
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => $data
                    ), 200);
                } else {
                    $this->response(array(
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Internal Server Error',
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    public function detail_portofolio_get() {
        $token = $this->getToken();
        if ($token) {
            $setData = array(
                'no_rekening' => $this->query('no_rekening')
            );
            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('no_rekening', 'Nomor Rekening', 'numeric|required');

            if (!$this->form_validation->run()) {
                $this->response(array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $noRek = $this->query('no_rekening');
                $user = $this->User->getUser($token->id);
                if(!$this->EmasModel->isUserRekening($user->cif, $noRek)){
                    return $this->set_response([
                        'status' => 'error',
                        'message' => 'Rekening tidak ditemukan',
                        'data' => [],
                        'code' => 101
                    ]);
                }
                
                $iReq = $this->transaksiTabEmas($noRek, $token->channelId);

                if ($iReq->responseCode == '00') {
                    $data = json_decode($iReq->data);
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => $data
                    ), 200);
                } else if($iReq->responseCode == '14'){
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => array()
                    ), 200);
                } else {
                    $this->response(array(
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Internal Server Error',
                        'reason' => $iReq
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    
    function transfer_post($method = 'inquiry') {
        $token = $this->getToken();
        if ($token) {

            $user = $this->User->getUser($token->id);

            if($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2' ){
                $this->set_response(array(
                    'status' => 'error',
                    'message' => $this->getAktifasiFinansialMsg($user->aktifasiTransFinansial),
                    'code' => 201
                ), 200);   
                return;
            }

            if ($method == 'payment') {
                
                $setData = array(
                    'payment' => $this->post('payment'),
                    'id_transaksi' => $this->post('id_transaksi'),
                    'booking_code' => $this->post('booking_code'),
                    'card_number' => $this->post('card_number'),
                    'token_response' => $this->post('token_response'),
                    'pin' => $this->post('pin'),
                    'va' => $this->post('va')
                );
                
                $payment = $this->post('payment');
                
                $this->form_validation->set_data($setData);
                
                if($payment == 'MANDIRI'){
                    $this->form_validation->set_rules('booking_code', 'jumlah', 'required');
                    $this->form_validation->set_rules('card_number', 'card_number', 'required|numeric');
                    $this->form_validation->set_rules('token_response', 'jumlah', 'required');
                }
                
                if($payment == 'WALLET' || $payment == 'GCASH'){
                    $this->form_validation->set_rules('pin','pin','required|exact_length[6]|numeric');
                }

                if($payment == 'GCASH'){
                    $this->form_validation->set_rules('va','va','required|numeric');    
                }
                
                $this->form_validation->set_rules('payment', 'payment', 'required');
                $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
                $this->form_validation->set_rules('pin', 'pin', 'required');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ));
                } else {
                    
                    $trxId = $this->post('id_transaksi');
                    $pin = $this->post('pin');
                    
                    //Check if valid user PIN
                    if($this->User->isValidPIN2($token->id, $pin)){
                        
                        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId);
                    
                        if($checkPayment){

                            if($payment == 'WALLET')
                            {
                                
                                $paymentData = array(
                                    "channelId" => $token->channelId,
                                    "gramTransaksi" =>  $checkPayment->gramTransaksi,
                                    "jenisTransaksi" => $checkPayment->jenisTransaksi,
                                    "namaNasabahTujuan" => $checkPayment->namaNasabahTujuan,
                                    "norek" => $checkPayment->norek,
                                    "norekTujuan" => $checkPayment->norekTujuan,
                                    "norekWallet" => $user->norek,
                                    "paymentMethod" => "WALLET",
                                    "reffBiller" => $checkPayment->id,
                                    "reffSwitching" => $checkPayment->reffSwitching,
                                    "tipeTransaksi" => "2",
                                    "totalKewajiban" => $checkPayment->totalKewajiban,
                                    "walletId" => $user->no_hp
                                );
                                
                                $reqPayment = $this->paymentTransferEmas($paymentData);
                                
                                if($reqPayment->responseCode == '00'){
                                    
                                    if(!isset($reqPayment->data)){
                                        $this->set_response(array(
                                            'status' => 'error',
                                            'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                                            'code' => 103,
                                            'reason' => $reqPayment
                                        ));
                                        return;
                                    }
                                    
                                    $reqPaymentData = json_decode($reqPayment->data);
                                    
                                    $updateData = (array) $reqPaymentData;
                                    $updateData['is_paid'] = '1';
                                    $updateData['payment'] = $payment;
                                    
                                    $this->PaymentModel->update($trxId, $updateData);
                                    
                                    //Send notifikasi pembayaran telah berhasil
                                    $template = $this->generateTransferEmasNotif($trxId);

                                    $emailTemplate= $template['email'];
                                    $mobileTemplate = $template['mobile'];
                                    $minimalTemplate = $template['minimal'];

                                    //Simpan notifikasi baru
                                    $idNotif = $this->NotificationModel->add(
                                        $token->id,
                                        NotificationModel::TYPE_EMAS,
                                        NotificationModel::CONTENT_TYPE_HTML,
                                        $this->ConfigModel->getNamaProduk('TR','62'),
                                        "Transfer Keluar Tabungan Emas ".$checkPayment->norekTujuan." berhasil",
                                        $mobileTemplate,
                                        $minimalTemplate,
                                        "TR"
                                    );

                                    //Kirim notifikasi pembayaran ke device user
                                    Message::sendFCMNotif(
                                        $this->User->getFCMToken($token->id)
                                        , [
                                        "id" => $idNotif,
                                        "tipe" => "TR",
                                        "title" => $this->ConfigModel->getNamaProduk('TR','62'),
                                        "tagline" => "Transfer Keluar Tabungan Emas ".$checkPayment->norekTujuan." berhasil",
                                        "content" => "",
                                        "token" => $token->no_hp    
                                    ]);

                                    
                                    //Kirim Email Notifikasi
                                    $this->load->helper('message');
                                    Message::sendEmail($token->email, "Transfer Keluar Tabungan Emas ".$checkPayment->norekTujuan,  $emailTemplate);
                                                                        
                                    //Kirim juga notifikasi ke user rekening tujuan
                                    $userTujuan = $this->User->getUserByNoRek($checkPayment->norekTujuan);

                                if($userTujuan)
                                {                                        
                                    log_message('debug', 'USER_TUJUAN => '. json_encode($userTujuan) );
                                    
                                    //Send notifikasi pembayaran telah berhasil
                                    $template = $this->generateTransferEmasNotif($trxId, true, $userTujuan->nama);

                                    $emailTemplate= $template['email'];
                                    $mobileTemplate = $template['mobile'];
                                    $minimalTemplate = $template['minimal'];

                                    //Simpan notifikasi baru
                                    $idNotif2 = $this->NotificationModel->add(
                                        $userTujuan->user_AIID,
                                        NotificationModel::TYPE_EMAS,
                                        NotificationModel::CONTENT_TYPE_HTML,
                                        $this->ConfigModel->getNamaProduk('TR','62'),
                                        "Transfer Masuk Tabungan Emas dari ".$user->nama." Sejumlah ".$checkPayment->gramTransaksi.' gram',
                                        $mobileTemplate,
                                        $minimalTemplate,
                                        "TR"    
                                    );

                                    //Kirim notifikasi pembayaran ke device user
                                    Message::sendFCMNotif(
                                        $userTujuan->fcm_token
                                        , [
                                        "id" => $idNotif2,
                                        "tipe" => "TR",
                                        "title" => $this->ConfigModel->getNamaProduk('TR','62'),
                                        "tagline" => "Transfer Masuk Tabungan Emas dari ".$user->nama." Sejumlah ".$checkPayment->gramTransaksi.' gram',
                                        "content" => "",
                                        "token" => $userTujuan->no_hp    
                                    ]);


                                    //Kirim Email Notifikasi
                                    $this->load->helper('message');
                                    Message::sendEmail($userTujuan->email, "Transfer Masuk Tabungan Emas ".$checkPayment->norekTujuan,  $emailTemplate);
                                }   
                                    
                                //Update Saldo Wallet
                                $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                $reqPaymentData->wallet = $saldoWallet;

                                $saldo = $this->getSaldo($checkPayment->norek);
                                $reqPaymentData->saldo = $saldo['saldo'];
                                $reqPaymentData->saldoAkhir = $saldo['saldoAkhir'];
                                $reqPaymentData->saldoBlokir = $saldo['saldoBlokir'];
                                
                                $this->set_response(array(
                                    'status' => 'success',
                                    'message' => 'Transfer emas berhasil',
                                    'data' => $reqPaymentData
                                ),200);
                                    
                                }else{
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $reqPayment
                                    ));
                                }
                                                                
                            }else{
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Pembayaran tidak valid',
                                    'code' => 102
                                ),200);
                            }
                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Transaksi tidak ditemukan',
                                'code' => 102
                            ),200);
                        }
                    }else{
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'PIN tidak valid',
                            'code' => 102
                        ),200);
                    }                    
                }
                
            } else if ($method == 'inquiry') {
                $setData = array(
                    'jumlah' => $this->post('jumlah'),
                    'norek' => $this->post('norek'),
                    'norek_tujuan' => $this->post('norek_tujuan')
                );

                $this->form_validation->set_data($setData);
                $this->form_validation->set_rules('norek', 'norek', 'required|exact_length[16]|numeric');
                $this->form_validation->set_rules('norek_tujuan', 'norek_tujuan', 'required|exact_length[16]|numeric');
                $this->form_validation->set_rules('jumlah', 'jumlah', 'required|numeric');

                if ($this->form_validation->run() == FALSE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ));
                } else {
                    $jumlah = $this->post('jumlah');
                    $norek = $this->post('norek');
                    $norekTujuan = $this->post('norek_tujuan');
                    
                    //cek apakah norek == noreTujuan
                    if($norek == $norekTujuan){
                        $this->set_response(array(
                            'status' => "error",
                            'message' => "Anda tidak dapat melakukan transfer ke no rekening yang sama!",
                            'code' => 102
                        ), 200);
                        return;
                    }

                    $inquiryData = array(
                        'channelId' => $token->channelId,
                        'jenisTransaksi' => 'TR',
                        'norek' => $norek,
                        'norekTujuan' => $norekTujuan,
                        'gramTransaksi' => $jumlah
                    );

                    $inquiry = $this->inquiryTransferEmas($inquiryData);

                    if ($inquiry->responseCode == '00') {

                        if (!isset($inquiry->data)) {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                                'reason' => $inquiry,
                                'code' => 103
                                    ), 200);
                            return;
                        }

                        $resData = json_decode($inquiry->data);
                        $resData->id_transaksi = $resData->reffSwitching;
                        
                        $saveData = (array) $resData;

                        $saveData['jenis_transaksi'] = 'TR';
                        $saveData['user_AIID'] = $token->id;
                        $saveData['id_transaksi'] = $resData->reffSwitching;
                        $saveData['kode_produk'] = '62';
                        $saveData['jenis_transaksi'] = 'TR';

                        $this->PaymentModel->add($saveData);

                        $this->set_response(array(
                            'status' => 'success',
                            'message' => '',
                            'data' => $resData
                                ), 200);
                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                            'reason' => $inquiry,
                            'code' => 103
                                ), 200);
                    }
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function buyback_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if ($token) {

            $user = $this->User->getUser($token->id);
            if($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2' ){
                $this->set_response(array(
                    'status' => 'error',
                    'message' => $this->getAktifasiFinansialMsg($user->aktifasiTransFinansial),
                    'code' => 201
                ), 200);   
                return;
            }

            if ($method == 'payment') {
                
                $setData = array(
                    'id_transaksi' => $this->post('id_transaksi'),
                    'pin' => $this->post('pin')
                );
                
                $payment = $this->post('payment');
                $this->form_validation->set_data($setData);
                
                $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
                $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');
                
                if ($this->form_validation->run() == FALSE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ));
                } else {
                    
                    $trxId = $this->post('id_transaksi');
                    $pin = $this->post('pin');
                    
                    //Check if valid user PIN
                    if($this->User->isValidPIN2($token->id, $pin)){
                        
                        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId);
                    
                        if($checkPayment){
                            
                            $payment = $checkPayment->payment;
                            
                            $paymentData = array(
                                'channelId' => $token->channelId,
                                'administrasi' => $checkPayment->administrasi,
                                'gramTransaksi' => $checkPayment->gramTransaksi,
                                'jenisTransaksi' => $checkPayment->jenisTransaksi,
                                'nilaiTransaksi' => $checkPayment->nilaiTransaksi,
                                'reffSwitching' => $checkPayment->reffSwitching,
                                'surcharge' => $checkPayment->surcharge,
                                'totalKewajiban' => $checkPayment->totalKewajiban,
                                'norek' => $checkPayment->norek,                                                               
                            );

                            if($payment == 'WALLET' || $payment == 'GCASH'){
                                
                                $paymentData['norekWallet'] = $user->norek; 
                                $paymentData['paymentMethod'] = 'WALLET';
                                $paymentData['walletId'] = $user->no_hp; 

                                if($payment=='WALLET'){
                                    $paymentData['tipeTransaksi'] = '1';
                                    $paymentData['norekWalletTujuan'] = $checkPayment->norekWalletTujuan;    
                                } else if($payment == 'GCASH'){
                                    $paymentData['gcashIdTujuan'] = $checkPayment->gcashIdTujuan;
                                    $paymentData['tipeTransaksi'] = '3';
                                }                               
                            }else if($payment == 'BANK'){
                                $paymentData['kodeBankTujuan'] = $checkPayment->kodeBankTujuan;
                                $paymentData['norekBankTujuan'] = $checkPayment->norekBankTujuan;
                                $paymentData['norekWallet'] = $user->norek;  
                                $paymentData['walletId'] = $user->no_hp; 
                                $paymentData['namaBankTujuan'] = $checkPayment->namaBankTujuan;
                                $paymentData['paymentMethod'] = 'WALLET'; 
                                $paymentData['tipeTransaksi'] = '2';
                                
                            }
                            
                            $reqPayment = $this->paymentBuyBackEmas($paymentData);
                                
                                if($reqPayment->responseCode == '00'){
                                    
                                    if(!isset($reqPayment->data)){
                                        $this->set_response(array(
                                            'status' => 'error',
                                            'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                                            'code' => 103,
                                            'reason' => $reqPayment
                                        ));
                                        return;
                                    }
                                    
                                    $reqPaymentData = json_decode($reqPayment->data);
                                    
                                    $updateData = (array) $reqPaymentData;
                                    $updateData['is_paid'] = '1';
                                    $updateData['payment'] = $payment;
                                    
                                    $this->PaymentModel->update($trxId, $updateData);
                                    
                                    //Send notifikasi pembayaran telah berhasil
                                    $template = $this->generateBuyBackNotif($trxId);

                                    $emailTemplate= $template['email'];
                                    $mobileTemplate = $template['mobile'];
                                    $minimalTemplate = $template['minimal'];

                                    //Simpan notifikasi baru
                                    $idNotif = $this->NotificationModel->add(
                                        $token->id,
                                        NotificationModel::TYPE_EMAS,
                                        NotificationModel::CONTENT_TYPE_HTML,
                                        $this->ConfigModel->getNamaProduk('BB','62'),
                                        "Jual Tabungan Emas ".$checkPayment->norek." berhasil",
                                        $mobileTemplate,
                                        $minimalTemplate,
                                        "BB"    
                                    );

                                    //Kirim notifikasi pembayaran ke device user
                                    Message::sendFCMNotif(
                                        $this->User->getFCMToken($token->id)
                                        , [
                                        "id" => $idNotif,
                                        "tipe" => "BB",
                                        "title" => $this->ConfigModel->getNamaProduk('BB','62'),
                                        "tagline" => "Jual Tabungan Emas ".$checkPayment->norek." berhasil",
                                        "content" => "",
                                        "token" => $token->no_hp    
                                    ]);

                                    //Kirim Email Notifikasi
                                    $this->load->helper('message');
                                    Message::sendEmail($token->email, "Jual Tabungan Emas ".$checkPayment->norek,  $emailTemplate);
                                    
                                    //Update Saldo Wallet
                                    $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                    $reqPaymentData->wallet = $saldoWallet;

                                    $saldo = $this->getSaldo($checkPayment->norek);
                                    $reqPaymentData->saldo = $saldo['saldo'];
                                    $reqPaymentData->saldoAkhir = $saldo['saldoAkhir'];
                                    $reqPaymentData->saldoBlokir = $saldo['saldoBlokir'];
                                    
                                    
                                    $this->set_response(array(
                                        'status' => 'success',
                                        'message' => 'Jual tabungan emas berhasil',
                                        'data' => $reqPaymentData
                                    ),200);
                                    
                                }else{
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $reqPayment
                                    ));
                                }
                            
                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Transaksi tidak ditemukan',
                                'code' => 102
                            ),200);
                        }
                    }else{
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'PIN tidak valid',
                            'code' => 102
                        ),200);
                    }                    
                }
                
            } else if ($method == 'inquiry') {
                $setData = array(
                    'jumlah' => $this->post('jumlah'),
                    'norek' => $this->post('norek'),
                    'payment' => $this->post('payment'),
                    'kode_bank_tujuan' => $this->post('kode_bank_tujuan'),
                    'nama_nasabah_bank' => $this->post('nama_nasabah_bank'),
                    'norek_bank' => $this->post('norek_bank'),
                    'va' => $this->post('va')
                );
                
                $payment = $this->post('payment');
                
                $this->form_validation->set_data($setData);
                $this->form_validation->set_rules('norek', 'norek', 'required|exact_length[16]|numeric');                
                $this->form_validation->set_rules('jumlah', 'jumlah', 'required|numeric');
                $this->form_validation->set_rules('payment', 'payment', 'required');

                if($payment == 'BANK'){
                    $this->form_validation->set_rules('kode_bank_tujuan', 'kode_bank_tujuan', 'required|numeric');
                    $this->form_validation->set_rules('nama_nasabah_bank', 'nama_nasabah_bank', 'required');
                    $this->form_validation->set_rules('norek_bank', 'norek_bank', 'required|numeric');
                }

                if($payment == 'GCASH'){
                    $this->form_validation->set_rules('va','va','required|numeric');
                }
                
                if ($this->form_validation->run() == FALSE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ));
                } else {
                    
                    $jumlah = $this->post('jumlah');
                    $norek = $this->post('norek');                    
                    $kodeBankTujuan = $this->post('kode_bank_tujuan');
                    $namaNasabahBank = $this->post('nama_nasabah_bank');
                    $norekBank = $this->post('norek_bank');
                    $va = $this->post('va');
                    
                    $hargaBeli = null;
                    
                    //Dapatkan harga beli emas
                    $hargaEmas = $this->hargaEmas($token->channelId);            
                    if($hargaEmas->responseCode == '00'){
                        $dataHarga = json_decode($hargaEmas->data);                        
                        $hargaBeli = $dataHarga->hargaBeli;
                    }else{
                        $this->set_response(array(
                            'status' => "error",
                            'message' => "Tidak dapat mengambil data",
                            'code' => 203,
                            'reason' => $hargaEmas
                        ), 401);
                        return;
                    }
                    
                    
                    $dataInquiry = array(
                        "channelId" => "6017",
                        "gramTransaksi" => $jumlah,
                        "jenisTransaksi" => "BB",
                        "nilaiTransaksi" => $jumlah * $hargaBeli * 100,
                        "norek" => $norek,
                    );
                    
                    if($payment == 'WALLET' || $payment == 'GCASH'){
                        //Pastikan sudah punya wallet
                        if($user->norek == ''){
                            $this->set_response(array(
                                'status' => 'error',
                                'messsage' => 'Anda harus memiliki wallet untuk melanjutkan transaksi',
                                'code' => 103
                            ),200);
                            return;
                        }

                        $dataInquiry["norekWallet"] = $user->norek;                        
                        $dataInquiry["paymentMethod"] = "WALLET";
                        $dataInquiry["walletId"] = $user->no_hp;  

                        if($payment == 'WALLET'){
                            $dataInquiry["norekWalletTujuan"] = $user->norek;
                            $dataInquiry["tipeTransaksi"] = "1";
                        } else if($payment == 'GCASH'){
                            $dataInquiry["tipeTransaksi"] = "3";
                            $dataInquiry["gcashIdTujuan"] = $va;
                        }
                        
                    }else if($payment == 'BANK'){
                        $dataInquiry["norekWalletTujuan"] = $user->norek;
                        $dataInquiry["kodeBankTujuan"] = $kodeBankTujuan;
                        $dataInquiry["namaBankTujuan"] = $namaNasabahBank;
                        $dataInquiry["norekBankTujuan"] = $norekBank;
                        $dataInquiry["paymentMethod"] = "BANK";
                        $dataInquiry["tipeTransaksi"] = "2";
                    }
                    
                    $inquiry = $this->inquiryBuyBackEmas($dataInquiry);

                    if ($inquiry->responseCode == '00') {

                        if (!isset($inquiry->data)) {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                                'reason' => $inquiry,
                                'code' => 103
                                    ), 200);
                            return;
                        }

                        $resData = json_decode($inquiry->data);
                        $resData->id_transaksi = $resData->reffSwitching;

                        $saveData = (array) $resData;

                        $saveData['jenis_transaksi'] = 'BB';
                        $saveData['user_AIID'] = $token->id;
                        $saveData['id_transaksi'] = $resData->reffSwitching;
                        $saveData['kode_produk'] = '62';
                        $saveData['jenis_transaksi'] = 'BB';
                        $saveData["payment"] = $payment;                        
                        $saveData["tipe"] = $payment;
                        $saveData['gcashIdTujuan'] = $va; 

                        $this->PaymentModel->add($saveData);

                        $this->set_response(array(
                            'status' => 'success',
                            'message' => '',
                            'data' => $resData
                                ), 200);
                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                            'reason' => $inquiry,
                            'code' => 103
                                ), 200);
                    }                    
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
   
    function cetak_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if ($token) {

            $user = $this->User->getUser($token->id);
            if($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2' ){
                $this->set_response(array(
                    'status' => 'error',
                    'message' => $this->getAktifasiFinansialMsg($user->aktifasiTransFinansial),
                    'code' => 201
                ), 200);   
                return;
            }

            if ($method == 'payment')
            {
                
                $setData = array(
                    'id_transaksi' => $this->post('id_transaksi'),
                    'payment' => $this->post('payment'),
                    'booking_code' => $this->post('booking_code'),
                    'card_number' => $this->post('card_number'),
                    'token_response' => $this->post('token_response'),
                    'pin' => $this->post('pin'),
                    'va' => $this->post('va'),
                ); 
           
                $this->form_validation->set_data($setData);

                $this->form_validation->set_rules('id_transaksi','id_transaksi','required');
                $this->form_validation->set_rules('payment','payment','required');
                $this->form_validation->set_rules('pin','pin','required|exact_length[6]|numeric');

                $payment = $this->post('payment');

                if($payment == 'MANDIRI'){
                     $this->form_validation->set_rules('booking_code','booking_code','required');            
                     $this->form_validation->set_rules('card_number','card_number','required');            
                     $this->form_validation->set_rules('token_response','token_response','required');  
                }

                //Jika payment adalah wallet dan user belum akttifasi wallet
                if($payment == 'WALLET' && $user->norek == ''){
                    $this->set_response(array(
                       'status' => 'error',
                       'message' => 'Anda belum melakukan aktifasi wallet',
                       'code' => 101                 
                   ),200);
                   return;
                }

                if($payment == 'GCASH'){
                    $this->form_validation->set_rules('va','va','required|numeric');
                }

                if($this->form_validation->run() == FALSE){
                    
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input',
                        'code' => 101,
                        'errors' => $this->form_validation->error_array()
                    ),200);
                    
                }else{
                
                    $idTransaksi = $this->post('id_transaksi');
                    $pin = $this->post('pin');
                    $promo_code = $this->post('promo_code');
                                 
                    //Check user pin                                 
                    if(!$this->User->isValidPIN2($token->id, $pin)){
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'PIN tidak valid',
                            'code' => 102
                        ),200);
                        return;
                    }
               
                    //Mendapatkan payment berdasarkan id transaksi
                    $checkPayment = $this->PaymentModel->getPaymentByTrxId($idTransaksi);
                    
                    $idPromosi = $checkPayment->idPromosi;
                    $discountAmount = $checkPayment->discountAmount;

                    if($checkPayment){
                        if ($payment == 'BNI') 
                        {
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD','BANK','62','009');
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD','BANK','62','009');
                            $createBillingBNI = $this->createBillingVABNI(
                                    $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount, 
                                    $user->email, 
                                    $token->nama, 
                                    $token->no_hp, 
                                    'K', 
                                    $checkPayment->jenisTransaksi,
                                    "PDSOD " . $idTransaksi . " " . $checkPayment->norek, 
                                    $checkPayment->norek, 
                                    '62', 
                                    $idTransaksi, 
                                    $token->channelId,
                                    $idPromosi,
                                    $discountAmount
                                );

                            if ($createBillingBNI->responseCode == '00') {

                                if (!isset($createBillingBNI->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBillingBNI
                                            ), 200);
                                    return;
                                }

                                $billingData = json_decode($createBillingBNI->data);

                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay,
                                    'kodeBankPembayar' => '009'
                                );

                                //Update payment                               
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $template = $this->generateCetakEmasNotif($idTransaksi);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                        $token->id, 
                                        NotificationModel::TYPE_EMAS, 
                                        NotificationModel::CONTENT_TYPE_HTML, 
                                        $this->ConfigModel->getNamaProduk('OD','62'),
                                        "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                        $mobileTemplate, 
                                        $minimalTemplate,
                                        "OD"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                        $this->User->getFCMToken($token->id)
                                        , [
                                    "id" => $notifId,
                                    "tipe" => "OD",
                                    "title" => $this->ConfigModel->getNamaProduk('OD','62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                    "token" => $token->no_hp        
                                ]);

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmail($user->email, 'Order Cetak Tabungan Emas', $emailTemplate);  

                                //get saldo terakhir            
                                $saldo = $this->getSaldo($checkPayment->norek);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir'] 
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:' . json_encode($createBillingBNI));
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBillingBNI
                                        ), 200);
                            }
                            
                        }
                        else if($payment == 'MANDIRI')
                        {

                                 $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD','BANK','62','008');
                                 $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD','BANK','62','008');

                                 $bookingCode = $this->post('booking_code');
                                 $cardNumber = $this->post('card_number');
                                 $tokenResponse = $this->post('token_response');

                                 //Bayar dengan mandiri click pay
                                 $clickPay = $this->mandiriClickPay(
                                     $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount, 
                                     $bookingCode, 
                                     $cardNumber, 
                                     $checkPayment->jenisTransaksi, 
                                     "PDSOD " . $idTransaksi . " " . $checkPayment->norek, 
                                     $user->no_hp, 
                                     $checkPayment->norek, 
                                     '62', 
                                     $tokenResponse, 
                                     $idTransaksi, 
                                     $token->channelId,
                                     $idPromosi,
                                     $discountAmount
                                 );

                                 if($clickPay->responseCode == '00'){

                                     $clickpayData = json_decode($clickPay->data);

                                     $updateData['bookingCode'] = $bookingCode;
                                     $updateData['cardNumber'] = $cardNumber;
                                     $updateData['tokenResponse'] = $tokenResponse;
                                     $updateData['reffBiller'] = $clickpayData->reffBiller;
                                     $updateData['kodeBankPembayar'] ='008';
                                     $updateData['payment'] = $payment;
                                     $updateData['biayaTransaksi'] = $biayaTransaksiDisplay;
                                     $updateData['is_paid'] = '1';

                                     //update data pembayaran
                                     $this->PaymentModel->update($idTransaksi, $updateData);

                                     //mendapatkan saldo terakhir
                                     $saldo = $this->getSaldo($checkPayment->norek);

                                     //Set response
                                     $this->set_response([
                                         'status' => 'success',
                                         'message' => 'Pembayaran Order Cetak Emas Berhasil',
                                         'data' => array(
                                            'reffBiller' => $clickpayData->reffBiller,
                                            'saldo' => $saldo['saldo'],
                                            'saldoAkhir' => $saldo['saldoAkhir'],
                                            'saldoBlokir' => $saldo['saldoBlokir']  
                                         )
                                     ], 200);                                

                                 }else{
                                     $this->set_response(array(
                                         'status' => 'error',
                                         'code' => 103,
                                         'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                         'reason' => $clickPay
                                     ));
                                 }
                        }
                        else if($payment == 'WALLET' || $payment == 'GCASH')
                        {

                                 $va = $this->post('va');                              

                                 $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD','WALLET','62','');
                                 $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD','WALLET','62','');

                                 log_message('debug', 'PAYMENT EMAS WALLET');
                                 
                                 $arrayOrder = json_decode($checkPayment->order, true);

                                 $paymentData = array(
                                     'administrasi' => $checkPayment->administrasi,                                     
                                     'jenisTransaksi' => $checkPayment->jenisTransaksi,
                                     'kodeCabang' => $checkPayment->kodeCabang,
                                     'noBuku' => $checkPayment->noBuku,
                                     'norek' => $checkPayment->norek,
                                     'order' => $arrayOrder,                                                                         
                                     'reffSwitching' => $checkPayment->id_transaksi,
                                     'surcharge' => $checkPayment->surcharge,
                                     'totalKewajiban' => $checkPayment->totalKewajiban - $discountAmount,                                
                                     'vendor' => $checkPayment->vendor,                            
                                     'channelId' => $token->channelId,
                                     'idPromosi' => $idPromosi,
                                     'discountAmount' => $discountAmount 
                                 );

                                 if($payment == 'WALLET'){
                                    $paymentData['paymentMethod'] = 'WALLET';
                                    $paymentData['norekWallet'] = $user->norek;
                                    $paymentData['walletId'] = $user->no_hp; 
                                 } else if($payment == 'GCASH'){
                                    $paymentData['paymentMethod'] = 'GCASH';
                                    $paymentData['gcashId'] = $va;
                                 }

                                 $walletPayment = $this->paymentCetakEmas($paymentData);

                                 if($walletPayment->responseCode == '00'){

                                     $walletData = json_decode($walletPayment->data, TRUE);

                                     $walletData['payment'] = $payment;
                                     $walletData['is_paid'] = '1';
                                     $walletData['biayaTransaksi'] = $biayaTransaksiDisplay;
                                     $walletData['order'] = json_encode($arrayOrder);

                                     //Update wallet data
                                     $this->PaymentModel->update($checkPayment->id_transaksi, $walletData);

                                     $template = $this->generateCetakEmasNotif($idTransaksi);

                                     $mobileTemplate = $template['mobile'];
                                     $emailTemplate = $template['email'];
                                     $minimalTemplate = $template['minimal'];

                                     //Simpan notifikasi baru
                                     $notifId = $this->NotificationModel->add(
                                             $token->id, NotificationModel::TYPE_EMAS, 
                                             NotificationModel::CONTENT_TYPE_HTML, 
                                             $this->ConfigModel->getNamaProduk('OD','62'),
                                             "Order Cetak Emas Barhasil",
                                             $mobileTemplate,
                                             $minimalTemplate,
                                             "OD"
                                     );

                                     //Kirim notifikasi pembayaran ke device user
                                     Message::sendFCMNotif(
                                             $this->User->getFCMToken($token->id)
                                             , [
                                         "id" => $notifId,
                                         "tipe" => "OD",
                                         "title" => $this->ConfigModel->getNamaProduk('OD','62'),
                                         "tagline" => "Order Cetak Emas Berhasil",
                                         "content" => "Order Cetak Emas Berhasil",
                                         "paymentType" => $payment,
                                         "token" => $token->no_hp
                                     ]);

                                     //Kirim Email Notifikasi
                                     $this->load->helper('message');
                                     Message::sendEmail(
                                             $token->email, 'Order Cetak Tabungan Emas', $emailTemplate
                                     );

                                    
                                     //Update Saldo Wallet
                                     $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                     $walletData['wallet'] = $saldoWallet;

                                     //get saldo
                                     $saldo = $this->getSaldo($checkPayment->getSaldo($checkPayment->norek));

                                     $walletData['saldo'] = $saldo['saldo'];
                                     $walletData['saldoAkhir'] = $saldo['saldoAkhir'];
                                     $walletData['saldoBlokir'] = $saldo['saldoBlokir']; 
                                     
                                    
                                     //Set response
                                     $this->set_response([
                                         'status' => 'success',
                                         'message' => 'Order Cetak Emas Berhasil',
                                         'data' => $walletData
                                     ], 200);

                                 }else{
                                     $this->set_response(array(
                                         'status' => 'error',
                                         'message' => '',
                                         'code' => 103,
                                         'reason' => $walletPayment                                    
                                     ),200);
                                 }
                        }
                        else if ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI')
                        {
                                $kodeBank = '';
                                if($payment == 'VA_BCA'){
                                    $kodeBank = '014';
                                } else if($payment == 'VA_MANDIRI'){
                                    $kodeBank = '008';
                                } else if($payment == 'VA_BRI'){
                                    $kodeBank = '002';
                                } 
                                $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD','BANK','62',$kodeBank);
                                $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD','BANK','62',$kodeBank);
                                $createBillingData = [
                                    'channelId' => $token->channelId,
                                    'amount' => $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount, 
                                    'customerEmail' => $user->email,
                                    'customerName' => $token->nama,
                                    'customerPhone' => $token->no_hp,
                                    'flag' => 'K',
                                    'jenisTransaksi' => $checkPayment->jenisTransaksi,
                                    'kodeProduk' => '62',
                                    'norek' => $checkPayment->no_rekening,
                                    'keterangan' => "PDSOD " . $idTransaksi . " " . $checkPayment->norek, 
                                    'reffSwitching' => $idTransaksi,
                                    'kodeBank' => $kodeBank,
                                    'idPromosi' => $idPromosi,
                                    'discountAmount' => $discountAmount
                                ];

                                $createBilling = $this->createBillingPegadaian($createBillingData);

                                if ($createBilling->responseCode == '00') {

                                    if (!isset($createBilling->data)) {
                                        $this->set_response(array(
                                            'status' => 'error',
                                            'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                            'code' => 103,
                                            'reason' => $createBilling
                                                ), 200);
                                        return;
                                    }

                                    $billingData = json_decode($createBilling->data);

                                    $virtualAccount = $billingData->vaNumber;
                                    $tglExpired = $billingData->tglExpired;

                                    $updatePaymentData = array(
                                        'tipe' => $payment,
                                        'virtual_account' => $virtualAccount,
                                        'tanggal_expired' => $tglExpired,
                                        'payment' => $payment,
                                        'biayaTransaksi' => $biayaTransaksiDisplay,
                                        'kodeBankPembayar' => $kodeBank
                                    );

                                    //Update payment                               
                                    $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                    $template = $this->generateCetakEmasNotif($idTransaksi);

                                    $mobileTemplate = $template['mobile'];
                                    $emailTemplate = $template['email'];
                                    $minimalTemplate = $template['minimal'];

                                    //Simpan notifikasi baru
                                    $notifId = $this->NotificationModel->add(
                                            $token->id, 
                                            NotificationModel::TYPE_EMAS, 
                                            NotificationModel::CONTENT_TYPE_HTML, 
                                            $this->ConfigModel->getNamaProduk('OD','62'),
                                            "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".") . ' ke ' . $virtualAccount,
                                            $mobileTemplate, 
                                            $minimalTemplate,
                                            "OD"
                                    );

                                    //Kirim notifikasi pembayaran ke device user
                                    Message::sendFCMNotif(
                                            $this->User->getFCMToken($token->id)
                                            , [
                                        "id" => $notifId,
                                        "tipe" => "OD",
                                        "title" => $this->ConfigModel->getNamaProduk('OD','62'),
                                        "tagline" => "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".") . ' ke ' . $virtualAccount,
                                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                        "token" => $token->no_hp        
                                    ]);

                                    //Kirim Email Notifikasi
                                    $this->load->helper('message');
                                    Message::sendEmail($user->email, 'Order Cetak Tabungan Emas', $emailTemplate);  

                                    //get saldo terakhir            
                                    $saldo = $this->getSaldo($checkPayment->norek);

                                    //Set response
                                    $this->set_response([
                                        'status' => 'success',
                                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                        'data' => array(
                                            'idTransaksi' => $idTransaksi,
                                            'virtualAccount' => $virtualAccount,
                                            'expired' => $tglExpired,
                                            'now' => date('Y-m-d H:i:s'),
                                            'saldo' => $saldo['saldo'],
                                            'saldoAkhir' => $saldo['saldoAkhir'],
                                            'saldoBlokir' => $saldo['saldoBlokir'] 
                                        )
                                            ], 200);
                                } else {
                                    log_message('debug', 'Create VA error. Reason:' . json_encode($createBilling));
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Mohon coba kembali beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBilling
                                            ), 200);
                                }
                            }
                            else if ($payment == 'VA_PERMATA')
                            {
                                $kodeBank = '013';
                                
                                $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD','BANK','62',$kodeBank);
                                $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD','BANK','62',$kodeBank);
                                $createBillingData = [
                                    'channelId' => $token->channelId,
                                    'amount' => $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount, 
                                    'customerEmail' => $user->email,
                                    'customerName' => $token->nama,
                                    'customerPhone' => $token->no_hp,
                                    'flag' => 'K',
                                    'jenisTransaksi' => $checkPayment->jenisTransaksi,
                                    'productCode' => '62',
                                    'norek' => $checkPayment->no_rekening,
                                    'keterangan' => "PDSOD " . $idTransaksi . " " . $checkPayment->norek, 
                                    'trxId' => $idTransaksi,
                                    'idPromosi' => $idPromosi,
                                    'discountAmount' => $discountAmount
                                ];
                                $createBilling = $this->createBillingPermata($createBillingData);

                                if ($createBilling->responseCode == '00') {

                                    if (!isset($createBilling->data)) {
                                        $this->set_response(array(
                                            'status' => 'error',
                                            'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                            'code' => 103,
                                            'reason' => $createBilling
                                                ), 200);
                                        return;
                                    }

                                    $billingData = json_decode($createBilling->data);

                                    $virtualAccount = $billingData->virtualAccount;
                                    $tglExpired = $billingData->tglExpired;

                                    $updatePaymentData = array(
                                        'tipe' => $payment,
                                        'virtual_account' => $virtualAccount,
                                        'tanggal_expired' => $tglExpired,
                                        'payment' => $payment,
                                        'biayaTransaksi' => $biayaTransaksiDisplay,
                                        'kodeBankPembayar' => $kodeBank
                                    );

                                    //Update payment                               
                                    $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                    $template = $this->generateCetakEmasNotif($idTransaksi);

                                    $mobileTemplate = $template['mobile'];
                                    $emailTemplate = $template['email'];
                                    $minimalTemplate = $template['minimal'];

                                    //Simpan notifikasi baru
                                    $notifId = $this->NotificationModel->add(
                                            $token->id, 
                                            NotificationModel::TYPE_EMAS, 
                                            NotificationModel::CONTENT_TYPE_HTML, 
                                            $this->ConfigModel->getNamaProduk('OD','62'),
                                            "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".") . ' ke ' . $virtualAccount,
                                            $mobileTemplate, 
                                            $minimalTemplate,
                                            "OD"
                                    );

                                    //Kirim notifikasi pembayaran ke device user
                                    Message::sendFCMNotif(
                                            $this->User->getFCMToken($token->id)
                                            , [
                                        "id" => $notifId,
                                        "tipe" => "OD",
                                        "title" => $this->ConfigModel->getNamaProduk('OD','62'),
                                        "tagline" => "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".") . ' ke ' . $virtualAccount,
                                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,                                    
                                        "token" => $token->no_hp        
                                    ]);

                                    //Kirim Email Notifikasi
                                    $this->load->helper('message');
                                    Message::sendEmail($user->email, 'Order Cetak Tabungan Emas', $emailTemplate);  

                                    //get saldo terakhir            
                                    $saldo = $this->getSaldo($checkPayment->norek);

                                    //Set response
                                    $this->set_response([
                                        'status' => 'success',
                                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                        'data' => array(
                                            'idTransaksi' => $idTransaksi,
                                            'virtualAccount' => $virtualAccount,
                                            'expired' => $tglExpired,
                                            'now' => date('Y-m-d H:i:s'),
                                            'saldo' => $saldo['saldo'],
                                            'saldoAkhir' => $saldo['saldoAkhir'],
                                            'saldoBlokir' => $saldo['saldoBlokir'] 
                                        )
                                            ], 200);
                                } else {
                                    log_message('debug', 'Create VA error. Reason:' . json_encode($createBilling));
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Mohon coba kembali beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBilling
                                            ), 200);
                                }
                            }
                         
                    }else{
                        log_message('debug', 'Payment not found'.$this->post('id_transaksi'));
                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'Payment not found',
                            'code' => 102
                        ),200);
                    }
                }
                
            }
            else if ($method == 'inquiry')
            {
                
                $setData = array(
                    'norek' => $this->post('norek'),
                    'vendor' => $this->post('vendor'),
                    'order' => $this->post('order'),
                    'kode_cabang' => $this->post('kode_cabang'),
                    'promo_code' => $this->post('promo_code')                    
                );
                
                $this->form_validation->set_data($setData);
                $this->form_validation->set_rules('norek', 'norek', 'required|exact_length[16]|numeric');                
                $this->form_validation->set_rules('vendor', 'vendor', 'required');
                $this->form_validation->set_rules('order', 'order', 'required');
                $this->form_validation->set_rules('kode_cabang', 'kode_cabang', 'required');

                
                if ($this->form_validation->run() == FALSE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ));
                } else {
                    
                    $norek = $this->post('norek');
                    $vendor = $this->post('vendor');
                    $order = $this->post('order');
                    $kodeCabang = $this->post('kode_cabang');
                    $promoCode = $this->post('promo_code');
                    
                    $arrayOrder = json_decode($order, true);
                    
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Invalid input order',
                            'code' => 101,
                        ),200);
                        return;
                    }
                    
                    //var_dump($arrayOrder);
                    // Find grand total biaya
                    $grandTotal = 0;
                    foreach ($arrayOrder as $ao) {
                        $grandTotal = $grandTotal + $ao['totalBiaya'];
                    }
                    
                    //Pertama cek apakah no rekening punya buku tabungan
                    $detailTabunganEmas = $this->detailTabunganEmas($norek, $token->channelId);
                    if($detailTabunganEmas->responseCode == '00'){
                        
                        if(!isset($detailTabunganEmas->data)){
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                'code' => 103,
                                'reason' => $detailTabunganEmas
                            ),200);
                            return;
                        }
                        
                        $dataDettabemas = json_decode($detailTabunganEmas->data);
                        if($dataDettabemas->status == "1"){
                            
                            //Transaksi bisa dilanjutkan
                            $dataInquiry = array(
                                'channelId' => $token->channelId,
                                'jenisTransaksi' => 'OD',
                                'kodeCabang' => $kodeCabang,
                                'noBuku' => $dataDettabemas->noBuku,
                                'norek' => $norek,
                                'order' => $arrayOrder,
                                'vendor' => $vendor
                            );

                            // Lakukan validasi promo/voucher gpoint jika ada
                            if($promoCode != null)
                            {

                                if($user->cif == null){
                                    return $this->set_response([
                                        'status' => 'error',
                                        'message' => 'Anda harus memiliki CIF untuk menggunakan fitur voucher',
                                        'code' => 102
                                    ]);
                                }

                                $validateData = [
                                    'promoCode' => $promoCode,
                                    'voucherId' => null,
                                    'userId' => $user->cif,
                                    'transactionAmount' => (int) $grandTotal,
                                    'validators' => [
                                        'channel' => $this->config->item('core_post_username'),
                                        'product' => '62',
                                        'transactionType' => 'OD',
                                        'unit' => 'rupiah',
                                    ]
                                ];

                                $validateVoucher = $this->gPointValidateVoucher($validateData);

                                if($validateVoucher->status == 'Error'){
                                    return $this->set_response([
                                        'status' => 'error',
                                        'message' => $validateVoucher->message,
                                        'code' => 103,                                
                                    ]);
                                } else if($validateVoucher->status == 'Success') {
                                    $dataInquiry['discountAmount'] = $validateVoucher->data->discount;
                                    $dataInquiry['idPromosi'] = $validateVoucher->data->journalAccount.';'.$promoCode;
                                }
                            }
                            
                            $inquiryCetakEmas = $this->inquiryCetakEmas($dataInquiry);
                            
                            if($inquiryCetakEmas->responseCode == '00'){
                                
                                if(!isset($inquiryCetakEmas->data)){
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $inquiryCetakEmas
                                    ),200);                                    
                                    return;                                   
                                }
                                
                                
                                $inquiryData = json_decode($inquiryCetakEmas->data);
                                
                                $saveData = (array) $inquiryData;
                                $saveData['user_AIID'] = $token->id;
                                $saveData['id_transaksi'] = $inquiryData->reffSwitching;
                                $saveData['order'] = json_encode($inquiryData->order);
                                $saveData['kodeCabang'] = $kodeCabang;
                                $saveData['vendor'] = $vendor;
                                $saveData['jenis_transaksi'] = 'OD';
                                $saveData['kode_produk'] = '62';

                                if(isset($dataInquiry['discountAmount'], $dataInquiry['idPromosi']))
                                {
                                    $saveData['discountAmount'] = $dataInquiry['discountAmount'];
                                    $saveData['idPromosi'] = $dataInquiry['idPromosi'];    
                                }
                                                                
                                $this->PaymentModel->add($saveData);
                                
                                //Get biaya channel untuk masing-masing metode pembayaran
                                $biayaChannel = array(
                                    'BNI' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','009'),
                                    'MANDIRI' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','008'),
                                    'WALLET' => $this->ConfigModel->getBiayaPayment('SL','WALLET','62',''),
                                    'VA_MANDIRI' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','008'),
                                    'VA_BCA' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','014'),
                                    'VA_PERMATA' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','013'),
                                    'VA_BRI' => $this->ConfigModel->getBiayaPayment('SL','BANK','62','002')
                                );
                                
                                $saveData['biayaChannel'] = $biayaChannel;
                        
                                //Set response
                                $this->set_response(array(
                                    'status' => 'success',
                                    'message' => '',
                                    'data' => $saveData
                                ),200);
                                
                                
                            }else{
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $inquiryCetakEmas
                                ),200);
                            }
                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Transaksi tidak bisa dilanjutkan. Anda harus memiliki buku tabungan. Hubungi cabang tempat Anda buka rekening tabungan emas.',
                                'code' => 102,
                                'reason' => $detailTabunganEmas
                            ),200);
                        }                        
                    }else{
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                            'code' => 103,
                            'reason' => $detailTabunganEmas
                        ),200);
                    }                    
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }



    function gte_up_post(){
        $token = $this->getToken();
        
        if($token){

            $setData = array(
                'waktu' => $this->post('waktu'),
                'gram' => $this->post('gram'),
                'rupiah' => $this->post('rupiah')                                    
            );
            
            $this->form_validation->set_data($setData);                
            $this->form_validation->set_rules('waktu', 'waktu', 'required|integer');
            $this->form_validation->set_rules('gram', 'gram', 'required');
            $this->form_validation->set_rules('rupiah', 'rupiah', 'required|integer');

            
            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 101,
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ));
            } else {
                //Mendapatkan harga emas yang berlaku saat ini
                $stl = $this->getHargaSTL($token->channelId);

                if($stl->responseCode == '00'){

                    if(isset($stl->data) && $stl->data != null){

                        $stlData = json_decode($stl->data);

                        $hargaEmas = $stlData->hargaEmas;

                        log_message("debug","HARGA".$hargaEmas);

                        $waktu = $this->post('waktu');
                        $gram = $this->post('gram');
                        $rupiah = $this->post('rupiah');

                        $rupiahAsli = $rupiah;

                        //Mendapatkan rasio taksiran
                        $rasio = $this->EmasModel->getRasioTaksiran($waktu);


                        if($rasio){

                            if($gram != 0){
                                $rupiah = $gram * $rasio * $hargaEmas;  
                            }else if($rupiah != 0){
                                $gram = $this->round_up( ($rupiah / ( $rasio * $hargaEmas )), 4, PHP_ROUND_HALF_UP ) ;
                                log_message('debug', 'HARGA gram '.$gram);
                            }

                            //Bulatkan uang pinjaman
                            $golongan = $this->EmasModel->getGolonganKCA($rupiah);

                            if($golongan){
                                $pembulatan = $golongan->pembulatan;

                                $rupiah = (int) ($pembulatan * floor($rupiah / $pembulatan));

                                $this->set_response(array(
                                    'status' => 'success',
                                    'message' => '',
                                    'data' => array(
                                        'gram' => $gram,
                                        'rupiah' => $rupiah,
                                        'rupiahAsli' => (int) $rupiahAsli,
                                        'golongan' => $golongan->golongan
                                    )
                                ),200);    
                            }else{
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Jumlah pinjaman harus lebih tinggi',
                                    'code' => 102
                                ),200);        
                            }
                                
                        }else{
                            $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Masukan hari tenor yang valid',
                                    'code' => 102
                                ),200); 
                        }

                    }else{
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                            'code' => 103,
                            'reason' => $stl
                        ),200);    
                    }

                }else{
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                        'code' => 103,
                        'reason' => $stl
                    ),200);
                }    
            }
        }else{
            $this->errorUnAuthorized();
        }

    }

    function round_up ($value, $places=0) {
        if ($places < 0) { $places = 0; }
        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }

    function gadai_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if ($token) 
        {
            $user = $this->User->getUser($token->id);
            if($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2' ){
                $this->set_response([
                    'status' => 'error',
                    'message' => $this->getAktifasiFinansialMsg($user->aktifasiTransFinansial),
                    'code' => 201
                ], 200);   
                return;
            }            

            if ($method == 'payment')
            {
                
                $setData = [
                    'id_transaksi' => $this->post('id_transaksi'),
                    'pin' => $this->post('pin')
                ]; 
           
                $this->form_validation->set_data($setData);

                $this->form_validation->set_rules('id_transaksi','id_transaksi','required');
                $this->form_validation->set_rules('pin','pin','required|numeric');

                //Jika payment adalah wallet dan user belum akttifasi wallet
                if($user->norek == ''){
                    $this->set_response(array(
                       'status' => 'error',
                       'message' => 'Anda belum melakukan aktifasi transaksi finansial',
                       'code' => 101                 
                   ),200);
                   return;
                }

                if($this->form_validation->run() == FALSE){
                    
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Invalid input',
                        'code' => 101,
                        'errors' => $this->form_validation->error_array()
                    ], 200);
                    
                }else{
                
                    $idTransaksi = $this->post('id_transaksi');
               
                    //Mendapatkan payment berdasarkan id transaksi
                    $checkPayment = $this->EmasModel->getGTE($idTransaksi);

                    if($checkPayment){

                        $pin = $this->post('pin');
                         
                        //Check user pin                                 
                        if(!$this->User->isValidPIN2($token->id, $pin)){
                             $this->set_response([
                                 'status' => 'error',
                                 'message' => 'PIN tidak valid',
                                 'code' => 102
                             ], 200);
                             return;
                        }
                         
                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP','WALLET','32','');
                        
                        $paymentData = [
                            "channelId" => $token->channelId,
                            "cif" => $user->cif,
                            "gram" => $checkPayment->gram,
                            "jenisTransaksi" => "OP",
                            "noKredit" => $checkPayment->noKredit,
                            "norekBankTujuan" => $checkPayment->norekBankTujuan,
                            "tenor" => $checkPayment->tenor,
                            "tipeTransaksi" =>  $checkPayment->tipeTransaksi,
                            "up" => $checkPayment->up,
                            "administrasi" => $checkPayment->administrasi,
                            "surcharge" => $checkPayment->surcharge,
                            "totalKewajiban" => $checkPayment->totalKewajiban,
                            "paymentMethod" => 'WALLET',
                            "walletId" => $user->no_hp,
                            "norekWallet" => $user->norek, 
                            "reffSwitching" => $checkPayment->reffSwitching,
                            "reffBiller" => $checkPayment->id,
                            "requestType" => $checkPayment->requestType,
                            "productCode" => $checkPayment->productCode,
                            'jenisPromo' => $checkPayment->jenisPromo
                        ];
                        
                        // Get promo (if Any)
                        $gpoin = $this->GpoinModel->get_promo($checkPayment->reffSwitching);
                        $idPromosi = '';
                        $discountAmount = 0;
                        $promoAmount = 0;
                        if ($gpoin != '') {
                            if ($gpoin->type == 'discount') {
                                $discountAmount = $gpoin->value;
                            }
                            $promoAmount = $gpoin->value;
                            $idPromosi = $gpoin->idPromosi;
                        };
                        $paymentData['idPromosi'] = $idPromosi;
                        $paymentData['discountAmount'] = $promoAmount;

                        if($checkPayment->gcashIdTujuan != null){
                            // Pembayaran dengan GCASH                            
                            $paymentData['gcashIdTujuan'] = $checkPayment->gcashIdTujuan;

                        }
                        else
                        {
                            // Pembayaran dengan WALLET
                            $paymentData["kodeBankTujuan"] = $checkPayment->kodeBankTujuan;
                            $paymentData["namaBankTujuan"] = $checkPayment->namaBankTujuan;                            
                        }

                         $walletPayment = $this->openGTE($paymentData);

                         if($walletPayment->responseCode == '00'){

                            $walletData = json_decode($walletPayment->data, TRUE);

                            $walletData['isPaid'] = '1';
                            $walletData['tenor'] = filter_var($walletData['tenor'], FILTER_SANITIZE_NUMBER_INT);

                            $this->EmasModel->updateGTE($idTransaksi, $walletData);

                            $walletData['nama'] = $user->nama;
                            $walletData['gram'] = $checkPayment->gram;

                            $saldo = $this->getSaldo($checkPayment->noKredit);
                            $walletData['saldo'] = $saldo['saldo'];
                            $walletData['noKredit'] = $checkPayment->noKredit;
                            $walletData['gcashIdTujuan'] = $checkPayment->gcashIdTujuan;


                             $template = $this->generateGTENotif($walletData);

                             $mobileTemplate = $template['mobile'];
                             $emailTemplate = $template['email'];
                             $minimalTemplate = $template['minimal'];

                             //Simpan notifikasi baru
                             $notifId = $this->NotificationModel->add(
                                     $token->id, NotificationModel::TYPE_GADAI, 
                                     NotificationModel::CONTENT_TYPE_HTML, 
                                     $this->ConfigModel->getNamaProduk('OP','32'), 
                                     "Gadai Tabungan Emas",
                                     $mobileTemplate,
                                     $minimalTemplate,
                                     "OP"
                             );

                             //Kirim notifikasi pembayaran ke device user
                             Message::sendFCMNotif(
                                     $this->User->getFCMToken($token->id)
                                     , [
                                 "id" => $notifId,
                                 "tipe" => "GT",
                                 "title" => $this->ConfigModel->getNamaProduk('OP','32'),
                                 "tagline" => "Gadai Tabungan Emas Berhasil",
                                 "content" => "Gadai Tabungan Emas Berhasil",
                                 "paymentType" => 'WALLET',
                                 "token" => $token->no_hp
                             ]);

                             //Kirim Email Notifikasi
                             $this->load->helper('message');
                             Message::sendEmail(
                                     $token->email, 'Gadai Tabungan Emas', $emailTemplate
                             );

                             //Update Saldo Wallet
                             $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                             $walletData['wallet'] = $saldoWallet;

                             $walletData['saldoBlokir'] = $saldo['saldoBlokir'];
                             $walletData['saldoAkhir'] = $saldo['saldoAkhir'];


                             //Set response
                             $this->set_response([
                                 'status' => 'success',
                                 'message' => 'Gadai  Tabungan Emas Berhasil',
                                 'data' => $walletData
                             ], 200);

                         }else{
                             $this->set_response(array(
                                 'status' => 'error',
                                 'message' => '',
                                 'code' => 103,
                                 'reason' => $walletPayment                                    
                             ),200);
                         }
                     
                         
                    }else{
                        log_message('debug', 'Payment not found'.$this->post('id_transaksi'));
                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'Payment not found',
                            'code' => 102
                        ),200);
                    }
                }
                
            } else if ($method == 'inquiry') {
                
                $setData = array(
                    'payment' => $this->post('payment'),
                    'norek' => $this->post('norek'),
                    'gram' => $this->post('gram'),
                    'id_bank' => $this->post('id_bank'),
                    'tenor' => $this->post('tenor'),
                    'up' => $this->post('up'),
                    'tipe' => $this->post('tipe'),
                    'va' => $this->post('va')
                );
                
                $this->form_validation->set_data($setData);
                $this->form_validation->set_rules('norek', 'norek', 'required|exact_length[16]|numeric');                
                $this->form_validation->set_rules('gram', 'vendor', 'required');                
                $this->form_validation->set_rules('tenor', 'tenor', 'required|integer');
                $this->form_validation->set_rules('up', 'up', 'required|integer');
                $this->form_validation->set_rules('tipe', 'tipe', 'required|integer');
                
                if($this->post('payment') != 'GCASH'){
                    $this->form_validation->set_rules('id_bank', 'id_bank', 'required|integer');
                }

                if($this->post('payment') == 'GCASH'){
                    $this->form_validation->set_rules('va', 'va', 'required|numeric|required');   
                }
                
                if ($this->form_validation->run() == FALSE) {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ]);
                } else {
                    
                    $norek = $this->post('norek');
                    $gram = $this->post('gram');
                    $id_bank = $this->post('id_bank');
                    $tenor = $this->post('tenor');
                    $up = $this->post('up');
                    $tipe = $this->post('tipe');
                    $productCode = $this->post('product_code') == null ? '32' : $this->post('product_code');
                    $jenisPromo = $this->post('jenis_promo');
                    $va = $this->post('va');
                    $payment = $this->post('payment');

                    $dataInquiry = [
                        'channelId' => $token->channelId,
                        'cif' => $user->cif,
                        'gram' => $gram,
                        'jenisTransaksi' => 'OP',                        
                        'noKredit' => $norek,
                        'tenor' => $tenor,
                        'up' => $up,                        
                        'requestType' => $tipe,
                        'productCode' => $productCode,                      
                    ];

                    if($payment == null || $payment == 'BANK'){

                        //mendapatkan detail bank sesuai id bank
                        $userBank = $this->BankModel->getById($id_bank);
                        
                        if(!$userBank){
                            $this->set_response([
                                'status' => 'error',
                                'message' => 'Rekening bank tidak valid',
                                'code' => 101
                            ], 200);
                            return;
                        }

                        $dataInquiry['tipeTransaksi'] = '2';
                        $dataInquiry['kodeBankTujuan'] = $userBank->kode_bank;                        
                        $dataInquiry['namaBankTujuan'] = $userBank->nama_pemilik;
                        $dataInquiry['norekBankTujuan'] = $userBank->nomor_rekening;
                    
                    }else if($payment == 'GCASH'){
                        // CEK USER WALLET                        
                        $dataInquiry['tipeTransaksi'] = '3';
                        $dataInquiry['gcashIdTujuan'] = $va;
                    }
                    
                    
                    if ($jenisPromo != null){
                        $dataInquiry['jenisPromo'] = strtoupper($jenisPromo);
                    }

                    $inquiryGTE = $this->inquiryGTE($dataInquiry);

                    if($inquiryGTE->responseCode == '00'){

                        if(!isset($inquiryGTE->data)){
                            $this->set_response([
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                'code' => 103,
                                'reason' => $inquiryGTE
                            ],200);
                            return;
                        }

                        $saveData = json_decode($inquiryGTE->data, true);
                        $saveData['user_AIID'] = $token->id;
                        $saveData['requestType'] = $tipe;
                        $saveData['productCode'] = $productCode;
                        $saveData['jenisPromo'] = $jenisPromo;

                        $this->EmasModel->saveGTE($saveData);

                        $this->set_response([
                            'status' => 'success',
                            'message' => '',
                            'data' => $saveData
                        ], 200);


                    }else{
                        $this->set_response([
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                            'code' => 103,
                            'reason' => $inquiryGTE
                        ],200);
                    }
                                        
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
        
    /**
     * Endpoint untuk mendapatkan biaya 
     */
    function minimal_transaksi_get($param = 'saldo'){
        
        $token = $this->getToken();
        
        if($token){
            $minBeli = 0.001;
            $minSaldo = 0.01;
            $minJual = 1;
            $minCetak = 1;
            $minTransfer = 1;
            
            $data['channelId'] = $token->channelId;
            $data['flag'] = "K";
            if($param == 'saldo'){
                $data['key'] = 'EMAS.MINIMAL.SALDO';
            }else if($param == 'beli'){
                $data['key'] = '62.1.MIN.SALE';
            }else if($param == 'jual'){
                $data['key'] = '62.1.MIN.BUY';
            }else if($param == 'transfer'){
                $data['key'] = '62.1.MIN.TRANSFER';
            }else if($param == 'cetak'){
                $data['key'] = '62.1.MIN.ORDER';
            }else{
                 $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Parameter tidak cocok',
                    'code' => 102
                        ), 200);
                return;
            }
            
            $inquiry = $this->systemParameter($data);
            if($inquiry->responseCode == '00'){
                if (!isset($inquiry->data)) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                        'code' => 103,
                        'reason' => $inquiry
                            ), 200);
                    return;
                }
                
                $inquiryData = json_decode($inquiry->data);
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => number_format($inquiryData->value, 4, ".", ",")
                ),200);
                
            }else{
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                    'code' => 103,
                    'reason' => $inquiry
                ),200);
            }            
        }else{
            $this->errorUnAuthorized();
        }
        
    }

    function simulasi_emas_post(){
        log_message('debug', 'response ' . __FUNCTION__ .' Start ');
        $token = $this->getToken();
        if (!$token){
            $this->errorUnAuthorized();
            log_message('debug', 'response ' . __FUNCTION__ .'UnAuthorized');
        }

        $this->form_validation->set_rules('amount', 'amount', 'required');
        $this->form_validation->set_rules('gram', 'gram', 'required');
        $this->form_validation->set_rules('norek', 'norek', 'required');

        if (!$this->form_validation->run()){
            $error = "";
            foreach ($this->form_validation->error_array() as $value){
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101
            ]);
            log_message('debug', 'response ' . __FUNCTION__ .' Message : '.$error);
        }
        $data = $this->post();

        //get productCode
        $productCode = $this->ProductMasterModel->get_id_product('TABUNGAN EMAS');
        $data['productCode'] = $productCode->productCode;

        //getCif
        $data['cif'] = $this->User->getUserCif($token->id);

        $data['jenisTransaksi'] = "OP";

        if ($data['gram'] != "0")
            $data['tipe'] = "1";
        else if($data['amount'] != "0")
            $data['tipe'] = "2";

        $req = $this->simulasiTE($data);
        if ($req->responseCode == "00") {
            $this->set_response([
                'response_code' => $req->responseCode,
                'status' => 'success',
                'message' => $req->responseDesc,
                'data' => json_decode($req->data)
            ], 200);
            log_message('debug', 'response ' . __FUNCTION__ .' Message : '.json_encode($req->responseDesc));
        } else {
            $this->set_response([
                'response_code' => $req->responseCode,
                'status' => 'error',
                'message' => $req->responseDesc,
                'data' => !empty($req->data) ? json_decode($req->data) : ''
            ], 201);
            log_message('debug', 'response ' . __FUNCTION__ .' Message : '.json_encode($req->responseDesc));
        }
        log_message('debug', 'response ' . __FUNCTION__ .' End ');

    }
    
    function generateBuybackNotif($trxId)
    {
        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        
        if($checkPayment['payment'] == 'BANK'){
            $this->load->model('BankModel');
            $checkPayment['namaBank'] = $this->BankModel->getNamaBank($checkPayment['kodeBankTujuan']);
        }
        
        $subject = "Jual Tabungan Emas ".$checkPayment['norek']." Berhasil";              
                
        $content = $this->load->view('mail/notif_buybackemas', $checkPayment, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function generateGTENotif($checkPayment)
    {
        
        $subject = "Gadai Tabungan Emas ".$checkPayment['norek']." Berhasil";
        
        if($checkPayment['gcashIdTujuan'] == null){
            $this->load->model('BankModel');
            $namaBank = $this->BankModel->getNamaBank($checkPayment['kodeBankTujuan']);
            $checkPayment['namaBank'] = $namaBank;    
        }                      
                
        $content = $this->load->view('mail/notif_gte', $checkPayment, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    function generateCetakEmasNotif($trxId)
    {
        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        
        $subject = "";
        
        if(
                $checkPayment['payment'] == 'BNI' || 
                $checkPayment['payment'] == 'VA_BCA' || 
                $checkPayment['payment'] == 'VA_MANDIRI' ||
                $checkPayment['payment'] == 'VA_BRI'
        ){
            $subject = "Konfirmasi Order Cetak Tabungan Emas Pegadaian ".$checkPayment['norek'];
        }else if($checkPayment['payment'] == 'WALLET' || $checkPayment['payment'] == 'GCASH'){
            $subject = "Selamat Order Cetak Tabungan Emas Berhasil";
            $this->load->model('MasterModel');
            $cabang = $this->MasterModel->getSingleCabang($checkPayment['kodeCabang']);            
            $checkPayment['namaCabang'] = $cabang->nama;
            $checkPayment['alamatCabang'] = $cabang->alamat.', '.$cabang->kelurahan.', '.$cabang->kecamatan.', '.$cabang->kabupaten.', '.$cabang->provinsi;
            $checkPayment['noTelpCabang'] = $cabang->telepon;
        }
        
        $content = $this->load->view('mail/notif_cetakemas', $checkPayment, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    
    
    function generateTransferEmasNotif($trxId, $isRekTujuan = false, $nasabahTujuan = null)
    {
        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        $subject = "";
        
        if($checkPayment['payment'] == 'BNI'){
            $subject = "Konfirmasi Transfer Tabungan Emas Pegadaian ".$checkPayment['norek'];
        }else if($checkPayment['payment'] == 'WALLET'){
            $subject = "Selamat Transfer Keluar Tabungan Emas Berhasil";
        }
        
        $content = "";
        if($isRekTujuan){
            $subject = "Selamat Transfer Masuk Tabungan Emas Berhasil";
            $checkPayment['nasabahTujuan'] = $nasabahTujuan;
            $content = $this->load->view('mail/notif_transferemas_tujuan', $checkPayment, true);
        }else{
            $content = $this->load->view('mail/notif_transferemas', $checkPayment, true);
        }
        
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function getAktifasiFinansialMsg($status){
        if($status == '0'){
            return 'Mohon maaf, Anda tidak dapat melanjutkan transaksi. Status Transaksi Finansial: Tidak Aktif. Informasi lebih lanjut, silakan datang ke Kantor Pegadaian terdekat atau hubungi call center';

        }else if($status=='2'){
            return 'Mohon maaf, Anda tidak dapat melanjutkan transaksi. Status Transaksi Finansial: Blokir. Informasi lebih lanjut, silakan datang ke Kantor Pegadaian terdekat atau hubungi call center';
        }
    }
}
