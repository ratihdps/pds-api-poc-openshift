<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'CorePegadaian.php';

class Mulia extends CorePegadaian{
    public function __construct()
    {
        parent::__construct();        
        $this->load->model([
            'MuliaModel',
            'MasterModel',
            'User',
            'NotificationModel',
            'ConfigModel',
            'GpoinModel'
        ]);
        $this->load->library('form_validation');
        $this->load->helper('message');
    }

    function index_get($id = null)
    {
    	$token = $this->getToken();
    	if($token)
    	{
            $imageURL = $this->config->item('mulia_image_path');

            if($id != null)
            {
                $data = $this->MuliaModel->getProdukById($id);                
                $data = $this->urlizeImage($data);
                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $data
                ]);
            }
            else
            {
                $page = $this->query('page');
                $page == null ? $page=0 : $page;
                
                $search = null;            
                if($this->query('search') != null){
                    $search = $this->query("search");                
                }
                
                $category = null;
                if($this->query('category') != null){
                    $category = $this->query('category');
                }

                $produksi = null;
                if($this->query('produksi') != null){
                    $produksi = $this->query('produksi');
                }
                
                $offset = 20 * $page;
                $getProduk = $this->MuliaModel->getProduk(
                        $offset, 
                        $search, 
                        $category, 
                        $produksi);   

                $this->load->library('pagination');        
                            
                $config['base_url'] = base_url().'mulia/';
                $config['total_rows'] = $getProduk['totalRow'];
                $config['per_page'] = 20;
                $config["use_page_numbers"] = TRUE;
                $config["page_query_string"] = TRUE;
                $config['query_string_segment'] = 'page';                        
                $this->pagination->initialize($config);            

                $produks = $this->urlizeImage($getProduk['data']);
                
                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'totalData' => $getProduk['totalRow'],
                    'page' => $page,                
                    'data' => $produks
                ], 200);
            }
    	}
    	else
    	{
    		$this->errorUnAuthorized();
    	}
    }

    /**
     * Endpoint untuk list produk overview di screen depan
     */
    function overview_get()
    {
        $token = $this->getToken();
        if($token)
        {
            $data = $this->MuliaModel->getProdukOverview();
            $ubs = $this->urlizeImage($data['ubs']);
            $antam = $this->urlizeImage($data['antam']);

            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => [
                    [
                        "vendor" => 'UBS',
                        "item" => $ubs
                    ],
                    [
                        "vendor" => 'ANTAM',
                        "item" => $antam
                    ],
                ]
            ]);
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Endpoint untuk get list cart user
     */
    function cart_get()
    {
        $token = $this->getToken();
        if($token)
        {
            $userCart = $this->MuliaModel->getCart($token->id);
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $this->urlizeImage($userCart->result())
            ]);
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Endpoint routing untuk cart method post
     * @param string $method
     */
    function cart_post($method = 'add')
    {
        $token = $this->getToken();
        if($token)
        {
            if($method == 'add')
            {
                $this->addCart($token);
            }
            else if($method == 'delete')
            {
                $this->deleteCart($token);
            }
            else
            {
                $this->errorForbbiden();
            }
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Menambahkan/update item cart
     * @param type $token
     */
    function addCart($token)
    {

        $this->form_validation->set_rules('produkId', 'productId', 'required|integer');
        $this->form_validation->set_rules('qty','qty','required|integer|greater_than[0]');

        if($this->form_validation->run() == FALSE)
        {
            $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 201,
                'error' => $this->form_validation->error_array()
            ]);
        }
        else
        {
            $produkId = $this->post('produkId');
            $qty = $this->post('qty');

            $this->load->helper('text');
            $cart = $this->MuliaModel->addCart($token->id, $produkId, $qty);

            $this->set_response([
                'status' => 'success',
                'message' => 'Cart added',
                'data' => $this->urlizeImage($cart)
            ]);
        }
    }

    /**
     * Hapus item dalam cart
     * @param type $token
     */
    function deleteCart($token)
    {        
        $this->form_validation->set_rules('id','id','integer|required');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 201,
                'error' => $this->form_validation->error_array()
            ]);
        }
        else
        {
            $id = $this->post('id');                
            $this->MuliaModel->deleteCart($id);
            $this->set_response([
                'status' => 'success',
                'message' => 'Cart removed',
                'data' => null
            ]);
        }              
    }

    /**
     * Memberi tambahan URL untuk field thumbnail dan images
     * @param  $data array
     * @return $data array
     */
    function urlizeImage($data)
    {
        //Get config untuK URL gambar
        $imageURL = $this->config->item('mulia_image_path');
        
        if(is_array($data))
        {
            foreach($data as $d)
            {
                if(isset($d->images) && $d->images != null){
                    $images = json_decode($d->images);
                    $returnImages = array();
                    foreach ($images as $i) {
                        $returnImages[] = $imageURL.$i;    
                    }
                    $d->images = $returnImages;    
                }
                 
                $d->thumbnail = $imageURL.$d->thumbnail;
                   
            }    
        }
        else
        {            
            if(isset($data->images) && $data->images != null){
                $images = json_decode($data->images);
                $returnImages = array();
                foreach ($images as $i) {
                    $returnImages[] = $imageURL.$i;    
                }
                $data->images = $returnImages;
            }

            $data->thumbnail = $imageURL.$data->thumbnail;                        
        }
        
        return $data;
    }
    
    function product_get($id = null)
    {
        $token = $this->getToken();
        if($token)
        {
            if($id != null){
                $product = $this->MuliaModel->getProductById($id);
                if($product)
                {
                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $product
                    ]);    
                }
                else
                {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Produk tidak ditemukan',
                        'code' => 102
                    ]);
                }
                  
            }
            else
            {
                $vendor = $this->query('vendor');
                if($vendor == null){
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Vendor harus diisi',
                        'code' => 102
                    ]);
                    return;
                }
                
                // Get daftar mulia
                $mulia = $this->MuliaModel->getProduct($vendor);
                if($mulia)
                {
                    $imagePath = $this->config->item('mulia_image_path'); 
                    $r = [];
                    foreach($mulia as $m)
                    {                    
                        if($m->images != '' || $m->images != null){
                            $imageList = json_decode($m->images);
                            $imageListPath = [];
                            foreach($imageList as $il)
                            {
                                $imageListPath[] = $imagePath.$il;
                            }
                            $m->images = $imageListPath;
                        }
                        
                        if($m->thumbnail != '' ||$m->thumbnail != null){
                            $m->thumbnail = $imagePath.$m->thumbnail;
                        }
                        
                        $r[] = $m;
                    }
                    
                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $r
                    ]);            
                }
                else
                {
                    $this->set_response([                    
                        'status' => 'error',
                        'code' => '102',
                        'message' => 'Produk tidak ditemukan'
                    ]);
                }
            }
                        
        }
        else
        {
            $this->errorUnAuthorized();
        }
        
    }

    function simulasi_post()
    {
        $token = $this->getToken();
        if($token)
        {
            $setData = [
                'tenor' => $this->post('tenor'),
                'vendor' => $this->post('vendor'),
                'item' => $this->post('item'),
                'uang_muka' => $this->post('uang_muka')
            ];
            $this->form_validation->set_data($setData);
            
            $this->form_validation->set_rules('tenor','tenor','required|integer');
            $this->form_validation->set_rules('vendor','vendor','required|numeric');
            $this->form_validation->set_rules('item','item','required');
            $this->form_validation->set_rules('uang_muka','uang_muka','required|integer');
            
            if($this->form_validation->run() == false)
            {
                $this->set_response([
                    'status' => 'succes',
                    'message' => '',
                    'code' => '101',
                    'error' => $this->form_validation->error_array()
                ]);
            }
            else
            {
                $item = $this->post('item');
                
                $arrayOrder = json_decode($item, true);
                    
                if (json_last_error() !== JSON_ERROR_NONE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input item. Not valid JSON: '.json_last_error_msg(),
                        'code' => 101
                    ),200);
                    return;
                }
                
                $keping = '';
                $totalKeping = 0;
                $totalGram = 0;
                $count = 0;

                foreach($arrayOrder as $key){
                    if(isset($key['qty']) && isset($key['gram'])){
                        $totalKeping = $key['gram'] * $key['qty'];
                    }
                    $totalGram += $totalKeping;    
                }

                foreach($arrayOrder as $o){
                    
                    if(isset($o['qty']) && isset($o['gram'])){
                        $keping = $keping . $o['gram'].','.$o['qty'];
                        if($count < count($arrayOrder)){
                            $keping = $keping.';';
                        }
                        
                    }
                    $count ++;
                }
                
                if($keping == ''){
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input item',
                        'code' => 101
                    ),200);
                    return;
                }
               
                $vendor = $this->post('vendor');
                $uangMuka = $this->post('uang_muka');
                $tenor = $this->post('tenor');
                
                
                // Simulasi Mulia
                $simulasi = $this->muliaSimulasi([
                    'uangMuka' => $uangMuka,
                    'tenor' => $tenor,
                    'idVendor' => $vendor,
                    'jenisTransaksi' => 'OP',
                    'kepingMulia' => $keping,
                    'channelId' => $token->channelId,
                    'flag' => 'K',
                    'productCode' => '37'
                ]);
                
                if($simulasi->responseCode == '00')
                {
                    $simulasiData = json_decode($simulasi->data);
                    $simulasiData->totalGramLM = $totalGram;

                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $simulasiData
                    ]);
                }
                else
                {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 103,
                        'message' => $simulasi->responseDesc                        
                    ]);
                }
            }
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    function inquiry_post()
    {
        $token = $this->getToken();
        if($token)
        {
            $setData = [
                'tenor' => $this->post('tenor'),
                'vendor' => $this->post('vendor'),
                'item' => $this->post('item'),
                'uang_muka' => $this->post('uang_muka'),
                'kode_outlet' => $this->post('kode_outlet'),
                'promo_code' => $this->post('promo_code')
            ];
            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('tenor','tenor','required|integer');
            $this->form_validation->set_rules('vendor','vendor','required|numeric');
            $this->form_validation->set_rules('item','item','required');
            $this->form_validation->set_rules('uang_muka','uang_muka','required|integer');
            $this->form_validation->set_rules('kode_outlet','kode_outlet','required|numeric');

            if($this->form_validation->run() == false)
            {
                $this->set_response([
                    'status' => 'succes',
                    'message' => '',
                    'code' => '101',
                    'error' => $this->form_validation->error_array()
                ]);
            }
            else
            {
                // Pastikan user sudah mengisi profile sebelum inquiry
                $user = $this->User->getUser($token->id);
                
                if($user->jenis_identitas == '' 
                        || $user->nama_ibu == null
                        || $user->tempat_lahir == null
                        || $user->tgl_lahir == null
                        || $user->jenis_kelamin == ''
                        || $user->kewarganegaraan == ''
                        || $user->no_ktp == null
                        || $user->alamat == null
                        || $user->id_kelurahan == ''
                        || $user->status_kawin == '') {
                    
                    return $this->set_response([
                        'status' => 'error',
                        'message' => 'Mohon lengkapi profile/Link CIF untuk melakukan pemesanan',
                        'code' => 111,
                    ]);
                }
                
                if($user)
                
                $item = $this->post('item');
                $promoCode = $this->post('promo_code');

                $arrayOrder = json_decode($item, true);

                if (json_last_error() !== JSON_ERROR_NONE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input item. Not valid JSON: '.json_last_error_msg(),
                        'code' => 101
                    ),200);
                    return;
                }

                $keping = '';
                $count = 1;

                $cartDetails = [];

                foreach($arrayOrder as $o){

                    if(isset($o['qty']) && isset($o['gram'])){
                        $keping = $keping . $o['gram'].','.$o['qty'];

                        // Get denom details
                        $denomDetails = $this->MuliaModel->getDenomDetail($o['gram']);

                        if($denomDetails)
                        {
                            $cartDetails[] = [
                                'qty' => $o['qty'],
                                'denom' => $o['gram'],
                                'harga' => $denomDetails->harga
                            ];
                        }

                        if($count < count($arrayOrder)){
                            $keping = $keping.';';
                        }
                    }
                    $count ++;
                }

                if($keping == ''){
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input item',
                        'code' => 101
                    ),200);
                    return;
                }

                $vendor = $this->post('vendor');
                $uangMuka = $this->post('uang_muka');
                $tenor = $this->post('tenor');
                $kodeOutlet = $this->post('kode_outlet');

                $_inquiryData = [
                    'uangMuka' => $uangMuka,
                    'tenor' => $tenor,
                    'idVendor' => $vendor,
                    'jenisTransaksi' => 'OP',
                    'kepingMulia' => $keping,
                    'channelId' => $token->channelId,
                    'flag' => 'K',
                    'productCode' => '37'
                ];

                // Simulasi Mulia
                $inquiry = $this->muliaInquiry($_inquiryData);

                 

                if($inquiry->responseCode == '00')
                {
                    $inquiryData = json_decode($inquiry->data, true);

                    // Add new data
                    $inquiryData['user_AIID'] = $token->id;
                    $inquiryData['kepingMulia'] = $keping;
                    $inquiryData['kepingMuliaJson'] = json_encode($cartDetails);
                    $inquiryData['flag'] = 'K';
                    $inquiryData['idVendor'] = $vendor;
                    $inquiryData['kodeOutlet'] = $kodeOutlet;
                    $inquiryData['realTglTransaksi'] = date('Y-m-d H:i:s');

                    // if(isset($_inquiryData['discountAmount'], $_inquiryData['idPromosi']))
                    // {
                    //     $inquiryData['discountAmount'] = $_inquiryData['discountAmount'];
                    //     $inquiryData['idPromosi'] = $_inquiryData['idPromosi'];    
                    // }

                    // if($promoCode != null)
                    // {
                    //     if($user->cif == null){
                    //         return $this->set_response([
                    //             'status' => 'error',
                    //             'message' => 'Anda harus memiliki CIF untuk menggunakan fitur voucher',
                    //             'code' => 102
                    //         ]);
                    //     }

                    //     $validateData = [
                    //         'promoCode' => $promoCode,
                    //         'voucherId' => null,
                    //         'userId' => $user->cif,
                    //         'validators' => [
                    //             'channel' => $this->config->item('core_post_username'),
                    //             'product' => '37',
                    //             'transactionType' => 'OP',
                    //         ]
                    //     ];

                    //     //** */
                    //     //New Method
                    //     //** */
                    //     $gpoin = $this->GpoinModel->get_promo($token, $promoCode, $inquiryData['reffSwitching']);
                    //     $idPromosi = $this->generateIdpromosi($gpoin);
                    //     // print_r($idPromosi); die();
                    //     if($gpoin == ''){
                    //         return $this->set_response([
                    //             'status' => 'warning',
                    //             'message' => 'Data Voucher tidak ditemukan',
                    //             'code' => 103,                                
                    //         ]);
                    //     } else {
                    //         $inquiryData['discountAmount'] = $gpoin->value;
                    //         $inquiryData['idPromosi'] = $idPromosi;
                    //         $inquiryData['totalKewajiban'] -= $gpoin->value;
                    //     }
                        

                    //     //** */
                    //     //Old Method
                    //     //** */
                    //     // $validateVoucher = $this->gPointValidateVoucher($validateData);

                    //     // if($validateVoucher->status == 'Error'){
                    //     //     return $this->set_response([
                    //     //         'status' => 'error',
                    //     //         'message' => $validateVoucher->message,
                    //     //         'code' => 103,                                
                    //     //     ]);
                    //     // } else if($validateVoucher->status == 'Success') {
                    //     //     $_inquiryData['discountAmount'] = $validateVoucher->data->discount;
                    //     //     $_inquiryData['idPromosi'] = $validateVoucher->data->journalAccount.';'.$promoCode;
                    //     // }

                    // }  

                    $this->MuliaModel->addPayment($inquiryData);

                    $inquiryData['idTransaksi'] = $inquiryData['reffSwitching'];
                    $biayaChannel = [
                        'BNI'     => $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', '009'),
                        'MANDIRI' => $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', '008'),
                        'WALLET'  => $this->ConfigModel->getBiayaPayment('OP', 'WALLET', '37', ''),
                        'VA_MANDIRI'  => $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', '008'),
                        'VA_BCA'  => $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', '014'),
                        'VA_PERMATA'  => $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', '013'),
                        'VA_BRI'  => $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', '002'),
                    ];
                    $inquiryData['biayaChannel'] = $biayaChannel;

                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $inquiryData
                    ]);
                }
                else
                {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 103,
                        'message' => $inquiry->responseDesc
                    ]);
                }
            }
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    

    function payment_post()
    {
        $token = $this->getToken();
        if($token)
        {
            $user = $this->User->getUser($token->id);

            $setData = [
                'id_transaksi' => $this->post('id_transaksi'),
                'payment' => $this->post('payment'),
                'booking_code' => $this->post('booking_code'),
                'card_number' => $this->post('card_number'),
                'token_response' => $this->post('token_response'),
                'pin' => $this->post('pin'),
                'va' => $this->post('va')
            ];

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('id_transaksi','id_transaksi','required');
            $this->form_validation->set_rules('payment','payment','required');

            $payment = $this->post('payment');

            if($payment == 'MANDIRI'){
                $this->form_validation->set_rules('booking_code','booking_code','required');
                $this->form_validation->set_rules('card_number','card_number','required');
                $this->form_validation->set_rules('token_response','token_response','required');
            }

            if($payment == 'WALLET' || $payment == 'GCASH'){
                $this->form_validation->set_rules('pin','pin','required|exact_length[6]|numeric');
            }

            if($payment == 'GCASH'){
                $this->form_validation->set_rules('va','va','required|numeric');
            }

            //Jika payment adalah wallet dan user belum akttifasi wallet
            if($payment == 'WALLET' && $user->norek == ''){
                $this->set_response([
                    'status' => 'error',
                    'message' => 'Anda belum melakukan aktifasi wallet',
                    'code' => 101
                ],200);
                return;
            }

            if($this->form_validation->run() == FALSE)
            {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ),200);
            }
            else
            {
                $idTransaksi = $this->post('id_transaksi');

                //Mendapatkan payment berdasarkan id transaksi
                $checkPayment = $this->MuliaModel->getPaymentByTrxId($idTransaksi);
                // Mendapatkan promo berdasarkan id transaksi
                $gpoin = $this->GpoinModel->get_promo($idTransaksi);
                $idPromosi = '';
                $promoCode = '';
                $discountAmount = 0;
                $promoAmount = 0;
                if ($gpoin != '') {
                    if ($gpoin->type == 'discount') {
                        $discountAmount = $gpoin->value;
                    }
                    $promoCode = $gpoin->promoCode;
                    $promoAmount = $gpoin->value;
                    $idPromosi = $gpoin->idPromosi;
                };
                
                if($checkPayment){
                    if ($payment == 'BNI')
                    {
                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', '009');

                        $createBillingBNI = $this->createBillingVABNI( 
                            $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount,
                            $user->email,
                            $token->nama,
                            $token->no_hp,
                            'K',
                            'OP',
                            "PDSOP " . $idTransaksi . " Mulia",
                            "",
                            '37',
                            $idTransaksi,
                            $token->channelId,
                            $idPromosi,
                            $promoAmount
                        );

                        if ($createBillingBNI->responseCode == '00') {

                            if (!isset($createBillingBNI->data)) {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBillingBNI
                                ), 200);
                                return;
                            }

                            $billingData = json_decode($createBillingBNI->data);

                            $virtualAccount = $billingData->virtualAccount;
                            $tglExpired = $billingData->tglExpired;

                            $updatePaymentData = array(
                                'tipe' => $payment,
                                'virtualAccount' => $virtualAccount,
                                'tglExpired' => $tglExpired,
                                'payment' => $payment,
                                'biayaTransaksi' => $biayaTransaksi,
                                'kodeBankPembayar' => '009',
                                'channelId' => $token->channelId
                            );

                            $this->MuliaModel->updatePayment($idTransaksi, $updatePaymentData);

                            $dateExpired = new DateTime($tglExpired);

                            // Get Cabang Detail
                            $outlet = $this->MasterModel->getSingleCabang($checkPayment->kodeOutlet);

                            $templateData = [
                                'nama'           => $token->nama,
                                'namaNasabah'    => $token->nama,
                                'payment'        => 'BNI',
                                'nama'           => $token->nama,
                                'va'             => $virtualAccount,
                                'totalKewajiban' => $checkPayment->totalKewajiban - $discountAmount,
                                'tglExpired'     => $dateExpired->format('d/M/Y H:i:s'),
                                'tglExpiredUnformated' => $tglExpired,
                                'reffSwitching'  => $idTransaksi,
                                'trxId'          => $idTransaksi,
                                'biayaTransaksi' => $biayaTransaksi,
                                'namaCabang'     => $outlet->nama,
                                'alamatCabang'   => $outlet->alamat.', '.$outlet->kelurahan.', '.$outlet->kecamatan.', '.$outlet->kabupaten.', '.$outlet->provinsi,
                                'telpCabang'     => $outlet->telepon,
                                'item'           => json_decode($checkPayment->kepingMuliaJson),
                                'tglTransaksi'   => $checkPayment->tglTransaksi,
                                'totalHarga'     => $checkPayment->totalHarga,
                                'administrasi'   => $checkPayment->administrasi,
                                'uangMuka'       => $checkPayment->nominalUangMuka,
                                'biayaTransaksi' => $biayaTransaksi,
                                'jenisLogamMulia'=> $checkPayment->jenisLogamMulia,
                                'realTglTransaksi' => $checkPayment->realTglTransaksi,
                                'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                                'tenor' => $checkPayment->tenor,
                                'angsuran' => $checkPayment->angsuran,
                                // Promo
                                'promoCode'       => $promoCode,
                                'discountAmount'  => $discountAmount,
                                'promoAmount'     => $promoAmount

                            ];

                            $this->sendNotifVA($token, $templateData);

                        }
                        else
                        {
                            $this->set_response([
                                'status' => 'error',
                                'message' => $createBillingBNI->responseDesc,
                                'code' => 103
                            ]);
                        }
                    }
                    else if ($payment == 'MANDIRI')
                    {

                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', '008');
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', '008');

                        log_message('debug', 'Mandiri Click Pay');

                        $bookingCode   = $this->post('booking_code');
                        $cardNumber    = $this->post('card_number');
                        $tokenResponse = $this->post('token_response');

                        $newTotalKewajiban = $checkPayment->totalKewajiban + $biayaTransaksi;

                        //Bayar dengan mandiri click pay
                        $clickPay = $this->mandiriClickPay(
                            $newTotalKewajiban,
                            $bookingCode,
                            $cardNumber,
                            'OP',
                            'Pembelian Mulia',
                            $user->no_hp,
                            "",
                            '37',
                            $tokenResponse,
                            $checkPayment->reffSwitching,
                            $token->channelId,
                            $idPromosi,
                            $promoAmount
                        );

                        if ($clickPay->responseCode == '00') {

                            $clickpayData = json_decode($clickPay->data);

                            $updateData['reffSwitching']    = $checkPayment->reffSwitching;
                            $updateData['bookingCode']      = $bookingCode;
                            $updateData['cardNumber']       = $cardNumber;
                            $updateData['tokenResponse']    = $tokenResponse;
                            $updateData['reffBiller']       = $clickpayData->reffBiller;
                            $updateData['kodeBankPembayar'] = '008';
                            $updateData['payment']          = $payment;
                            $updateData['biayaTransaksi']   = $biayaTransaksiDisplay;
                            $updateData['channelId']        = $token->channelId;

                            //update data pembayaran
                            $this->MuliaModel->updatePayment($updateData);

                            //Set response
                            $this->set_response([
                                'status'  => 'success',
                                'message' => 'Pembelian Mulia berhasil',
                                'data'    => array(
                                    'reffBiller' => $clickpayData->reffBiller,
                                    'redaksiPayment' => ''
                                ),
                            ], 200);

                        } else {
                            $this->set_response(array(
                                'status'  => 'error',
                                'code'    => 103,
                                'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                'reason'  => $clickPay,
                            ));
                        }
                    }
                    else if ($payment == 'WALLET' || $payment == 'GCASH')
                    {
                        $pin = $this->post('pin');
                        $va = $this->post('va');
                        //Check user pin
                        if (!$this->User->isValidPIN2($token->id, $pin)) {

                            $this->set_response(array(
                                'status'  => 'error',
                                'message' => 'PIN tidak valid',
                                'code'    => 102,
                            ), 200);

                            return;
                        }

                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'WALLET', '37', '');
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OP', 'WALLET', '37', '');

                        $openData = [
                            'uangMuka' => $checkPayment->uangMuka,
                            'tenor' => $checkPayment->tenor,
                            'idVendor' => $checkPayment->idVendor,
                            'jenisTransaksi' => 'OP',
                            'kepingMulia' => $checkPayment->kepingMulia,
                            'channelId' => $token->channelId,
                            'flag' => 'K',
                            'productCode' => '37',
                            'namaNasabah' => $user->nama,
                            'ibuKandung' => $user->nama_ibu,
                            'tempatLahir' => $user->tempat_lahir,
                            'tanggalLahir' => $user->tgl_lahir,
                            'kodeCabang' => $checkPayment->kodeOutlet,
                            'jenisKelamin' => $user->jenis_kelamin,
                            'kewarganegaraan' => $user->kewarganegaraan,
                            'statusKawin' => $user->status_kawin,
                            'jalan' => $user->alamat,
                            'idKelurahan' => $user->id_kelurahan,
                            'tipeIdentitas' => $user->jenis_identitas,
                            'noIdentitas' => $user->no_ktp,
                            'noHp' => $user->no_hp,
                            'tanggalExpiredId' => $user->tanggal_expired_identitas,
                            'administrasi' => $checkPayment->administrasi,
                            'reffSwitching' => $checkPayment->reffSwitching,
                            'nominalUangMuka' => $checkPayment->nominalUangMuka,
                            'totalKewajiban' => $checkPayment->totalKewajiban,
                            'totalHarga' => $checkPayment->totalHarga,
                            'biayaTransaksi' => $biayaTransaksi,
                            'jenisLogamMulia'=> $checkPayment->jenisLogamMulia,
                            'realTglTransaksi' => $checkPayment->realTglTransaksi,
                            'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                            'tenor' => $checkPayment->tenor,
                            'angsuran' => $checkPayment->angsuran,
                            'discountAmount' => $promoAmount,
                            'idPromosi' => $idPromosi
                        ];

                        if($payment == 'WALLET'){
                            $openData['paymentMethod'] = 'WALLET';
                            $openData['norekWallet'] = $user->norek;
                            $openData['walletId'] = $user->no_hp; 
                            $openData['surcharge'] = $checkPayment->surcharge;                           
                        } else if($payment == 'GCASH'){
                            $openData['gcashId'] = $va;
                            $openData['paymentMethod'] = 'GCASH';
                            $openData['amount'] = $checkPayment->totalKewajiban;
                            $openData['surcharge'] = "0";
                        }

                        $walletPayment = $this->muliaOpen($openData);

                        if ($walletPayment->responseCode == '00')
                        {

                            if (!isset($walletPayment->data)) {
                                $this->set_response(array(
                                    'status'  => 'error',
                                    'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                                    'code'    => 103,
                                ), 200);
                                return;
                            }

                            $walletData = json_decode($walletPayment->data, true);

                            $walletData['isPaid']        = '1';
                            $walletData['channelId']     = $token->channelId;
                            $walletData['payment'] = $payment;
                            $walletData['gcashId'] = $va;

                            //Update wallet data
                            $this->MuliaModel->updatePayment($checkPayment->reffSwitching, $walletData);


                            // Get Cabang Detail
                            $outlet = $this->MasterModel->getSingleCabang($checkPayment->kodeOutlet);

                            //Send success notification
                            $templateData = [
                                'nama'           => $token->nama,
                                'namaNasabah'    => $token->nama,
                                'payment'        => $payment,
                                'nama'           => $token->nama,
                                'va'             => '',
                                'totalKewajiban' => $checkPayment->totalKewajiban - $discountAmount,
                                'tglExpired'     => '',
                                'tglExpiredUnformated' => '',
                                'reffSwitching'  => $idTransaksi,
                                'trxId'          => $idTransaksi,
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'namaCabang'     => $outlet->nama,
                                'alamatCabang'   => $outlet->alamat.', '.$outlet->kelurahan.', '.$outlet->kecamatan.', '.$outlet->kabupaten.', '.$outlet->provinsi,
                                'telpCabang'     => $outlet->telepon,
                                'item'           => json_decode($checkPayment->kepingMuliaJson),
                                'tglTransaksi'   => $checkPayment->tglTransaksi,
                                'noKredit'       => isset($walletData->noKredit) ? $walletData->noKredit : '-',
                                'totalHarga'     => $checkPayment->totalHarga,
                                'administrasi'     => $checkPayment->administrasi,
                                'uangMuka'       => $checkPayment->uangMuka,
                                'biayaTransaksi' => $checkPayment->biayaTransaksi,
                                'jenisLogamMulia'=> $checkPayment->jenisLogamMulia,
                                'realTglTransaksi' => $checkPayment->realTglTransaksi,
                                'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                                'tenor' => $checkPayment->tenor,
                                'angsuran' => $checkPayment->angsuran,
                                // Promo
                                'promoCode'       => $promoCode,
                                'discountAmount'  => $discountAmount,
                                'promoAmount'     => $promoAmount
                            ];

                            $this->sendNotifVA($token, $templateData);

                        }
                        else
                        {
                            $this->set_response(array(
                                'status'  => 'error',
                                'message' => 'Nomor telepon terblokir',
                                'code'    => 103,
                                'reason'  => $walletPayment,
                            ), 200);
                        }
                    }
                    else if ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI')
                    {
                        $kodeBank = '';
                        if($payment == 'VA_BCA'){
                            $kodeBank = '014';
                        } else if($payment == 'VA_MANDIRI') {
                            $kodeBank = '008';
                        } else if($payment == 'VA_BRI') {
                            $kodeBank = '002';
                        }

                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', $kodeBank);
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', $kodeBank);

                        $createBillingData = [
                            'channelId' => $token->channelId,
                            'amount' => $checkPayment->totalKewajiban + $biayaTransaksi,
                            'customerEmail' => $user->email,
                            'customerName' => $user->nama,
                            'customerPhone' => $user->no_hp,
                            'norek' => $checkPayment->tenor,
                            'flag' => 'K',
                            'jenisTransaksi' => 'OP',
                            'kodeProduk' => '37',
                            'keterangan' => "PDSOP " . $idTransaksi . " Mulia",
                            'reffSwitching' => $checkPayment->reffSwitching,
                            'kodeBank' => $kodeBank,
                            'discountAmount' => $promoAmount,
                            'idPromosi' => $idPromosi
                        ];

                        $createBilling = $this->createBillingPegadaian($createBillingData);

                        if ($createBilling->responseCode == '00') {

                            if (!isset($createBilling->data)) {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                ), 200);
                                return;
                            }

                            $billingData = json_decode($createBilling->data);

                            $virtualAccount = $billingData->vaNumber;
                            $tglExpired = $billingData->tglExpired;

                            $updatePaymentData = array(
                                'tipe' => $payment,
                                'virtualAccount' => $virtualAccount,
                                'tglExpired' => $tglExpired,
                                'payment' => $payment,
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'kodeBankPembayar' => $kodeBank,
                                'channelId' => $token->channelId
                            );

                            $this->MuliaModel->updatePayment($idTransaksi, $updatePaymentData);

                            $dateExpired = new DateTime($tglExpired);

                            // Get Cabang Detail
                            $outlet = $this->MasterModel->getSingleCabang($checkPayment->kodeOutlet);

                            $templateData = [
                                'nama'           => $token->nama,
                                'namaNasabah'    => $token->nama,
                                'payment'        => $payment,
                                'nama'           => $token->nama,
                                'va'             => $virtualAccount,
                                'totalKewajiban' => $checkPayment->totalKewajiban - $discountAmount,
                                'tglExpired'     => $dateExpired->format('d/M/Y H:i:s'),
                                'tglExpiredUnformated' => $tglExpired,
                                'reffSwitching'  => $idTransaksi,
                                'trxId'          => $idTransaksi,
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'namaCabang'     => $outlet->nama,
                                'alamatCabang'   => $outlet->alamat.', '.$outlet->kelurahan.', '.$outlet->kecamatan.', '.$outlet->kabupaten.', '.$outlet->provinsi,
                                'telpCabang'     => $outlet->telepon,
                                'item'           => json_decode($checkPayment->kepingMuliaJson),
                                'tglTransaksi'   => $checkPayment->tglTransaksi,
                                'totalHarga'     => $checkPayment->totalHarga,
                                'administrasi'   => $checkPayment->administrasi,
                                'uangMuka'       => $checkPayment->nominalUangMuka,
                                'biayaTransaksi' => $biayaTransaksi,
                                'jenisLogamMulia'=> $checkPayment->jenisLogamMulia,
                                'realTglTransaksi' => $checkPayment->realTglTransaksi,
                                'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                                'tenor' => $checkPayment->tenor,
                                'angsuran' => $checkPayment->angsuran,
                                // Promo
                                'promoCode'       => $promoCode,
                                'discountAmount'  => $discountAmount,
                                'promoAmount'     => $promoAmount
                            ];

                            $this->sendNotifVA($token, $templateData);

                        }
                        else
                        {
                            $this->set_response([
                                'status' => 'error',
                                'message' => $createBilling->responseDesc,
                                'code' => 103
                            ]);
                        }
                    }
                    else if ($payment == 'VA_PERMATA')
                    {
                        $kodeBank = '013';

                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', $kodeBank);
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', $kodeBank);

                        $createBillingData = [
                            'channelId' => $token->channelId,
                            'amount' => $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount,
                            'cif' => $user->cif,
                            'customerEmail' => $user->email,
                            'customerName' => $user->nama,
                            'customerPhone' => $user->no_hp,
                            'flag' => 'K',
                            'jenisTransaksi' => 'OP',
                            'productCode' => '37',
                            'norek' => '',
                            'keterangan' => "PDSOP " . $idTransaksi . " Mulia",
                            'trxId' => $checkPayment->reffSwitching,
                            'discountAmount' => $promoAmount,
                            'idPromosi' => $idPromosi
                        ];

                        $createBilling = $this->createBillingPermata($createBillingData);

                        if ($createBilling->responseCode == '00') {

                            if (!isset($createBilling->data)) {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                ), 200);
                                return;
                            }

                            $billingData = json_decode($createBilling->data);

                            $virtualAccount = $billingData->virtualAccount;
                            $tglExpired = $billingData->tglExpired;

                            $updatePaymentData = array(
                                'tipe' => $payment,
                                'virtualAccount' => $virtualAccount,
                                'tglExpired' => $tglExpired,
                                'payment' => $payment,
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'kodeBankPembayar' => $kodeBank,
                                'channelId' => $token->channelId
                            );

                            $this->MuliaModel->updatePayment($idTransaksi, $updatePaymentData);

                            $dateExpired = new DateTime($tglExpired);

                            // Get Cabang Detail
                            $outlet = $this->MasterModel->getSingleCabang($checkPayment->kodeOutlet);

                            $templateData = [
                                'nama'           => $token->nama,
                                'namaNasabah'    => $token->nama,
                                'payment'        => $payment,
                                'nama'           => $token->nama,
                                'va'             => $virtualAccount,
                                'totalKewajiban' => $checkPayment->totalKewajiban,
                                'tglExpired'     => $dateExpired->format('d/M/Y H:i:s'),
                                'tglExpiredUnformated' => $tglExpired,
                                'reffSwitching'  => $idTransaksi,
                                'trxId'          => $idTransaksi,
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'namaCabang'     => $outlet->nama,
                                'alamatCabang'   => $outlet->alamat.', '.$outlet->kelurahan.', '.$outlet->kecamatan.', '.$outlet->kabupaten.', '.$outlet->provinsi,
                                'telpCabang'     => $outlet->telepon,
                                'item'           => json_decode($checkPayment->kepingMuliaJson),
                                'tglTransaksi'   => $checkPayment->tglTransaksi,
                                'totalHarga'     => $checkPayment->totalHarga,
                                'administrasi'   => $checkPayment->administrasi,
                                'uangMuka'       => $checkPayment->nominalUangMuka,
                                'biayaTransaksi' => $biayaTransaksi,
                                'jenisLogamMulia'=> $checkPayment->jenisLogamMulia,
                                'realTglTransaksi' => $checkPayment->realTglTransaksi,
                                'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                                'tenor' => $checkPayment->tenor,
                                'angsuran' => $checkPayment->angsuran,
                                // Promo
                                'promoCode'       => $promoCode,
                                'discountAmount'  => $discountAmount,
                                'promoAmount'     => $promoAmount
                            ];

                            $this->sendNotifVA($token, $templateData);

                        }
                        else
                        {
                            $errMsg = isset($createBilling->responseDesc) ? $createBilling->responseDesc
                                        : 'Terjadi kesalahan. Mohon coba beberapa saat lagi';

                            $this->set_response([
                                'status' => 'error',
                                'message' => $errMsg,
                                'code' => 103
                            ]);
                        }
                    }
                }
                else
                {
                    log_message('debug', 'Payment not found'.$this->post('id_transaksi'));
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Payment not found',
                        'code' => 102
                    ),200);
                }
            }
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    function history_get($id = null)
    {
        $token = $this->getToken();
        if($token)
        {

            $history = $this->MuliaModel->getHistory($token->id, $id);
            if($history){
                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $history
                ]);
            }
            else
            {
                $this->set_response([
                    'status' => 'error',
                    'message' => 'History tidak ditemukan',
                    'code' => '102'
                ]);
            }
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    function vendor_get()
    {
        $token = $this->getToken();
        if($token)
        {
            $vendor = $this->MuliaModel->getVendor();
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $vendor
            ]);
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    function kontrak_get()
    {        
        $headers = $this->input->request_headers();
        if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateMuliaToken($headers['Authorization']);
            
            if(isset($token)){
                // Check apakah payment benar benar ada
                $payment = $this->MuliaModel->getPaymentByTrxId($token->reffSwitching);
                
                if($payment){
                    
                    // Untuk security pastikan email token dan data user cocok
                    if($payment->emailNasabah == $token->email)
                    {                        
                        // Jumlah keping dan jumlah gram
                        $jumlahKeping = 0;
                        $jumlahGram = 0;

                        $itemData = json_decode($payment->kepingMuliaJson);

                        foreach ($itemData as $i){
                            $jumlahGram = $jumlahGram + $i->denom;
                            $jumlahKeping = $jumlahKeping + $i->qty;
                        }

                        // Pokok pinjaman= marginBersih / tenor
                        $pokokPinjaman = $payment->marginBersih / $payment->tenor;

                        $tglKredit = $payment->tglKredit;
                        
                        // Ekstrak terkait tanggal
                        $hariTerbilangKredit = $this->_toDay($tglKredit);
                        $tglAngsuran = substr($tglKredit, 8, 2);
                        $tglTerbilangKredit = $this->_toBilangan($tglAngsuran); 
                        $bulanTerbilanKredit = $this->_toBulan(substr($tglKredit, 5, 2));
                        $tahunTerbilangKredit = $this->_toBilangan(substr($tglKredit,0, 4));

                        // Get outlet pegadaian
                        $outlet = $this->MasterModel->getSingleCabang($payment->kodeOutlet);

                        $sewaModal = $payment->marginBersih *  $payment->tenor;

                        // Get user full address
                        $fullAddress = $this->User->getFullAddress($payment->user_AIID);

                        // Set response data
                        $kontrakData = [
                            'noKredit' => $payment->noKredit,
                            'tglKredit' => $payment->tglKredit,
                            'hariTerbilangKredit' => $hariTerbilangKredit,
                            'tglTerbilangKredit' => $tglTerbilangKredit,
                            'bulanTerbilanKredit' => $bulanTerbilanKredit,
                            'tahunTerbilangKredit' => $tahunTerbilangKredit,
                            'nama' => $payment->namaNasabah,
                            'alamat' => $fullAddress !== null ? strtoupper($fullAddress): $payment->alamatNasabah,
                            'noTelp' => $payment->noHpNasabah,
                            'noKtp' => $payment->noKtpNasabah,
                            'namaCabang' => $outlet->nama,
                            'pokokPembiayaan' => $payment->pokokPembiayaan,
                            'pokokPembiayaanTerbilang' =>$this->_toBilangan($payment->pokokPembiayaan),
                            'jumlahGram' => $jumlahGram,
                            'jumlahKeping' => $jumlahKeping,
                            'jenisLogamMulia' =>$payment->jenisLogamMulia,
                            'totalHarga' => $payment->totalHarga,
                            'totalHargaTerbilang' => $this->_toBilangan($payment->totalHarga),
                            'tenor' => $payment->tenor,
                            'tenorTerbilang' => $this->_toBilangan($payment->tenor),
                            'marginBersih' => $payment->marginBersih,
                            'marginBersihTerbilang' => $this->_toBilangan($payment->marginBersih),
                            'administrasi' => $payment->administrasi,
                            'administrasiTerbilangan' => $this->_toBilangan($payment->administrasi),
                            'angsuran' =>$payment->angsuran,
                            'angsuranTerbilang' => $this->_toBilangan($payment->angsuran),
                            'tglAngsuran' => $tglAngsuran,
                            'tglAngsuranTerbilang' => $tglTerbilangKredit,
                            'sewaModal' => $sewaModal,
                            'sewaModalTerbilang' =>  $this->_toBilangan($sewaModal)
                        ];

                        $this->set_response([
                            'status' => 'success',
                            'message' => '',
                            'data' => $kontrakData
                        ]);
                    }
                    else
                    {
                        $this->set_response([
                            'status' => 'error',
                            'code' => 102,
                            'message' => 'Data tidak ditemukan'
                        ]);
                    }    
                }
                else
                {
                    $this->set_response([
                            'status' => 'error',
                            'code' => 102,
                            'message' => 'Data tidak ditemukan'
                        ]);
                }                
            } else {
                $this->set_response([
                    'status' => 'error',
                    'code' => 102,
                    'message' => 'Data tidak ditemukan'
                ]);
            }
            
        }else{
            $this->errorForbbiden();
            return;
        }
    }

     function _toWord($x){
        $x=abs($x);
        $angka=array("","satu","dua","tiga","empat","lima",
        "enam","tujuh","delapan","sembilan","sepuluh","sebelas");
        $temp="";
        if($x<12){
            $temp=" ".$angka[$x];
        }elseif($x<20){
            $temp=$this->_toWord($x-10)." belas";
        }elseif($x<100){
            $temp=$this->_toWord($x/10)." puluh".$this->_toWord($x%10);
        }elseif($x<200){
            $temp=" seratus".$this->_toWord($x-100);
        }elseif($x<1000){
            $temp=$this->_toWord($x/100)." ratus".$this->_toWord($x%100);
        }elseif($x<2000){
            $temp=" seribu".$this->_toWord($x-1000);
        }elseif($x<1000000){
            $temp=$this->_toWord($x/1000)." ribu".$this->_toWord($x%1000);
        }elseif($x<1000000000){
            $temp=$this->_toWord($x/1000000)." juta".$this->_toWord($x%1000000);
        }elseif($x<1000000000000){
            $temp=$this->_toWord($x/1000000000)." milyar".$this->_toWord(fmod($x,1000000000));
        }elseif($x<1000000000000000){
            $temp=$this->_toWord($x/1000000000000)." trilyun".$this->_toWord(fmod($x,1000000000000));
        }    
            return $temp;
    }
 
 
    function _toBilangan($x, $style=2){
        if($x<0){
            $hasil="minus ".trim(_toWord($x));
        }else{
            $hasil=trim($this->_toWord($x));
        }    
        switch($style){
            case 1:
                $hasil=strtoupper($hasil);
                break;
            case 2:
                $hasil=strtolower($hasil);
                break;
            case 3:
                $hasil=ucwords($hasil);
                break;
            default:
                $hasil=ucfirst($hasil);
                break;
        }    
        return $hasil;
    }

    function _toDay($strDate)
    {
        $day = date('D', strtotime($strDate));
        $dayList = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );
        return $dayList[$day];
    }

    function _toBulan($bulan)
    {
        Switch ($bulan){
            case 1 : $bulan="Januari";
                Break;
            case 2 : $bulan="Februari";
                Break;
            case 3 : $bulan="Maret";
                Break;
            case 4 : $bulan="April";
                Break;
            case 5 : $bulan="Mei";
                Break;
            case 6 : $bulan="Juni";
                Break;
            case 7 : $bulan="Juli";
                Break;
            case 8 : $bulan="Agustus";
                Break;
            case 9 : $bulan="September";
                Break;
            case 10 : $bulan="Oktober";
                Break;
            case 11 : $bulan="November";
                Break;
            case 12 : $bulan="Desember";
                Break;
        }
        return $bulan;
    }     
   

    /**
     * Param templateData:
     * - nama
     * - totalKewajiban
     * - reffSwitching
     * - biayaTransaksi
     * - va
     * - tglExpired
     * - namaCabang
     * - alamatCabang
     * - telpCabang
     * - item (json)
     *
     * @param $token object user token
     * @param $templateData template data
     * @return void
     */
    function sendNotifVA($token, $templateData)
    {
        $template = $this->generateNotif($templateData);

        $emailTemplate   = $template['email'];
        $mobileTemplate  = $template['mobile'];
        $minimalTemplate = $template['minimal'];

        $totalBayar = $templateData['totalKewajiban'] + $templateData['biayaTransaksi'];
        $fTotalBayar = number_format($totalBayar, 0, ",", ".");

        $subtitle = "Segera Bayar Rp " .$fTotalBayar. " ke " .$templateData['va'];

        // Simpan notifikasi baru
        $idNotif = $this->NotificationModel->add($token->id,
            NotificationModel::TYPE_MPO,
            NotificationModel::CONTENT_TYPE_HTML,
            $this->ConfigModel->getNamaProduk('OP', '37'),
            $subtitle,
            $mobileTemplate,
            $minimalTemplate,
            "ML");

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id)
            , [
            "id"      => $idNotif,
            "tipe"    => "ML",
            "title"   => $this->ConfigModel->getNamaProduk('OP', '37'),
            "tagline" => $subtitle,
            "content" => "Bayar ke " . $templateData['va'] . " sebelum " . $templateData['tglExpired'],
            "token"   => $token->no_hp,
        ]);

        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmail($token->email, "Pembelian Mulia", $emailTemplate);

        //Set response
        $this->set_response([
            'status'  => 'success',
            'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
            'data'    => [
                'idTransaksi'    => $templateData['reffSwitching'],
                'virtualAccount' => $templateData['va'],
                'expired'        => $templateData['tglExpiredUnformated'],
                'now'            => date('Y-m-d H:i:s'),
            ],
        ], 200);
    }

    /**
     * Generate template notifikasi mobile, email dan versi minify untuk content webview
     * @param array $data
     * @return void
     */
    public function generateNotif($data)
    {
        $subject = "Pembelian Logam Mulia";

        $content = $this->load->view('mail/mulia/inquiry', $data, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }

    
}
