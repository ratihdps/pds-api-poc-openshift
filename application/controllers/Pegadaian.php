<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Pegadaian extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User');
        $this->load->library('form_validation');
        $this->load->helper('Pegadaian');
    }
    
     public function coreRequest($path, $data, $operationId)
    {
        
        $tokenChar = Pegadaian::getCoreToken();
        if($tokenChar == null ){
            return (object) array(
                //set response code selain 00 untuk menandakan error
                'responseCode' => '0000'
            );
        }
        
        $dataSend = json_encode($data);
        
        $process = curl_init("https://devpds.pegadaian.co.id:9090".$path);
        curl_setopt($process, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $tokenChar
        ));

        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_PROXY, '');
        curl_setopt($process, CURLOPT_POSTFIELDS, $dataSend);
        curl_setopt($process, CURLOPT_HTTPPROXYTUNNEL, false);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($process, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($process);        
        
        
        if($return == false){
            log_message('error', 'Curl error: ' . curl_error($process));
            
        }else{
            $responseData = json_decode($return);
            return $responseData;
            
        }        
        curl_close($process);        
    }
}