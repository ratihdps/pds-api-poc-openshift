<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Mpo extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $models = [
            'User',
            'Otp',
            'BankModel',
            'PaymentModel',
            'NotificationModel',
            'MpoModel',
            'ConfigModel'
        ];
        $this->load->model($models);
        $this->load->library('form_validation');
        $this->load->helper('message');
    }

    //Inquiry Pembayaran
    public function pembayaran_post() 
    {
        log_message('debug', __FUNCTION__ .' Inquiry Pembayaran MPO ' . 'Start');
        $token = $this->getToken();
        if ($token) {
            $setData = [
                'kode_biller' => $this->post('kode_biller'),
                'kode_mpo'    => $this->post('kode_mpo'),
                'norek'       => $this->post('norek'),
                'jumlah'      => $this->post('jumlah'),
                'kode_area'   => $this->post('kode_area'),
                'group'       => $this->post('group'),
            ];

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('kode_biller', 'kode_biller', 'required|numeric');
            $this->form_validation->set_rules('kode_mpo', 'kode_mpo', 'required|numeric');
            $this->form_validation->set_rules('norek', 'norek', 'required|numeric');
            $this->form_validation->set_rules('group', 'group', 'required');
            $this->form_validation->set_rules('jumlah', 'jumlah', 'numeric');

            if ($this->form_validation->run() == false) {
                $this->set_response([
                    'status'  => 'error',
                    'message' => 'Invalid Input',
                    'code'    => 101,
                    'errors'  => $this->form_validation->error_array(),
                ], 200);
                log_message('debug', __FUNCTION__  . ' Inquiry Pembayaran MPO ERROR'. json_encode($this->form_validation->error_array()));
            } else {
                $kodeBiller     = $this->post('kode_biller');
                $kodeLayananMpo = $this->post('kode_mpo');
                $norek          = $this->post('norek');
                $jumlah         = $this->post('jumlah');
                $kodeArea       = $this->post('kode_area');

                $group      = $this->post('group');
                $idTambahan = null;

                //Check apakah configurasi memakai idTambahan atau tidak
                $cekConfig = $this->MpoModel->getMpoProduct($kodeLayananMpo, $kodeBiller, $group);

                if (empty($cekConfig)) {
                    log_message('debug', __FUNCTION__ . ' Inquiry Pembayaran MPO ERROR Invalid data mpo');

                    return $this->send_response('error', 'Invalid data mpo', '', 101);
                }

                if ($cekConfig->idTambahan == '1') {
                    $idTambahan = $jumlah;
                }

                $data = [
                    'jenisTransaksi' => 'MP',
                    'kodeBiller'     => $kodeBiller,
                    'kodeLayananMpo' => $kodeLayananMpo,
                    'norek'          => $norek,
                    'channelId'      => $token->channelId,
                    'flag'           => 'K',
                    'group'          => $group == 'telkom' ? 'pascabayar' : $group,
                    'idTambahan'     => $idTambahan,
                    'amount'         => $jumlah
                ];

                //Inquiry ke core
                $inquiry = $this->inquiryMpoSeluler($data);
                if ($inquiry->responseCode == '00') {
                    $data = json_decode($inquiry->data);
                    $dataArr                = (array) $data;
                    $dataArr['user_AIID']   = $token->id;
                    $dataArr['bookingCode'] = sprintf("%06d", mt_rand(1, 999999));
                
                    if(!isset($dataArr['idTambahan'])){
                        $dataArr['idTambahan'] = $idTambahan;
                    }
                    $dataArr['amount'] = $jumlah;
                    $descErr = [];                     
                    if (isset($dataArr['dataTagihan']) && $dataArr['dataTagihan'] != null) {
                        $dataTagihanArr         = $dataArr['dataTagihan'];
                        $dataArr['dataTagihan'] = json_encode($dataArr['dataTagihan']);
                    } else {
                        // ini validasi jika array Data tagihan kosong.
                        if ($dataArr['group'] == 'pascabayar') {
                            $this->set_response(array(
                                'status'  => 'warning',
                                'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi.',
                                'code'    => 103,
                                'reason'  => $descErr = [
                                    "responseCode" => $inquiry->responseCode = 12, 
                                    "responseDesc" => $inquiry->responseDesc = 'Tidak Ada Tagihan',
                                    "data" => $inquiry->data = '-'
                                ]
                            ), 200);
                            return;
                        }
                    }

                    $dataArr['norek']          = $norek;
                    $dataArr['jenisTransaksi'] = 'MP';

                    // Store data mpo
                    $code_mpo = $dataArr['kodeLayananMpo'] ?? null;
                    $is_store = $this->MpoModel->storeInquiryByMpo($code_mpo, $dataArr);

                    if (!$is_store) {
                        $this->response(['code' => 101, 'status' => 'error', 'message' => 'Internal Server Error', 'reason' => $inquiry], 200);
                    }

                    $dataArr['idTransaksi'] = $data->reffSwitching;

                    if (isset($dataArr['dataTagihan']) && $dataArr['dataTagihan'] != null) {
                        $dataArr['dataTagihan'] = $dataTagihanArr;
                    }

                    //Get biaya channel untuk masing-masing metode pembayaran
                    $biayaChannel = array(
                        'BNI'     => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '009'),
                        'MANDIRI' => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '008'),
                        'WALLET'  => $this->ConfigModel->getBiayaPayment('MP', 'WALLET', '50', ''),
                        'VA_MANDIRI'  => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '008'),
                        'VA_BCA'  => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '014'),
                    );

                    $dataArr['biayaChannel'] = $biayaChannel;
                    $dataArr['tglTransaksi'] = date('Y-m-d H:i:s');

                    $mpoProduct = $this->MpoModel->getMpoProduct($kodeLayananMpo, $kodeBiller);
                    
                    $plnCode = null;
                    if($mpoProduct->namaLayanan == 'PLN Prepaid'){
                        $plnCode = 0;
                    }else if($mpoProduct->namaLayanan == 'PLN Postpaid'){
                        $plnCode = 1;
                        $dataArr['periode'] = $this->_formatPLNTglPeriode($dataArr['periode']);                        
                    }

                    $dataArr['plnCode'] = $plnCode;
                    $dataArr['product'] = $mpoProduct->namaLayanan;    

                    $this->set_response(array(
                        'status'  => 'success',
                        'message' => '',
                        'data'    => $dataArr,
                    ), 200);
                } else {

                    if (!isset($inquiry->data)) {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi.',
                            'code'    => 103,
                            'reason'  => $inquiry,
                        ), 200);
                        log_message('debug', __FUNCTION__  . ' Inquiry Pembayaran MPO ERROR'. json_encode($inquiry));
                        return;
                    }

                    $data = json_decode($inquiry->data);

                    if (!isset($data->respCodeMpo)) {
                        $this->set_response(array(
                            'status'  => 'warning',
                            'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi.',
                            'code'    => 103,
                            'reason'  => $inquiry,
                        ), 200);
                        log_message('debug', __FUNCTION__  . ' Inquiry Pembayaran MPO ERROR'. json_encode($inquiry));
                        return;
                    }

                    $inquiry->responseCode = $data->respCodeMpo;

                    if ($inquiry->responseCode == '13') {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Voucher tidak tersedia',
                            'code'    => 103,
                        ), 200);
                    } else if ($inquiry->responseCode == '70') {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Maaf Nilai Kupon isi ulang tidak tersedia',
                            'code'    => 103,
                        ), 200);
                    } else if ($inquiry->responseCode == '91') {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Transaksi tidak dapat di proses sementara waktu',
                            'code'    => 103,
                        ), 200);
                    } else if ($inquiry->responseCode == '89') {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Transaksi tidak dapat di proses sementara waktu',
                            'code'    => 103,
                        ), 200);
                    } else if ($inquiry->responseCode == 'A0') {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Time out respon dari Biller Provider',
                            'code'    => 103,
                        ), 200);
                    } else if ($inquiry->responseCode == '14') {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Maaf anda salah memasukkan nomor telepon',
                            'code'    => 103,
                        ), 200);
                    } else if ($inquiry->responseCode == '79') {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Nomor telepon terblokir',
                            'code'    => 103,
                        ), 200);
                    } else if ($inquiry->responseCode == '81') {
                        $this->set_response(array(
                            'status'  => 'error',
                            'message' => 'Nomor telepon terblokir',
                            'code'    => 103,
                        ), 200);
                    } else {
                        $this->response(array(
                            'code'    => 101,
                            'status'  => 'error',
                            'message' => 'Internal Server Error',
                            'reason'  => $inquiry,
                        ), 200);
                    }
                    log_message('debug', __FUNCTION__  . ' Inquiry Pembayaran MPO'. json_encode($inquiry));
                }

            }

        } else {
            $this->errorForbbiden();
            log_message('debug', __FUNCTION__  . ' Inquiry Pembayaran MPO'. json_encode($this->errorForbbiden()));
        }
        log_message('debug', __FUNCTION__ . ' Inquiry Pembayaran MPO ' . 'End');
    }

    public function seluler_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if ($token) {
            $setData = array(
                'jenis_transaksi' => $this->post('jenis_transaksi'),
                'kode_bank'       => $this->post('kode_bank'),
                'kode_biller'     => $this->post('kode_biller'),
                'kode_mpo'        => $this->post('kode_mpo'),
                'no_hp'           => $this->post('no_hp'),
                'norek_wallet'    => $this->post('norek_wallet'),
                'wallet_id'       => $this->post('wallet_id'),
                'payment_method'  => $this->post('payment_method'),
            );

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('jenis_transaksi', 'jenis_transaksi', 'required|exact_length[2]');
            $this->form_validation->set_rules('kode_biller', 'kode_biller', 'required|numeric');
            $this->form_validation->set_rules('kode_mpo', 'kode_mpo', 'required|numeric');
            $this->form_validation->set_rules('no_hp', 'no_hp', 'required|numeric');

            if ($this->form_validation->run() == false) {
                $this->set_response(array(
                    'status'  => 'error',
                    'message' => 'Invalid Input',
                    'code'    => 101,
                    'errors'  => $this->form_validation->error_array(),

                ), 200);
            } else {
                $jenisTransaksi   = $this->post('jenis_transaksi');
                $kodeBankPembayar = $this->post('kode_bank');
                $kodeBiller       = $this->post('kode_biller');
                $kodeLayananMpo   = $this->post('kode_mpo');
                $noHp             = $this->post('no_hp');
                $norekWallet      = $this->post('norek_wallet');
                $walletId         = $this->post('wallet_id');
                $paymentMethod    = $this->post('payment_method');

                $data = array(
                    'jenisTransaksi' => $jenisTransaksi,
                    'kodeBiller'     => $kodeBiller,
                    'kodeLayananMpo' => $kodeLayananMpo,
                    'norek'          => $noHp,
                    'channelId'      => $token->channelId,
                    'group'          => 'seluler',
                    'flag'           => 'K',
                );

                if ($paymentMethod == 'BANK') {
                    $data['kodeBankPembayar'] = $kodeBankPembayar;
                } else if ($this->post('payment_method') == 'WALLET') {
                    $data['norekWallet'] = $norekWallet;
                    $data['walletId']    = $walletId;
                }

                //Inquiry ke core
                $inquiry = $this->inquiryMpoSeluler($data);

                if ($inquiry->responseCode == '00') {
                    $data = json_decode($inquiry->data);

                    $dataArr                     = (array) $data;
                    $dataArr['user_AIID']        = $token->id;
                    $dataArr['jenisTransaksi']   = $jenisTransaksi;
                    $dataArr['kodeBankPembayar'] = $kodeBankPembayar;
                    $dataArr['kodeBiller']       = $kodeBiller;
                    $dataArr['kodeLayananMpo']   = $kodeLayananMpo;
                    $dataArr['noHp']             = $noHp;
                    $dataArr['norekWallet']      = $norekWallet;
                    $dataArr['walletId']         = $walletId;
                    $dataArr['bookingCode']      = sprintf("%06d", mt_rand(1, 999999));
                    //$dataArr['paymentMethod'] = $paymentMethod;

                    $this->MpoModel->add($dataArr);
                    $dataArr['idTransaksi'] = $data->reffSwitching;

                    $mpoProduct = $this->MpoModel->getMpoProduct($kodeLayananMpo, $kodeBiller);
                    $dataArr['product'] = $mpoProduct->namaLayanan;

                    // Get biaya channel dari config
                    $biayaChannel = [
                        'BNI'     => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '009'),
                        'MANDIRI' => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '008'),
                        'WALLET'  => $this->ConfigModel->getBiayaPayment('MP', 'WALLET', '50', ''),
                        'VA_MANDIRI'  => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '008'),
                        'VA_BCA'  => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '014'),
                        'VA_PERMATA' => $this->ConfigModel->getBiayaPayment('MP', 'BANK', '50', '013')
                    ];
                    $dataArr['biayaChannel'] = $biayaChannel;
                    $dataArr['tglTransaksi'] = date('Y-m-d H:i:s');

                    $this->set_response(array(
                        'status'  => 'success',
                        'message' => '',
                        'data'    => $dataArr,
                    ), 200);

                } else if ($inquiry->responseCode == '13') {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Voucher tidak tersedia',
                        'code'    => 103,
                    ), 200);
                } else if ($inquiry->responseCode == '70') {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Maaf Nilai Kupon isi ulang tidak tersedia',
                        'code'    => 103,
                    ), 200);
                } else if ($inquiry->responseCode == '91') {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Transaksi tidak dapat di proses sementara waktu',
                        'code'    => 103,
                    ), 200);
                } else if ($inquiry->responseCode == '89') {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Transaksi tidak dapat di proses sementara waktu',
                        'code'    => 103,
                    ), 200);
                } else if ($inquiry->responseCode == 'A0') {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Time out respon dari Biller Provider',
                        'code'    => 103,
                    ), 200);
                } else if ($inquiry->responseCode == '14') {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Maaf anda salah memasukkan nomor telepon',
                        'code'    => 103,
                    ), 200);
                } else if ($inquiry->responseCode == '79') {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Nomor telepon terblokir',
                        'code'    => 103,
                    ), 200);
                } else if ($inquiry->responseCode == '81') {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Nomor telepon terblokir',
                        'code'    => 103,
                    ), 200);
                } else {
                    $this->response(array(
                        'code'    => 101,
                        'status'  => 'error',
                        'message' => 'Internal Server Error',
                        'reason'  => $inquiry,
                    ), 200);
                }
            }
        } else {
            $this->errorForbbiden();
        }
    }

    public function payment_post()
    {
        log_message('debug', __FUNCTION__ . ' Pembayaran MPO ' . 'Start');
        $token = $this->getToken();
        if ($token) {
            
            //Get user data
            $user = $this->User->getUser($token->id);

            $setData = array(
                'id_transaksi'   => $this->post('id_transaksi'),
                'payment'        => $this->post('payment'),
                'booking_code'   => $this->post('booking_code'),
                'card_number'    => $this->post('card_number'),
                'token_response' => $this->post('token_response'),
                'pin'            => $this->post('pin'),
                'va'             => $this->post('va')
            );

            $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
            $this->form_validation->set_rules('payment', 'payment', 'required');

            $paymentMethod = $this->post('payment');

            if ($paymentMethod == 'MANDIRI') {
                $this->form_validation->set_rules('booking_code', 'booking_code', 'required');
                $this->form_validation->set_rules('card_number', 'card_number', 'required');
                $this->form_validation->set_rules('token_response', 'token_response', 'required');
            }

            if ($paymentMethod == 'WALLET' || $paymentMethod == 'GCASH') {
                $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');
            }

            if ($paymentMethod == 'GCASH') {
                $this->form_validation->set_rules('va', 'va', 'required|numeric');
            }

            if ($this->form_validation->run() == false) {
                $this->set_response(array(
                    'status'  => 'error',
                    'message' => 'Invalid Input',
                    'code'    => 101,
                    'errors'  => $this->form_validation->error_array(),
                ), 200);
            } else {
                $idTransaksi = $this->post('id_transaksi');

                $mpo = $this->MpoModel->getMpo($idTransaksi);
                log_message('debug', 'Payment MPO Bank reffSwitching:' . $idTransaksi);

                if ($mpo) {

                    //Get info product berdasarkan kode layanan dan kode biller
                    $mpoProduct = $this->MpoModel->getMpoProduct($mpo->kodeLayananMpo, $mpo->kodeBiller);                    
                    $product = $mpoProduct->namaLayanan;
                    $mpoGroup   = $mpoProduct->groups;

                    $newTotalKewajiban = $mpo->totalKewajiban + 2500;

                    if ($paymentMethod == 'BNI') 
                    {

                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($mpo->jenisTransaksi, 
                                                                              'BANK', // payment
                                                                              '50',   // kode produk
                                                                              '009'); // kode bank
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($mpo->jenisTransaksi,
                                                                              'BANK', // payment
                                                                              '50',   // kode produk
                                                                              '009'); // kode bank
                        
                        //BNI
                        $billingBNI = $this->createBillingVABNI(
                            $mpo->totalKewajiban + $biayaTransaksi,
                            $user->email,
                            $user->nama,
                            $user->no_hp,
                            'K',
                            $mpo->jenisTransaksi,
                            'Pembayaran MPO',
                            $mpo->norek,
                            '50',
                            $mpo->reffSwitching,
                            $token->channelId
                        );
                        if ($billingBNI->responseCode == '00') {

                            if (!isset($billingBNI->data)) {
                                $this->set_response(array(
                                    'status'  => 'error',
                                    'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                                    'code'    => 103,
                                ), 200);
                                return;
                            }

                            $billingData    = json_decode($billingBNI->data);
                            $virtualAccount = $billingData->virtualAccount;
                            $tglExpired     = $billingData->tglExpired;
                            $updateData     = array(
                                'reffSwitching'    => $mpo->reffSwitching,
                                'virtualAccount'   => $virtualAccount,
                                'tglExpired'       => $tglExpired,
                                'kodeBankPembayar' => '009',
                                'payment'          => $paymentMethod,
                                'biayaTransaksi'   => $biayaTransaksiDisplay,
                            );

                            $this->MpoModel->update($updateData);

                            $dateExpired = new DateTime($tglExpired);

                            $redaksiPayment = $this->ConfigModel->getRedaksiPayment('009', 
                                                                                    $virtualAccount,
                                                                                    $newTotalKewajiban);
                            $templateData = [
                                'paymentMethod'  => 'BNI',
                                'nama'           => $mpo->nama,
                                'va'             => $virtualAccount,
                                'amount'         => $newTotalKewajiban,
                                'tglExpired'     => $dateExpired->format('d/m/Y H:i:s'),
                                'trxId'          => $idTransaksi,
                                'product'        => $product,
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'totalKewajiban' => $mpo->totalKewajiban,
                                'tglTransaksi'   => $mpo->updated_at,
                                'noHp'           => $mpo->noHp,
                                'nominalPulsa'   => filter_var($product, FILTER_SANITIZE_NUMBER_INT),
                                'mpoProduct'     => $mpoProduct,
                                'mpoGroup'       => $mpoGroup,
                                'redaksi'        => $redaksiPayment,
                                'idTambahan'     => $mpo->idTambahan,
                                'norek'          => $mpo->norek,
                                'namaPelanggan'  => $mpo->namaPelanggan,
                                'tarifDaya'      => $mpo->segmen . '/' . $mpo->power,
                                'periode'        => $mpo->periode,
                                'keterangan2'    => $mpo->keterangan2,
                                'administrasi'   => $mpo->administrasi,
                                'npwp'           => $mpo->npwp,
                                'jumlahTagihan'  => $mpo->jumlahTagihan,
                                'hargaJual'      => $mpo->hargaJual,   
                                'jumlahBill'     => $mpo->jumlahBill                              
                            ];

                            $dataTagihan = null;
                            if($mpo->dataTagihan !== ''){
                                $dataTagihan = json_decode($mpo->dataTagihan);
                                $templateData['dataTagihan'] = $dataTagihan;
                            }

                            
                            $template = $this->generateNotif($templateData);

                            $emailTemplate   = $template['email'];
                            $mobileTemplate  = $template['mobile'];
                            $minimalTemplate = $template['minimal'];

                            $subtitle = "Segera Bayar Rp " . 
                                        number_format($mpo->totalKewajiban+$biayaTransaksiDisplay, 0, ",", ".") . " ke " .
                                        $virtualAccount;

                            //Simpan notifikasi baru
                            $productName = $this->ConfigModel->getNamaProduk('MP', '50');                            
                            
                            // $notif = new NotificationModel();

                            // $notif->userId = $token->id;
                            // $notif->type = NotificationModel::TYPE_MPO;
                            // $notif->contentType = NotificationModel::CONTENT_TYPE_HTML;
                            // $notif->title = $productName;
                            // $notif->tagline = $subtitle;
                            // $notif->body = $mobileTemplate;
                            // $notif->bodyMin = $minimalTemplate;
                            // $notif->jenisTransaksi = 'MP';
                            // $idNotif = $notif->save();
                            
                            $idNotif = $this->NotificationModel->add($token->id,
                                                                     NotificationModel::TYPE_MPO,
                                                                     NotificationModel::CONTENT_TYPE_HTML,
                                                                     $productName,
                                                                     $subtitle,
                                                                     $mobileTemplate,
                                                                     $minimalTemplate,
                                                                     "MP");

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                    "id"      => $idNotif,
                                    "tipe"    => "MP",
                                    "title"   => $this->ConfigModel->getNamaProduk('MP', '50'),
                                    "tagline" => $subtitle,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                    "token"   => $token->no_hp,
                                ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail($token->email, "Pembelian " . $product, $emailTemplate);

                            //Set response
                            $this->set_response([
                                'status'  => 'success',
                                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                'data'    => [
                                    'idTransaksi'    => $idTransaksi,
                                    'virtualAccount' => $virtualAccount,
                                    'expired'        => $tglExpired,
                                    'now'            => date('Y-m-d H:i:s'),
                                    'redaksiPayment' => $redaksiPayment                       
                                ],
                            ], 200);

                        } else {
                            $this->set_response([
                                'status'  => 'error',
                                'message' => 'Something went wrong. Please try again later.',
                                'code'    => 103,
                                'reason'  => $billingBNI,
                            ], 200);
                            log_message('debug', __FUNCTION__ . ' Pembayaran MPO Error ' . json_encode($billingBNI));
                        }
                    } 
                    else if ($paymentMethod == 'MANDIRI') 
                    {

                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($mpo->jenisTransaksi, 'BANK', '50', '008');
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($mpo->jenisTransaksi, 'BANK', '50', '008');

                        log_message('debug', 'Mandiri Click Pay');

                        $bookingCode   = $this->post('booking_code');
                        $cardNumber    = $this->post('card_number');
                        $tokenResponse = $this->post('token_response');

                        $newTotalKewajiban = $mpo->totalKewajiban + $biayaTransaksi;

                        //Bayar dengan mandiri click pay
                        $clickPay = $this->mandiriClickPay(
                            $newTotalKewajiban,
                            $bookingCode,
                            $cardNumber,
                            'MP',
                            'Pembayaran MPO',
                            $user->no_hp,
                            $mpo->norek,
                            '50',
                            $tokenResponse,
                            $mpo->reffSwitching,
                            $token->channelId);

                        if ($clickPay->responseCode == '00') {

                            $clickpayData = json_decode($clickPay->data);

                            $updateData['reffSwitching']    = $mpo->reffSwitching;
                            $updateData['bookingCode']      = $bookingCode;
                            $updateData['cardNumber']       = $cardNumber;
                            $updateData['tokenResponse']    = $tokenResponse;
                            $updateData['reffBiller']       = $clickpayData->reffBiller;
                            $updateData['kodeBankPembayar'] = '008';
                            $updateData['payment']          = $paymentMethod;
                            $updateData['biayaTransaksi']   = $biayaTransaksiDisplay;

                            //update data pembayaran
                            $this->MpoModel->update($updateData);

                            //Set response
                            $this->set_response([
                                'status'  => 'success',
                                'message' => 'Pembelian ' . $product . ' berhasil',
                                'data'    => array(
                                    'reffBiller' => $clickpayData->reffBiller,
                                    'redaksiPayment' => ''
                                ),
                            ], 200);

                        } else {
                            $this->set_response(array(
                                'status'  => 'error',
                                'code'    => 103,
                                'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                'reason'  => $clickPay,
                            ));
                            log_message('debug', __FUNCTION__ . ' Pembayaran MPO Error ' . json_encode($clickPay));
                        }
                    } 
                    else if ($paymentMethod == 'WALLET' || $paymentMethod == 'GCASH') 
                    {

                        $pin = $this->post('pin');
                        $va = $this->post('va');

                        //Check user pin
                        if (!$this->User->isValidPIN2($token->id, $pin)) {

                            $this->set_response(array(
                                'status'  => 'error',
                                'message' => 'PIN tidak valid',
                                'code'    => 102,
                            ), 200);

                            return;
                        }

                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($mpo->jenisTransaksi, 'WALLET', '62', '');
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($mpo->jenisTransaksi, 'WALLET', '62', '');

                        $paymentData = array(
                            'amount'         => $mpo->totalKewajiban + $biayaTransaksi,
                            'jenisTransaksi' => 'MP',
                            'reffSwitching'  => $mpo->reffSwitching,
                            'channelId'      => $token->channelId,
                            'norek'          => $mpo->norek,
                            'flag'           => 'K',
                        );

                        if($paymentMethod == 'WALLET'){
                            $paymentData['norekWallet'] = $user->norek;
                            $paymentData['walletId'] = $user->no_hp;
                            $paymentData['paymentMethod'] = 'WALLET';
                            $paymentData['administrasi']   = $mpo->administrasi;
                            $paymentData['counter'] = 1;
                            $paymentData['kodeBiller'] = $mpo->kodeBiller;
                            $paymentData['kodeLayananMpo'] = $mpo->kodeLayananMpo;
                            $paymentData['noHp'] = $mpo->noHp;
                            $paymentData['group'] =  $mpo->group;
                        } else if($paymentMethod == 'GCASH'){
                            $paymentData['gcashId'] = $va;
                            $paymentData['paymentMethod'] = 'GCASH';
                            $paymentData['productCode'] = '50';
                        }

                        $walletPayment = $this->paymentMpoSeluler($paymentData);

                        if ($walletPayment->responseCode == '00') {

                            if (!isset($walletPayment->data)) {
                                $this->set_response(array(
                                    'status'  => 'error',
                                    'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                                    'code'    => 103,
                                ), 200);
                                return;
                            }

                            $walletData = json_decode($walletPayment->data, true);

                            $walletData['payment']        = $paymentMethod;
                            $walletData['reffSwitching']  = $idTransaksi;
                            $walletData['is_paid']        = '1';
                            $walletData['biayaTransaksi'] = $biayaTransaksiDisplay;

                            //Update wallet data
                            $this->MpoModel->update($walletData);

                            //Send success notification
                            $templateData = array(
                                'paymentMethod'  => $paymentMethod,
                                'nama'           => $mpo->nama,
                                'amount'         => $mpo->amount,
                                'trxId'          => $idTransaksi,
                                'product'        => $product,
                                'reffMpo'        => isset($walletData['reffMpo']) ? $walletData['reffMpo'] : '',
                                'sid'            => isset($walletData['sid']) ? $walletData['sid'] : '-',
                                'serialNumber'   => isset($walletData['serialNumber']) ? $walletData['serialNumber'] : '-',
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'totalKewajiban' => $mpo->totalKewajiban,
                                'tglTransaksi'   => $mpo->updated_at,
                                'noHp'           => $mpo->noHp,
                                'nominalPulsa'   => filter_var($product, FILTER_SANITIZE_NUMBER_INT),
                                'mpoProduct'     => $mpoProduct,
                                'mpoGroup'       => $mpoGroup,
                                'redaksi'        => [],
                                'dataTagihan'    => $mpo->dataTagihan,
                                'idTambahan'     => $mpo->idTambahan,
                                'norek'          => $mpo->norek,
                                'namaPelanggan'  => $mpo->namaPelanggan,
                                'tarifDaya'      => $mpo->segmen . '/' . $mpo->power,
                                'periode'        => $mpo->periode,
                                'keterangan2'    => $mpo->keterangan2,
                                'administrasi'   => $mpo->administrasi,
                                'npwp'           => $mpo->npwp,
                                'jumlahTagihan'  => $mpo->jumlahTagihan,
                                'hargaJual'      => $mpo->hargaJual,
                                'jumlahBill'     => $mpo->jumlahBill
                            );

                            $template = $this->generateNotif($templateData);

                            $emailTemplate   = $template['email'];
                            $mobileTemplate  = $template['mobile'];
                            $minimalTemplate = $template['minimal'];

                            //Simpan notifikasi baru
                            $idNotif = $this->NotificationModel->add(
                                $token->id,
                                NotificationModel::TYPE_MPO,
                                NotificationModel::CONTENT_TYPE_HTML,
                                $this->ConfigModel->getNamaProduk('MP', '50'),
                                "Pembelian " . $product . " berhasil",
                                $mobileTemplate,
                                $minimalTemplate,
                                "MP"
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                    "id"      => $idNotif,
                                    "tipe"    => "MP",
                                    "title"   => $this->ConfigModel->getNamaProduk('MP', '50'),
                                    "tagline" => "Pembelian " . $product . " berhasil",
                                    "content" => "",
                                    "token"   => $token->no_hp,
                                ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail($token->email, "Pembelian " . $product, $emailTemplate);

                            //Update Saldo Wallet
                            if($paymentMethod == 'WALLET'){
                                $saldoWallet          = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                $walletData['wallet'] = $saldoWallet;
                                $walletData['redaksiPayment'] = '';    
                            }

                            //Set response
                            $this->set_response([
                                'status'  => 'success',
                                'message' => 'Pembelian ' . $product . ' berhasil',
                                'data'    => $walletData,
                            ], 200);

                        } else {
                            $data = json_decode($walletPayment->data);
                            if(isset($data->resCodeMpo)){
                                $walletPayment->responseCode = $data->respCodeMpo;

                                if ($walletPayment->responseCode == '13') {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Voucher tidak tersedia',
                                        'code'    => 103,
                                    ), 200);
                                } else if ($walletPayment->responseCode == '70') {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Maaf Nilai Kupon isi ulang tidak tersedia',
                                        'code'    => 103,
                                    ), 200);
                                } else if ($walletPayment->responseCode == '91') {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Transaksi tidak dapat di proses sementara waktu',
                                        'code'    => 103,
                                    ), 200);
                                } else if ($walletPayment->responseCode == '89') {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Transaksi tidak dapat di proses sementara waktu',
                                        'code'    => 103,
                                    ), 200);
                                } else if ($walletPayment->responseCode == 'A0') {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Time out respon dari Biller Provider',
                                        'code'    => 103,
                                    ), 200);
                                } else if ($walletPayment->responseCode == '14') {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Maaf anda salah memasukkan nomor telepon',
                                        'code'    => 103,
                                    ), 200);
                                } else if ($walletPayment->responseCode == '79') {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Nomor telepon terblokir',
                                        'code'    => 103,
                                    ), 200);
                                } else if ($walletPayment->responseCode == '81') {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Nomor telepon terblokir',
                                        'code'    => 103,
                                    ), 200);
                                } else {
                                    $this->set_response(array(
                                        'status'  => 'error',
                                        'message' => 'Nomor telepon terblokir',
                                        'code'    => 103,
                                        'reason'  => $walletPayment,
                                    ), 200);
                                }
                            } else {
                                $this->set_response([
                                    'status' => 'error',
                                    'message' => $walletPayment->responseCode,
                                    'code' => 103,
                                    'reason' => $walletPayment
                                ]);
                            }
                            log_message('debug', __FUNCTION__ . ' Pembayaran MPO Error ' . json_encode($walletPayment));
                            
                        }
                    }
                    else if ($paymentMethod == 'VA_BCA' || $paymentMethod == 'VA_MANDIRI' || $paymentMethod == 'VA_BRI') 
                    {
                        $kodeBank = '';
                        if($paymentMethod == 'VA_BCA'){
                            $kodeBank = '014';
                        } else if($paymentMethod == 'VA_MANDIRI') {
                            $kodeBank = '008';
                        } else if($paymentMethod == 'VA_BRI') {
                            $kodeBank = '002';
                        }
                        
                        log_message('debug','Kode Bank'.$kodeBank);
                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment($mpo->jenisTransaksi, 
                                                                              'BANK', // payment
                                                                              '50',   // kode produk
                                                                              $kodeBank); // kode bank
                        $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment($mpo->jenisTransaksi,
                                                                              'BANK', // payment
                                                                              '50',   // kode produk
                                                                              $kodeBank); // kode bank
                        
                        $createBillingData = [
                            'channelId' => $token->channelId,
                            'amount' => $mpo->totalKewajiban + $biayaTransaksi,
                            'customerEmail' => $user->email,
                            'customerName' => $user->nama,
                            'customerPhone' => $user->no_hp,
                            'flag' => 'K',
                            'jenisTransaksi' => $mpo->jenisTransaksi,
                            'kodeProduk' => '50',
                            'norek' => $mpo->norek,
                            'keterangan' => 'Pembayaran MPO',
                            'reffSwitching' => $mpo->reffSwitching,
                            'kodeBank' => $kodeBank
                        ];
                        
                        $billing = $this->createBillingPegadaian($createBillingData);

                        if ($billing->responseCode == '00') 
                        {
                            if (!isset($billing->data)) {
                                $this->set_response(array(
                                    'status'  => 'error',
                                    'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                                    'code'    => 103,
                                ), 200);
                                return;
                            }

                            $billingData    = json_decode($billing->data);
                            $virtualAccount = $billingData->vaNumber;
                            $tglExpired     = $billingData->tglExpired;
                            $updateData     = array(
                                'reffSwitching'    => $mpo->reffSwitching,
                                'virtualAccount'   => $virtualAccount,
                                'tglExpired'       => $tglExpired,
                                'kodeBankPembayar' => $kodeBank,
                                'payment'          => $paymentMethod,
                                'biayaTransaksi'   => $biayaTransaksiDisplay,
                            );

                            $this->MpoModel->update($updateData);

                            $dateExpired = new DateTime($tglExpired);

                            $redaksiPayment = $this->ConfigModel->getRedaksiPayment($kodeBank, 
                                                                                    $virtualAccount,
                                                                                    $newTotalKewajiban);
                            $templateData = [
                                'paymentMethod'  => $paymentMethod,
                                'nama'           => $mpo->nama,
                                'va'             => $virtualAccount,
                                'amount'         => $newTotalKewajiban,
                                'tglExpired'     => $dateExpired->format('d/m/Y H:i:s'),
                                'trxId'          => $idTransaksi,
                                'product'        => $product,
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'totalKewajiban' => $mpo->totalKewajiban,
                                'tglTransaksi'   => $mpo->updated_at,
                                'noHp'           => $mpo->noHp,
                                'nominalPulsa'   => filter_var($product, FILTER_SANITIZE_NUMBER_INT),
                                'mpoProduct'     => $mpoProduct,
                                'mpoGroup'       => $mpoGroup,
                                'redaksi'        => $redaksiPayment,
                                'idTambahan'     => $mpo->idTambahan,
                                'norek'          => $mpo->norek,
                                'namaPelanggan'  => $mpo->namaPelanggan,
                                'tarifDaya'      => $mpo->segmen . '/' . $mpo->power,
                                'periode'        => $mpo->periode,
                                'keterangan2'    => $mpo->keterangan2,
                                'administrasi'   => $mpo->administrasi,
                                'npwp'           => $mpo->npwp,
                                'jumlahTagihan'  => $mpo->jumlahTagihan,
                                'hargaJual'      => $mpo->hargaJual,   
                                'jumlahBill'     => $mpo->jumlahBill                              
                            ];

                            $dataTagihan = null;
                            if($mpo->dataTagihan !== ''){
                                $dataTagihan = json_decode($mpo->dataTagihan);
                                $templateData['dataTagihan'] = $dataTagihan;
                            }

                            
                            $template = $this->generateNotif($templateData);

                            $emailTemplate   = $template['email'];
                            $mobileTemplate  = $template['mobile'];
                            $minimalTemplate = $template['minimal'];

                            $subtitle = "Segera Bayar Rp " . 
                                        number_format($mpo->totalKewajiban+$biayaTransaksiDisplay, 0, ",", ".") . " ke " .
                                        $virtualAccount;

                            //Simpan notifikasi baru
                            $productName = $this->ConfigModel->getNamaProduk('MP', '50');                            

                            
                            $idNotif = $this->NotificationModel->add($token->id,
                                                                     NotificationModel::TYPE_MPO,
                                                                     NotificationModel::CONTENT_TYPE_HTML,
                                                                     $productName,
                                                                     $subtitle,
                                                                     $mobileTemplate,
                                                                     $minimalTemplate,
                                                                     "MP");

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                    "id"      => $idNotif,
                                    "tipe"    => "MP",
                                    "title"   => $this->ConfigModel->getNamaProduk('MP', '50'),
                                    "tagline" => $subtitle,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                    "token"   => $token->no_hp,
                                ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail($token->email, "Pembelian " . $product, $emailTemplate);

                            //Set response
                            $this->set_response([
                                'status'  => 'success',
                                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                'data'    => [
                                    'idTransaksi'    => $idTransaksi,
                                    'virtualAccount' => $virtualAccount,
                                    'expired'        => $tglExpired,
                                    'now'            => date('Y-m-d H:i:s'),
                                    'redaksiPayment' => $redaksiPayment                       
                                ],
                            ], 200);

                        } 
                        else
                        {
                            $this->set_response([
                                'status'  => 'error',
                                'message' => 'Something went wrong. Please try again later.',
                                'code'    => 103,
                                'reason'  => $billing,
                            ], 200);
                            log_message('debug', __FUNCTION__ . ' Pembayaran MPO Error ' . json_encode($billing));
                        }
                    }
                    else if ($paymentMethod == 'VA_PERMATA')
                    {
                        $kodeBank = '013';                        
                        $biayaTransaksi = $this->ConfigModel
                                               ->getRealBiayaPayment($mpo->jenisTransaksi, 
                                                                 'BANK', // payment
                                                                 '50',   // kode produk
                                                                 $kodeBank); // kode bank
                        $biayaTransaksiDisplay = $this->ConfigModel
                                               ->getBiayaPayment($mpo->jenisTransaksi,
                                                                 'BANK', // payment
                                                                 '50',   // kode produk
                                                                 $kodeBank); // kode bank
                        
                        $createBillingData = [
                            'channelId' => $token->channelId,
                            'amount' => $mpo->totalKewajiban + $biayaTransaksi,
                            'cif' => $user->cif,
                            'customerEmail' => $user->email,
                            'customerName' => $user->nama,
                            'customerPhone' => $user->no_hp,
                            'flag' => 'K',
                            'jenisTransaksi' => $mpo->jenisTransaksi,
                            'productCode' => '50',
                            'norek' => $mpo->norek,
                            'keterangan' => 'Pembayaran MPO',
                            'trxId' => $mpo->reffSwitching,
                        ];
                        
                        $billing = $this->createBillingPermata($createBillingData);

                        if ($billing->responseCode == '00') 
                        {
                            if (!isset($billing->data)) {
                                $this->set_response(array(
                                    'status'  => 'error',
                                    'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                                    'code'    => 103,
                                ), 200);
                                return;
                            }

                            $billingData    = json_decode($billing->data);
                            $virtualAccount = $billingData->virtualAccount;
                            $tglExpired     = $billingData->tglExpired;
                            $updateData     = array(
                                'reffSwitching'    => $mpo->reffSwitching,
                                'virtualAccount'   => $virtualAccount,
                                'tglExpired'       => $tglExpired,
                                'kodeBankPembayar' => $kodeBank,
                                'payment'          => $paymentMethod,
                                'biayaTransaksi'   => $biayaTransaksiDisplay,
                            );

                            $this->MpoModel->update($updateData);

                            $dateExpired = new DateTime($tglExpired);

                            $redaksiPayment = $this->ConfigModel->getRedaksiPayment($kodeBank, 
                                                                                    $virtualAccount,
                                                                                    $newTotalKewajiban);
                            $templateData = [
                                'paymentMethod'  => $paymentMethod,
                                'nama'           => $mpo->nama,
                                'va'             => $virtualAccount,
                                'amount'         => $newTotalKewajiban,
                                'tglExpired'     => $dateExpired->format('d/m/Y H:i:s'),
                                'trxId'          => $idTransaksi,
                                'product'        => $product,
                                'biayaTransaksi' => $biayaTransaksiDisplay,
                                'totalKewajiban' => $mpo->totalKewajiban,
                                'tglTransaksi'   => $mpo->updated_at,
                                'noHp'           => $mpo->noHp,
                                'nominalPulsa'   => filter_var($product, FILTER_SANITIZE_NUMBER_INT),
                                'mpoProduct'     => $mpoProduct,
                                'mpoGroup'       => $mpoGroup,
                                'redaksi'        => $redaksiPayment,
                                'idTambahan'     => $mpo->idTambahan,
                                'norek'          => $mpo->norek,
                                'namaPelanggan'  => $mpo->namaPelanggan,
                                'tarifDaya'      => $mpo->segmen . '/' . $mpo->power,
                                'periode'        => $mpo->periode,
                                'keterangan2'    => $mpo->keterangan2,
                                'administrasi'   => $mpo->administrasi,
                                'npwp'           => $mpo->npwp,
                                'jumlahTagihan'  => $mpo->jumlahTagihan,
                                'hargaJual'      => $mpo->hargaJual,   
                                'jumlahBill'     => $mpo->jumlahBill                              
                            ];

                            $dataTagihan = null;
                            if($mpo->dataTagihan !== ''){
                                $dataTagihan = json_decode($mpo->dataTagihan);
                                $templateData['dataTagihan'] = $dataTagihan;
                            }

                            
                            $template = $this->generateNotif($templateData);

                            $emailTemplate   = $template['email'];
                            $mobileTemplate  = $template['mobile'];
                            $minimalTemplate = $template['minimal'];

                            $subtitle = "Segera Bayar Rp " . 
                                        number_format($mpo->totalKewajiban+$biayaTransaksiDisplay, 0, ",", ".") . " ke " .
                                        $virtualAccount;

                            //Simpan notifikasi baru
                            $productName = $this->ConfigModel->getNamaProduk('MP', '50');                            

                            
                            $idNotif = $this->NotificationModel->add($token->id,
                                                                     NotificationModel::TYPE_MPO,
                                                                     NotificationModel::CONTENT_TYPE_HTML,
                                                                     $productName,
                                                                     $subtitle,
                                                                     $mobileTemplate,
                                                                     $minimalTemplate,
                                                                     "MP");

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id)
                                , [
                                    "id"      => $idNotif,
                                    "tipe"    => "MP",
                                    "title"   => $this->ConfigModel->getNamaProduk('MP', '50'),
                                    "tagline" => $subtitle,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                    "token"   => $token->no_hp,
                                ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail($token->email, "Pembelian " . $product, $emailTemplate);

                            //Set response
                            $this->set_response([
                                'status'  => 'success',
                                'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                'data'    => [
                                    'idTransaksi'    => $idTransaksi,
                                    'virtualAccount' => $virtualAccount,
                                    'expired'        => $tglExpired,
                                    'now'            => date('Y-m-d H:i:s'),
                                    'redaksiPayment' => $redaksiPayment                       
                                ],
                            ], 200);
                        } 
                        else
                        {
                            $this->set_response([
                                'status'  => 'error',
                                'message' => 'Something went wrong. Please try again later.',
                                'code'    => 103,
                                'reason'  => $billing,
                            ], 200);
                            log_message('debug', __FUNCTION__ . ' Pembayaran MPO Error ' . json_encode($billing));
                        }
                    }
                } else {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Data tidak ditemukan',
                        'code'    => 102,
                    ), 200);
                }

            }
        } else {
            $this->errorForbbiden();
            log_message('debug', __FUNCTION__ . ' Pembayaran MPO Error ' . json_encode($this->errorForbbiden()));
        }
        log_message('debug', __FUNCTION__ . ' Pembayaran MPO ' . 'End');
    }

    /**
     * Generate template notifikasi mobile, email dan versi minify
     * untuk content webview
     *
     * @param array $data
     * @return void
     */
    public function generateNotif($data)
    {
        $mpoProduct = $data['mpoProduct'];
        $subject = "Pembelian " . $data['product'];
        
        $content = '';

        if ($mpoProduct->groups === 'seluler')
        {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_seluler', $data, true);    
        }
        else if ($mpoProduct->groups == 'listrik' && $mpoProduct->namaLayanan == 'PLN Prepaid')
        {            
            $content = $this->load->view('mail/mpo/notif_payment_mpo_plnpre', $data, true);
        }
        else if ($mpoProduct->groups === 'listrik' && $mpoProduct->namaLayanan === 'PLN Postpaid')
        {
            $data['periode'] = $this->_formatPLNTglPeriode($data['periode']);
            $content = $this->load->view('mail/mpo/notif_payment_mpo_plnpost', $data, true);   
        } 
        else  if ($mpoProduct->groups == 'air')
        {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_pdam', $data, true);   
        }
        else if ($mpoProduct->groups == 'asuransi')
        {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_bpjs', $data, true);   
        }
        else  if ($mpoProduct->groups == 'telkom' && $mpoProduct->namaLayanan == 'Telkom')
        {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_telkom', $data, true); 
        }
        else  if ($mpoProduct->groups == 'telkom')
        {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_halo', $data, true); 
        }
        else if ($mpoProduct->groups == 'voucher')
        {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_ewallet', $data, true); 
        }

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }

    function _formatPLNToken($str)
    {
        $out = substr($str, 0, 4);
        $out = $out. '-' . substr($str, 5, 4);
        $out = $out. '-' . substr($str, 9, 4);
        $out = $out. '-' . substr($str, 13, 4);
        $out = $out. '-' . substr($str, 17, 4);
        return $out;
    }

    function _formatPLNTglPeriode($str)
    {
        $arrPeriode = json_decode($str);
        $strFormat = '';
        if($arrPeriode){
            $c = 1;
            foreach ($arrPeriode as $a){
                $withoutSpace = str_replace(" ", "", $a);
                if($withoutSpace !== ""){
                    $d = DateTime::createFromFormat('Ym', $withoutSpace); 
                    $strFormat = $strFormat.$d->format('M Y');
                    if($c < count($arrPeriode)){
                        $strFormat = $strFormat.', ';
                    }
                }
                $c++;
            }
            
            return $strFormat;
            
        } else {
            return '';
        }
    }


    /**
     * Endpoint untuk mendapatakan daftar history transaksi 30 terakhir
     * 
     * @return void
     */
    function history_get()
    {
        $token = $this->getToken();
        if($token){
            $history = $this->MpoModel->getHistory($token->id);
            $this->set_response([
                'status' => 'success',
                'message' => 'Success',
                'data' => $history
            ]);
        }else{
            $this->errorForbbiden();
        }
    }
}
