<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';
require_once APPPATH . 'helpers/message_helper.php';

class Gpoint extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('User', 
            'NotificationModel',
            'ConfigModel',
            'GpoinModel',
            'ProductMasterModel',
            'MuliaModel'));
        $this->load->model('MasterModel');
        $this->load->library('form_validation');
    }

    function point_get(){
        $token = $this->getToken();
        if($token){
            $user = $this->User->getUser($token->id);
            $point = $this->gPointGetPoint($user->cif);            
            $this->set_response($point);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function vouchers_get($id = null){
        $token = $this->getToken();
        if($token){
            
            $vouchers = null;
            if($id == null){
                $page = $this->query('page');
                $limit = $this->query('limit');
                $vouchers = $this->gPointVoucher($page, $limit);    
            }else{
                $vouchers = $this->gPointVoucherDetails($id);
            }

            $this->set_response($vouchers);
        } else {
            $this->errorUnAuthorized();
        }    
    }

    function myvoucher_get(){
        $token = $this->getToken();
        if($token){
            $user = $this->User->getUser($token->id);
            $page = $this->query('page');
            $limit = $this->query('limit');
            $vouchers = $this->gPointUserVoucher($user->cif, $page, $limit);
            $this->set_response($vouchers);
        } else {
            $this->errorUnAuthorized();
        }    
    }

    function buy_post(){
        $token = $this->getToken();
        if($token){

            $this->form_validation->set_rules('id_voucher', 'id_voucher', 'required|integer');

            if($this->form_validation->run() == false){
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ]);
            }

            $user = $this->User->getUser($token->id);
            $voucherId = $this->post('id_voucher');
            $response = $this->gPointBuy($user->cif, $voucherId);
            $this->set_response($response);
        } else {
            $this->errorUnAuthorized();
        }    
    }

    function history_get(){
        $token = $this->getToken();
        if($token){
            $user = $this->User->getUser($token->id);
            $response = $this->gPointHistory($user->cif);
            $this->set_response($response);
        } else {
            $this->errorUnAuthorized();
        }   
    }

    function validate_post()
    {
        $token = $this->getToken();
        if($token)
        {
            $user = $this->User->getUser($token->id);

            $this->form_validation->set_rules('promo_code', 'promo_code', 'required');
            $this->form_validation->set_rules('jenis_transaksi', 'jenis_transaksi', 'required');
            $this->form_validation->set_rules('kode_produk', 'kode_produk', 'required');


            if($this->form_validation->run() == false)
            {
                $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ]);
            }
            else 
            {
                $voucherId = $this->post('voucher_id');
                $promoCode = $this->post('promo_code');
                $jenisTransaksi = $this->post('jenis_transaksi');
                $kodeProduk = $this->post('kode_produk');
                $unit = $this->post('unit');
                $amount = $this->post('amount');

                $validators = [
                    'channel' => $this->config->item('core_post_username'),
                    'product' => $kodeProduk,
                    'transactionType' => $jenisTransaksi,
                    
                ];

                if($unit != null){
                    $validators['unit'] = $unit;
                }

                $validateData = [
                    'promoCode' => $promoCode,
                    'voucherId' => $voucherId,
                    'userId' => $user->cif,                    
                    'validators' => $validators
                ];

                if($amount != null){
                    $validateData['transactionAmount'] = (int) $amount;
                }

                $validate = $this->gPointValidateVoucher($validateData);

                $this->set_response($validate);

            }
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    

    function voucherimage_post($method = 'add')
    {
        if(!$this->authCore())
        {
            $this->errorUnAuthorized();
        }

        if($method == 'add')
        {
            $uploadDir = $this->config->item('upload_dir');

            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            $config['encrypt_name'] = true;
            $config['upload_path'] = $uploadDir . '/gpoint/vouchers';
            log_message('debug', 'GPOINT IMAGE PATH: '.$config['upload_path']);
            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('file'))
            {
                $this->response([
                    'code' => 101,
                    'status' => 'error',
                    'message' => '',
                    'errors' => $this->upload->display_errors()
                ], 200);
            }
            else
            {
                $data = $this->upload->data();

                $this->response([
                    'status'=>'success',
                    'message'=>'',
                    'data'=> array(
                        'filename' => $data['file_name']
                    )
                ], 200);
            }
        }
        else if($method == 'delete')
        {
            $this->form_validation->set_rules('filename','filename','required');
            if($this->form_validation->run() == false)
            {
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ]);
            }
            else
            {
                $fileName = $this->post('filename');
                $uploadDir = $this->config->item('upload_dir');
                if(file_exists($uploadDir.'/gpoint/vouchers/'.$fileName)){
                    unlink($uploadDir.'/gpoint/vouchers/'.$fileName);
                    
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Voucher image deleted',
                        'data' => [
                            'filename' => $fileName
                        ]
                    ]);
                }
                else
                {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'File not found.',
                        'data' => null
                    ]);
                }
                
            }
        }

    }
    function gpoin_inquiry_post(){
        $token = $this->getToken();
        if ($token) {
            
            // $this->form_validation->set_rules('cif','CIF', 'required');
            $this->form_validation->set_rules('kodePromo', 'Promo Code', 'required');
            $this->form_validation->set_rules('transactionAmount', 'Transaction Amount', 'required');
            $this->form_validation->set_rules('reffSwitching', 'reffSwitching', 'required');
            $this->form_validation->set_rules('jenis', 'jenis', 'required');


            if ($this->form_validation->run() == FALSE){
                foreach ($this->form_validation->error_array() as $key => $value) {
                    
                    $error = $value;
                    break;
                }
                
                $this->set_response([
                    'status' => 'error',
                    'message' => $error,
                    'code' => 101,
                ]);
            } else {
                // $now = DateTime::createFromFormat('U.u', microtime(true));
                $type = $this->post('jenis');
                switch ($type) {
                    case 'LM':
                        $productCode = '37';
                        $transactionType = 'OP';
                    break;
                    case 'OP':
                        $productCode = '62';
                        $transactionType = 'OP';
                    break;
                    case 'SL':
                        $productCode = '62';
                        $transactionType  ='SL';
                    break;
                    case 'GTE':
                        $productCode = '32';
                        $transactionType = 'OP';
                    break;
                    default:
                        $productCode = '';
                        $transactionType = '';
                    break;
                }
                $t = microtime(true);
                $micro = sprintf("%06d",($t - floor($t)) * 1000000);
                $data = [
                    'cif' => $this->post('cif') ? $this->post('cif') : '',
                    'promoCode' => strtoupper($this->post('kodePromo')),
                    'transactionAmount' => $this->post('transactionAmount'),
                    'productCode' => $productCode,
                    'transactionType' => $transactionType,
                    'phone' => $this->post('phone'),
                    'customerName' => $this->post('customerName'),
                    'transactionDate' => date('Y-m-d H:i:s.'.substr($micro,0,3), $t),
                    'reffSwitching' => $this->post('reffSwitching')
                ];
                
                $inquiry = $this->gPointInquiry($data);

                if ($inquiry->responseCode == '00') {
                    $dataInq = json_decode($inquiry->data);
                    $dataArr = (array) $dataInq;
                    $dataArr['rewards']->value = floatval($dataArr['rewards']->value);
                    
                    if ($dataArr) {
                        //check jika refTrx Promo sudah ada, update transaksi id promo.
                        $dataGpoin = $this->GpoinModel->check($dataArr['reffSwitching']);
                        if ($dataGpoin == '') {
                            $this->GpoinModel->add($dataArr, $data);
                        } else {
                            $this->GpoinModel->update_trans_id($dataArr, $data['promoCode']);
                        }

                        $product = $this->ProductMasterModel->checkTable($data['productCode']);

                        if ($product) {
                            $queryPayment = $this->db->where(['reffSwitching' => $data['reffSwitching']])
                                            ->get($product->table);

                            // if ($data['productCode'] == '62') {
                            //     $totalKewajiban = floatval($queryPayment->row()->total_kewajiban);
                            // } else if ($data['productCode'] == '37') {
                            //     $totalKewajiban = floatval($queryPayment->row()->totalKewajiban);
                            // } 

                            // Hitung total kewajiban jika ada promo.
                            switch ($data['productCode'] ) {
                                case '62':
                                    $totalKewajiban = floatval($queryPayment->row()->total_kewajiban);
                                    break;
                                case '37':
                                    $totalKewajiban = floatval($queryPayment->row()->totalKewajiban);
                                    break;
                                case '32':
                                    $totalKewajiban = floatval($queryPayment->row()->totalKewajiban);
                                    break;
                            }
                            
                            if (($totalKewajiban > 0) && ($dataArr['rewards']->type == 'discount')) {
                                $dataArr['rewards']->totalKewajiban = $totalKewajiban - $dataArr['rewards']->value;
                            } else {
                                $dataArr['rewards']->totalKewajiban = $totalKewajiban;
                            }
                        }
                        
                        $this->set_response([
                            'status' => 'success',
                            'message' => '',
                            'data' => $dataArr['rewards']
                        ]);
                        log_message('debug', 'gpoin sukses log' . json_encode($inquiry));
                    } else {
                        $this->set_response([
                            'status' => 'error',
                            'code' => '102',
                            'message' => $inquiry->data
                        ]);
                        log_message('debug', 'gpoin error log' . json_encode($inquiry). ' Body:'. json_encode($data));
                    }
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => '102',
                        'message' => $inquiry->data 
                    ]);
                    log_message('debug', 'gpoin error log' . json_encode($inquiry). ' Body:'. json_encode($data));
                }
                
            }

        } else {
            $this->errorUnAuthorized();
        }
    }

    function gpoin_inquiry_cancel_post(){
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_rules('reffSwitching', 'ID Transaksi', 'required');
            $this->form_validation->set_rules('jenis', 'jenis', 'required');
            
            if ($this->form_validation->run() == FALSE){
                foreach ($this->form_validation->error_array() as $key => $value) {
                    
                    $error = $value;
                    break;
                }
                
                $this->set_response([
                    'status' => 'error',
                    'message' => $error,
                    'code' => 101,
                ]);
            } else {
                $reffSwitching = $this->post('reffSwitching');
                $type = $this->post('jenis');
                switch ($type) {
                    case 'LM':
                        $productCode = '37';
                        $transactionType = 'OP';
                    break;
                    case 'OP':
                        $productCode = '62';
                        $transactionType = 'OP';
                    break;
                    case 'SL':
                        $productCode = '62';
                        $transactionType  ='SL';
                    break;
                    case 'GTE':
                        $productCode = '32';
                        $transactionType = 'OP';
                    break;
                    default:
                        $productCode = '';
                        $transactionType = '';
                    break;
                }

                $data = [
                    'jenisTransaksi' => $transactionType,
                    'reffIdSwitching' => $this->post('reffSwitching')
                ];

                // inquiry cancel inquiry promo
                $inquiryCancel = $this->gPointCancelInquiry($data);
                if ($inquiryCancel->responseCode == '00') {
                    $this->GpoinModel->cancel_gpoin($data);
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Pembatalan Promo Sukses',
                        'response' => $inquiryCancel
                    ]);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => '102',
                        'message' => 'Pembatalan Promo Gagal',
                        'response' => $inquiryCancel
                        
                    ]);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
}
