<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

require_once 'CorePegadaian.php';

class Master extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('MasterModel', 'BankModel', 'GadaiModel', 'User'));
        $this->load->library(array('form_validation'));
        $this->load->library('session');
    }

    /**
     * Endpoint untuk mendapatkan daftar provinsi
     * @return any
     */
    function provinsi_get()
    {
        $token = $this->getToken();
        if ($token) {
            $id = $this->query('kode_provinsi');
            $this->response([
                'status' => 'success',
                'message' => '',
                'data' => $this->MasterModel->getProvinsi($id)
            ], 200);
        } else {
            $this->errorUnAuthorized();
        }

    }

    /**
     * Endpoint untuk mendapatkan daftar kabupaten
     * @return any
     */
    function kabupaten_get()
    {
        $token = $this->getToken();
        if ($token) {
            $idProvinsi = $this->query('kode_provinsi');
            $idKabupaten = $this->query('kode_kabupaten');

            $this->response([
                'status' => 'success',
                'message' => '',
                'data' => $this->MasterModel->getKabupaten($idKabupaten, $idProvinsi)
            ], 200);
        } else {
            $this->errorUnAuthorized();
        }

    }

    /**
     * Endpoint untuk mendapatkan daftar kecamatan
     * @return any
     */
    function kecamatan_get()
    {
        $token = $this->getToken();
        if ($token) {
            $id = $this->query('kode_kecamatan');
            $idKabupaten = $this->query('kode_kabupaten');

            $this->response([
                'status' => 'success',
                'message' => '',
                'data' => $this->MasterModel->getKecamatan($id, $idKabupaten)
            ], 200);
        } else {
            $this->errorUnAuthorized();
        }

    }

    /**
     * Endpoint untuk mendapatkan daftar cabang
     * @return any
     */
    function kelurahan_get()
    {
        $token = $this->getToken();
        if ($token) {
            $id = $this->query('kode_kelurahan');
            $idKecamatan = $this->query('kode_kecamatan');
            $this->response([
                'status' => 'success',
                'message' => '',
                'data' => $this->MasterModel->getKelurahan($id, $idKecamatan)
            ], 200);
        } else {
            $this->errorUnAuthorized();
        }

    }

    /**
     * Endpoint untuk mendapatkan daftar bank untuk transaksi
     * @return any
     */
    function bank_get()
    {
        $bank = $this->BankModel->getBankList();
        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $bank
        ), 200);
    }

    /**
     * Endpoint untuk mendapatkan daftar cabang berdasarkan posisi terdekat user
     * @param double $latitude user latitude
     * @param double $longitude user longitude
     * @param double $radius radius cabang dari latlong user
     * @return any
     */
    function cabang_terdekat_post()
    {
        if ($this->getToken()) {
            $this->form_validation->set_rules('latitude', 'Latitude', 'required|decimal');
            $this->form_validation->set_rules('longitude', 'Longitude', 'required|decimal');
            $this->form_validation->set_rules('radius', 'Radius', 'required|integer');


            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $latitude = $this->post('latitude');
                $longitude = $this->post('longitude');
                $radius = $this->post('radius');

                $cabang = $this->MasterModel->getNearestBranch($latitude, $longitude, $radius);

                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $cabang
                ), 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function lokasi_pengiriman_cabang_post()
    {
        if ($this->getToken()) {
            $this->form_validation->set_rules('latitude', 'Latitude', 'required|decimal');
            $this->form_validation->set_rules('longitude', 'Longitude', 'required|decimal');
            $this->form_validation->set_rules('radius', 'Radius', 'required|integer');

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $latitude = $this->post('latitude');
                $longitude = $this->post('longitude');
                $radius = $this->post('radius');

                $cabang = $this->MasterModel->getNearestBranch($latitude, $longitude, $radius);

                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $cabang
                ), 200);
                $detail_transaksi_logam_mulia = $cabang;
                $this->session->item = $detail_transaksi_logam_mulia;
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function detail_transaksi_logam_mulia_post()
    {
        foreach ($this->session->item as $data ) {
            if ($data->kodeOutlet== $this->input->post('kodeOutlet')){
                $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $data
                ), 200);
            }
        };
    }

    /**
     * Endpoint untuk set Lokasi nasabah
     * @param double $latitude user latitude
     * @param double $longitude user longitude
     * @return any
     */
    function lokasi_nasabah_god_post()
    {
        $token = $this->getToken();
        if ($this->getToken()) {
            $this->form_validation->set_rules('latitude', 'Latitude', 'required|decimal');
            $this->form_validation->set_rules('longitude', 'Longitude', 'required|decimal');

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $latitude = $this->post('latitude');
                $longitude = $this->post('longitude');
                $user = $token->id;

                $lokasi = $this->MasterModel->setLokasiNasabah($latitude, $longitude, $user);

                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $lokasi
                ), 200);
            }
        } else {
            $this->errorUnAuthorized();
        }

    }

    /**
     * Endpoint untuk mendapatkan daftar cabang berdasarkan pencarian nama cabang
     * @param string $keyword nama yang dicari
     * @return any
     */
    function outlet_get()
    {
        if ($this->getToken()) {
            $setData = array(
                'keyword' => $this->query('keyword'),
                'kode_kabupaten' => $this->query('kode_kabupaten')
            );

            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('kode_kabupaten','kode_kabupaten','numeric');

            if($this->query('keyword') == null && $this->query('kode_kabupaten') == null)
            {
                $this->set_response([
                    'status' => 'error',
                    'message' => 'Isi keyword atau kode_kabupaten',
                    'code' => 101
                ]);
                return;
            }

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $keyword = $this->query('keyword');
                $kodeKabupaten = $this->query('kode_kabupaten');

                $outlet = $this->MasterModel->searchOutlet($keyword, $kodeKabupaten);
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $outlet
                ), 200);
            }
        } else {
            $this->errorUnAuthorized();
        }

    }

    function cabang_get()
    {
        if ($this->getToken()) {
            $kode_outlet = $this->query('kode_outlet');
            $cabang = [];
            if ($kode_outlet != null) {
                $cabang = $this->MasterModel->getCabangPegadaian($kode_outlet);
            }
            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $cabang
            ), 200);
        } else {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Endpoint untuk mendapatkan cabang dengan kode outlet
     * @param string $kode_outlet Detail cabang berdasarkan kode outlet
     * @return any
     */
    function cabang_single_get()
    {
        if ($this->getToken()) {
            $kode_outlet = $this->query('kode_outlet');
            $cabang = [];

            if ($kode_outlet != null) {
                $cabang = $this->MasterModel->getSingleCabang($kode_outlet);
            }

            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $cabang
            ), 200);
        } else {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Endpoint untuk mendapatkan daftar merk elektronik untuk gadai elektronik
     * @return any
     */
    function elektronik_get()
    {
        $tipe = $this->query('tipe');
        $id = $this->query('id');
        $masterElektronik = $this->GadaiModel->ref_elektronik($tipe, $id);
        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $masterElektronik
        ), 200);
    }

    /**
     * Endpoint untuk mendapatkan daftar merk kendaraan untuk gadai kendaraan
     * @return any
     */
    function kendaraan_get()
    {
        $tipe = $this->query('tipe');
        $id = $this->query('id');
        $master = $this->GadaiModel->ref_kendaraan($tipe, $id);
        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $master
        ), 200);
    }

    /**
     * Endpoint untuk mendapatkan daftar merk laptop untuk gadai laptop
     * @return any
     */
    function laptop_get()
    {
        $id = $this->query('id');
        $master = $this->GadaiModel->ref_laptop($id);
        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $master
        ), 200);
    }

    /**
     * Endpoint untuk mendapatkan daftar merk handphone untuk gadai handphone
     * @return any
     */
    function handphone_get()
    {
        $id = $this->query("id");
        $master = $this->GadaiModel->ref_smartphone($id);
        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $master
        ), 200);
    }

    /**
     * Endpoint untuk mendapatkan daftar harga mulia jual beli
     * dari endpoint core
     * @return any
     */
    public function harga_mulia_get()
    {
        $token = $this->getToken();
        if ($token) {
            $iReq = $this->harga_mulia($token->channelId);

            if ($iReq->responseCode == '00') {
                $data = json_decode($iReq->data);
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $data
                ), 200);
            } else {
                $this->response(array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => 'Internal Server Error',
                    'reason' => $iReq
                ), 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Endpoint untuk mendapatkan daftar produk MPO
     * @param  string $method jika group maka mendapatkan daftar produk by group
     * @return [type]
     */
    function mpo_seluler_get($method = null)
    {
        $token = $this->getToken();
        
        if($token){

            if($method == 'group'){
                $seluler = $this->db->where(['groups'=>'seluler','status'=>'1'])->order_by('index','asc')->get('ref_mpo_seluler');
                $air = $this->db->where(['groups'=>'air','status'=>'1'])->order_by('index','asc')->get('ref_mpo_seluler');
                $listrik = $this->db->where(['groups'=>'listrik','status'=>'1'])->order_by('index','asc')->get('ref_mpo_seluler');
                $asuransi = $this->db->where(['groups'=>'asuransi','status'=>'1'])->order_by('index','asc')->get('ref_mpo_seluler');
                $telkom = $this->db->where(['groups'=>'telkom','status'=>'1'])->order_by('index','asc')->get('ref_mpo_seluler');
                $eWallet = $this->db->where(['groups'=>'voucher','status'=>'1'])->order_by('index','asc')->get('ref_mpo_seluler');

                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => [
                        'seluler' => $seluler->result(),
                        'air' => $air->result(),
                        'listrik' => $listrik->result(),
                        'asuransi' => $asuransi->result(),
                        'telkom' => $telkom->result(),
                        'ewallet' => $eWallet->result(),
                        'link_aja_validation' => $this->config->item('link_aja_validation')
                    ]
                ]);
            }
            else
            {
                $mpo = $this->MasterModel->getMPOSeluler();
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $mpo
                ), 200);    
            }  
        }else{
            $this->errorUnAuthorized();
        }
    }

    function singletoken_get()
    {
        $token = $this->getToken();
        if ($token) {
            echo json_encode($token);
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Endpoint untuk medapatkan biaya transaksi berdasarkan jenis bank
     * dan jenis transaksi (OP/TU)
     * @return any
     */
    function biaya_post(){
        $token = $this->getToken();
        if($token){
            $this->load->model('ConfigModel');
            $this->form_validation->set_rules('productCode', 'Product Code', 'required');
            if ($this->form_validation->run()) {
                // print_r($this->post('productCode')); exit;
                $this->set_response(array(
                    'OP' => array(
                        'BNI' => $this->ConfigModel->getBiayaPayment('OP','BANK',$this->post('productCode'),'009'),
                        'MANDIRI' => $this->ConfigModel->getBiayaPayment('OP','BANK',$this->post('productCode'),'008'),
                        'WALLET' => $this->ConfigModel->getBiayaPayment('OP','WALLET',$this->post('productCode'),''),
                        'VA_BCA' => $this->ConfigModel->getBiayaPayment('OP','BANK',$this->post('productCode'),'014'),
                        'VA_MANDIRI' => $this->ConfigModel->getBiayaPayment('OP','BANK',$this->post('productCode'),'008'),
                        'VA_BRI' => $this->ConfigModel->getBiayaPayment('OP','BANK',$this->post('productCode'),'002'),
                        'VA_PERMATA' => $this->ConfigModel->getBiayaPayment('OP','BANK',$this->post('productCode'),'013'),
                        
                    ),
                    'TU' => array(                
                        'BNI' => $this->ConfigModel->getBiayaPayment('TU','BANK','63','009'),
                        'MANDIRI' => $this->ConfigModel->getBiayaPayment('TU','BANK','63','008'),
                        'WALLET' => $this->ConfigModel->getBiayaPayment('TU','WALLET','63',''),
                        'VA_BCA' => $this->ConfigModel->getBiayaPayment('TU','BANK',$this->post('productCode'),'014'),
                        'VA_MANDIRI' => $this->ConfigModel->getBiayaPayment('TU','BANK',$this->post('productCode'),'008'),
                        'VA_BRI' => $this->ConfigModel->getBiayaPayment('TU','BANK',$this->post('productCode'),'002'),
                        'VA_PERMATA' => $this->ConfigModel->getBiayaPayment('TU','BANK',$this->post('productCode'),'013'),            
                    )
                ),200);
            } else {
                $error = "";
                foreach ($this->form_validation->error_array() as $value){
                    $error = $value;
                    break;
                }

                $this->set_response([
                    'status' => 'error',
                    'message' => $error,
                    'code' => 101
                ]);
            }
            
        }else{
            $this->errorUnAuthorized();
        }
    }


    function biaya_get(){
        $token = $this->getToken();
        if($token){
            $this->load->model('ConfigModel');
            $this->set_response(array(
                'OP' => array(
                    'BNI' => $this->ConfigModel->getBiayaPayment('OP','BANK','62','009'),
                    'MANDIRI' => $this->ConfigModel->getBiayaPayment('OP','BANK','62','008'),
                    'WALLET' => $this->ConfigModel->getBiayaPayment('OP','WALLET','62',''),
                    'VA_BCA' => $this->ConfigModel->getBiayaPayment('OP','BANK','62','014'),
                    'VA_MANDIRI' => $this->ConfigModel->getBiayaPayment('OP','BANK','62','008'),
                    'VA_BRI' => $this->ConfigModel->getBiayaPayment('OP','BANK','62','002'),
                    'VA_PERMATA' => $this->ConfigModel->getBiayaPayment('OP','BANK','62','013'),
                    
                ),
                'TU' => array(                
                    'BNI' => $this->ConfigModel->getBiayaPayment('TU','BANK','63','009'),
                    'MANDIRI' => $this->ConfigModel->getBiayaPayment('TU','BANK','63','008'),
                    'WALLET' => $this->ConfigModel->getBiayaPayment('TU','WALLET','63',''),
                    'VA_BCA' => $this->ConfigModel->getBiayaPayment('TU','BANK',$this->post('productCode'),'014'),
                    'VA_MANDIRI' => $this->ConfigModel->getBiayaPayment('TU','BANK',$this->post('productCode'),'008'),
                    'VA_BRI' => $this->ConfigModel->getBiayaPayment('TU','BANK',$this->post('productCode'),'002'),
                    'VA_PERMATA' => $this->ConfigModel->getBiayaPayment('TU','BANK',$this->post('productCode'),'013'),            
                )
            ),200);
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Endpoint untuk mendapatkan tarif biaya cetak emas
     * dengan mengampil dari endpoint core
     * @return any
     */
    function cetak_emas_get()
    {
        $token = $this->getToken();
        if($token){
            //Mendapatkan master vendor
            $vendor = $this->MasterModel->getVendorCetakEmas();

            //Mendapatkan harga cetak emas
            $inquiryBiayaCetak = $this->biayaCetak($token->channelId);
            
            if($inquiryBiayaCetak->responseCode == '00'){
                
                if(!isset($inquiryBiayaCetak->data)){
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                        'code' => 103,
                        'reason' => $inquiryBiayaCetak
                    ),200);                    
                }else{
                    $biayaCetak = json_decode($inquiryBiayaCetak->data);
                    
                    $resBiayaCetak = array();
                    
                    foreach($biayaCetak as $bc){
                        $v = $bc->vendor;
                        
                        foreach($bc->biaya as $b){
                            $resBiayaCetak[] = array(
                                'vendor' => $v,
                                'amount' => $b->amount,
                                'berat' => $b->berat
                            );
                        }                        
                    }
                    
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => '',
                        'data' => array(
                            'vendor' => $vendor,
                            'biaya' => $resBiayaCetak
                        )                        
                    ), 200);
                    
                }
                
            }else{
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                    'code' => 103,
                    'reason' => $inquiryBiayaCetak
                ),200);
            }
            
        }else{
            $this->errorUnAuthorized();
        }
        
    }
    
    /**
     * Endpoint untuk mendapatkan tarif sewa mikro
     * untuk proses simulasi
     * @return any
     */
    function tarif_sewa_mikro_get(){
        
        $mikroTarifSewa = $this->MasterModel->getMikroTarifSewa();
        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $mikroTarifSewa
        ),200);

    }

    /**
     * Endpoint untuk mendapatkan beberapa parameter system PDS
     * seperti menu yang aktif, virtual account yang aktif dll
     * @return any
     */
    function parameters_get() {
        $token = $this->getToken();
        if ($token) {

            $data = [
                'MPO_MENU' => [
                    'SELULER' => $this->MasterModel->getSystemParameters('mpo_seluler'),
                    'AIR' => $this->MasterModel->getSystemParameters('mpo_air'),
                    'LISTRIK' => $this->MasterModel->getSystemParameters('mpo_listrik'),
                    'ASURANSI' => $this->MasterModel->getSystemParameters('mpo_asuransi'),
                    'TELKOM' => $this->MasterModel->getSystemParameters('mpo_telkom'),
                    'PASCABAYAR' => $this->MasterModel->getSystemParameters('mpo_pascabayar'),
                    'BELI_TABUNGAN_EMAS' => $this->MasterModel->getSystemParameters('beli_tabungan_emas'),
                    'TOP_UP_E_WALLET' => $this->MasterModel->getSystemParameters('top_up_e_wallet'),
                    'TOP_UP_LINK_AJA' => $this->MasterModel->getSystemParameters('top_up_link_aja'),
                    'BAYAR_GADAI' => $this->MasterModel->getSystemParameters('bayar_gadai'),
                    'BAYAR_ANGSURAN' => $this->MasterModel->getSystemParameters('bayar_angsuran')
                ],
                'GADAI_MENU' => [
                    'EFEK' => $this->MasterModel->getSystemParameters('gadai_efek'),
                ],
                'PAYMENT' => [
                    'VA_BNI' => $this->MasterModel->getSystemParameters('va_bni'),
                    'VA_MANDIRI' => $this->MasterModel->getSystemParameters('va_mandiri'),
                    'VA_BCA' => $this->MasterModel->getSystemParameters('va_bca'),
                    'VA_PERMATA' => $this->MasterModel->getSystemParameters('va_permata'),
                    'CLICK_MANDIRI' => $this->MasterModel->getSystemParameters('click_mandiri'),
                    'WALLET' => $this->MasterModel->getSystemParameters('wallet'),
                    'VA_BRI' => $this->MasterModel->getSystemParameters('va_bri')
                ],
                'TABEMAS' => [
                    'BIAYA_OPEN' => (double) $this->MasterModel->getBiayaOpenTabemas(),
                    'BIAYA_TITIP' => (double) $this->MasterModel->getBiayaTitipOpenTabemas()
                ]
            ];

            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $data
            ]);
        } else {
            $this->errorUnAuthorized();
        }
    }

    //Get GOD Parameters in table
    function parameters_god_get() {
        $token = $this->getToken();
        if ($token) {
            log_message('debug', 'Masuk GET Params GoD');
            $data = [
                'GOD' => [
                    'BIAYA_GOJEK_GOD' => (double) $this->MasterModel->getParametersGod('biaya_gojek_god'),
                    'WAKTU_BUKA_GOD_SENIN_JUMAT' => (double) $this->MasterModel->getParametersGod('waktu_buka_god_senin_jumat'),
                    'WAKTU_TUTUP_GOD_SENIN_JUMAT' => (double) $this->MasterModel->getParametersGod('waktu_tutup_god_senin_jumat'),
                    'WAKTU_BUKA_GOD_SABTU' => (double) $this->MasterModel->getParametersGod('waktu_buka_god_sabtu'),
                    'WAKTU_TUTUP_GOD_SABTU' => (double) $this->MasterModel->getParametersGod('waktu_tutup_god_sabtu')
                ]
            ];
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $data
            ]);
            log_message('debug', 'Masuk GET Params GoD'. json_encode($data));
        } else {
            $this->errorUnAuthorized();
        }
    }

    // Get Apps Parameters in table config
    function parameters_apps_get() {
            $data = [
                'APPS' => [
                    'LAST_VER_ANDRO' => $this->MasterModel->getParametersApps('version_apps_android'),
                    'LAST_VER_IOS' => $this->MasterModel->getParametersApps('version_apps_ios')
                ]
            ];
            $this->set_response([
                 'status' => 'success',
                'message' => '',
                'data' => $data
            ]);
    }
    
    /**
     * Endpoint untuk mendapatkan promo gto
     */
    function gte_promo_get()
    {
        $token = $this->getToken();
        if($token){
            $data = $this->MasterModel->getGTEPromo();
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $data
            ], 200);
            
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    function outlet_cabang_god_post()
    {
        if ($this->getToken()) {
            $this->form_validation->set_rules('latitude', 'Latitude', 'required|decimal');
            $this->form_validation->set_rules('longitude', 'Longitude', 'required|decimal');
            $this->form_validation->set_rules('radius', 'Radius', 'required|integer');

            if ($this->form_validation->run() == FALSE) {
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Invalid input',
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $latitude = $this->post('latitude');
                $longitude = $this->post('longitude');
                $radius = $this->post('radius');
                $cabang = $this->MasterModel->getNearestBranchGOD($latitude, $longitude, $radius);

                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $cabang
                ), 200);
                $detail_transaksi_logam_mulia = $cabang;
                $this->session->item = $detail_transaksi_logam_mulia;
                
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    function get_profile_get()
    {
        log_message('debug', __FUNCTION__ . 'Start');
        $token = $this->getToken();
        if (!$token) {
            log_message('debug', __FUNCTION__ . 'UnAuthorized');
            $this->errorUnAuthorized();
            return;
        }

        // Every user fetch profile, update data from core
        $profile = $this->User->profile($token->id);
        if ($profile->cif != NULL) {
            $inquiryPortofolio = $this->inquiryTabEmas($profile->cif, $token->channelId);
            if ($inquiryPortofolio->responseCode == '00') {
                $tabunganEmas = json_decode($inquiryPortofolio->data);
                $profile->tabunganEmas = $tabunganEmas;
            } else {
                $profile->tabunganEmas = [];
            }

            // Check user data to core
            $checkProfile = $this->check_cif($profile->cif);
            log_message('debug', __FUNCTION__ . json_encode($checkProfile));
            if ($checkProfile->responseCode == '00') {
                // Update user info
                $coreProfileData = json_decode($checkProfile->data);
                $this->User->updateUser($token->id, [
                    'no_ktp' => $coreProfileData->noIdentitas,
                    'jenis_identitas' => $coreProfileData->tipeIdentitas,
                    'jenis_kelamin' => $coreProfileData->jenisKelamin,
                    'nama' => $coreProfileData->namaNasabah,
                    'nama_ibu' => $coreProfileData->ibuKandung,
                    'tempat_lahir' => $coreProfileData->tempatLahir,
                    'tgl_lahir' => $coreProfileData->tglLahir,
                    'is_dukcapil_verified' => $coreProfileData->isDukcapilVerified
                ]);

                // Edit profile response

                $profile->noKTP = $coreProfileData->noIdentitas;
                $profile->noIdentitas = $coreProfileData->noIdentitas;
                $profile->jenisKelamin = $coreProfileData->jenisKelamin;
                $profile->tempatLahir = $coreProfileData->tempatLahir;
                $profile->tglLahir = $coreProfileData->tglLahir;
                $profile->nama = $coreProfileData->namaNasabah;
                $profile->namaIbu = $coreProfileData->ibuKandung;
                $profile->jenisIdentitas = $coreProfileData->tipeIdentitas;
            }
        } else {
            $profile->tabunganEmas = [];
        }

        $this->response(array(
            'status' => 'success',
            'message' => '',
            'data' => $profile
        ));
        log_message('debug', __FUNCTION__ . 'End');
    }

}