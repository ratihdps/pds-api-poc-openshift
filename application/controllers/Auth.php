<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Auth extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('User','ResetPasswordModel','NotificationModel','EmasModel','MasterModel'));
        $this->load->library('form_validation');
        $this->load->helper('message');
    }

    public function login_post()
    {
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('agen', 'agen', 'required');
        //$this->form_validation->set_rules('version', 'version', 'required|integer');

        if(!$this->form_validation->run()){
            $this->response([
                'status' => 'error',
                'message' => 'Invalid Input',
                'code' => 101,
                'errors' => $this->form_validation->error_array()
            ], 200);
        }else{

            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $agen = $this->input->post('agen');
            $version = $this->input->post('version');
            
            $login = $this->User->login($email, $password);

            if ($login['status'] == 'success') {
                
                $user = $login['data'];
                 
                $access_token = bin2hex(random_bytes(78));
                
                $channelId = $agen == "web" ? "6014" : "6017";
                
                $tokenData = array(
                    'id' => $user->user_AIID,
                    'email' => $user->email,
                    'nama' => $user->nama,
                    'no_hp' => $user->no_hp,
                    'access_token' => $access_token,
                    'agen' => $agen,
                    'channelId' => $channelId,
                    'version' => $version
                );

                $token = authorization::generateToken($tokenData);
                
                $dataUpdate = array();
                if($agen=='android'){
                    $dataUpdate['token'] = $access_token;
                }else if($agen=='web'){
                    $dataUpdate['token_web'] = $access_token;
                }

                //Update user toke in db
                $this->User->updateUser($user->user_AIID, $dataUpdate);
                $profile = $this->User->profile($user->user_AIID);
                
                if($profile->cif != NULL){
                    $inquiryPortofolio = $this->inquiryTabEmas($profile->cif, $channelId);
                        
                        if(!isset($inquiryPortofolio->responseCode)){
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                                'code' => 103
                            ),200);
                            return;
                        }
                    
                    if($inquiryPortofolio->responseCode == '00'){                        
                        if(!isset($inquiryPortofolio->data)){
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                                'code' => 103
                            ),200);
                            return;
                        }

                        $tabunganEmas = json_decode($inquiryPortofolio->data);
                        $profile->tabunganEmas = $tabunganEmas;
                    }else{
                        $profile->tabunganEmas = [];
                    }
                }else{
                    $profile->tabunganEmas = [];
                }
                                
                $response = array(
                    'status' => 'success',
                    'user' => $profile,
                    'token' => $token
                );

                $this->set_response($response, 200);
                return;
            }else{
                $response = [
                    'code' => 102,
                    'status' => 'error',
                    'message' => $login['message']
                ];
                $this->set_response($response, 200);
            }
        }
    }

    public function register_post()
    {
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('no_telepon', 'no_telepon', 'required|numeric');        
        $this->form_validation->set_rules('agen', 'agen', 'required');
        //$this->form_validation->set_rules('version', 'version', 'required|integer');
        

        if(!$this->form_validation->run()){
            $response = array(
                'code' => 101,
                'status' => 'error',
                'message' => 'Invalid input',
                'errors' => $this->form_validation->error_array()
            );
            $this->set_response($response, 200);
        }else{

            $no_telepon = $this->input->post('no_telepon');
            $nama = $this->input->post('nama');
            $agen = $this->input->post('agen');
            $version = $this->input->post('version');

            //Check if user phone exist
            if($this->User->isPhoneExist($no_telepon) && $this->User->isActive($no_telepon)){

                $this->response(array(
                    'code' => 102,
                    'status' => 'error',
                    'message' => 'No telepon sudah digunakan',
                ), 200);

            }else{

                //Generate random 6 number for OTP                
                $this->load->helper('message');               

                $r = $this->User->register($nama, $no_telepon);                            

                $sendOtp = $this->sendOTP($no_telepon, $r['reffId']);
                
                if($sendOtp->responseCode == '00'){
                    $access_token = bin2hex(random_bytes(78));
                    
                    $channelId = $agen == "web" ? "6014" : "6017";
                    
                    $tokenData = array(
                        'id' => $r['userId'],
                        'email' => '',
                        'nama' => $nama,
                        'no_hp' => $no_telepon,
                        'access_token' => $access_token,
                        'agen' => $agen,
                        'channelId' => $channelId,
                        'version' => $version
                    );
                    
                    $updateData = array();
                    
                    if($agen=='android'){
                        $updateData['token'] = $access_token;
                    }else if($agen=='web'){
                        $updateData['token_web'] = $access_token;
                    }
                    
                    $this->User->updateUser($r['userId'], $updateData);
                    

                    $token = Authorization::generateToken($tokenData);

                    $response = array(
                        'status' => 'success',
                        'token' => $token,
                        'reffId' => $r['reffId'],
                        'message' => 'Kami mengirimkan kode OTP ke no telepon anda',
                        'data' => $sendOtp->data
                    );
                    $this->set_response($response, 200);
                }else{
                    $this->set_response(array(
                        'code' => 102,
                        'status'=>'error',
                        'message'=> 'otp tidak berhasil dikirim',
                        'reason' => $sendOtp
                    ));
                }
            }
        }
    }


    public function verify_otp_post()
    {
        $this->form_validation->set_rules('otp', 'otp', 'required|numeric|exact_length[6]');
        $this->form_validation->set_rules('reff_id', 'reff id', 'required');

        if(!$this->form_validation->run()){
            $this->response(array(
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 101,
                'errors' => $this->form_validation->error_array()
            ), 200);
        }else{           
            
            $otp = $this->input->post('otp');
            $reffId = $this->input->post('reff_id');
            
            //Get no hp user berdasarkan reff id
            $user = $this->db->where('otp_AIID', $reffId)
                            ->join('user','otp.user_AIID=user.user_AIID')
                            ->get('otp')
                            ->row();
            
            $checkOtp = $this->checkOTP($otp, $user->no_hp);
            
            if($checkOtp->responseCode == '00'){
                $this->response(array(
                    'status'=>'success',
                    'message'=>'',
                ), 200);
            }else{
                $response = array(
                    'status' => 'error',
                    'message' => 'OTP tidak ditemukan',
                    'code' => 102                    
                );
                $this->set_response($response, 200);
            }            
        }        
    }
    
    function register_fcm_token_put()
    {        
        
        $token = $this->getToken();
        if($token){
            
            $user = $this->User->getUser($token->id);
            
             $set_data = array(                
                'fcm_token' => $this->put('fcm_token')
            );
             
            $this->form_validation->set_data($set_data);            
            $this->form_validation->set_rules('fcm_token','fcm_token','required');
            if(!$this->form_validation->run()){
                $this->set_response(array(
                    'status'=>'error',
                    'message'=>'Invalid input',
                    'errors'=>$this->form_validation->error_array()
                ), 200);
            }else{
                $prevFCMToken = $user->fcm_token;
                
                $fcm_token = $this->put('fcm_token');
                
                //Kirimkan notifikasi FCM dengan tipe logout ke device sebelumnya
                if($prevFCMToken != $fcm_token){
                    $this->load->helper('message');
                    Message::sendFCMNotif($prevFCMToken, array(
                        'id' => '00000',
                        'tipe' => 'LOGOUT'
                    ));
                }
                
                $update = $this->User->updateUser($token->id, array('fcm_token'=>$fcm_token));
                $this->set_response(array(
                    'status'=>'success',
                    'message'=>'',                    
                ), 200);
            }
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function activate_post()
    {
        $headers = $this->input->request_headers();
        $token = $this->getToken();
        log_message('debug', 'Token User:'.json_encode($token));
        log_message('debug', 'Header User:'. json_encode($headers));
        
        if($token){
            $this->form_validation->set_rules('email', 'email', 'valid_email');
            $this->form_validation->set_rules('password', 'password', 'required|min_length[6]|max_length[12]');
            
            if($token->agen == 'android'){
                $this->form_validation->set_rules('fcm_token', 'FCM Token', 'required');    
            }
            
            if(!$this->form_validation->run()){
                $error = "";
                foreach ($this->form_validation->error_array() as $key => $value) {
                    $error = $value; 
                    break;
                }
                $response = array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => $error,
                );
                $this->set_response($response, 200);
            }else{
                
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $fcm_token = $this->input->post('fcm_token');
                $headers = $this->input->request_headers();
                
                if($this->User->isEmailExist2($email)){
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 102,
                        'message' => 'Email sudah digunakan'
                    ),200);
                    return;
                }
                
                //Generate email verification token
                $emailVerifyToken = bin2hex(random_bytes(78));
                               
                //Update user
                $update = array(
                    'email' => $email,
                    'password' => md5($password),
                    'email_verification_token' => $emailVerifyToken,
                    'status'=> 1,
                    'fcm_token' => $fcm_token
                );
                
                if($token->agen=='android'){
                    $update['token'] = $token->access_token;
                }else if($token->agen=='web'){
                    $update['token'] = $token->access_token;
                }

                $this->User->updateUser($token->id, $update);
                
                //Buat notifikasi baru dengan tipe profile
                $templateData = array(
                    'nama' => $token->nama,
                    'email' => $email
                );
                
                $template = $this->load->view('notification/email_notification', $templateData, TRUE);
                //Simpan notifikasi baru
                
                $notifType = NotificationModel::TYPE_ACCOUNT;
                $notifTitle = "Selamat Datang di Pegadaian Digital Service";
                $notifTagline = "Hai ".$token->nama.', lakukan verifikasi email untuk melanjutkan registrasi';                

                $newNotif = $this->NotificationModel->add($token->id, $notifType, 'html', $notifTitle, $notifTagline, $template);
                $this->load->helper('message');
                //Kirim notifikasi FCM
                Message::sendNotifWelcome($fcm_token, $newNotif, $token->nama);
                
                $this->load->helper('message');
                Message::sendEmailVerificationEmail($email, $token->nama, $emailVerifyToken);
                
                log_message("info", "Email Verification register 3 ".$emailVerifyToken);  
                $response = array(
                    'status' => 'success',
                    'message' => 'Thank You',
                    'data' => $this->User->profile($token->id) 
                );
                $this->set_response($response, 200);
            }
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    function request_password_reset_post()
    {
        $this->form_validation->set_rules('email', 'email', 'required');

        //Check form validation
        if(!$this->form_validation->run()){
            $this->response([
                'code'=> 101,
                'status' => 'error',
                'message' => 'Invalid Input',
                'errors' => $this->form_validation->error_array()
            ], 200);
        }else{
            $email = $this->input->post('email');

            if($this->User->isResetPasswordAvailable($email))
            {
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Kami mendeteksi Anda terlalu sering melakukan permintaan reset password. Mohon tunggu 30 menit lagi.',
                    'code' => 101
                ]);
            }

            $date = $nextReset = new DateTime('now');
            $date->add(new DateInterval('P1D'));
            $nextReset->add(new DateInterval('PT30M'));
                             
            //Cek no telepon atau email ada
            if($this->User->isEmailExist2($email))
            {
                $user = $this->User->profile($email);

                $token = bin2hex(random_bytes(78));
                
                $newRequestData = array(
                    'user_AIID' => $user->id,
                    'token' => $token,
                    'valid_until' => $date->format('Y-m-d H:i:s'),
                );

                //simpan request reset baru
                $this->ResetPasswordModel->add($newRequestData);
                
                $this->User->updateUser($email, [
                    'next_password_reset' => $nextReset->format('Y-m-d H:i:s'),
                ]);

                //Kirim email berserta dengan link reset password
                Message::sendResetPasswordEmail($user->email, $user->nama, $token);

                $response = array(
                    'status' => 'success',
                    'message' => 'Petunjuk reset password telah dikirimkan ke email anda',
                    'method' => 'email'
                );
                $this->set_response($response, 200);


            }
            else if($user = $this->User->isPhoneExist($email))
            {
                $newRequestData = array(
                    'user_AIID' => $user->user_AIID,
                    'no_hp' => $email, //nama inputnya email
                    'valid_until' => $date->format('Y-m-d H:i:s'),
                    'next_password_reset' => $nextReset->format('Y-m-d H:i:s')
                );

                //simpan request reset baru
                $idReset = $this->ResetPasswordModel->add($newRequestData);

                $this->User->updateUser($email, [
                    'next_password_reset' => $nextReset->format('Y-m-d H:i:s'),
                ]);
                
                //Request OTP untuk reset password
                $this->sendOTP($email, $idReset, "resetpassword");
                
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Kode reset password telah dikirimkan ke nomor '.$email.'. Masukan kode untuk melakukan reset password pada aplikasi Pegadaian Digital Service',
                    'method' => 'phone'    
                ), 200);
            }
            else
            {
                $this->response([
                    'code' => 102,
                    'status' => 'error',
                    'message' => 'Profile tidak ditemukan atau email belum terverifikasi'
                ], 200);
            }

        }
    }
    
    function reset_password_otp_post()
    {
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('repassword', 'Retype Password', 'required|matches[password]');
        $this->form_validation->set_rules('otp', 'Kode', 'required|numeric');
        
        if($this->form_validation->run() == false){
            $this->set_response(array(
                'status' => 'success',
                'message' => 'Invalid input',
                'code' => 102,
                'errors' => $this->form_validation->error_array(),
            ) , 200);
        }else{
            $password = $this->post('password');
            $otp = $this->post('otp'); 
                       
            //Pertama lakukan check OTP, jika sukses, update user password berdasarkan nomor
            $checkOtp = $this->checkOTP($otp, "", "resetpassword");
            
            if($checkOtp->responseCode == '00'){
                $otpData = json_decode($checkOtp->data);
                $noHp = $otpData->noHp;
                if($user = $this->ResetPasswordModel->isValidPhone($noHp)){
                    $updateUser = array(
                        'password' => md5($password)
                    );
                    
                    $this->User->updateUser($user->user_AIID, $updateUser);
                    
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Password diperbaharui'
                    ),200);
                    
                }else{
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Akun Anda Belum Aktif',
                        'code' => 102,
                        'reason' => $checkOtp
                    ), 200);
                }
            }else{
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Kode reset password tidak ditemukan atau expired',
                    'code' => 102,
                    'reason' => $checkOtp
                ), 200);
            }
        }
    }

    function reset_password_get()
    {
        $token = $this->query('t');

        if($token != null){
            //Cek reset password apakah masih valid
            if($this->ResetPasswordModel->isValid($token)){
                $data = array(
                    'token' => $token
                );
                $this->load->view('password/reset_password', $data);
            }else{
                $this->response([
                    'code' => 102,
                    'status' => 'error',
                    'message' => 'Invalid token or token expired'
                ], 200);
            }
        }
    }

    function reset_password_post()
    {
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        $this->form_validation->set_rules('repassword', 'Password', 'required|min_length[8]|matches[password]');
        $this->form_validation->set_rules('token', 'token', 'required');
        

        if(!$this->form_validation->run()){            
            
            if(!$this->ResetPasswordModel->isValid($this->post('token'))){
                redirect('/');                
            }
            
            $this->load->view('password/reset_password', array(
                'token' => $this->post('token')
            ));
        }else{

            $token = $this->post('token');

            if(!$this->ResetPasswordModel->isValid($token)){
                $this->response(array(
                    'code' => 102,
                    'status' => 'error',
                    'message' => 'Invalid input',
                ),200);
                return;
            }

            $id = $this->ResetPasswordModel->getUserId($token);

            //Update user password
            $password = $this->post('password');
            $this->User->updateUser($id, ['password'=>md5($password)]);

            redirect('/auth/reset_password_success?t='.$token);
        }
    }

    function reset_password_success_get()
    {
        $token = $this->query('t');
        if(!$this->ResetPasswordModel->isValid($token)){
            redirect('/');
        }

        //non aktifkan status update token
        $this->ResetPasswordModel->off($token);

        $this->load->view('password/reset_password_success');
    }
    
    function verify_email_get()
    {
        
        $token = $this->query("t");           
        
        if($this->User->isValidEmailVerificationToken($token)){
                       
            $user = $this->User->verifyEmail($token);            
            
            //send fcm message to user
            Message::sendFCMNotif($user->fcm_token, array(
                'tipe' => 'EMAIL_VERIFIED',
                'emailStatus' => '1'
            ));
            
            $this->load->view('mail/email_verification_success');
        }else{
            $this->load->view('mail/already_verified');
        }
    }
    
    function check_token_get()
    {
        $token = $this->getToken();
        if($token){
            $this->set_response(array(
                'status' => 'success',
                'message' => 'Valid Token',
                'data' => array(
                    'accessToken' => $token->access_token,
                    'agen' => $token->agen,
                    'valid' => true
                )
            ),200);
        }else{
            $this->set_response(array(
                'status' => 'success',
                'message' => 'Invalid Token',
                'data' => array(
                    'accessToken' => null,
                    'agen' => null,
                    'valid' => false
                )
            ),200);
        }
    }

    function core_token_post() {
        if (!$this->validateToken()) {
            return;           
        }
        
        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => array(
                'accessToken' => $this->getCoreToken(),
            )
        ),200);
    }

    function send_otp_get()
    {
        // get token
        $token = $this->getToken();
        // token validation
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'Data Finansial = Unauthorized');
            return;
        }

        log_message('debug', 'start of ' . __FUNCTION__ . ' request');

        $reffId = mt_rand(1000, 9999);
        $sendOTP = $this->sendOTP($token->no_hp, mt_rand(1000, 9999), 'aktivasi');
        log_message('debug', 'send otp core response' . json_encode($sendOTP));

        $result = $sendOTP;
        if ($sendOTP->responseCode != '00') {
            $this->send_response('error', 'Terjadi kesalahan mohon coba lagi', '');
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($result));
            return;
        }

        $this->send_response('success', '', $result);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($result));
    }

    function check_otp_post()
    {
        log_message('debug', 'start of ' . __FUNCTION__ . ' request with body : ' . json_encode($this->post()));

        // get token and validate it
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        // set rules for the request body
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('otp', 'otp', 'numeric|required|exact_length[6]');

        // validate request body
        if ($this->form_validation->run() == false) {
            $response = $this->send_response('error', 'Invalid Input', [], '', 101, $this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // send check otp to core
        log_message('debug', 'start sending check_otp request to core');
        $checkOtp = $this->checkOTP($this->post('otp'), $token->no_hp, 'aktivasi');
        log_message('debug', 'end sending check_otp request to core with response : ' . json_encode($checkOtp));

        // validate check otp response
        if ($checkOtp->responseCode != '00') {
            $response = $this->send_response('error', 'OTP tidak ditemukan/expired', $checkOtp);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // send response
        $otpData = json_decode($checkOtp->data);
        $response = $this->send_response('success', 'OTP valid', $otpData);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
    }
}
