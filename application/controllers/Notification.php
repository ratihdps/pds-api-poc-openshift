<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'CorePegadaian.php';
class Notification extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('User','NotificationModel','PaymentModel','EmasModel','GadaiModel','MikroModel','MpoModel','WalletModel','ConfigModel','MuliaModel','GpoinModel'));
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->load->helper(array('Message'));
    }
    
    function index_get()
    {
        $token = $this->getToken();
        if($token){
            $page = $this->query('page');
            $page == null ? $page=0 : $page;
            
            $search = null;            
            if($this->query('search') != null){
                $search = $this->query("search");                
            }
            
            $filter = null;
            if($this->query('filter') != null){
                $filter = $this->query('filter');
            }
            
            $offset = 20 * $page;
            $getNotification = $this->NotificationModel->getNotification($token->id, $offset, $search, $filter);           
            
            $config = [];
            $config['base_url'] = base_url().'notification/';
            $config['total_rows'] = $this->NotificationModel->countNotif($token->id);
            $config['per_page'] = 20;
            $config["use_page_numbers"] = TRUE;
            $config["page_query_string"] = TRUE;
            $config['query_string_segment'] = 'page';                        
            $this->pagination->initialize($config);            
                        
            $data  = array();
            foreach($getNotification as $n){                
                $n->attachment = $this->NotificationModel->getAttachment($n->id);
                $data[] = $n;                
            }          
            
            $unreadNotif = $this->NotificationModel->countUnreadNotif($token->id);
            $readedNotif = $config['total_rows'] - $unreadNotif;
            
            $this->set_response(array(
                'status' => 'success',
                'message' => '',                
                'totalUnread' => $unreadNotif,
                'totalReaded' => $readedNotif,
                'totalData' => $config['total_rows'],
                'page' => $page,                
                'data' => $data
            ), 200);
        }else{
            $this->errorUnAuthorized();
        }        
    }

    function detail_get()
    {
        $token = $this->getToken();
        if($token){
            $notifId = $this->query('id');
            if($notifId == null){
                return;
            }else{
                $notifDetails = $this->NotificationModel->getDetail($token->id, $notifId);
                if($notifDetails){
                    $notifDetails->attachment = $this->NotificationModel->getAttachment($notifId);
                    if($notifDetails){
                        $this->set_response(array(
                            'status'=> 'success',
                            'message'=> '',
                            'data' => $notifDetails
                        ), 200);
                    }else{
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Notifikasi tidak ditemukan'
                        ), 401);
                    }
                }                
            }            
        }else{
            $this->errorUnAuthorized();            
        }
    }


    /**
     * Endpoint ini tidak benar benar menghapus data notifikasi, hanya mengunpublish saja
     * Untuk kepentingan dokumentasi
     */
    function index_delete()
    {        
        $token = $this->getToken();
        if($token){
            $id = $this->query('id');
            $this->NotificationModel->delete($id, $token->id);
            $this->set_response(array(
                'status' => 'success',
                'message' => ''
            ), 200);            
        }else{
            $this->errorUnAuthorized();
        }
    }
    
    /**
     * Endpoint untuk mengupdate notifikasi sudah dibaca oleh user
     */
    function readed_post()
    {
        $token = $this->getToken();
        if($token){
            $setData = array(
                'id' => $this->post('id')
            );
            
            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('id','id','required');
            
            if($this->form_validation->run() == FALSE){
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ),200);
            }else{
                $id = $this->post('id');
                $this->NotificationModel->readed($id, $token->id);                
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Read status berhasil diperbaharui'
                ),200);
            }
        }else{
            $this->errorUnAuthorized();
        }
    }
       
    
    function admin_post($method = 'single')
    {
        $admin = $this->checkAdminToken($this->post('token'));
        if($admin){
            
            if($method=='single'){
                $setData = array(
                    'id_user' => $this->post('id_user'),
                    'id_notifikasi' => $this->post('id_notifikasi'),               
                    'type' => $this->post('type'),               
                    'title' => $this->post('title'),
                    'tagline' => $this->post('tagline')
                );

                $this->form_validation->set_data($setData);

                $this->form_validation->set_rules('id_user','id_user','required');
                $this->form_validation->set_rules('id_notifikasi','id_notifikasi','required');
                $this->form_validation->set_rules('type','type','required');
                $this->form_validation->set_rules('title','Title','required');
                $this->form_validation->set_rules('tagline','Tagline','required');

                if($this->form_validation->run() ==FALSE ){
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 102,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ), 200);
                }else{
                    //Get user token
                    $user = $this->User->getUser($this->post('id_user'));
                    $fcmToken = $user->fcm_token;
                    $noHp = $user->no_hp;
                    if($fcmToken != false){
                        
                         Message::sendFCMNotif(
                            $fcmToken
                                , [
                            "id" => $this->post('id_notifikasi'),        
                            "tipe" => $this->post('type'),
                            "title" => $this->post('title'),
                            "tagline" => $this->post('tagline'),
                            "token" => $noHp
                        ]);
                         $this->set_response(array(
                             'status' => 'success',
                             'message' => 'Notifikasi berhasil dikirim'
                         ),200);

                    }else{
                        $this->errorForbbiden();
                    }
                }
                
            }
            else if($method=='multi')
            {                
                $setData = array(
                    'ids' => $this->post('ids')                    
                );
                
                $idsraw = $this->post('ids');
                $ids = json_decode($idsraw, true);                    
                if (json_last_error() !== JSON_ERROR_NONE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input order',
                        'code' => 101,
                    ),200);
                    return;
                }
                
                $this->load->helper('message');
                
                log_message('debug', 'ADMIN NOTIF FCM BEGIN WITH '.count($ids).' DATA');
                
                $success = 0;
                
                foreach($ids as $id){
                    //Get id notifikasi berdasarkan detail
                    $cek = $this->db->where([
                        'notifikasi_AIID' => $id,
                        'status_send' => '0'
                    ])->get('notifikasi');
                    
                    if($cek->num_rows() > 0){
                        
                        $user = $this->db->select('fcm_token, no_hp')->where('user_AIID', $cek->row()->user_AIID)->get('user')->row();
                        
                        if($user)
                        {
                            Message::sendFCMNotif(
                                $user->fcm_token
                                    , [
                                "id" => $id,        
                                "tipe" => $cek->row()->type,
                                "title" => $cek->row()->judul,
                                "tagline" => $cek->row()->tagline,
                                "token" => $user->no_hp
                            ]);
                            
                            $success ++;

                            $this->db->where('notifikasi_AIID', $cek->row()->notifikasi_AIID)
                                     ->update('notifikasi', array('status_send'=>'1'));    
                        }
                    }
                }
                
                log_message('debug', 'ADMIN NOTIF FCM END WITH '.count($ids).' DATA');
                
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Kirim notif sukses '.$success.' dari '.count($ids),
                    'data' => null
                ),200);
                
            }
            else if($method == 'resetpin')
            {
                 $setData = array(
                    'id_user' => $this->post('id_user'),
                    'pin' => $this->post('pin'),               
                    'cabang' => $this->post('cabang'),
                    'waktu' => $this->post('waktu')

                );

                $this->form_validation->set_data($setData);

                $this->form_validation->set_rules('id_user','id_user','required|integer');
                $this->form_validation->set_rules('pin','pin','required|numeric');
                $this->form_validation->set_rules('cabang','cabang','required');
                $this->form_validation->set_rules('waktu','waktu','required');
                
                if($this->form_validation->run() ==FALSE ){
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 102,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ), 200);
                }
                else
                {
                    $userId = $this->post('id_user');
                    $pin = $this->post('pin');
                    $cabang = $this->post('cabang');
                    $waktu = $this->post('waktu');


                    $user = $this->User->getUser($userId);
                    log_message('debug','Notifikasi user '.($user->nama));

                    $template = $this->generateResetPinNotif($user->nama, $pin, $cabang, $waktu);

                    $emailTemplate = $template['email'];

                    $this->load->helper('message');
                    Message::sendEmail(
                            $user->email,
                            'Reset PIN Transaksi Finansial',
                            $emailTemplate
                            );
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Email sent',
                        'data' => null
                    ));

                }
            }
            
            
        }else{
            $this->errorForbbiden();
        }
    }
    
    /**
     * Method untuk mendapatkan admin token pegadaian untuk kebutuhan notifikasi
     */
    function checkAdminToken($token)
    {
        $this->load->model('AdminModel');
        $token = $this->AdminModel->getAdminToken($token);
        return $token;        
    }
    
    function kyc_core()
    {
        $headers = $this->input->request_headers();
        if(Authorization::tokenIsExist($headers)){           
            if($headers['Authorization'] == "Basic YXBsaWthc2lqczphcGxpa2FzaTEyMw=="){
                $setData = array(
                    'trxId' => $this->post('trxId'),
                    'tglPembayaran' => $this->post('tglPembayaran')
                );
                $this->form_validation->set_data($setData);
                
                $this->form_validation->set_rules('trxId','ID Transaksi','required');
                $this->form_validation->set_rules('tglPembayaran','Tanggal Pembayaran','required');
            }else{
                
            }
        }else{
            $this->errorForbbiden();
        }
    }    
  
    function isAdmin()
    {
        $headers = $this->input->request_headers();
        if (Authorization::tokenIsExist($headers)) {
            $token =  $headers['Authorization'];
            return $this->NotificationModel->isAdmin($token);
        }else{
            $this->errorForbbiden();
            return;
        }
    }
    
    /**
     * AREA ENDPOINT UNTUK CALLBACK DARI CORE
     * AREA ENDPOINT UNTUK CALLBACK DARI CORE
     * AREA ENDPOINT UNTUK CALLBACK DARI CODE
     * AREA ENDPOINT UNTUK CALLBACK DARI CODE
     * AREA ENDPOINT UNTUK CALLBACK DARI CODE
     * AREA ENDPOINT UNTUK CALLBACK DARI CODE
     * AREA ENDPOINT UNTUK CALLBACK DARI CODE
     * AREA ENDPOINT UNTUK CALLBACK DARI CODE
     */
    
    
    //Endpoint untuk core payment
    function core_payment_post() {
        if (!$this->authCore()) {
            $this->errorUnAuthorized();
            return;
        }

        $setData = array(
            'trxId' => $this->post('trxId'),
            'tglPembayaran' => $this->post('tglPembayaran'),
            'serialNo' => $this->post('serialNo'),
            'data' => $this->post('data')
        );

        $this->form_validation->set_data($setData);

        $this->form_validation->set_rules('trxId', 'ID Transaksi', 'required');
        $this->form_validation->set_rules('tglPembayaran', 'Tanggal Pembayaran', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->set_response(array(
                'status' => 'error',
                'message' => 'Invalid input',
                'errors' => $this->form_validation->error_array()
                    ), 200);
            log_message('debug', 'Notifikasi Core Check Payment: '.json_encode($this->form_validation->error_array()));
        } else {
            $trxId = $this->post('trxId');
            $tglPembayaran = $this->post('tglPembayaran');
            $serialNo = $this->post('serialNo');
            $reffBiller = $this->post('reffBiller');
            $reffCore = $this->post('reffCore');
            $responseCode = $this->post('responseCode');
            $responseDesc = $this->post('responseDesc');
            
            //Check payment ke payment emas
            $checkPayment = $this->PaymentModel->check($trxId, $tglPembayaran);

            // Get promo (if Any)
            $gpoin = $this->GpoinModel->get_promo($trxId);
            $promoCode = '';
            $idPromosi = '';
            $discountAmount = 0;
            $promoAmount = 0;
            if ($gpoin != '') {
                if ($gpoin->type == 'discount') {
                    $discountAmount = $gpoin->value;
                }
                $promoCode = $gpoin->promoCode;
                $promoAmount = $gpoin->value;
                $idPromosi = $gpoin->idPromosi;
            };
            log_message('debug', 'Notifikasi Core Check Payment: '.json_encode($checkPayment));
            if ($checkPayment)
            {

                // Tembak
                log_message('debug', 'Notifikasi Core Masuk: '.json_encode($checkPayment));
                error_log("\n Notifikasi Core Payment Masuk: ".json_encode($checkPayment)."\n", 3, "/data/log/Notif.log");
                //echo $checkPayment->jenis_transaksi;
                if ($checkPayment->jenis_transaksi == 'OP') {
                    
                    //Set tabungan emas jadi paid
                    $this->PaymentModel->update($trxId, [
                        'is_paid'=>'1',
                        'tanggal_pembayaran' => $tglPembayaran,
                        'openTabSuccess'=>'0',
                        'reffCore' => $reffCore
                    ]);
                }
                else if ($checkPayment->jenis_transaksi == 'SL')
                {
                    //Mendapatkan data tabungan user berdasarkan id transaksi
                    $dataTabunganEmas = $this->EmasModel->getRekeningEmasByPayment($trxId);
                    
                    if($dataTabunganEmas){
                        //Lakukan pengecekan saldo customer dan update data rekening tabungan
                        $checkCustomer = $this->detailTabunganEmas($dataTabunganEmas->no_rekening);

                        if ($checkCustomer->responseCode == '00') {
                            $checkCustomerData = json_decode($checkCustomer->data);

                            $namaNasabah = $checkCustomerData->namaNasabah;
                            $noRek = $checkCustomerData->norek;
                            $saldo = $checkCustomerData->saldo;
                            $cif = $checkCustomerData->cif;



                            $this->PaymentModel->paid($trxId, $tglPembayaran);
                            $this->EmasModel->updateRekening($noRek, array('saldo_emas' => $saldo));

                            //Get detail user berdasarkan nomor rekening user tabungan                                    
                            $user = $this->User->getUserByNoRek($noRek);

                            //Jika user ada, kirimkan notifikasi
                            if ($user) {                            

                                //Masukan saldo emas ke dalam transaksi (saldo dan saldo akhir jumlahnya sama)
                                $this->EmasModel->addHistoryIn($noRek, $checkPayment->gram, $saldo, '1', 'SL');
                                
                                $paymentMethod = $checkPayment->tipe ==  '' ? $checkPayment->payment : $checkPayment->tipe;

                                $template = $this->generateBeliEmasNotif(
                                        $namaNasabah,
                                        $namaNasabah, //untuk info rekening
                                        $cif,
                                        $tglPembayaran,
                                        $paymentMethod,
                                        $checkPayment->amount,
                                        $checkPayment->gram,
                                        $checkPayment->surcharge,
                                        $checkPayment->harga,
                                        $checkPayment->satuan,
                                        $checkPayment->total_kewajiban,
                                        $noRek, $saldo, $trxId,
                                        $checkPayment->biayaTransaksi,
                                        //promo
                                        $promoCode,
                                        $promoAmount,
                                        $discountAmount
                                        );

                                $emailTemplate = $template['email'];
                                $mobile = $template['mobile'];
                                $minimal = $template['minimal'];

                                //Tambahkan ke notifikasi
                                $idNotif = $this->NotificationModel->add(
                                        $user->user_AIID, 
                                        NotificationModel::TYPE_EMAS, 
                                        NotificationModel::CONTENT_TYPE_HTML, 
                                        $this->ConfigModel->getNamaProduk("SL", "62"),
                                        $checkPayment->gram.' gram/Rp. '. number_format($checkPayment->amount, 0, ",", ".").' sukses.', 
                                        $mobile,
                                        $minimal,
                                        "OP"
                                );
                                //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                                Message::sendFCMNotif(
                                        $user->fcm_token
                                        , [
                                    "id" => $idNotif,
                                    "title" => $this->ConfigModel->getNamaProduk("SL", "62"),
                                    "tagline" => $checkPayment->gram.' gram/Rp. '. number_format($checkPayment->amount, 0, ",", ".").' sukses.',        
                                    "tipe" => "SL_SUCCESS",
                                    "noRekening" => $noRek,
                                    "namaNasabah" => $namaNasabah,
                                    "gram" => $checkPayment->gram,
                                    "idTransaksi" => $trxId,
                                    "saldoEmas" => $saldo,
                                    "cif" => $cif,
                                    "paymentType" => $checkPayment->tipe ? $checkPayment->tipe : $checkPayment->payment,
                                    "token" => $user->no_hp        
                                ]);

                                //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                                $this->load->helper('message');
                                Message::sendEmailBeliEmasSuccess(
                                        $user->email,
                                        $emailTemplate
                                        );

                                //Jika id user rekening tidak sama dengan id user tabungan, berarti top up untuk orang lain
                                if($user->user_AIID != $checkPayment->user_AIID){
                                    $user2 = $this->User->getUser($checkPayment->user_AIID);

                                    $body2 = $this->generateBeliEmasNotif(
                                        $user2->nama,
                                        $namaNasabah, //untuk info rekening
                                        $user2->cif,
                                        $tglPembayaran,
                                        $checkPayment->tipe,
                                        $checkPayment->amount,
                                        $checkPayment->gram,
                                        $checkPayment->administrasi,
                                        $checkPayment->harga,
                                        $checkPayment->satuan,
                                        $checkPayment->total_kewajiban,
                                        $noRek,
                                        $saldo,
                                        $trxId,
                                        $checkPayment->biayaTransaksi,
                                        '',
                                        '',
                                        ''
                                    );

                                    //Tambahkan ke notifikasi
                                    $idNotif2 = $this->NotificationModel->add(
                                            $user2->user_AIID, 
                                            NotificationModel::TYPE_EMAS, 
                                            NotificationModel::CONTENT_TYPE_HTML, 
                                            $this->ConfigModel->getNamaProduk("SL", "62"),
                                            $checkPayment->gram.' gram/Rp. '. number_format($checkPayment->amount, 0, ",", ".").' sukses.',  
                                            $body2['mobile'],
                                            $body2['minimal'],
                                            "SL"
                                    );
                                    //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                                    Message::sendFCMNotif(
                                            $user2->fcm_token
                                            , [
                                        "id" => $idNotif2,
                                        "title" => $this->ConfigModel->getNamaProduk("SL", "62"),
                                        "tagline" => $checkPayment->gram.' gram/Rp. '. number_format($checkPayment->amount, 0, ",", ".").' sukses.',        
                                        "tipe" => "SL_SUCCESS",
                                        "noRekening" => $noRek,
                                        "namaNasabah" => $namaNasabah,
                                        "gram" => $checkPayment->gram,
                                        "idTransaksi" => $trxId,
                                        "saldoEmas" => $saldo,
                                        "cif" => $cif,
                                        "paymentType" => $checkPayment->tipe ? $checkPayment->tipe : $checkPayment->payment,
                                    ]);

                                    //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                                    $this->load->helper('message');
                                    Message::sendEmailBeliEmasSuccess(
                                            $user2->email,
                                            $body2['email']
                                            );
                                }
                            }else{
                                $user2 = $this->User->getUser($checkPayment->user_AIID);

                                //Jika nasabah pemilik rekening belum menggunakan PDS
                                $body2 = $this->generateBeliEmasNotif(
                                    $user2->nama,
                                    $namaNasabah, //untuk info rekening
                                    $user2->cif, //kosongkan nomor cif
                                    $tglPembayaran,
                                    $checkPayment->tipe,
                                    $checkPayment->amount,
                                    $checkPayment->gram,
                                    $checkPayment->administrasi,
                                    $checkPayment->harga,
                                    $checkPayment->satuan,
                                    $checkPayment->total_kewajiban,
                                    $noRek,
                                    $saldo,
                                    $trxId,
                                    $checkPayment->biayaTransaksi,
                                    '',
                                    '',
                                    ''
                                );


                                //Tambahkan ke notifikasi
                                $idNotif2 = $this->NotificationModel->add(
                                        $user2->user_AIID, 
                                        NotificationModel::TYPE_EMAS, 
                                        NotificationModel::CONTENT_TYPE_HTML, 
                                        $this->ConfigModel->getNamaProduk("SL", "62"),
                                        $checkPayment->gram.' gram/Rp. '. number_format($checkPayment->amount, 0, ",", ".").' sukses.',  
                                        $body2['mobile'],
                                        $body2['minimal'],
                                        "SL"
                                );
                                //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                                Message::sendFCMNotif(
                                        $user2->fcm_token
                                        , [
                                    "id" => $idNotif2,
                                    "title" => $this->ConfigModel->getNamaProduk("SL", "62"),
                                    "tagline" => $checkPayment->gram.' gram/Rp. '. number_format($checkPayment->amount, 0, ",", ".").' sukses.',
                                    "tipe" => "SL_SUCCESS",
                                    "noRekening" => $noRek,
                                    "namaNasabah" => $namaNasabah,
                                    "gram" => $checkPayment->gram,
                                    "idTransaksi" => $trxId,
                                    "saldoEmas" => $saldo,
                                    "cif" => $cif,
                                    "paymentType" => $checkPayment->tipe ? $checkPayment->tipe : $checkPayment->payment,
                                    "token" => $user2->no_hp        
                                ]);

                                //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                                $this->load->helper('message');
                                Message::sendEmailBeliEmasSuccess(
                                        $user2->email,
                                        $body2['email']
                                        );
                            }                       

                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Pembayaran Berhasil',
                                    ], 200);


                        } else {
                            $this->set_response(array(
                                'status' => 'error',
                                'code' => 103,
                                'message' => 'Check Customer Error',
                                'reason' => $checkCustomer
                                    ), 200);
                        }
                    }else if($user2 = $this->EmasModel->getUserByTrxId($trxId)){
                        
                        $checkCustomer = $this->detailTabunganEmas($user2->no_rekening);
                        
                        if ($checkCustomer->responseCode == '00') {
                            
                            $checkCustomerData = json_decode($checkCustomer->data);
                            log_message('debug', "Notification Check Customer".json_encode($checkCustomerData));
                            
                            //Jika nasabah pemilik rekening belum menggunakan PDS
                            $body2 = $this->generateBeliEmasNotif(
                                    $user2->nama,
                                    $checkCustomerData->namaNasabah, //untuk info rekening
                                    $user2->cif, //kosongkan nomor cif 
                                    $tglPembayaran,
                                    $checkPayment->tipe,
                                    $checkPayment->amount,
                                    $checkPayment->gram,
                                    $checkPayment->administrasi,
                                    $checkPayment->harga,
                                    $checkPayment->satuan,
                                    $checkPayment->total_kewajiban,
                                    $checkCustomerData->norek, 
                                    $checkCustomerData->saldo, 
                                    $trxId,
                                    $checkPayment->biayaTransaksi,
                                    //promo
                                    $promoCode,
                                    $promoAmount,
                                    $discountAmount
                                    );                           


                            //Tambahkan ke notifikasi
                            $idNotif2 = $this->NotificationModel->add(
                                    $user2->user_AIID, 
                                    NotificationModel::TYPE_EMAS, 
                                    NotificationModel::CONTENT_TYPE_HTML, 
                                    $this->ConfigModel->getNamaProduk("SL", "62"),
                                    $checkPayment->gram.' gram/Rp. '. number_format($checkPayment->amount, 0, ",", ".").' sukses.',  
                                    $body2['mobile'],
                                    $body2['minimal'],
                                    "SL"
                            );
                            //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                            Message::sendFCMNotif(
                                    $user2->fcm_token
                                    , [
                                "id" => $idNotif2,
                                "title" => $this->ConfigModel->getNamaProduk("SL", "62"),
                                "tagline" => $checkPayment->gram.' gram/Rp. '. number_format($checkPayment->amount, 0, ",", ".").' sukses.',
                                "tipe" => "SL_SUCCESS",
                                "noRekening" => $checkCustomerData->norek,
                                "namaNasabah" => $checkCustomerData->namaNasabah,
                                "gram" => $checkPayment->gram,
                                "idTransaksi" => $trxId,
                                "saldoEmas" => $checkCustomerData->saldo,
                                "cif" => "-",
                                "paymentType" => $checkPayment->tipe ? $checkPayment->tipe : $checkPayment->payment,
                                "token" => $user2->no_hp        
                            ]);

                            //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                            $this->load->helper('message');
                            Message::sendEmailBeliEmasSuccess(
                                    $user2->email,
                                    $body2['email']
                                    );
                            
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Top Up Emas Sukses',
                                    ], 200);
                            
                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'code' => 103,
                                'message' => 'Check Customer Error',
                                'reason' => $checkCustomer
                                    ), 200);
                        }
                    }
                } else if($checkPayment->jenisTransaksi == 'OD'){
                    
                    //Get user data
                    $user = $this->User->getUser($checkPayment->user_AIID);
                    
                    $this->PaymentModel->update($trxId, array(
                        'is_paid' => '1',
                        'tanggal_pembayaran' => $tglPembayaran
                    ));
                    
                    $template = $this->generateCetakEmasNotif($trxId);

                    $emailTemplate = $template['email'];
                    $mobile = $template['mobile'];
                    $minimal = $template['minimal'];

                    //Tambahkan ke notifikasi
                    $idNotif = $this->NotificationModel->add(
                            $user->user_AIID, 
                            NotificationModel::TYPE_EMAS, 
                            NotificationModel::CONTENT_TYPE_HTML,
                            $this->ConfigModel->getNamaProduk("OD", "62"),
                            "Order Cetak Emas Sukses",
                            $mobile,
                            $minimal,
                            "OD"
                    );
                    //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                    Message::sendFCMNotif(
                            $user->fcm_token
                            , [
                        "id" => $idNotif,       
                        "tipe" => "OD",
                        "title" => $this->ConfigModel->getNamaProduk("OD", "62"),
                        "tagline" => "Order Cetak Emas Sukses",
                        "content" => "",
                        "paymentType" => $checkPayment->payment,
                        "token" => $user->no_hp,
                        "totalPembayaran" => $checkPayment->amount,
                        "cif" => $user->cif,
                        "nama" => $user->nama
                    ]);

                    //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                    $this->load->helper('message');
                    Message::sendEmail($user->email, "Order Cetak Emas ".$checkPayment->norek.' Sukses', $emailTemplate);

                    $this->set_response([
                            'status' => 'success',
                            'message' => 'Pembayaran Berhasil',
                                ], 200);
                    
                } else if ($checkPayment->jenisTransaksi == 'BB'){
                    //Get user data
                    $user = $this->User->getUser($checkPayment->user_AIID);
                    
                    $this->PaymentModel->update($trxId, array(
                        'is_paid' => '1',
                        'tanggal_pembayaran' => $tglPembayaran
                    ));
                    
                    $template = $this->generateBuybackEmasNotif($trxId);

                    $emailTemplate = $template['email'];
                    $mobile = $template['mobile'];
                    $minimal = $template['minimal'];

                    //Tambahkan ke notifikasi
                    $idNotif = $this->NotificationModel->add(
                            $user->user_AIID, 
                            NotificationModel::TYPE_EMAS, 
                            NotificationModel::CONTENT_TYPE_HTML,
                            $this->ConfigModel->getNamaProduk("BB", "62"),
                            "Pembayaran Buyback Emas Berhasil",
                            $mobile,
                            $minimal,
                            "BB"
                    );
                    //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                    Message::sendFCMNotif(
                            $user->fcm_token
                            , [
                        "id" => $idNotif,
                        "tipe" => "BB",
                        "title" => $this->ConfigModel->getNamaProduk("BB", "62"),
                        "tagline" => "Pembayaran Buyback Emas Berhasil",
                        "content" => "",
                        "paymentType" => $checkPayment->payment,
                        "token" => $user->no_hp
                    ]);

                    //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                    $this->load->helper('message');
                    Message::sendEmail($user->email, "Pembayaran Buyback Emas ".$checkPayment->norek.' Sukses', $emailTemplate);

                    $this->set_response([
                            'status' => 'success',
                            'message' => 'Pembayaran Berhasil',
                                ], 200);
                }

                /**
                 * Callback untuk redeem dan refresh point user setelah ada transaksi penukaran voucher
                 * Gpoint di transaksi tabungan emas
                 */
                if($checkPayment->jenisTransaksi != 'OP' && $checkPayment->discountAmount != null)
                {
                    
                    $user = $this->User->getUser($checkPayment->user_AIID);

                    $data = [
                        'userId' => $user->cif,
                        "channel" => $this->config->item("core_post_username"),
                        "product" => "62",
                        "transactionType" => $checkPayment->jenisTransaksi,
                        "unit" => "gram",
                        "transactionAmount" => (double) $checkPayment->gram,
                        "reffCore" => $reffCore
                    ];

                    log_message('debug', 'GPOINT REFRESH POINT');
                    log_message('debug', 'GPOINT REFRESH POINT DATA: '.json_encode($data));

                    $refrehPoint = $this->gPointValue($data);

                    log_message('debug', 'GPOINT REFRESH POINT RESPONSE: '.json_encode($refrehPoint));

                    // Redeem vouchers
                    $promoCode = substr($checkPayment->idPromosi, strpos($checkPayment->idPromosi, ";") + 1);
                    $redeemData = [
                        "promoCode" => $promoCode,
                        "voucherId" => null,
                        "userId" => $user->cif,
                        "transactionAmount" => $checkPayment->totalKewajiban

                    ];

                    log_message('debug', 'GPOINT REDEEM VOUCHER');
                    log_message('debug', 'GPOINT REDEEM VOUCHER DATA: '.json_encode($redeemData));

                    $redeem = $this->gPointRedeem($data);
                    log_message('debug', 'GPOINT REFRESH POINT RESPONSE: '.json_encode($redeem));

                }

            }
            else
            {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Not Found',
                    'errors' => null
                        ), 200);
            }
            
            //Jika tidak ada check payemnt emas, check payment gadai
            $cekPaymentGadai = $this->GadaiModel->getPaymentByTrxId($trxId);
            if($cekPaymentGadai){
                
                log_message('debug', 'Notifikasi Core Masuk: '.json_encode($cekPaymentGadai));

                $tenor = $cekPaymentGadai->tenor;

                $dTglPembayaran = new DateTime($tglPembayaran);
                
                //Update tglJatuhTempo dengan tglPembayaran + tenor-1

                $nTenor = $tenor - 1;
                $newTglJatuhTempo = $dTglPembayaran->add(new DateInterval('P'.$nTenor.'D'));


                //Update paid status menjadi 1
                $this->GadaiModel->updatePayment($trxId, array(
                    'paid'=>'1',
                    'tanggal_pembayaran' => $tglPembayaran,
                    'tglJatuhTempo' => $newTglJatuhTempo->format('Y-m-d')
                ));

                $template = $this->generatePaymentGadaiNotif(
                         $cekPaymentGadai->nama,
                         $trxId
                        );

                $emailTemplate = $template['email'];
                $mobile = $template['mobile'];
                $minimal = $template['minimal'];
                
                $productCode = substr($cekPaymentGadai->norek, 7, 2);

                //Tambahkan ke notifikasi
                $idNotif = $this->NotificationModel->add(
                        $cekPaymentGadai->user_AIID, 
                        NotificationModel::TYPE_GADAI, 
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk($cekPaymentGadai->jenisTransaksi, $productCode ),
                        "Transaksi Sukses",
                        $mobile,
                        $minimal,
                        "OP"
                );
                //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                Message::sendFCMNotif(
                        $cekPaymentGadai->fcm_token
                        , [
                    "id" => $idNotif,
                    "title" => $this->ConfigModel->getNamaProduk($cekPaymentGadai->jenisTransaksi, $productCode ),
                    "tagline" => "Transaksi Sukses",        
                    "tipe" => "GD",
                    "content" => "", 
                    "paymentType" => $cekPaymentGadai->tipe ? $cekPaymentGadai->tipe : $cekPaymentGadai->payment,        
                    "token" => $cekPaymentGadai->no_hp            
                ]);
                
                $jt = $this->ConfigModel->getNamaProduk($cekPaymentGadai->jenisTransaksi, $productCode);
                                               

                //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                $this->load->helper('message');
                Message::sendEmail($cekPaymentGadai->email, "Transaksi ".$jt.' '.$cekPaymentGadai->norek.' Sukses', $emailTemplate);
                
                $this->set_response([
                        'status' => 'success',
                        'message' => 'Pembayaran Berhasil',
                            ], 200);
            }
            
            //Jika tidak ada check payemnt gadai, check payment mikro
            $cekPaymentMikro = $this->MikroModel->getPaymentByTrxId($trxId);
            if($cekPaymentMikro){
                
                log_message('debug', 'Notifikasi Core Masuk: '.json_encode($cekPaymentMikro));
                
                //Update paid status menjadi 1
                $this->MikroModel->updatePayment($trxId, array('paid'=>'1','tanggal_pembayaran'=>$tglPembayaran));

                $template = $this->generatePaymentMikroNotif(
                         $cekPaymentMikro->nama,
                         $trxId
                        );

                $emailTemplate = $template['email'];
                $mobile = $template['mobile'];
                $minimal = $template['minimal'];
                
                $productCode = substr($cekPaymentMikro->norek, 7, 2);

                //Tambahkan ke notifikasi
                $idNotif = $this->NotificationModel->add(
                        $cekPaymentMikro->user_AIID, 
                        NotificationModel::TYPE_MICRO, 
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk($cekPaymentMikro->jenisTransaksi, $productCode ),
                        "Transaksi Sukses",
                        $mobile,
                        $minimal,
                        "MC"
                );
                //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                Message::sendFCMNotif(
                        $cekPaymentMikro->fcm_token
                        , [
                    "id" => $idNotif,       
                    "tipe" => "MC",
                    "title" => $this->ConfigModel->getNamaProduk($cekPaymentMikro->jenisTransaksi, $productCode),
                    "tagline" => "Transaksi Sukses",
                    "content" => "",
                    "paymentType" => $cekPaymentMikro->tipe ? $cekPaymentMikro->tipe : $cekPaymentMikro->payment,
                    "token" => $cekPaymentMikro->no_hp        
                ]);
                
                //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                $this->load->helper('message');
                Message::sendEmail($cekPaymentMikro->email, "Transaksi ".$cekPaymentGadai->norek.' Sukses', $emailTemplate);
                
                $this->set_response([
                        'status' => 'success',
                        'message' => 'Pembayaran Berhasil',
                            ], 200);
            } 
            
            //Jika tidka ada payment mikro, check payment MPO
            $cekPaymentMpo = $this->MpoModel->getMpo($trxId);
            if($cekPaymentMpo)
            {
                //parameter data adalah json. Digunakan untuk data payment MPO
                $data = $this->post('data');
                $dataMPO = json_decode($data);

                // Pastikan tidak ada error saat json decode
                if($data !== null && json_last_error() !== JSON_ERROR_NONE){
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Bad JSON data'
                    ]);
                    return;
                }

                $this->paymentMPO($cekPaymentMpo, 
                                  $tglPembayaran,
                                  $trxId, 
                                  $serialNo, 
                                  $reffBiller,
                                  $reffCore,
                                  $responseCode,
                                  $responseDesc,
                                  $dataMPO);                
            }
            
            //Jika tidak ada check payment wallet
            $checkWalletPayment = $this->WalletModel->getPaymentByTrxId($trxId);
            if($checkWalletPayment){
                
                log_message('debug', 'Notifikasi Core Masuk: '.json_encode($checkWalletPayment));
                
                $this->WalletModel->update(array(
                    'reffSwitching' => $trxId,
                    'serialNumber' => $serialNo,
                    'paid' => '1',
                    'tanggal_pembayaran' => $tglPembayaran
                ));
                
                
                $template = $this->generatePaymentWalletNotifSuccess($checkWalletPayment);

                $emailTemplate = $template['email'];
                $mobile = $template['mobile'];
                $minimal = $template['minimal'];

                //Tambahkan ke notifikasi
                $idNotif = $this->NotificationModel->add(
                        $checkWalletPayment->user_AIID, 
                        NotificationModel::TYPE_MPO, 
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('TU', '63'),
                        "Top Up ".$checkWalletPayment->amount,
                        $mobile,
                        $minimal,
                        "WL"
                );
                //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                Message::sendFCMNotif(
                        $checkWalletPayment->fcm_token
                        , [
                    "id" => $idNotif,
                    "tipe" => "WL",
                    "title" => $this->ConfigModel->getNamaProduk('TU', '63'),
                    "tagline" => "Top Up Rp. ".number_format($checkWalletPayment->amount, 0, ",", ".")." berhasil",
                    "content" => "",
                    "paymentType" => $checkWalletPayment->tipe ? $checkWalletPayment->tipe : $checkWalletPayment->payment,
                    "token" => $checkWalletPayment->no_hp        
                ]);
                
                //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                $this->load->helper('message');
                Message::sendEmail($checkWalletPayment->email, "Top Up ".$checkWalletPayment->amount, $emailTemplate);
                
                $this->set_response([
                        'status' => 'success',
                        'message' => 'Pembayaran Berhasil',
                            ], 200);                
            }

            //Jika tidak ada cek GTE. Jika GTE maka notifikasi dana teleah berhasil ditransfer ke bank nasabah
            $checkGTE = $this->EmasModel->getGTE($trxId);

            if($checkGTE){
                
                log_message('debug', 'Notifikasi Core Masuk: '.json_encode($checkGTE));
                
                //set status bank terbayar 1
                $this->EmasModel->updateGTE($trxId, array('isBankPaid'=>'1'));
                
                //Send notifikasi ke device user
                $template = $this->generatePaymentGTENotif($checkGTE);

                $emailTemplate = $template['email'];
                $mobile = $template['mobile'];
                $minimal = $template['minimal'];

                //Tambahkan ke notifikasi
                $idNotif = $this->NotificationModel->add(
                        $checkGTE->user_AIID, 
                        NotificationModel::TYPE_EMAS, 
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP', '32'),
                        "Pencairan Uang Gadai ".$checkGTE->noKredit,
                        $mobile,
                        $minimal,
                        "GT"
                );
                //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                Message::sendFCMNotif(
                        $checkGTE->fcm_token
                        , [
                    "id" => $idNotif,
                    "tipe" => "GT",
                    "title" => $this->ConfigModel->getNamaProduk('OP', '32'),
                    "tagline" => "Pencairan Uang Gadai ".$checkGTE->noKredit,
                    "content" => "",                    
                    "token" => $checkGTE->no_hp        
                ]);
                
                //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                $this->load->helper('message');
                Message::sendEmail($checkGTE->email, "Pencairan Uang Gadai ".$checkGTE->noKredit, $emailTemplate);
                
                $this->set_response([
                        'status' => 'success',
                        'message' => 'Pembayaran Berhasil',
                            ], 200);
            }

            // Check payment mulia

            $checkPaymentMulia = $this->MuliaModel->getPaymentByTrxId($trxId);
            if($checkPaymentMulia)
            {
                log_message('debug', 'Notifikasi Core Masuk (Mulia): '.json_encode($checkPaymentMulia));

                // Set payment status paid
                $this->MuliaModel->updatePayment($trxId, [
                    'isPaid'=>'1',
                    'tglPembayaran' => $tglPembayaran,
                    'isOpenSuccess'=>'0',
                    'reffCore' => $reffCore
                ]);
            }
                        
        }
    }

    /**
     * Proses callback payment MPO
     * @param  object $cekPaymentMpo object pembayaran
     * @param  string $tglPembayaran tanggal pembayaran
     * @param  string $trxId         reffswitching/id transaksi
     * @param  string $serialNo      serialNumber
     * @param  string $reffBiller    reffBiller Core
     * @param  string $reffCore      reff Core
     * @param  string $responseCode  response code
     * @param  string $reffDesc      response description     
     * @param  string $data          data detail payment
     * @return void                  response
     */
    function paymentMPO(
        $cekPaymentMpo, 
        $tglPembayaran,
        $trxId, 
        $serialNo, 
        $reffBiller,
        $reffCore,
        $responseCode,
        $responseDesc,
        $mpoData
    ) {
        log_message('debug', 'Core Notification MPO Payment In!');                
        
        // Maping payment status berdasarkan responseCode
        $paymentStatusCode = '0';
        $paymentStatusStr = '';
        $pendingStatus = ['68', 'A0', '01', '97'];
        
        if($responseCode === '00'){
            $paymentStatusCode = '1'; // success
            $paymentStatusStr = 'Sukses';
        } else if (in_array($responseCode, $pendingStatus)) {
            $paymentStatusCode = '0'; // pending
            $paymentStatusStr = 'Pending';
        } else {
            $paymentStatusCode = '2'; // failed
            $paymentStatusStr = 'Gagal';
        }

        // Payment status untuk template notifikasi
        $paymentStatus = [
            'code' => $paymentStatusCode,
            'label' => $paymentStatusStr,
            'message' => $responseDesc
        ];

        // Update MPO payment data
        $this->MpoModel->update([
            'reffSwitching' => $trxId,
            'serialNo' => $serialNo,
            'is_paid' => '1',
            'paymentStatus' => $paymentStatusCode,
            'tanggal_pembayaran' => $tglPembayaran
        ]);
        
        // Get detail produk untuk redaksi template
        $mpoProduct = $this->MpoModel->getMpoProduct($cekPaymentMpo->kodeLayananMpo, $cekPaymentMpo->kodeBiller);  
        $mpoName = $mpoProduct->namaLayanan;
        
        // Generate notif template               
        $template = $this->generatePaymentMPONotif($cekPaymentMpo, 
                                                   $mpoProduct,
                                                   $mpoData,
                                                   $paymentStatus);

        $emailTemplate = $template['email'];
        $mobile = $template['mobile'];
        $minimal = $template['minimal'];

        // Tambahkan ke notifikasi
        $idNotif = $this->NotificationModel->add(
                $cekPaymentMpo->user_AIID, 
                NotificationModel::TYPE_MPO, 
                NotificationModel::CONTENT_TYPE_HTML,
                $this->ConfigModel->getNamaProduk('MP', '50'),
                "Transaksi ".$paymentStatusStr." ".$mpoName,
                $mobile,
                $minimal,
                "MP");

        // Send notifikasi ke device user
        Message::sendFCMNotif(
                $cekPaymentMpo->fcm_token
                , [
            "id" => $idNotif,
            "tipe" => "MP",
            "title" => $this->ConfigModel->getNamaProduk('MP', '50'),
            "tagline" => "Transaksi ".$paymentStatusStr." ".$mpoName,
            "content" => "",
            "paymentType" => $cekPaymentMpo->payment,
            "token" => $cekPaymentMpo->no_hp        
        ]);
        
        // Send email notifikasi
        $this->load->helper('message');
        Message::sendEmail($cekPaymentMpo->email, "Pembelian ".$mpoName.' '.$paymentStatusStr, $emailTemplate);
        
        $this->set_response([
                'status' => 'success',
                'message' => 'Pembayaran Berhasil',
        ], 200);
    }
    
    /**
     * Method untuk menggenerate HTML Konten notifikasi sukses buka tabungan emas
     * @param string $nama
     * @param string $noRekening
     * @param string $cif
     * @param string $tanggalBuka
     * @param string $saldo
     * @param string $namaOutlet
     * @param string $alamatOutlet
     * @param string $teleponOutlet
     * @return String $message HTML Content yang sudah tergenerate
     */
    function generateOpenTabemasNotif(
        $nama,
        $noRekening,
        $cif,
        $tanggalBuka, 
        $saldo,
        $namaOutlet,
        $alamatOutlet,
        $teleponOutlet,
        $trxId
    ) {
        $subject = "Selamat Rekening Tabungan Emas Anda Sudah Aktif";
        
        //Batas waktu KYC = tanggalBuka + 6 bulan
        $batasWaktu = new DateTime($tanggalBuka);
        $interval = new DateInterval('P6M');
        $batasWaktu->add($interval);
        
        //Format tanggal buka
        $fTanggalBuka = new DateTime($tanggalBuka);
        
        
        $viewData = array(
            'nama' => $nama,
            'noRekening' => $noRekening,
            'cif' => $cif,
            'tanggalBuka' => $fTanggalBuka->format('d/m/Y'),
            'saldo' => $saldo,
            'namaOutlet' =>$namaOutlet,
            'alamatOutlet' => $alamatOutlet,
            'teleponOutlet' => $teleponOutlet,
            'batasWaktu' => $batasWaktu->format('d/m/Y'),
            'trxId' => $trxId
        );
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$this->load->view('mail/notif_opentabemas_success', $viewData, true);
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$this->load->view('mail/notif_opentabemas_success', $viewData, true);
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile
        );
    }
    
    /**
     * Method untuk menggenerate template notifikasi HTML beli emas success
     * @param string $nama
     * @param string $tglBayar
     * @param string $payment
     * @param string $amount
     * @param string $gram
     * @param string $administrasi
     * @param string $harga
     * @param string $satuan
     * @param string $totalKewajiban
     * @param string $noRekening
     * @param string $saldo
     * @param string $referensi
     * @return string
     */
    function generateBeliEmasNotif(
        $nama,
        $namaNasabah,
        $cif,
        $tglBayar,
        $payment,
        $amount,
        $gram,
        $administrasi,
        $harga,
        $satuan,
        $totalKewajiban,
        $noRekening,
        $saldo,
        $referensi,
        $biayaTransaksi,
        //promo
        $promoCode,
        $promoAmount,
        $discountAmount
    ) {
        $fTglBayar = new DateTime($tglBayar);
        
        $subject = "Selamat Top Up Tabungan Emas Berhasil";
        
        if($payment == 'BNI'){
            $payment = 'BNI Virtual Account';
        }else if($payment == 'MANDIRI'){
            $payment = 'Mandiri Click Pay';
        }
        //sean
        
        $viewData = array(
            'nama' => $nama,
            'namaNasabah' => $namaNasabah,
            'tglPembayaran' => $fTglBayar->format('d/m/Y'),
            'payment' => $payment,
            'amount' => $amount,
            'gram' => $gram,
            'noRekening' => $noRekening,
            'saldo' => $saldo,
            'administrasi' => $administrasi,
            'harga' => $harga,
            'satuan' => $satuan,
            'totalKewajiban' => $totalKewajiban,
            'referensi' => $referensi,
            'cif' => $cif,
            'biayaTransaksi' => $biayaTransaksi,
            'promoCode' => $promoCode,
            'promoAmount' => $promoAmount,
            'discountAmount' => $discountAmount
        );
        
        log_message('debug', 'View data sendEmailBeliEmasSuccess: '.json_encode($viewData)); 
        error_log("\n DEBUG Notif Beli Emas Sukses: ".json_encode($viewData)."\n", 3, "/data/log/Notif.log");
        
        $content = $this->load->view('mail/notif_beliemas_success', $viewData, true);
        
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$content;
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    function generatePaymentGadaiNotif($nama, $trxId)
    {
        $payment = $this->GadaiModel->getPaymentByTrxId($trxId);
        
        $productCode = substr($payment->norek, 7, 2);
        $jt = $this->ConfigModel->getNamaProduk($payment->jenisTransaksi, $productCode);
        
        $subject = "Transaksi ".$payment->norek.' - '.$jt.' sukses';
        $fTglJatuhTempo = new DateTime($payment->tglJatuhTempo);
       
        $paymentMethod = $payment->payment;
       
        if($paymentMethod == 'BNI'){
            $paymentMethod = 'BNI Virtual Account';
        }else if($paymentMethod == 'MANDIRI'){
            $paymentMethod = 'Mandiri Click Pay';
        }

        $jtName = "";

        if($payment->jenisTransaksi=='CC'){
            $jtName = 'Cicil';
        }else if($payment->jenisTransaksi=='UG'){
            $jtName = 'Ulang Gadai';
        }else if($payment->jenisTransaksi=='TB'){
            $jtName = 'Tebus Gadai';
        }
        
        $viewData = array(
            'amount' => $payment->totalKewajiban,
            'nama' => $nama,
            'namaNasabah' => $payment->namaNasabah,
            'jenisTransaksi' => $jtName,
            'va' => $payment->virtual_account,
            'tglExpired' => $fTglJatuhTempo->format('d/m/Y H:i:s'),
            'norek' => $payment->norek,
            'payment' => $payment->tipe,
            'trxId' => $trxId,
            'hariTarif' => $payment->jumlahHariTarif,
            'up' => $payment->up,
            'sewaModal' => $payment->sewaModal,
            'administrasi' => $payment->administrasi,
            'tglJatuhTempo' => $fTglJatuhTempo->format('d/m/Y')  ,
            'upCicil' => $payment->minimalUpCicil,
            'biayaTransaksi' => $payment->biayaTransaksi,
            'totalKewajiban' => $payment->totalKewajiban ,
            'payment' => $paymentMethod,
            'upLama' => $payment->upLama,
            'nilaiTransaksi' => $payment->nilaiTransaksi,
            'jtCode' => $payment->jenisTransaksi
            
        );
    
        $content = $this->load->view('mail/notif_payment_gadai_success', $viewData, true);
        
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$content;
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
        /**
     * Generate notifikasi payment mikro
     * @param string $nama
     * @param string $trxId
     * @return array
     */
    function generateCetakEmasNotif($trxId) {
        $payment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);

        $subject = "Selamat Order Cetak Tabungan Emas Berhasil";
        $this->load->model('MasterModel');
        $cabang = $this->MasterModel->getSingleCabang($payment['kodeCabang']);            
        $payment['namaCabang'] = $cabang->nama;
        $payment['alamatCabang'] = $cabang->alamat.', '.$cabang->kelurahan.', '.$cabang->kecamatan.', '.$cabang->kabupaten.', '.$cabang->provinsi;
        $payment['noTelpCabang'] = $cabang->telepon;

        $content = $this->load->view('mail/notif_cetakemas_success', $payment, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    /**
    * Reset User PIN EMail notification
    */
    function generateResetPinNotif($nama, $pin, $cabang, $waktu) {

        $viewData = array(
            'nama' => $nama,
            'pin' => $pin,
            'cabang' => $cabang,
            'waktu' => $waktu
        );

        $subject = "Reset PIN Transaksi Finansial";
        $content = $this->load->view('mail/notif_reset_pin', $viewData, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    function generateBuybackEmasNotif($trxId) {
        $payment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        
        $this->load->model('BankModel');
        //Mendapatkan nama bank tujuan transfer
        $payment['namaBank'] = $this->BankModel->getNamaBank($payment['kodeBankTujuan']);

        $subject = "Pembayaran Buyback Emas " . $payment['norek'] . ' Sukses';

        $content = $this->load->view('mail/notif_buybackemas_success', $payment, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function generatePaymentGTENotif($payment){

        $viewData = (array) $payment;
        
        $this->load->model('BankModel');
        //Mendapatkan nama bank tujuan transfer
        $viewData['namaBank'] = $this->BankModel->getNamaBank($viewData['kodeBankTujuan']);

        $subject = "Pembayaran Uang Gadai " . $viewData['noKredit'];

        $content = $this->load->view('mail/notif_gte_success', $viewData, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    /**
     * Generate notifikasi payment mikro
     * @param string $nama
     * @param string $trxId
     * @return array
     */
    function generatePaymentMikroNotif($nama, $trxId)
    {
       $payment = $this->MikroModel->getPaymentByTrxId($trxId);
        
       $subject = "Transaksi Pembayaran Angsuran ".$payment->norek.' Sukses';
       
       $paymentMethod = $payment->payment;
       
        if($paymentMethod == 'BNI'){
            $paymentMethod = 'BNI Virtual Account';
        }else if($paymentMethod == 'MANDIRI'){
            $paymentMethod = 'Mandiri Click Pay';
        }
        
        $viewData = array(
            'nama' => $nama,
            'namaNasabah' => $payment->namaNasabah,            
            'norek' => $payment->norek,
            'trxId' => $trxId,
            'produk' => $payment->namaProduk,
            'angsuranKe' => $payment->angsuranKe,
            'administrasi' => $payment->administrasi,
            'angsuran' => $payment->angsuran,
            'denda' => $payment->denda,
            'total' => $payment->totalKewajiban,
            'totalKewajiban' => $payment->totalKewajiban,
            'biayaTransaksi' => $payment->biayaTransaksi,
            'payment' => $paymentMethod
        );
        
        $content = $this->load->view('mail/notif_payment_mikro_success', $viewData, true);
        
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$content;
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    function generatePaymentMPONotif($mpo, $mpoProduct, $mpoData, $paymentStatus)
    {
        $mpoName = $mpoProduct->namaLayanan;

        $mpoProduct->footer;

        //Translate parameter nama_layanan pada footer
        $mpoFooter = str_replace('{{nama_layanan}}', $mpoProduct->namaLayanan, $mpoProduct->footer);
        
        $subject = 'Pembelian '.$mpoName;

        $content = '';

        if ($mpoProduct->groups === 'seluler') 
        {
            // Order viewData berdasarkan kemunculan pada template
            $viewData = [
                'nama' => $mpo->nama,
                'trxId' => $mpo->reffSwitching,
                'tglTransaksi' => $mpo->updated_at,
                'product' => $mpoName,
                'norek' => $mpo->norek,
                'totalKewajiban' => $mpo->totalKewajiban,
                'paymentMethod' => $mpo->payment,
                'va' => $mpo->virtualAccount,
                'biayaTransaksi' => $mpo->biayaTransaksi,
                'reffBiller' => $mpo->reffBiller,
                'reffMpo' => $mpo->reffMpo,
                'reffCore' => $mpoData->reffCore,
                'serialNumber' => $mpo->serialNumber,
                'sid' => $mpo->sid,
                'mpoFooter' => $mpoFooter,
                'paymentStatus' => $paymentStatus
            ];

            $content = $this->load->view('mail/mpo/notif_payment_mpo_seluler_success', $viewData, true);
        } 
        else if ($mpoProduct->groups === 'listrik' && $mpoProduct->namaLayanan === 'PLN Postpaid') 
        {
            $viewData = [
                'nama' => $mpo->nama,
                'namaPelanggan' => $mpo->namaPelanggan,
                'trxId' => $mpo->reffSwitching,
                'tglTransaksi' => $mpo->updated_at,
                'product' => $mpoName,
                'norek' => $mpo->norek,
                'totalKewajiban' => $mpo->totalKewajiban,
                'paymentMethod' => $mpo->payment,
                'va' => $mpo->virtualAccount,
                'biayaTransaksi' => $mpo->biayaTransaksi,
                'reffBiller' => $mpo->reffBiller,
                'reffMpo' => isset($mpoData->reffMpo) ? $mpoData->reffMpo : $mpo->reffMpo,
                'reffCore' => $mpoData->reffCore,
                'serialNumber' => $mpo->serialNumber,
                'sid' => $mpo->sid,
                'mpoFooter' => $mpoFooter,
                'paymentStatus' => $paymentStatus,
                'idTambahan' => $mpo->idTambahan,
                'tarifDaya' => $mpo->segmen . '/' . $mpo->power,
                'periode' => $this->_formatPLNTglPeriode($mpo->periode)  ,
                'keterangan2' => $mpo->keterangan2,
                'hargaJual' => $mpo->hargaJual,
                'administrasi' => $mpo->administrasi                
            ];

            $content = $this->load->view('mail/mpo/notif_payment_mpo_plnpost_success', $viewData, true);   
        } 
        else if ($mpoProduct->groups === 'listrik' && $mpoProduct->namaLayanan === 'PLN Prepaid') 
        {
            $viewData = [
                'nama' => $mpo->nama,
                'namaPelanggan' => $mpo->namaPelanggan,
                'trxId' => $mpo->reffSwitching,
                'tglTransaksi' => $mpo->updated_at,
                'product' => $mpoName,
                'norek' => $mpo->norek,
                'totalKewajiban' => $mpo->totalKewajiban,
                'paymentMethod' => $mpo->payment,
                'va' => $mpo->virtualAccount,
                'biayaTransaksi' => $mpo->biayaTransaksi,
                'reffBiller' => $mpo->reffBiller,
                'reffMpo' => isset($mpoData->reffMpo) ? $mpoData->reffMpo : $mpo->reffMpo,
                'reffCore' => $mpoData->reffCore,
                'serialNumber' => $mpo->serialNumber,
                'sid' => $mpo->sid,
                'mpoFooter' => $mpoFooter,
                'paymentStatus' => $paymentStatus,
                'idTambahan' => $mpo->idTambahan,
                'tarifDaya' => $mpo->segmen . '/' . $mpo->power,
                'administrasi'  => $mpo->administrasi                  
            ];

            if($paymentStatus['code'] == '1' ){
                $viewData['jumlahKwh']      = $mpoData->jumlahKwh;
                $viewData['token']          = $this->_formatPLNToken($mpoData->token);
                $viewData['byAdministrasi'] = $mpoData->byAdministrasi;
                $viewData['byMaterai']      = $mpoData->byMaterai;
                $viewData['byPpn']          = $mpoData->byPpn;
                $viewData['byPpj']          = $mpoData->byPpj;
                $viewData['angsuran']       = $mpoData->angsuran;              
            }

            $content = $this->load->view('mail/mpo/notif_payment_mpo_plnpre_success', $viewData, true);   
        } 
        else  if ($mpoProduct->groups == 'air') 
        {
            $viewData = [
                'nama' => $mpo->nama,
                'namaPelanggan' => $mpo->namaPelanggan,
                'trxId' => $mpo->reffSwitching,
                'tglTransaksi' => $mpo->updated_at,
                'product' => $mpoName,
                'norek' => $mpo->norek,
                'totalKewajiban' => $mpo->totalKewajiban,
                'paymentMethod' => $mpo->payment,
                'va' => $mpo->virtualAccount,
                'biayaTransaksi' => $mpo->biayaTransaksi,
                'reffBiller' => $mpo->reffBiller,
                'reffMpo' => isset($mpoData->reffMpo) ? $mpoData->reffMpo : $mpo->reffMpo,
                'reffCore' => $mpoData->reffCore,
                'serialNumber' => $mpo->serialNumber,
                'sid' => $mpo->sid,
                'mpoFooter' => $mpoFooter,
                'paymentStatus' => $paymentStatus,
                'idTambahan' => $mpo->idTambahan,
                'tarifDaya' => $mpo->segmen . '/' . $mpo->power,
                'periode' => $mpo->periode,
                'keterangan2' => $mpo->keterangan2,
                'administrasi'  => $mpo->administrasi,
                'hargaJual' => $mpo->hargaJual                
            ];

            $content = $this->load->view('mail/mpo/notif_payment_mpo_pdam_success', $viewData, true);   
        } 
        else  if ($mpoProduct->groups == 'asuransi') 
        {
            $viewData = [
                'nama' => $mpo->nama,
                'namaPelanggan' => $mpo->namaPelanggan,
                'trxId' => $mpo->reffSwitching,
                'tglTransaksi' => $mpo->updated_at,
                'product' => $mpoName,
                'norek' => $mpo->norek,
                'totalKewajiban' => $mpo->totalKewajiban,
                'paymentMethod' => $mpo->payment,
                'va' => $mpo->virtualAccount,
                'biayaTransaksi' => $mpo->biayaTransaksi,
                'reffBiller' => $mpo->reffBiller,
                'reffMpo' => isset($mpoData->reffMpo) ? $mpoData->reffMpo : $mpo->reffMpo,
                'reffCore' => $mpoData->reffCore,
                'serialNumber' => $mpo->serialNumber,
                'sid' => $mpo->sid,
                'mpoFooter' => $mpoFooter,
                'paymentStatus' => $paymentStatus,
                'idTambahan' => $mpo->idTambahan,
                'tarifDaya' => $mpo->segmen . '/' . $mpo->power,
                'periode' => $mpo->periode,
                'keterangan2' => $mpo->keterangan2,
                'administrasi'  => $mpo->administrasi                
            ];

            $content = $this->load->view('mail/mpo/notif_payment_mpo_bpjs_success', $viewData, true);   
        }  
        else  if ($mpoProduct->groups == 'telkom' && $mpoProduct->namaLayanan == 'Telkom') 
        {
            $viewData = [
                'id' => $mpo->id,
                'nama' => $mpo->nama,
                'namaPelanggan' => $mpo->namaPelanggan,
                'trxId' => $mpo->reffSwitching,
                'tglTransaksi' => $mpo->updated_at,
                'product' => $mpoName,
                'norek' => $mpo->norek,
                'totalKewajiban' => $mpo->totalKewajiban,
                'paymentMethod' => $mpo->payment,
                'va' => $mpo->virtualAccount,
                'biayaTransaksi' => $mpo->biayaTransaksi,
                'reffBiller' => $mpo->reffBiller,
                'reffMpo' => isset($mpoData->reffMpo) ? $mpoData->reffMpo : $mpo->reffMpo,
                'reffCore' => $mpoData->reffCore,
                'serialNumber' => $mpo->serialNumber,
                'sid' => $mpo->sid,
                'mpoFooter' => $mpoFooter,
                'paymentStatus' => $paymentStatus,
                'idTambahan' => $mpo->idTambahan,
                'tarifDaya' => $mpo->segmen . '/' . $mpo->power,
                'periode' => $mpo->periode,
                'keterangan2' => $mpo->keterangan2,
                'administrasi'  => $mpo->administrasi,
                'jumlahTagihan' => $mpo->jumlahTagihan,
                'npwp'  => $mpo->npwp,
                'jumlahBill' => $mpo->jumlahBill,
                'hargaJual' => $mpo->hargaJual
            ];

            $dataTagihan = null;
            if ($mpo->dataTagihan !== '') {
                $dataTagihan = json_decode($mpo->dataTagihan);
                $viewData['dataTagihan'] = $dataTagihan;
            }


            $content = $this->load->view('mail/mpo/notif_payment_mpo_telkom_success', $viewData, true);   
        }
        else if($mpoProduct->groups == 'telkom') //Halo
        {
            $viewData = [
                'id' => $mpo->id,
                'nama' => $mpo->nama,
                'namaPelanggan' => $mpo->namaPelanggan,
                'trxId' => $mpo->reffSwitching,
                'tglTransaksi' => $mpo->updated_at,
                'product' => $mpoName,
                'norek' => $mpo->norek,
                'totalKewajiban' => $mpo->totalKewajiban,
                'paymentMethod' => $mpo->payment,
                'va' => $mpo->virtualAccount,
                'biayaTransaksi' => $mpo->biayaTransaksi,
                'reffBiller' => $mpo->reffBiller,
                'reffMpo' => isset($mpoData->reffMpo) ? $mpoData->reffMpo : $mpo->reffMpo,
                'reffCore' => $mpoData->reffCore,
                'serialNumber' => $mpo->serialNumber,
                'sid' => $mpo->sid,
                'mpoFooter' => $mpoFooter,
                'paymentStatus' => $paymentStatus,
                'idTambahan' => $mpo->idTambahan,
                'tarifDaya' => $mpo->segmen . '/' . $mpo->power,
                'periode' => $mpo->periode,
                'keterangan2' => $mpo->keterangan2,
                'administrasi'  => $mpo->administrasi,
                'jumlahTagihan' => $mpo->jumlahTagihan,
                'npwp'  => $mpo->npwp
            ];

            $content = $this->load->view('mail/mpo/notif_payment_mpo_halo_success', $viewData, true);
        }
        else if ($mpoProduct->groups == 'voucher')
        {
            $viewData = [
                'id' => $mpo->id,
                'nama' => $mpo->nama,
                'namaPelanggan' => $mpo->namaPelanggan,
                'trxId' => $mpo->reffSwitching,
                'tglTransaksi' => $mpo->updated_at,
                'product' => $mpoName,
                'norek' => $mpo->norek,
                'totalKewajiban' => $mpo->totalKewajiban,
                'paymentMethod' => $mpo->payment,
                'va' => $mpo->virtualAccount,
                'biayaTransaksi' => $mpo->biayaTransaksi,
                'reffBiller' => $mpo->reffBiller,
                'reffMpo' => isset($mpoData->reffMpo) ? $mpoData->reffMpo : $mpo->reffMpo,
                'reffCore' => $mpoData->reffCore,
                'serialNumber' => $mpo->serialNumber,
                'sid' => $mpo->sid,
                'mpoFooter' => $mpoFooter,
                'paymentStatus' => $paymentStatus,
                'idTambahan' => $mpo->idTambahan,
                'tarifDaya' => $mpo->segmen . '/' . $mpo->power,
                'periode' => $mpo->periode,
                'keterangan2' => $mpo->keterangan2,
                'administrasi'  => $mpo->administrasi,
                'jumlahTagihan' => $mpo->jumlahTagihan,
                'npwp'  => $mpo->npwp
            ];

            $content = $this->load->view('mail/mpo/notif_payment_mpo_ewallet_success', $viewData, true);
        }
        
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$content;
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
        
    }
    
    function _formatPLNToken($str)
    {
        $out = substr($str, 0, 4);
        $out = $out. '-' . substr($str, 4, 4);
        $out = $out. '-' . substr($str, 8, 4);
        $out = $out. '-' . substr($str, 12, 4);
        $out = $out. '-' . substr($str, 16, 4);

        return $out;
    }

    function _formatPLNTglPeriode($str)
    {
        $arrPeriode = json_decode($str);
        $strFormat = '';
        if($arrPeriode){
            $c = 1;
            foreach ($arrPeriode as $a){
                $withoutSpace = str_replace(" ", "", $a);
                if($withoutSpace !== ""){
                    $d = DateTime::createFromFormat('Ym', $withoutSpace);
                    $strFormat = $strFormat.$d->format('M Y');
                    if($c < count($arrPeriode)){
                        $strFormat = $strFormat.', ';
                    }
                }
                $c++;
            }
            
            return $strFormat;
            
        } else {
            return '';
        }
    }

    function core_kyc_post()
    {
        if(!$this->authCore()){
            $this->errorUnAuthorized();
            return;            
        }
        
        $setData = array(
            'cif' => $this->post('cif'),
            'status' => $this->post('status'),
                      
        );
        
        $this->form_validation->set_data($setData);
        
        $this->form_validation->set_rules('cif','CIF','required|numeric');
        $this->form_validation->set_rules('status','status','required|integer');
        
        if($this->form_validation->run()==false){
            $this->set_response(array(
                'status' => 'error',
                'code' => 101,
                'message' => 'Invalid input',
                'errors' => $this->form_validation->error_array()
            ),200);
        }else{
            
            $cif = $this->post('cif');
            $status = $this->post('status');
            $deskripsi = $this->post('deskripsi');
            
            $allowedStatus = ["0","1"];
            
            if(!in_array($status, $allowedStatus)){
                $this->set_response(array(
                    'status' => 'error',
                    'messag' => 'Status tidak valid'
                ),200);
                return;
            }
            
            //Update status KCY di tabel user dan tabungan emas berdasarkan KCY
            $this->NotificationModel->updateKYC($cif,$status);
            
            $kycMessage  = "";
            
            if($status == "1"){
                $kycMessage = "Selamat, status KYC anda telah terverifikasi. Terima kasih telah melakukan verifikasi KYC anda.";
            }else if(status == "0") {
                $kycMessage = "Status KYC anda belum terverifikasi. Mohon lakukan verifikasi KYC untuk menikmati berbagai fitur pegadaian.";
            }
            
            //Mendapatkan user berdasarkan CIF
            $user = $this->User->profile($cif);
            
            //Simpan notifikasi baru
            $idNotif = $this->NotificationModel->add(
                        $user->id, 
                        NotificationModel::TYPE_PROFILE, 
                        NotificationModel::CONTENT_TYPE_TEXT, 
                        "KYC", 
                        "Pembaharuan Status KYC",
                        $kycMessage                        
                        );            

            //Kirim notifikasi pembayaran ke device user
            Message::sendFCMNotif(
                $this->User->getFCMToken($user->id)
                    , [
                "id" => $idNotif,
                "tipe" => "profile",
                "title" => "Pengajuan Pembiayaan Usaha",
                "tagline" => "Pembaharuan Status KYC",
                "content" => $kycMessage,
                "token" => $user->noHP        
            ]);
                        

            //Kirim Email Notifikasi
            $this->load->helper('message');
            Message::sendEmailKYC($user->email, $user->nama, $status, $deskripsi);
            
            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => array(
                    'nama' => $user->nama,
                    'cif' => $user->cif                    
                ),
            ));
        }
    }
    
    function core_mikro_post()
    {
        if(!$this->authCore()){
            $this->errorUnAuthorized();
            return;            
        }       
        
        $this->form_validation->set_rules('kodeBooking','Kode Booking','required|numeric');
        $this->form_validation->set_rules('status','Status','callback_checkStatus');
        $this->form_validation->set_rules('tglPengajuan','Tanggal Pengajuan','required');
        if($this->post('status') == '1'){
            $this->form_validation->set_rules('tglCair','Tanggal Cair','required');
        }
        
        if($this->form_validation->run() == FALSE){
            $this->set_response(array(
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 201,                
                'errors' => $this->form_validation->error_array()
            ), 200);
        }else{
            
            //Load Gadai Model
            $this->load->model('MikroModel');
            
            $status = $this->post('status');
            $kodeBooking = $this->post('kodeBooking');
            $tglPengajuan = $this->post('tglPengajuan');
            $tglCair = $this->post('tglCair');
            $data = $this->post('data');
            
            //Check kode boking exist dan status = 1
            $mikro = $this->MikroModel->getMikroByKodeBooking($kodeBooking);
            
            if(!$mikro){
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Data mikro tidak ditemukan',
                    'code' => 102,                    
                ), 200);     
            }else{
                //Cek validasi data 
                if(!$this->isValidGadaiData($data)){
                     $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid data format',
                        'code' => 101,                    
                    ), 200);
                    return;
                }                
                
                
                $message  = "";
            
                if($status == "1"){
                    
                    $mikroData = json_decode($data);

                    $message = "Hi ".$mikro->nama.", \n"
                            . "Selamat pengajuan Pembiayaan Usaha Anda dengan kode booking ".$mikro->no_pengajuan." yang anda ajukan pada "
                            . "tanggal ".$this->formatTanggal($tglPengajuan)." telah cair pada ".$this->formatTanggal($tglCair).".\n"
                            . "Berikut merupakan rincian pengajuan Anda:\n\n"
                            . "Nomor Kredit: \t\t".$mikroData->noKredit."\n"
                            . "Tanggal Kredit: \t".$this->formatTanggal($mikroData->tglKredit)."\n"
                            . "Tanggal Jatuh Tempo: \t".$this->formatTanggal($mikroData->tglJatuhTempo)."\n"
                            . "Tanggal Lelang: \t".$this->formatTanggal($mikroData->tglLelang)."\n"
                            . "Uang Pinjaman: \t\tRp ".number_format($mikroData->up, "0", ",", ".")."\n"
                            . "Taksiran: \t\tRp ".number_format($mikroData->taksiran, "0", ",", ".")."\n"
                            . "Bunga: \t\t\t".$mikroData->bunga."\n"
                            . "Sewa Modal: \t\tRp ".number_format($mikroData->sewaModal, "0", ",", ".")."\n"
                            . "sewa Modal Maksimal: \tRp ".number_format($mikroData->sewaModalMaksimal, "0", ",", ".")."\n\n"
                            . "Demikian, terima kasih."
                            ;
                }else if($status == "0") {
                    $message = "Hi ".$mikro->nama.", maaf pengajuan gadai Anda dengan kode booking ".$mikro->kode_booking." tidak dapat dicairkan. "
                            . "Mohon hubungi outlet Pegadaian untuk informasi selengkapnya\n\n Demikian, terima kasih ";
                }
                
                $this->MikroModel->mikroDiproses($kodeBooking, $status, $mikroData);

                //Simpan notifikasi baru
                $idNotif = $this->NotificationModel->add(
                            $mikro->user_AIID, 
                            NotificationModel::TYPE_MICRO, 
                            NotificationModel::CONTENT_TYPE_TEXT, 
                            "Pengajuan Pembiayaan Usaha", 
                            "Update Pengajuan Pembiayaan Usaha ",
                            $message,
                            "MC"
                            );            

                //Kirim notifikasi pembayaran ke device user
                Message::sendFCMNotif(
                    $mikro->fcm_token
                        , [
                    "id" => $idNotif,
                    "tipe" => "MC",
                    "title" => "Pengajuan Pembiayaan Usaha",
                    "tagline" => "Update Gadai ",
                    "content" => ""
                ]);


                //Kirim Email Notifikasi
                $this->load->helper('message');
                Message::sendEmailGeneral($mikro->email, "Pengajuan Pembiayaan Usaha", $message);
                
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Notifikasi terkirim'
                ), 200);
            }            
        }
    }
    
    function generateGadaiSuccessNotif($nama, $tglPengajuan, $tglCair, $gadaiData)
    {
           
        $subject = "Pencairan Gadai Berhasil";
        
        $viewData = array(
            'nama' => $nama,
            'tglPengajuan' => $this->formatTanggal($tglPengajuan),
            'tglCair' => $this->formatTanggal($tglCair),
            'noKredit' => $gadaiData->noKredit,
            'tglKredit' => $this->formatTanggal($gadaiData->tglKredit),
            'tglJatuhTempo' => $this->formatTanggal($gadaiData->tglJatuhTempo),
            'tglLelang' => $this->formatTanggal($gadaiData->tglLelang),
            'up' => number_format($gadaiData->up, 0, ",", "."),
            'taksiran' => number_format($gadaiData->taksiran, 0, ",", "."),
            'bunga' => $gadaiData->bunga,
            'sewaModal' => number_format($gadaiData->sewaModal, 0, ",", "."),
            'sewaModalMaksimal' => number_format($gadaiData->sewaModalMaksimal, 0, ",", "."),
            'administrasi' => $gadaiData->administrasi,
            'asuransi' =>  $gadaiData->asuransi,
            'kodeNamaCabang' => $gadaiData->kodeNamaCabang,
            'biayaProsesLelang' => $gadaiData->biayaProsesLelang,
            'biayaLelang' => $gadaiData->biayaLelang,
            'biayaAdmBjpdl'=> $gadaiData->biayaAdmBjpdl,
            'biayaBjdplMax'=> $gadaiData->biayaBjdplMax,
            'namaPinca'=> $gadaiData->namaPinca,
            'noID'=> $gadaiData->noID,
            'namaNasabah'=> $gadaiData->namaNasabah,
            'cif'=> $gadaiData->cif,
            'alamat'=> $gadaiData->alamat,
            'rubrik'=> $gadaiData->rubrik
        );
        
        $content = $this->load->view('mail/notif_gadai_success', $viewData, true);
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$content;
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    function generatePaymentWalletNotifSuccess($data)
    {
           
        $subject = "Top Up Wallet Success";
        
        $viewData = (array) $data;
        
        $content = $this->load->view('mail/notif_topup_wallet_success', $viewData, true);
        
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $message = $message.$content;
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    
    /**
     * Endpoint notifikasi dari core untuk pengajuan gadai     * 
     */
    function detail_kontrak($kodeBooking, $jenis_gadai)
    {
      if(!$this->authCore()){
            $this->errorUnAuthorized();
            return;            
      }
        $detailGadai = $this->GadaiModel->getDetailKontrakGod($kodeBooking, $jenis_gadai);
        $gadaiByKodeBooking = $this->GadaiModel->getGadaiByKodeBooking($kodeBooking);
        $subjekEmail = 'Detail Kontrak (Kode Booking ' . $kodeBooking . ')';
        include "phpqrcode/qrlib.php"; 
        $tempdir = "uploaded/user/gadai/"; 
        if (!file_exists($tempdir))
            mkdir($tempdir);
        $nasabah = $detailGadai->nasabah;
        $cif = $detailGadai->cif;
        $noIdentitas = $detailGadai->noIdentitas;
        $kodeOtp = $detailGadai->kodeOtp;
        $tglBooking = $detailGadai->tglBooking;
        $isi_teks = array('namaNasabah'=>$nasabah,
                          'cif'=>$cif,
                          'no_ktp'=>$noIdentitas,
                          'kodeOtp'=>$kodeOtp,
                          'tglBooking'=>date('d-m-Y', strtotime($tglBooking))
                        );
        $data= json_encode($isi_teks);
        $hash_kodeBooking = sha1($kodeBooking);
        $namafile = $hash_kodeBooking.".png";
        $quality = 'H'; 
        $ukuran = 5; 
        $padding = 0;
        $image=QRCode::png($data,$tempdir.$namafile,$quality,$ukuran,$padding);
        if ($jenis_gadai=='2'){
          $this->GadaiModel->simpanBarcode(
            $detailGadai->user_AIID,
            $kodeBooking,
            $isi_teks,
            $namafile
          );
        } else {
          $this->GadaiModel->simpanBarcodePerhiasan(
            $detailGadai->user_AIID,
            $kodeBooking,
            $isi_teks,
            $namafile
          );
        }
       
        $user = $this->User->getUser($detailGadai->user_AIID);
        $checkPayment=$this->GadaiModel->getPaymentGadaiByKodeBooking($kodeBooking); 
        $templateData = array(
          'noPengajuan' => $kodeBooking,
          'barcode' => $namafile,
          'tglLelang' => $detailGadai->tglLelang,
          'tglTransaksi' => $detailGadai->tglTransaksi,
          'administrasi' => $detailGadai->administrasi,
          'serialNumber' => $detailGadai->serialNumber,
          'sewaModal' => $detailGadai->sewaModal,
          'tglJatuhTempo' => $detailGadai->tglJatuhTempo,
          'biayaProsesLelang' => $detailGadai->biayaProsesLelang,
          'biayaLelang' => $detailGadai->biayaLelang,
          'nasabah' => $detailGadai->nasabah,
          'kodeNamaCabang' => $detailGadai->kodeNamaCabang,
          'biayaAdmBjpdl' => $detailGadai->biayaAdmBjpdl,
          'biayaBjdplMax' => $detailGadai->biayaBjdplMax,
          'tanggal_pembayaran' => $detailGadai->tanggal_pembayaran,
          'jumlahHariTarif' => $detailGadai->jumlahHariTarif,
          'taksir_ulang_admin' => $detailGadai->taksir_ulang_admin,
          'namaPetugas' => $detailGadai->nama,
          'noIdentitas' => $detailGadai->noIdentitas,
          'jalan' => $detailGadai->jalan,
          'rubrik' => $detailGadai->rubrik,
          'taksiran' => $detailGadai->taksiran,
          'up' => $detailGadai->up,
          'tglKredit' => $detailGadai->tglKredit,
          'cif' => $detailGadai->cif,
          'no_kredit' => $detailGadai->no_kredit,
          'nama_group' => $detailGadai->nama_group,
          'nama_cabang' => $detailGadai->nama_cabang,
          'namaPinca' => $detailGadai->namaPinca,
          'detailGadai' => $detailGadai,
          'checkPayment' => $checkPayment,
          'asuransi' => $detailGadai->asuransi,
          'diterimaNasabah' => $detailGadai->diterimaNasabah
        );

        $content = $this->load->view('mail/lihat_detail_gadai', $templateData, TRUE);
        Message::sendEmail($detailGadai->email, $subjekEmail, $content);
        //send email to cabang
        Message::sendEmail($gadaiByKodeBooking['data']->email_outlet, $subjekEmail, $content);
        $mobile = $this->load->view('notification/top_template', array('title'=>"Detail Kontrak"), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);

        $idNotif = $this->NotificationModel->add(
                        $detailGadai->user_AIID, 
                        NotificationModel::TYPE_GADAI, 
                        NotificationModel::CONTENT_TYPE_HTML, 
                        "Detail Kontrak Berhasil", 
                        "Kode Booking ".$kodeBooking,
                        $mobile,
                        $content,
                        "GOD"
                        );
        
        Message::sendFCMNotif(
               $this->User->getFCMToken($detailGadai->user_AIID)
                   , [
               "id" => $idNotif,
               "tipe" => "GOD",
               "title" => "Detail Kontrak Berhasil",
               "tagline" => "Kode Booking ".$kodeBooking,
               "content" => '',
               "action_url" => 'Lihat detail kontrak',
        ]);

        $this->set_response(array(
          'status' => 'success',
          'message'=>'Lihat Detail Kontrak ',
          'data' => 'true',
          'html' => $content
        ),200);

    }

    function core_gadai_post()
    {
        if(!$this->authCore()){
            $this->errorUnAuthorized();
            return;            
        }
        $this->form_validation->set_rules('kodeBooking','Kode Booking','required|numeric');
        $this->form_validation->set_rules('status','Status','callback_checkStatus');
        $this->form_validation->set_rules('tglPengajuan','Tanggal Pengajuan','required');
        if($this->post('status') == '1'){
            $this->form_validation->set_rules('tglCair','Tanggal Cair','required');
        }
                
        if($this->form_validation->run() == FALSE){
            log_message('debug', 'RESPONSE INFO FUNCTION: ' . __FUNCTION__);
            log_message('debug', 'RESPONSE INFO MESSAGE: Invalid input');
            $this->set_response(array(
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 201,                
                'errors' => $this->form_validation->error_array()
            ), 200);
        }else{
            //Load Gadai Model
            $this->load->model('GadaiModel');
            

            $status = $this->post('status');
            $kodeBooking = $this->post('kodeBooking');
            $tglPengajuan = $this->post('tglPengajuan');
            $jenis_gadai = $this->post('jenis_gadai');
            $tglCair = $this->post('tglCair');
            $data = $this->post('data');

            //Check kode boking exist dan status = 1
            $checkGadai = $this->GadaiModel->getGadaiByKodeBooking($kodeBooking);
            
            if(!$checkGadai){
                log_message('debug', 'RESPONSE INFO FUNCTION: ' . __FUNCTION__);
                log_message('debug', 'RESPONSE INFO MESSAGE: Data gadai tidak ditemukan');
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Data gadai tidak ditemukan',
                    'code' => 102,                    
                ), 200);     
            }else{
                //Cek validasi data 
                if(!$this->isValidGadaiData($data)){
                    log_message('debug', 'RESPONSE INFO FUNCTION: ' . __FUNCTION__);
                    log_message('debug', 'RESPONSE INFO MESSAGE: Invalid data format');
                     $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid data format',
                        'code' => 101,                    
                    ), 200);
                    return;
                }
                
                $gadai = $checkGadai['data'];
                $message  = "";
                $emailMessage = "";
                $contentType = "";

                $gadaiData = null;
            
                if($status == "1"){
                    
                    $gadaiData = json_decode($data);
                    $template = $this->generateGadaiSuccessNotif($gadai->nama, $tglPengajuan, $tglCair, $gadaiData);

                    $mobileTemplate = $template['mobile'];
                    $emailTemplate = $template['email'];

                    $contentType = NotificationModel::CONTENT_TYPE_HTML;
                    $message = $mobileTemplate;
                    $emailMessage = $emailTemplate;
                    
                }else if($status == "0") {

                    $message = "Hi ".$gadai->nama.", maaf pengajuan gadai Anda dengan kode booking ".$gadai->kode_booking." tidak dapat dicairkan. "
                            . "Mohon hubungi outlet Pegadaian untuk informasi selengkapnya\n\n Demikian, terima kasih ";
                    $contentType = NotificationModel::CONTENT_TYPE_TEXT;
                    $emailMessage = $message;
                    
                }
                  $this->GadaiModel->gadaiDiproses($kodeBooking, $checkGadai['table'], $status, $gadaiData);
                  $this->GadaiModel->gadaiDiprosesGod($checkGadai['data']->user_AIID, $kodeBooking, $checkGadai['table'], $status, $gadaiData);
                  $this->detail_kontrak($kodeBooking, $jenis_gadai);

                //Simpan notifikasi baru
                $idNotif = $this->NotificationModel->add(
                            $gadai->user_AIID, 
                            NotificationModel::TYPE_GADAI, 
                            $contentType, 
                            "Pengajuan Gadai", 
                            "Update Gadai ".$checkGadai['type'],
                            $message,
                            "GD"
                            );            

                //Kirim notifikasi pembayaran ke device user
                // Message::sendFCMNotif(
                //     $gadai->fcm_token
                //         , [
                //     "id" => $idNotif,
                //     "tipe" => "GD",
                //     "title" => "Gadai",
                //     "tagline" => "Update Gadai ".$checkGadai['type'],
                //     "content" => ""
                // ]);


                //Kirim Email Notifikasi
                $this->load->helper('message');
                // Message::sendEmail($gadai->email, $gadai->email_outlet, "Pengajuan Gadai", $emailMessage);
                log_message('debug', 'RESPONSE INFO FUNCTION: ' . __FUNCTION__);
                log_message('debug', 'RESPONSE INFO MESSAGE: Notifikasi terkirim kode booking '.$kodeBooking);
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Notifikasi terkirim kode booking '.$kodeBooking,
                    'data' => $gadaiData
                ), 200);
            }

        }

    }

    function core_gadai_efek_post(){		    
        if (!$this->authCore()) {		
            $this->errorUnAuthorized();		
            return;		
        }		
        $this->form_validation->set_rules('kodeBooking','kodeBooking','required');		
        $this->form_validation->set_rules('status','status','required|integer');		
        $this->form_validation->set_rules('data','data','required');		
        $this->form_validation->set_rules('tglPengajuan','tglPengajuan','required');       		
        $this->form_validation->set_rules('tglCair','tglCair','required');    		
                		
        if($this->form_validation->run() == false)		
        {		
            return $this->set_response([		
                'status' => 'error',		
                'message' => 'Invalid input',		
                'code' => 102,		
                'errors' => $this->form_validation->error_array()		
            ]);		
        }		
        else		
        {		
            $status = $this->post('status');		
            $kodeBooking = $this->post('kodeBooking');		
            $data = $this->post('data');		
            $tglPengajuan = $this->post('tglPengajuan');		
            $tglCair = $this->post('tglCair');		
            if ($status === "1" || $status === "0")		
            {		
                $updateData = [];		
                $subtitle = '';		
                if($status == '1'){		
                    $payload = json_decode($data);		
                    $updateData = [		
                        'noKredit' => isset($payload->noKredit) ? $payload->noKredit : null,		
                        'noBookingLos' => isset($payload->noBookingLos) ? $payload->noBookingLos : null,		
                        'tglKredit' => isset($payload->tglKredit) ? $payload->tglKredit : null,		
                        'tglJatuhTempo' => isset($payload->tglJatuhTempo) ? $payload->tglJatuhTempo : null,		
                        'tglLelang' => isset($payload->tglLelang) ? $payload->tglLelang : null,		
                        'taksiran' => isset($payload->taksiran) ? $payload->taksiran : null,		
                        'up' => isset($payload->up) ? $payload->up : null,		
                        'bunga' => isset($payload->bunga) ? $payload->bunga : null,		
                        'sewaModal' => isset($payload->sewaModal) ? $payload->sewaModal : null,		
                        'sewaModalMaksimal' => isset($payload->sewaModalMaksimal) ? $payload->sewaModalMaksimal : null,		
                        'tglCair' => $tglCair,		
                        'status' => '2' // status cair di PDS 2		
                    ];		
                    $subtitle = "Pengajuan Gadai Cair (Kode Booking ".  $kodeBooking. ")";    		
                }else if($status == '0'){		
                    $updateData = [		
                        'tglCair' => $tglCair,		
                        'status' => '0' // status ditolak 0 di pds		
                    ];		
                    $subtitle = "Pengajuan Gadai Ditolak (Kode Booking ".  $kodeBooking. ")";  		
                }		
                		
                $updateEfek = $this->GadaiModel->updateGadaiEfek($kodeBooking, $updateData);		
                // Get booking details		
                $details = $this->GadaiModel->getGadaiEfek($kodeBooking);		
                if(!$details){		
                    return $this->set_response([   		
                        'status' => 'error',		
                        'message' => 'Data booking gadai tidak ditemukan',		
                        'code' => 102		
                    ]);		
                }		
                		
                $details->status = $status;		
                if($details)		
                {		
                    // Send notif		
                    $template = $this->generateEfekNotif((array) $details);		
                    $mobileTemplate = $template['mobile'];		
                    $emailTemplate = $template['email'];		
                    $minimalTemplate = $template['minimal'];		
                    //Simpan notifikasi baru		
                    $notifId = $this->NotificationModel->add(		
                            $details->user_AIID, 		
                            NotificationModel::TYPE_GADAI, 		
                            NotificationModel::CONTENT_TYPE_HTML,		
                            "Gadai Efek",		
                            $subtitle,		
                            $mobileTemplate, $minimalTemplate, "GD"		
                    );		
                    //Kirim notifikasi pembayaran ke device user		
                    Message::sendFCMNotif(		
                            $details->fcm_token		
                            , [		
                        "id" => $notifId,		
                        "tipe" => "GD",		
                        "title" => "Gadai Efek",		
                        "tagline" => $subtitle,		
                        "content" => "",		
                        "token" => $details->no_hp        		
                    ]);		
                    //Kirim Email Notifikasi		
                    $this->load->helper('message');		
                    Message::sendEmail($details->email, $subtitle, $emailTemplate);		
                    $this->set_response(array(		
                        'status' => 'success',		
                        'message' => 'Notifikasi terkirim'		
                    ), 200);		
                }		
                else		
                {		
                    $this->set_response([		
                        'status' => 'error',		
                        'message' => 'Data tidak ditemukan',		
                        'code' => 102		
                    ]);		
                }		
            } else if ($status == "3") {		
                $updateEfek = $this->GadaiModel->updateGadaiEfek($kodeBooking, [		
                    'tglCair' => $tglCair,		
                    'status' => '3'        		
                ]);		
                $this->set_response(array(		
                    'status' => 'success',		
                    'message' => 'Notifikasi terkirim'		
                ), 200);		
            } 		
        }		
    }		
    function generateEfekNotif($data){		
        $subject = "Pengajuan Gadai Cair (Kode Booking ".  $data['bookingId']. ")";		
                		
        $content = $this->load->view('mail/efek/callback', $data, true);		
        		
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 		
        $email = $email.$content;		
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);		
        		
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 		
        $mobile = $mobile.$content;		
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);		
                		
        return array(		
            'email' => $email,		
            'mobile' => $mobile,		
            'minimal' => $content		
        );    		
    }
    
    function passion_post($method){
        if(!$this->authCore()){
            $this->errorUnAuthorized();
            return;            
        }
        
        //callback link cif dari core
        if($method == 'cif'){
            
            $this->form_validation->set_rules('cif','cif','required|numeric');
            $this->form_validation->set_rules('noHp','noHp','required');
            $this->form_validation->set_rules('namaNasabah','namaNasabah','required');
            $this->form_validation->set_rules('ibuKandung','ibuKandung','required');
            $this->form_validation->set_rules('tanggalLahir','tanggalLahir','required');
            
            $this->form_validation->set_rules('jenisKelamin','jenisKelamin','required');
            $this->form_validation->set_rules('tempatLahir','tempatLahir','required');
            $this->form_validation->set_rules('noIdentitas','noIdentitas','required');
            $this->form_validation->set_rules('statusKawin','statusKawin','required');
            $this->form_validation->set_rules('kewarganegaraan','kewarganegaraan','required');
            $this->form_validation->set_rules('tipeIdentitas','tipeIdentitas','required');
            $this->form_validation->set_rules('idKelurahan','idKelurahan','required');
            $this->form_validation->set_rules('jalan','jalan','required');
            $this->form_validation->set_rules('kodeCabang','kodeCabang','required');
            
            $this->form_validation->set_rules('statusKyc','statusKyc','required');
            
            
            if($this->form_validation->run() == FALSE){
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 101,
                    'message' => 'Invalid input',
                    'data' => $this->form_validation->error_array()
                ),200);
            }else{
                $cif = $this->post('cif');
                $noHp = $this->post('noHp');
                $namaNasabah = $this->post('namaNasabah');
                $ibuKandung = $this->post('ibuKandung');
                $tanggalLahir = $this->post('tanggalLahir');
                
                $jenisKelamin = $this->post('jenisKelamin');
                $tempatLahir = $this->post('tempatLahir');
                $noIdentitas = $this->post('noIdentitas');
                $statusKawin = $this->post('statusKawin');
                $kewarganegaraan = $this->post('kewarganegaraan');
                $tipeIdentitas = $this->post('tipeIdentitas');
                $idKelurahan = $this->post('idKelurahan');
                $jalan = $this->post('jalan');
                $kodeCabang = $this->post('kodeCabang');
                
                $statusKyc = $this->post('statusKyc');
                

                //Get user berdasarkan no hp
                $user = $this->User->getUser($noHp);
            
                if($user){

                    //Lakukan lin cif
                    $cek = $this->customerLink($cif, $ibuKandung, $namaNasabah, $tanggalLahir, $noHp, '6017');

                    if($cek->responseCode == '00')
                    {
                        //Update user dan aktifasi wallet
                        $updateData = array(
                            'nama' => $namaNasabah,
                            'nama_ibu' => $ibuKandung,
                            'tgl_lahir' => $tanggalLahir,
                            'cif' => $cif,
                            'last_update_link_cif' => date('Y-m-d H:i:s'),
                            'jenis_kelamin' =>$jenisKelamin,
                            'tempat_lahir' => $tempatLahir,
                            'no_ktp' => $noIdentitas,
                            'status_kawin' => $statusKawin,
                            'kewarganegaraan' => $kewarganegaraan,
                            'jenis_identitas' => $tipeIdentitas,
                            'id_kelurahan' => $idKelurahan,
                            'alamat' => $jalan,
                            'kyc_verified' => $statusKyc,
                            'kode_cabang' => $kodeCabang
                        );

                        $this->User->updateUser($user->user_AIID, $updateData);

                        //Aktifasi wallet
                        //Lakukan aktivasi wallet
                        $paramsWallet = array(
                            'channelId' => '6017',
                            'cif' => $cif,
                            'noHp' => $noHp,
                            'tipe' => 1
                        );

                        $responseWallet = $this->aktivasiWallet($paramsWallet);

                        if ($responseWallet->responseCode == '00') {

                            $response_data = json_decode($responseWallet->data);
                            $data_update = array(
                                'saldo' => $response_data->saldo,
                                'norek' => $response_data->norek
                            );
                            $this->User->updateUser($user->user_AIID, $data_update);

                            //Send Notifikasi Link CIF success
                            $notifData = array(
                                'namaNasabah' => $user->nama,
                                'cif' => $cif,
                                'tanggalCif' => date('Y-m-d H:i:s')
                            );

                            $template = $this->generateLinkCifNotif($notifData);

                            $mobileTemplate = $template['mobile'];
                            $emailTemplate = $template['email'];
                            $minimalTemplate = $template['minimal'];

                            //Simpan notifikasi baru
                            $notifId = $this->NotificationModel->add(
                                $user->user_AIID, 
                                NotificationModel::TYPE_PROFILE, 
                                NotificationModel::CONTENT_TYPE_HTML, 
                                "Profile Info", 
                                "Link CIF Berhasil",
                                $mobileTemplate,
                                $minimalTemplate,
                                "LK"
                            );

                            $this->load->helper('message');
                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $user->fcm_token
                                , [
                            "id" => $notifId,
                            "tipe" => "LK",
                            "title" => "Profile Info",
                            "tagline" => "Link CIF Berhasil",
                            "content" => "Link CIF Berhasil",
                            "token" => $user->no_hp
                            ]);

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail(
                                $user->email, 'Selamat LINK CIF Anda Berhasil', $emailTemplate
                            );
                            
                            $this->set_response(array(
                                'status' => 'success',
                                'message' => 'Link CIF dan aktifasi wallet berhasil',
                                'data' => array(
                                    'cif' =>$cif,
                                    'noHp' => $noHp,
                                    'ibuKandung' => $ibuKandung,
                                    'namaNasabah' => $namaNasabah,
                                    'tanggalLahir' => $tanggalLahir
                                )
                            ),200);                        
                            
                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => $responseWallet->responseDesc,
                                'code' => $responseWallet->responseCode,
                                'data' => null
                                
                            ),200);
                        }
                    }else{
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => $cek->responseDesc,
                            'code' => $cek->responseCode,
                            'data' => null
                        ),200);
                        
                    } 
                    log_message('debug', __FUNCTION__ . 'Link CIF Response '. json_encode($cek));                   

                }else{
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'User tidak ditemukan',
                        'data' => null
                    ),200);
                }               

            }
        }else if($method == 'transaksi_finansial'){
            $this->form_validation->set_rules('cif','cif','required|numeric');
            $this->form_validation->set_rules('noHp','noHp','required');
            $this->form_validation->set_rules('flag','flag','required|integer');
            
            if($this->form_validation->run() == FALSE){
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 101,
                    'message' => 'Invalid input',
                    'data' => null
                ),200);
            }else{
                $cif = $this->post('cif');
                $noHp = $this->post('noHp');
                $flag = $this->post('flag');

                if($flag != '0' && $flag != '1' && $flag != '2'){
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'flag harus 0 atau 1',
                        'data' => null
                    ),200);
                    return;                
                }

                //Get user berdasarkan no hp dan cif
                $user = $this->User->getUser(array(
                    'cif' => $cif,
                    'no_hp' => $noHp
                ));

                if($user){
                    
                    //Update status user
                    $this->User->updateUser($user->user_AIID, 
                        array('aktifasiTransFinansial'=>$flag,
                        'kyc_verified'=>'1',
                        'tanggal_aktifasi_finansial'=>date('Y-m-d H:i:s'),
                        'is_dukcapil_verified'=>'1'
                    ));

                    $status = null;
                    if($flag == '0'){
                        $status = 'Tidak Aktif';
                    }else if($flag == '1'){
                        $status = 'Aktif';
                    }else if($flag == '2'){
                        $status = 'Blokir';
                    }

                    //Send Notifikasi Transaksi Finansial
                    $notifData = array(
                        'namaNasabah' => $user->nama,
                        'cif' => $cif,
                        'status' => $status,
                        'tanggal' => date('Y-m-d H:i:s')
                    );

                    $template = $this->generateAktifasiTransFinansialNotif($notifData);

                    $mobileTemplate = $template['mobile'];
                    $emailTemplate = $template['email'];
                    $minimalTemplate = $template['minimal'];

                    //Simpan notifikasi baru
                    $notifId = $this->NotificationModel->add(
                        $user->user_AIID, 
                        NotificationModel::TYPE_PROFILE, 
                        NotificationModel::CONTENT_TYPE_HTML, 
                        "Profile Info", 
                        "Aktifasi Transaksi Finansial",
                        $mobileTemplate,
                        $minimalTemplate,
                        "AF"
                    );

                    $this->load->helper('message');
                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $user->fcm_token
                        , [
                    "id" => $notifId,
                    "tipe" => "AF",
                    "title" => "Profile Info",
                    "tagline" => "Aktifasi Transaksi Finansial",
                    "content" => "Aktifasi Transaksi Finansial",
                    "status" => $flag,
                    "token" => $user->no_hp
                    ]);

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmail(
                        $user->email, 'Aktifasi Transaksi Finansial', $emailTemplate
                    );

                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Status aktifasi transaksi finansial dirubah',
                        'data' => array(
                            'noHp' => $noHp,
                            'cif' => $cif,
                            'flag' => $flag
                        )
                    ));                   


                }else{
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'User tidak ditemukan',
                        'data' => null
                    ),200);
                }
                
                
            }

        }else if($method == 'bank'){
            $this->form_validation->set_rules('cif','cif','required|numeric');
            $this->form_validation->set_rules('noHp','noHp','required');
            $this->form_validation->set_rules('rekening','rekening','required');

            if($this->form_validation->run() == FALSE){
                $this->set_response(array(
                    'status' => 'error',
                    'code' => 101,
                    'message' => 'Invalid input'
                ),200);
            }else{
                $cif = $this->post('cif');
                $noHp = $this->post('noHp');
                $rawRekening = $this->post('rekening');
                $rekening =  json_decode($rawRekening, true);

                if (json_last_error() !== JSON_ERROR_NONE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input. Rekening harus array of object ',
                        'code' => 101,
                    ),200);
                    return;

                }else{

                    //Check setiap key->value dari json
                    $validJSON = true;
                    foreach ($rekening as $r) {
                        if(
                            !array_key_exists("kodeBank", $r) || 
                            !array_key_exists("norekBank", $r) || 
                            !array_key_exists("namaNasabahBank", $r)
                        ){
                            $validJSON = false;
                            break;
                        }
                    }

                    

                    if($validJSON){

                        //Mendapatkan user berdasarkan cif dan no hp
                        $user = $this->User->getUser(array(
                            'cif' => $cif,
                            'no_hp' => $noHp
                        ));

                        if($user){

                            $rekening2 = array();

                            $this->load->model('BankModel');  
                            
                            //Begin transsaction                            
                            $this->db->trans_begin();
                            
                            //Hapus rekening bank user
                            $this->db->where('user_AAID', $user->user_AIID)->delete('rekening_bank');
                            
                            foreach($rekening as $r){
                                
                                $rtemp = $r;
                                $rtemp['namaBank'] = $this->BankModel->getNamaBank($r['kodeBank']);
                                $rekening2[] = $rtemp;
                                
                                //Untuk setiap rekening simpan
                                $this->load->model('BankModel');

                                $this->db->insert('rekening_bank', array(
                                    'kode_bank' => $r['kodeBank'],
                                    'nama_pemilik' => $r['namaNasabahBank'],
                                    'nomor_rekening' => $r['norekBank'],
                                    'user_AAID' => $user->user_AIID
                                ));             
                                
                            }
                            
                            if ($this->db->trans_status() === FALSE)
                            {
                                    $this->db->trans_rollback();
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'DB Error',
                                        'data' => null
                                    ),200);
                            }
                            else
                            {
                                $this->db->trans_commit();
                                
                                //Send Notifikasi Transaksi Finansial
                                $notifData = array(
                                    'namaNasabah' => $user->nama,
                                    'cif' => $cif,
                                    'tanggal' => date('Y-m-d H:i:s'),
                                    'rekening' => $rekening2
                                );

                                $template = $this->generateBankNotif($notifData);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                    $user->user_AIID, 
                                    NotificationModel::TYPE_PROFILE, 
                                    NotificationModel::CONTENT_TYPE_HTML, 
                                    "Profile Info", 
                                    "Rekening Bank",
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "BK"
                                );

                                $this->load->helper('message');
                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $user->fcm_token
                                    , [
                                "id" => $notifId,
                                "tipe" => "BK",
                                "title" => "Profile Info",
                                "tagline" => "Rekening Bank",
                                "content" => "Rekening Bank",
                                "bank" => $rekening2,
                                "token" => $user->no_hp
                                ]);

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmail(
                                    $user->email, 'Rekening Bank', $emailTemplate
                                );

                                $this->set_response(array(
                                    'status' => 'success',
                                    'message' => 'Bank berhasil ditambahkan',
                                    'data' => $rekening2
                                ),200);
                                    
                            }

                        }else{
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'User tidak ditemukan',
                                'data' => null
                            ),200);
                        }

                    }else{
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Invalid input rekening',
                            'data' => null
                        ),200);
                    }


                }

            }

        }
          
    }


    function generateAktifasiTransFinansialNotif($viewData)
    {
        $subject = "Aktifasi Transaksi Finansial";              
                
        $content = $this->load->view('mail/notif_aktifasi_trans_finansial', $viewData, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function generateLinkCifNotif($viewData)
    {
        $subject = "Selamat Link CIF Berhasil";              
                
        $content = $this->load->view('mail/notif_link_cif', $viewData, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function generateBankNotif($viewData)
    {
        $subject = "Rekening Bank";              
                
        $content = $this->load->view('mail/notif_rekening_bank', $viewData, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    function formatTanggal($tanggal)
    {
        $date = new DateTime($tanggal);
        return $date->format('d-m-Y');
    }
    
    function checkStatus($status)
    {
        $allowedStatus = [0 , 1];
        if(!in_array($status, $allowedStatus)){
            $this->form_validation->set_message('checkStatus', 'The {field} only allow 0 or 1');
            return FALSE;
        }else{
            return TRUE;
        }
    }
    
    function isValidGadaiData($data)
    {
        $d = json_decode($data);
        if(json_last_error() == JSON_ERROR_NONE){
            if($d->noKredit == NULL){
                return FALSE;
            }
            
            if($d->tglKredit == NULL){
                return FALSE;
            }
            
            if($d->tglJatuhTempo == NULL){
                return FALSE;
            }
            
            if($d->tglLelang == NULL){
                return FALSE;
            }
            
            if($d->up == NULL){
                return FALSE;
            }
            
            if($d->taksiran == NULL){
                return FALSE;
            }
            
            if($d->bunga == NULL){
                return FALSE;
            }
            
            if($d->sewaModal == NULL){
                return FALSE;
            }
            
            if($d->sewaModalMaksimal == NULL){
                return FALSE;
            }
            
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Endpoint untuk mengirimkan notifikasi tarik tunai dari core ke user PDS
     */
    function core_gcash_post()
    {
        if(!$this->authCore()){
            $this->errorUnAuthorized();
            return;            
        }

        $this->form_validation->set_rules('userId','userId','required|numeric');
        $this->form_validation->set_rules('jenisTransaksi','jenisTransaksi','required');
        $this->form_validation->set_rules('reffId', 'reffId','required');
        $this->form_validation->set_rules('gcashId','gcashId','required');
        $this->form_validation->set_rules('tglTransaksi', 'tglTransaksi', 'required');
        $this->form_validation->set_rules('bankName','required','required');
        $this->form_validation->set_rules('status','status','required');
        $this->form_validation->set_rules('amount','amount','required');

        if($this->form_validation->run() == FALSE)
        {
            $this->set_response([
                'status' => 'error',
                'code' => 101,
                'message' => 'Invalid input',
                'errors' => $this->form_validation->error_array()
            ]);
        }
        else
        {
            $userId = $this->post('userId');
            $jenisTransaksi = $this->post('jenisTransaksi');
            $reffSwitching = $this->post('reffId');
            $gcashId = $this->post('gcashId');
            $tglTransaksi = $this->post('tglTransaksi');
            $bankName = $this->post('bankName');
            $status = $this->post('status');
            $amount = $this->post('amount');

            $user = $this->User->getUser($userId);
            if(!$user)
            {
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'User tidak ditemukan',
                    'code' => 101
                ]);
            }

            $notifData = [
                'nama' => $user->nama,
                'cif' => $userId,
                'jenisTransaksi' => $jenisTransaksi,
                'reffSwitching' => $reffSwitching,
                'gcashId' => $gcashId,
                'tglTransaksi' => $tglTransaksi,
                'bankName' => $bankName,
                'status' => $status,
                'amount' => $amount
            ];

            $template = $this->generateTarikTunaiNotif($notifData);

            $mobileTemplate = $template['mobile'];
            $emailTemplate = $template['email'];
            $minimalTemplate = $template['minimal'];

            //Simpan notifikasi baru
            $notifId = $this->NotificationModel->add(
                $user->user_AIID, 
                NotificationModel::TYPE_GCASH, 
                NotificationModel::CONTENT_TYPE_HTML, 
                $jenisTransaksi.' G-Cash',
                $jenisTransaksi.' G-Cash '.$status,
                $mobileTemplate,
                $minimalTemplate,
                "GC"
            );

            $this->load->helper('message');
            //Kirim notifikasi pembayaran ke device user
            Message::sendFCMNotif(
                $user->fcm_token
                , [
            "id" => $notifId,
            "tipe" => "GC",
            "title" => $jenisTransaksi.' G-Cash',
            "tagline" => $jenisTransaksi.' G-Cash '.$status,
            "content" => $jenisTransaksi.' G-Cash '.$status,
            "token" => $user->no_hp
            ]);

            //Kirim Email Notifikasi
            $this->load->helper('message');
            Message::sendEmail(
                $user->email, $jenisTransaksi.' G-Cash '.$status, $emailTemplate
            );

            $this->set_response(array(
                'status' => 'success',
                'message' => 'Notifikasi berhasil dikirim',
                'data' => $notifData
            ),200);
            
        }

    }

    function generateTarikTunaiNotif($data)
    {
        $subject = $data['jenisTransaksi'].' G-Cash';             
                
        $content = $this->load->view('mail/gcash/tariktopup', $data, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true); 
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true); 
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    // Fungsi untuk mengirim notifikasi promo yang di terima oleh nasabah
    function send_sms_promo_post(){
        if(!$this->authCore()){
            $this->errorUnAuthorized();
            return;            
        }
        // set rules parameter
        $this->form_validation->set_rules('message','message','required');
        $this->form_validation->set_rules('noHp','noHp','required');

        if ($this->form_validation->run()) {
            // create array value for send to core
            $data = [
                'message' => $this->post('message'),
                'noHp' => $this->post('noHp')
            ];
            $sendSMS = $this->smsNotifPromo($data);

            if ($sendSMS->responseCode == '00') {
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Notifikasi SMS berhasil dikirim',
                    'response' => $sendSMS->responseDesc,
                ),200);
                log_message('debug', 'Notifikasi SMS Promo Sukses : ' . json_encode($sendSMS));
            } else {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Notifikasi SMS gagal dikirim',
                    'response' => $sendSMS->responseDesc
                ),200);
                log_message('debug', 'Notifikasi SMS Promo Error : ' . json_encode($sendSMS));
            }
        } else {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }
            
            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101,
            ]);
            log_message('debug', 'Notifikasi SMS Promo Error : ' . json_encode($error));
        }
    }

}
    
