<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'CorePegadaian.php';
require_once APPPATH . 'helpers/message_helper.php';

class Gcash extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('GcashModel', 'BankModel', 'User', 'NotificationModel','ConfigModel'));
        $this->load->model('MasterModel');
        $this->load->library('form_validation');
    }

    function index_get(){
        $token = $this->getToken();
        if($token){
            $user = $this->User->getUser($token->id);

            $list = $this->GcashModel->getVA($token->id);

            $rList = [];

            // Sinkronisasi data dengan CORE
            $inquiryData = [
                'channelId' => $token->channelId,
                'noHp' => $user->no_hp,
                'flag' => 'K'
            ];
            
            $inquiry = $this->gcashRekening($inquiryData);
            if($inquiry->responseCode == '00'){
                $vaCoreData = json_decode($inquiry->data, true);

                $coreListRekening = $vaCoreData['listRekening'];

                foreach($list as $l){
                    foreach ($coreListRekening as $coreList) {
                        if($l->virtualAccount == $coreList['vaNumber']){
                            $l->amount = $coreList['saldoAkhir'];
                            $rList[] = $l;
                        }
                    }
                }

                // Cek kondisi di mana ada data baru di core
                if(count($rList) < count($coreListRekening))
                {                       

                    // Insert setiap data baru
                    foreach($coreListRekening as $clr){
                        $insertData = [
                            'user_AIID' => $token->id,
                            'amount' => $clr['saldoAkhir'],
                            'tglExpired' => $clr['tglExpired'],
                            'virtualAccount' => $clr['vaNumber'],
                            'kodeBank' => $clr['kodeBank'] 
                        ];

                        $this->GcashModel->addVA($insertData);    
                    }

                    // Fetch ulang data
                    $rList = $this->GcashModel->getVA($token->id);                    
                }
                
                // Hitung total Saldo, convert amount to double,
                // dan cek status bank yang bisa dibuka
                $bankParams = $this->GcashModel->getBankParams();

                $vaAvailable = array();
                foreach ($bankParams as $bp) {
                    array_push($vaAvailable, $bp->kode);
                }          
                
                $totalSaldo = 0;
                
                foreach($rList as $r){
                    
                    // Dapatkan amount ulang dari BANK
                    $inquiryData = [
                        'channelId' => $token->channelId,
                        'bankCode' => $r->kodeBank,
                        'flag' => 'K',
                        'vaNumber' => $r->virtualAccount
                    ];
                    
                    $inquiry = $this->gcashInquiry($inquiryData);
                    
                    if($inquiry->responseCode == '00'){
                        $resInqData = json_decode($inquiry->data);
                        $r->amount = $resInqData->balance;
                    } else {
                        $r->amount = floatval($r->amount);
                    }

                    $totalSaldo = $totalSaldo + floatval($r->amount);

                    $isVaExist = false;
                    foreach ($bankParams as $bp) {
                        if($r->kodeBank == $bp->kode){
                            $vaAvailable = array_diff($vaAvailable, [$bp->kode]);
                        }                        
                    }

                }

                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => [
                        'totalSaldo' => $totalSaldo, 
                        'va' => $rList,
                        'vaAvailable' => array_values($vaAvailable)
                    ]
                ]);

            } else if($inquiry->responseCode == '14'){
                $vaAvailable = [];
                $bankParams = $this->GcashModel->getBankParams();

                foreach ($bankParams as $bp) {
                    $vaAvailable[] = $bp->kode;
                }
                $this->set_response([
                    'status' => 'success',
                    'message' => 'Belum melakukan aktivasi G-Cash',
                    'data' => [
                        'totalSaldo' => 0, 
                        'va' => [],
                        'vaAvailable' => $vaAvailable
                    ]
                ]);

            }else {
                $this->set_response([
                    'status' => 'error',
                    'message' => $inquiry->responseCode.':'.$inquiry->responseDesc,
                    'code' => 103,
                    'reason' => $inquiry
                ]);
            }
        } else {
            $this->errorUnAuthorized();
        }
         
        // $this->set_response(
        //     array (
        //       'status' => 'success',
        //       'message' => '',
        //       'data' => 
        //       array (
        //         'totalSaldo' => 3750000,
        //         'va' => 
        //         array (
        //           0 => 
        //           array (
        //             'id' => '89',
        //             'user_AIID' => '1310',
        //             'amount' => 3750000,
        //             'kodeBank' => '009',
        //             'trxId' => '',
        //             'tglExpired' => '2024-03-27 00:00:00',
        //             'virtualAccount' => '298878123476951',
        //             'vaNumber' => NULL,
        //             'createdAt' => '2019-03-30 15:10:22',
        //             'lastUpdate' => '2019-03-30 15:10:22',
        //             'namaBank' => 'BANK BNI',
        //             'thumbnail' => 'http://pds-api.hookiarisan.com/uploaded/bank/bni.jpg',
        //           ),
        //         ),
        //         'vaAvailable' => 
        //         array (
        //           0 => '002',
        //         ),
        //       ),
        //     )
        // );
    }

    function open_post(){
        $token = $this->getToken();
        if($token)
        {
            // Check user identity
            if(!$this->User->isIdentityOK($token->id)){
                return $this->set_response([
                    'status' => 'error',
                    'code' => 102,
                    'message' => 'Mohon lengkapi identitas Anda terlebih dahulu'
                ]);
            }

            $this->form_validation->set_rules('kode_bank','kode_bank','required');
            $this->form_validation->set_rules('pin','pin','required');

            if($this->form_validation->run() == FALSE){
                return $this->set_response([
                   'status' => 'error',
                   'message' => 'Invalid input',
                   'code' => 102,
                   'errors' => $this->form_validation->error_array()
                ]);
            }
            else
            {
                $kodeBank = $this->post('kode_bank');
                $pin = $this->post('pin');

                $user = $this->User->getUser($token->id);

                // Check aktivasi finansial
                if($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2' ){
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Anda belum melakukan aktifasi finansial/status blokir.',
                        'code' => 201
                    ), 200);   
                    return;
                }

                // check user pin
                if(!$this->User->isValidPIN2($token->id, $pin)){
                    return $this->set_response(array(
                        'status' => 'error',
                        'message' => 'PIN tidak valid',
                        'code' => 102
                    ),200);
                }

                $openData = [
                    'channelId' => $token->channelId,
                    'customerEmail' => $user->email,
                    'customerName' => $user->nama,
                    'customerPhone' => $user->no_hp,
                    'description' => 'Register Wallet',
                    'bankCode' => $kodeBank,
                    'flag' => 'K',
                    'trxId' => rand(1,100000000),
                    'cif' => $user->cif,
                ];

                $openGcash = $this->gcashOpen($openData);

                if($openGcash->responseCode == '00'){

                    // Tunggu proses selama 10 detik
                    sleep(30);

                    //$openResponse = json_decode($openGcash->data, true);
                    $openResponse = json_decode($openGcash->data, true);

                    $openResponse['user_AIID'] = $token->id;
                    $openResponse['kodeBank'] = $kodeBank;


                    $saveData = [
                        'user_AIID' => $token->id,
                        'amount' => $openResponse['amount'],
                        'virtualAccount' => $openResponse['virtualAccount'],
                        'trxId' => $openResponse['trxId'],
                        'tglExpired' => $openResponse['tglExpired'],
                        'kodeBank' => $kodeBank
                    ];

                    $this->GcashModel->addVA($saveData);

                    $openResponse['nama'] = $user->nama;
                    $openResponse['cif'] = $user->cif;
                    $bankDetails = $this->BankModel->getBankList($kodeBank);
                    $openResponse['bankName'] = $bankDetails->nama;

                    // Send notification
                    $template = $this->generateNotif($openResponse, 'open');

                    $mobileTemplate = $template['mobile'];
                    $emailTemplate = $template['email'];
                    $minimalTemplate = $template['minimal'];

                    //Simpan notifikasi baru
                    $notifId = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_GCASH,
                        NotificationModel::CONTENT_TYPE_HTML,
                        "G-Cash",
                        "Aktivasi G-Cash Sukses",
                        $mobileTemplate,
                        $minimalTemplate,
                        "GC"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id)
                        , [
                        "id" => $notifId,
                        "tipe" => "GC",
                        "title" => "G-Cash",
                        "tagline" => "Aktivasi G-Cash Sukses",
                        "content" => "Aktivasi G-Cash Sukses",
                        "token" => $token->no_hp
                    ]);

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmail($token->email, "Aktivasi G-Cash Sukses ", $emailTemplate);

                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Aktivasi G-Cash Sukses',
                        'data' => $openResponse
                    ], 200);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 102,
                        'message' => $openGcash->responseCode. ''.$openGcash->responseDesc,
                        'reason' => $openGcash
                    ]);
                }

               //  $this->set_response(array (
               //     'status' => 'success',
               //     'message' => 'Registrasi Virtual Account G-Cash Pegadaian Berhasil',
               //     'data' =>
               //         array (
               //             'amount' => '0',
               //             'trxId' => '087863165600',
               //             'tglExpired' => '2020-01-01 10:10:10',
               //             'virtualAccount' => '123456789',
               //             'user_AIID' => '1310',
               //             'kodeBank' => '009',
               //             'nama' => 'BUDA',
               //         ),
               // ));


            }
        }else{
            $this->errorUnAuthorized();
        }
    }

    function inquiry_post(){
        $token = $this->getToken();
        if($token){

            $this->form_validation->set_rules('kode_bank','kode_bank','required');
            $this->form_validation->set_rules('va','va','required');

            if($this->form_validation->run() == false){
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 102,
                    'errors' => $this->form_validation->error_array()
                ]);
            }
            else
            {
                $kodeBank = $this->post('kode_bank');
                $va = $this->post('va');

                $inquiryData = [
                    'channelId' => $token->channelId,
                    'bankCode' => $kodeBank,
                    'flag' => 'K',
                    'vaNumber' => $va
                ];

                $inquiry = $this->gcashInquiry($inquiryData);

                if($inquiry->responseCode == '00'){
                    $responseData = json_decode($inquiry->data);
                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $responseData
                    ]);
                }else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 103,
                        'message' => $inquiry->responseCode.' '. $inquiry->responseDesc
                    ]);
                }
            }
        }else{
            $this->errorUnAuthorized();
        }
    }

    function transfer_post($method = "inquiry")
    {
        $token = $this->getToken();
        if($token)
        {
            if($method == 'inquiry'){
                $this->_transferInquiry($token);
            } else if($method == 'payment'){
                $this->_transferPayment($token);
            }else{
                $this->errorForbbiden();
            }
            
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    function _transferInquiry($token)
    {
        $this->form_validation->set_rules('va_id','va_id','required|numeric');
        $this->form_validation->set_rules('dest_va','dest_va','required|numeric');
        $this->form_validation->set_rules('dest_va_kode_bank','dest_va_kode_bank','required|numeric');
        $this->form_validation->set_rules('amount','amount','required|integer');

        if($this->form_validation->run() == false){
            return $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 102,
                'errors' => $this->form_validation->error_array()
            ]);
        }

        $vaId = $this->post('va_id');

        $userVa = $this->GcashModel->getVaDetails($vaId);
        if(!$userVa){
            return $this->set_response([
                'status' => 'errror',
                'message' => 'VA tidak ditemukan',
                'code' => 102
            ]);
        }

        $vaNumber = $userVa->virtualAccount;
        $destinationAccountNumber = $this->post('dest_va');
        $destinationBankCode = $this->post('dest_va_kode_bank');
        $bankCode = $userVa->kodeBank;
        $amount = $this->post('amount');

        // Check saldo
        $inquirySaldoData = [
            'channelId' => $token->channelId,
            'bankCode' => $userVa->kodeBank,
            'flag' => 'K',
            'vaNumber' => $userVa->virtualAccount
        ];
        $inquirySaldo = $this->gcashInquiry($inquirySaldoData);

        if($inquirySaldo->responseCode != '00'){
            return $this->set_response([
                'status' => 'error',
                'code' => 103,
                'message' => 'Tidak dapat mengambil saldo:'. $inquirySaldo->responseCode.' '. $inquirySaldo->responseDesc
            ]);
        }

        $inquirySaldoResponse = json_decode($inquirySaldo->data);        

        if($inquirySaldoResponse->balance < $amount){
            return $this->set_response([
                'status' => 'error',
                'message' => 'Saldo tidak mencukupi. Jumlah transfer: Rp '.number_format($amount,0,',','.').', Saldo: Rp '.number_format($inquirySaldoResponse->balance, 0, ',','.'),
                'code' => 101
            ]);
        }
        

        if($amount % 100 != 0){
            return $this->set_response([
                'status' => 'error',
                'message' => 'Jumlah transfer harus kelipatan Rp 100',
                'code' => 101
            ]);
        }

        $inquiryData = [
            'channelId' => $token->channelId,
            'flag' => 'K',
            'amount' => $amount,
            'vaNumber' => $vaNumber,
            'bankCode' => $bankCode,
            'destinationBankCode' => $destinationBankCode,
            'destinationAccountNumber' => $destinationAccountNumber,            
        ];

        $inquiry = $this->gcashInquiryTransfer($inquiryData);

        if($inquiry->responseCode == '00'){
            $inquiryData = json_decode($inquiry->data);

            // Save payment data
            $this->GcashModel->addPayment([
                'user_AIID' => $token->id,
                'type' => 'TR',
                'amount' => $inquiryData->amount,
                'administrasi' => $inquiryData->administrasi,
                'bankCode' => $inquiryData->bankCode,
                'destinationAccountNumber' => $inquiryData->destinationAccountNumber,
                'destinationAccountName' => $inquiryData->destinationAccountName,
                'destinationAccountBankCode' => $inquiryData->destinationAccountBankCode,
                'reffSwitching' => $inquiryData->reffSwitching,
                'tglTransaksi' => $inquiryData->tglTransaksi,
                'totalKewajiban' => $inquiryData->totalKewajiban,
                'vaNumber' => $inquiryData->vaNumber,
                'status' => '1' 
            ]);

            $inquiryData->id_transaksi = $inquiryData->reffSwitching;
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $inquiryData
            ]);

        }else{
            $this->set_response([
                'status' => 'error',
                'message' => $inquiry->responseCode.':'.$inquiry->responseDesc,
                'code' => 103,
                'reason' => $inquiry
            ]);
        }
        
        // $this->set_response(array (
        //   'status' => 'success',
        //   'message' => '',
        //   'data' => 
        //   array (
        //     'bankCode' => '009',
        //     'destinationAccountNumber' => '0115476151',
        //     'destinationAccountName' => 'ROBERT NARO ROBERT NARO',
        //     'destinationAccountBankCode' => '009',
        //     'reffSwitching' => '15531756088762257',
        //     'amount' => '100000',
        //     'administrasi' => '2500',
        //     'id_transaksi' => '15531756088762257',
        //   ),
        // ));        
    }

    function _transferPayment($token)
    {
        $this->form_validation->set_rules('id_transaksi','id_transaksi','required');
        $this->form_validation->set_rules('pin','pin','required|numeric');

        if($this->form_validation->run() == false)
        {
            log_message('debug', json_encode($this->form_validation->error_array()));
            $this->set_response([
                'status' => 'error',
                'message' => 'invalid input',
                'code' => 101,
                'errors' => $this->form_validation->error_array()
            ]);
        }
        else
        {
            $reffSwitching = $this->post('id_transaksi');
            $pin = $this->post('pin');

            if(!$this->User->isValidPIN2($token->id, $pin)){
                return $this->set_response([
                    'status' => 'errror',
                    'message' => 'PIN tidak valid',
                    'code' => 102
                ]);
            }

            $payment = $this->GcashModel->getPayment($reffSwitching);
            if(!$payment){
                return $this->set_response([
                    'status' => 'errror',
                    'message' => 'ID transaksi tidak ditemukan',
                    'code' => 102
                ]);
            }
           
            $bankDetails = $this->BankModel->getBankList($payment->destinationAccountBankCode);
            log_message('debug', 'BANK DETAIL: '.json_encode($bankDetails));
            $transferData = [
                'channelId' => $token->channelId,
                'flag' => 'K',
                'bankCode' => $payment->bankCode,
                'vaNumber' => $payment->vaNumber,
                'amount' => $payment->amount,
                'destinationAccountNumber' => $payment->destinationAccountNumber,
                'destinationAccountName' => $payment->destinationAccountName,
                'destinationBankCode' => $payment->destinationAccountBankCode,
                'destinationBankName' => $bankDetails->nama,
                'reffSwitching' => $reffSwitching,
                'administrasi' => $payment->administrasi,
                'surcharge' => '0'
            ];

            $gcashTransfer = $this->gcashTransfer($transferData);

            if($gcashTransfer->responseCode == '00')
            {
                $resData = json_decode($gcashTransfer->data);

                $saveData = [
                    'user_AIID' => $token->id,
                    'tipeTransaksi' => 'TR',
                    'tglTransaksi' => date('Y-m-d H:i:s'),
                    'virtualAccount' => $payment->vaNumber,
                    'kodeBank' => $payment->bankCode,
                    'amount' => $payment->amount,
                    'destVa' => $payment->destinationAccountNumber,
                    'destVaNama' => $payment->destinationAccountName,
                    'destVaKodeBank' => $payment->destinationAccountBankCode,
                    'destVaNamaBank' => $bankDetails->nama,
                    'reffBiller' => isset($resData->reffBiller) ? $resData->reffBiller : '-' ,
                    'reffSwitching' => $reffSwitching,
                    'tglPayment' => date('Y-m-d'),
                    'administrasi' => $payment->administrasi,
                ];

                $this->GcashModel->addHistory($saveData);
                $this->GcashModel->updatePayment($reffSwitching, [
                    'status' => '1'
                ]);

                // Send notification
                // Send notification
                $saveData['nama'] = $token->nama;
                $bankDetailsPengirim = $this->BankModel->getBankList($payment->bankCode);
                $saveData['vaNamaBank'] = $bankDetailsPengirim->nama;
                $template = $this->generateNotif($saveData, 'transfer');

                $mobileTemplate = $template['mobile'];
                $emailTemplate = $template['email'];
                $minimalTemplate = $template['minimal'];


                $resData->tgl = date('Y-m-d H:i:s'); 

                //Simpan notifikasi baru
                $notifId = $this->NotificationModel->add(
                    $token->id,
                    NotificationModel::TYPE_GCASH,
                    NotificationModel::CONTENT_TYPE_HTML,
                    "G-Cash",
                    "Transfer G-Cash",
                    $mobileTemplate,
                    $minimalTemplate,
                    "GC"
                );

                //Kirim notifikasi pembayaran ke device user
                Message::sendFCMNotif(
                    $this->User->getFCMToken($token->id)
                    , [
                    "id" => $notifId,
                    "tipe" => "GC",
                    "title" => "G-Cash",
                    "tagline" => "Transfer G-Cash",
                    "content" => "Transfer G-Cash",
                    "token" => $token->no_hp
                ]);

                //Kirim Email Notifikasi
                $this->load->helper('message');
                Message::sendEmail($token->email, "Transfer G-Cash", $emailTemplate);

                //Set response
                $this->set_response([
                    'status' => 'success',
                    'message' => 'Transfer Gcash Sukses',
                    'data' => $resData
                ], 200);
            } 
            else 
            {
                $this->set_response([
                    'status' => 'error',
                    'message' => $gcashTransfer->responseCode.' '.$gcashTransfer->responseDesc,
                    'code' => 103,
                    'reason' => $gcashTransfer
                ]);
            }
        
            // $this->set_response(array (
            //   'status' => 'success',
            //   'message' => 'Transfer Gcash Sukses',
            //   'data' => 
            //   array (
            //     'bankCode' => '009',
            //     'vaNumber' => '8364008127511590',
            //     'amount' => '200000',
            //     'destinationAccountNumber' => '0115476151',
            //     'destinationAccountName' => 'ROBERT NARO ROBERT NARO',
            //     'destinationBankCode' => '009',
            //     'destinationBankName' => 'BNI',
            //     'reffBiller' => '0091903212054355',
            //     'tgl' => '2019-03-25 10:10:10',
            //   ),
            // ));
        }
        
    }

    function history_get(){
        $token = $this->getToken();
        if($token){
            $va = $this->query('va');

            $vaDetails = $this->GcashModel->getVaDetailsByVa($va);

            if($vaDetails){
                $history = $this->gcashHistory([
                    "channelId" => $token->channelId,
                    "bankCode" => $vaDetails->kodeBank,
                    "tglAwal" => date('Y-m-d', strtotime("-60 days")),
                    "tglAkhir" => date('Y-m-d'),
                    "vaNumber" => $va
                ]);

                if($history->responseCode == '00'){
                    $historyData = json_decode($history->data);
                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $historyData->dataMutasi
                    ]);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => $history->responseDesc,
                        'code' => 103,
                        'reason' => $history
                    ]);
                }
            }

        }else{
            $this->errorUnAuthorized();
        }
    }

    function otp_bri_post()
    {
        $token = $this->getToken();
        if($token){
            $this->form_validation->set_rules('id_va','id_va','required');
            $this->form_validation->set_rules('amount','amount','required');
            $this->form_validation->set_rules('pin','pin','required|numeric');
            $this->form_validation->set_rules('tipe','tipe','required|integer');
            if($this->form_validation->run() == FALSE){
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 103,
                    'errors' => $this->form_validation->error_array()
                ]);
            }

            $vaId = $this->post('id_va');
            $amount = $this->post('amount');
            $pin = $this->post('pin');
            $tipe = $this->post('tipe');

            if(!$this->User->isValidPIN2($token->id, $pin)){
                return $this->set_response(array(
                    'status' => 'error',
                    'message' => 'PIN tidak valid',
                    'code' => 102
                ),200);
            }

            // Jika tipe = 2 (teller) inputan harus kelipatan Rp 100
            if($amount % 100 != 0 && $tipe == '2'){
                return $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Jumlah penarikan harus kelipatan Rp 100',
                    'code' => 102
                ),200);
            }

            $userVa = $this->GcashModel->getVaDetails($vaId);
            if(!$userVa){
                return $this->set_response([
                    'status' => 'errror',
                    'message' => 'VA tidak ditemukan',
                    'code' => 102
                ]);
            }

            $otpData = [
                'channelId' => $token->channelId,
                'bankCode' => $userVa->kodeBank,
                'vaNumber' => $userVa->virtualAccount,
                'amount' => $amount,
                'requestType' => $tipe
            ];

            $sendOTP = $this->gcashOtpBri($otpData);

            if($sendOTP->responseCode == '00'){
                $dataOtp = json_decode($sendOTP->data);

                $user = $this->User->getUser($token->id);
                
                // Send notification
                $template = $this->generateNotifOtp([
                    'nama' => $user->nama,
                    'va' => $userVa->virtualAccount,
                    'tglRequest' => $dataOtp->tglRequest,
                    'tglExpired' => $dataOtp->tglExpired,
                    'amount' => $amount,
                    'otp' => $dataOtp->token,
                    'requestType' => $tipe
                ]);

                $mobileTemplate = $template['mobile'];
                $emailTemplate = $template['email'];
                $minimalTemplate = $template['minimal'];

                //Simpan notifikasi baru
                $notifId = $this->NotificationModel->add(
                    $token->id,
                    NotificationModel::TYPE_GCASH,
                    NotificationModel::CONTENT_TYPE_HTML,
                    "G-Cash",
                    "Tarik tunai G-Cash",
                    $mobileTemplate,
                    $minimalTemplate,
                    "GC"
                );

                //Kirim notifikasi pembayaran ke device user
                Message::sendFCMNotif(
                    $this->User->getFCMToken($token->id)
                    , [
                    "id" => $notifId,
                    "tipe" => "GC",
                    "title" => "G-Cash",
                    "tagline" => "Tarik Tunai G-Cash",
                    "content" => "Tarik Tunai G-Cash",
                    "token" => $token->no_hp
                ]);

                //Kirim Email Notifikasi
                $this->load->helper('message');

                Message::sendEmail($token->email, "Tarik Tunai G-Cash", $emailTemplate);

                $this->set_response([
                    'status' => 'success',
                    'message' => 'OTP tarik tunai akan dikirimkan ke no telepon Anda.',
                    'data' => $dataOtp
                ]);
            }else{
                $this->set_response([
                    'status' => 'error',
                    'message' => $sendOTP->responseCode.':'.$sendOTP->responseDesc,
                    'code' => 103,
                    'reason' => $sendOTP
                ]);
            }
        }else{
            $this->errorUnAuthorized();
        }
//         $this->set_response([
//             'status' => 'success',
//             'message' => 'OTP tarik tunai akan dikirimkan ke no telepon Anda.',
//             'data' => null
//         ]);
    }

    function bankparams_get(){
        $token = $this->getToken();
        if($token){
            $list = $this->GcashModel->getBankParams();
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $list
            ]);
        }
        else
        {
            $this->errorUnAuthorized();
        }
    }

    public function generateNotif($data, $type = 'open')
    {
        $subject = "Aktivasi G-Cash Sukses";
        $content = '';

        if($type == 'open'){
            $content = $this->load->view('mail/gcash/open', $data, true);    
        } else if($type == 'transfer'){
            $content = $this->load->view('mail/gcash/transfer', $data, true);
        }

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }

    public function generateNotifOtp($data)
    {
        $subject = "Tarik Tunai G-Cash";
        $content = $this->load->view('mail/gcash/otp', $data, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }



}