<?php

class Gadai_service extends MY_Service
{
    public function updateGadaiGosend($data_gadai, $data_gosend)
    {
        $code_booking = $data_gadai->kodeBooking;

        switch ($code_booking[0]) {
            case 1:
                $data = $this->updateGadai($data_gadai, $data_gosend, 'gadai_perhiasan');
                break;
            case 2:
                $data = $this->updateGadai($data_gadai, $data_gosend, 'gadai_logam_mulia');
                break;
            default:
                $data = null;
        }

        return $data;
    }

    private function updateGadai($data_gadai, $data_gosend, $table)
    {
        $status_gosend_no_pickup = ['Enroute Pickup', 'Out For Pickup', 'Out For Delivery'];
        $fcm_gadai_no_pickup = [0, 99, 98];
        $update_data = [];

        if (in_array($data_gosend->status, $status_gosend_no_pickup) && in_array($data_gadai->fcm, $fcm_gadai_no_pickup)) {
            $update_data['status'] = 5;
            $update_data['status_fcm'] = 1;
        }

        if ($data_gosend->status == 'Driver not found' && $data_gadai->statusFcm == 0) {
            $update_data['status'] = 8;
            if ($data_gadai->statusFcm == 0) {
                $update_data['status_fcm'] = 99;
            }
        }

        if ($data_gosend->status == 'Completed') {
            $update_data['status'] = 9;
            if ($data_gadai->statusFcm == 1) {
                $update_data['status_fcm'] = 2;
            }
        }

        if ($data_gosend->status == 'Cancelled') {
            $update_data['status'] = 8;
            if ($data_gadai->statusFcm == 1 || $data_gadai->statusFcm == 0) {
                $update_data['status_fcm'] = 98;
            }
        }

        // Except condition query on service :v because call dynamic table
        log_message('debug', 'update data => '. json_encode($update_data) .' table  => ' . $table);
        $this->db
            ->where('kode_booking', $data_gadai->kodeBooking)
            ->update($table, $update_data);

        return $data_gadai->kodeBooking;
    }
}