<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductMasterModel extends CI_Model {
    function checkTable($productCode) {
        $data = $this->db->where(['productCode' => $productCode])
                        ->get('master_product');
                        
    return $data->row();
    }

    public function get_id_product($productName){
        $result = $this->db->where(['productName' => $productName])->get('master_product');
        return $result->row();
    }
}
