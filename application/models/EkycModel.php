<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EkycModel extends CI_Model {

    // GET DATA BASE ON "user_id"
    function getbyUser($userId) {
        $where = [
            'user_id' => $userId
        ];
        $cek = $this->db->where($where)->get('ekyc');
    
        if($cek->num_rows() > 0){
            return $cek->row();
        } else {
            return '';
        }
    }

    // UPDATE BASE ON "user_id"
    function update($id, $fields = array()){
        $this->db->trans_start();
        $this->db->where('user_id', $id)->update('ekyc', $fields);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }

    // INSERT
    function insert($fields = array()){
        $this->db->trans_start();
        $this->db->insert('ekyc', $fields);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE) {
            return 'gagal';
        } else {
            return 'sukses';
        }      
    }

}
