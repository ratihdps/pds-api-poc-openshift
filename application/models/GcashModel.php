<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GcashModel extends CI_Model
{
    function getVA($userId){
        $va =  $this->db
                    ->select('gcash.*, ref_bank.nama as namaBank')
                    ->select('ref_bank.thumbnail')
                    ->join('ref_bank','gcash.kodeBank=ref_bank.kode')
                    ->where('user_AIID', $userId)
                    ->get('gcash')
                    ->result();
        $return = array();
        foreach ($va as $v) {
            $v->thumbnail = $this->config->item('bank_image_path').$v->thumbnail;
            $return[] = $v;
        }

        return $return;
    }

    function addVA($data){
        // Check terlebih dahulu agar VA tidak duplicate
        $cek = $this->db->where([
            'virtualAccount' => $data['virtualAccount'],
            'kodeBank' => $data['kodeBank']
        ])->get('gcash');

        if($cek->num_rows() == 0){
            $this->db->insert('gcash', $data);
        }else{
            
        }

    }

    function getVADetails($id)
    {
    	$cek = $this->db->where('id', $id)->get('gcash');
    	if($cek->num_rows() > 0)
    	{
    		return $cek->row();
    	}
    	return false;
    }

    function getVaDetailsByVa($va){
        $va =  $this->db
                    ->select('gcash.*, ref_bank.nama as namaBank')
                    ->select('ref_bank.thumbnail')
                    ->join('ref_bank','gcash.kodeBank=ref_bank.kode')
                    ->where('virtualAccount', $va)
                    ->get('gcash');
        if($va->num_rows() > 0){
            $row = $va->row();
            $row->thumbnail = $this->config->item('bank_image_path').$row->thumbnail;
            return $row;
        }
                    
        return false;

    }

    function addHistory($data){
    	$this->db->insert('gcash_history', $data);
    }

    function getHistory($userId, $virtualAccount){
        return $this->db->where([
            'virtualAccount' => $virtualAccount,
            'user_AIID' => $userId
        ])->get('gcash_history')->result();
    }

    function clearVA($userId){
        $this->db->where('user_AIID', $userId)->delete('gcash');
    }

    function getBankParams(){
        $bank = $this->db->where('showOnGcash', '1')->get('ref_bank')->result();
        $imagePath = $this->config->item('bank_image_path');
        $return = [];
        foreach($bank as $b){
            $b->thumbnail = $imagePath.$b->thumbnail;
            $return[] = $b;
        }
        return $return;
    }

    function addPayment($data){
        $this->db->insert('payment_gcash', $data);
    }

    function getPayment($trxId){
        $cek = $this->db->where('reffSwitching', $trxId)->get('payment_gcash');
        if($cek->num_rows() > 0){
            return $cek->row();
        } 

        return false;
    }

    function updatePayment($trxId, $data){
        $this->db->where('reffSwitching', $trxId)->update('payment_gcash', $data);
    }
}
