<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterModel extends CI_Model{

    function getProvinsi($id = null){
        if($id == null){
            return $this->db
                        ->select('kode_provinsi as kodeProvinsi, nama_provinsi as namaProvinsi')
                        ->order_by('nama_provinsi','asc')
                        ->get('ref_provinsi')->result();
        }else{
            return $this->db
                        ->select('kode_provinsi as kodeProvinsi, nama_provinsi as namaProvinsi')                                                
                        ->where('kode_provinsi', $id)
                        ->order_by('nama_provinsi','asc')
                        ->get('ref_provinsi')->result();
        }
    }

    function getKabupaten($idKabupaten = null, $idProvinsi = null){
        if($idKabupaten != null){
            return $this->db
                        ->select('ref_provinsi.kode_provinsi AS kodeProvinsi, nama_provinsi as namaProvinsi')
                        ->select('ref_kabupaten.kode_kabupaten as kodeKabupaten, nama_kabupaten as namaKabupaten')
                        ->where('ref_kabupaten.kode_kabupaten', $idKabupaten)
                        ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi')
                        ->order_by('nama_kabupaten','asc')
                        ->get('ref_kabupaten')
                        ->result();
        }

        if($idProvinsi != null){
            return $this->db
                        ->select('ref_provinsi.kode_provinsi AS kodeProvinsi, nama_provinsi as namaProvinsi')
                        ->select('ref_kabupaten.kode_kabupaten as kodeKabupaten, nama_kabupaten as namaKabupaten')
                        ->where('ref_provinsi.kode_provinsi', $idProvinsi)
                        ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi')
                        ->order_by('nama_kabupaten','asc')
                        ->get('ref_kabupaten')
                        ->result();
        }

        return [];
    }

    function getKecamatan($id =null , $idKabupaten = null){
        if($id != null){
            return $this->db
                        ->select('ref_kecamatan.kode_kecamatan as kodeKecamatan, nama_kecamatan as namaKecamatan')
                        ->select('ref_kabupaten.kode_kabupaten as kodeKabupaten, nama_kabupaten as namaKabupaten')
                        ->select('ref_provinsi.kode_provinsi as kodeProvinsi, nama_provinsi as namaProvinsi')
                        ->where('ref_kecamatan.kode_kecamatan', $id)
                        ->join('ref_kabupaten','ref_kecamatan.kode_kabupaten=ref_kabupaten.kode_kabupaten')
                        ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi')
                        ->order_by('nama_kecamatan','asc')
                        ->get('ref_kecamatan')
                        ->result();
        }

        if($idKabupaten != null){
            return $this->db
                        ->select('ref_kecamatan.kode_kecamatan as kodeKecamatan, nama_kecamatan as namaKecamatan')
                        ->select('ref_kabupaten.kode_kabupaten as kodeKabupaten, nama_kabupaten as namaKabupaten')
                        ->select('ref_provinsi.kode_provinsi as kodeProvinsi, nama_provinsi as namaProvinsi')
                        ->where('ref_kabupaten.kode_kabupaten', $idKabupaten)
                        ->join('ref_kabupaten','ref_kecamatan.kode_kabupaten=ref_kabupaten.kode_kabupaten')
                        ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi')
                        ->order_by('nama_kecamatan','asc')
                        ->get('ref_kecamatan')
                        ->result();
        }

        return [];
    }

    function getKelurahan($id =null , $idKecamatan = null){
        if($id != null){
            return $this->db
                        ->select('ref_kelurahan.kode_kelurahan as kodeKelurahan, nama_kelurahan as namaKelurahan')
                        ->select('ref_kecamatan.kode_kecamatan as kodeKecamatan, nama_kecamatan as namaKecamatan')
                        ->select('ref_kabupaten.kode_kabupaten as kodeKabupaten, nama_kabupaten as namaKabupaten')
                        ->select('ref_provinsi.kode_provinsi as kodeProvinsi, nama_provinsi as namaProvinsi')
                        ->where('ref_kelurahan.kode_kelurahan', $id)
                        ->join('ref_kecamatan', 'ref_kelurahan.kode_kecamatan=ref_kecamatan.kode_kecamatan')
                        ->join('ref_kabupaten','ref_kecamatan.kode_kabupaten=ref_kabupaten.kode_kabupaten')
                        ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi')
                        ->order_by('nama_kelurahan','asc')
                        ->get('ref_kelurahan')
                        ->result();
        }

        if($idKecamatan != null){
            return $this->db
                        ->select('ref_kelurahan.kode_kelurahan as kodeKelurahan, nama_kelurahan as namaKelurahan')
                        ->select('ref_kecamatan.kode_kecamatan as kodeKecamatan, nama_kecamatan as namaKecamatan')
                        ->select('ref_kabupaten.kode_kabupaten as kodeKabupaten, nama_kabupaten as namaKabupaten')
                        ->select('ref_provinsi.kode_provinsi as kodeProvinsi, nama_provinsi as namaProvinsi')
                        ->where('ref_kelurahan.kode_kecamatan', $idKecamatan)
                        ->join('ref_kecamatan', 'ref_kelurahan.kode_kecamatan=ref_kecamatan.kode_kecamatan')
                        ->join('ref_kabupaten','ref_kecamatan.kode_kabupaten=ref_kabupaten.kode_kabupaten')
                        ->join('ref_provinsi','ref_kabupaten.kode_provinsi=ref_provinsi.kode_provinsi')
                        ->order_by('nama_kelurahan','asc')
                        ->get('ref_kelurahan')
                        ->result();        
        }

        return [];
    }
    
    function getCabangPegadaian($kode)
    {        
        return $this->db
                    ->select('id, kode_outlet as kodeOutlet')
                    ->select('bidang, kelas, klasifikasi_cabang as klasifikasiCabang')
                    ->select('status, alamat, rt_rw as rtwr, tanggal_berdiri as tanggalBerdiri')
                    ->select('telepon, latitude, longitude, kode_area as kodeArea')
                    ->select('kode_cabang as kodeCabang, kode_kanwil as kodeKanwil')
                    ->where('kode_outlet', $kode)
                    ->get('ref_cabang')->result();
        
    }

    function getSingleCabang($kode)
    {
        $cek = $this->db
                    ->select('id, kode_outlet as kodeOutlet')
                    ->select('nama_outlet as nama, alamat, telepon')
                    ->select('latitude, longitude')
                    ->select('nama_kelurahan as kelurahan, nama_kecamatan  as kecamatan')
                    ->select('nama_kabupaten as kabupaten, nama_provinsi as provinsi')
                    ->select('kode_outlet as kodeCabang')
                    ->where('kode_outlet', $kode)
                    ->get('ref_cabang_flat');
        if($cek->num_rows() > 0){
            return $cek->row();
        }

        return false;

    }
    
    function getNearestBranch($latitude, $longitude, $radius)
    {
        $sql = "SELECT 
                kode_outlet as kodeOutlet,
                nama_outlet as namaOutlet,
                alamat,
                latitude,
                longitude,
                telepon,
                nama_kelurahan as kelurahan,
                nama_kecamatan as kecamatan,
                nama_kabupaten as kabupaten,
                nama_provinsi as provinsi,
                (
                   3959 *
                   acos(cos(radians(".$latitude.")) * 
                   cos(radians(latitude)) * 
                   cos(radians(longitude) - 
                   radians(".$longitude.")) + 
                   sin(radians(".$latitude.")) * 
                   sin(radians(latitude )))
                ) AS distance 
                FROM ref_cabang_flat 

                HAVING distance < ".$radius."                 
                ORDER BY distance LIMIT 0, 20";
        return $this->db->query($sql)->result();        
    }

    function getNearestBranchGOD($latitude, $longitude, $radius)
    {
        $sql = "SELECT 
                kode_outlet as kodeOutlet,
                nama_outlet as namaOutlet,
                alamat,
                latitude,
                longitude,
                telepon,
                (
                   3959 *
                   acos(cos(radians(".$latitude.")) * 
                   cos(radians(latitude)) * 
                   cos(radians(longitude) - 
                   radians(".$longitude.")) + 
                   sin(radians(".$latitude.")) * 
                   sin(radians(latitude )))
                ) AS distance 
                FROM  ref_cabang 
                WHERE god = 1 AND isActive = '1'
                HAVING distance < ".$radius."                 
                ORDER BY distance LIMIT 0, 20";
        
        return $this->db->query($sql)->result();        
    }

    function setLokasiNasabah($latitude, $longitude, $user)
    {
        $sql = "INSERT INTO lokasi_nasabah (
                latitude,
                longitude,
                user_AIID)
                values(".$latitude.", ".$longitude.",".$user.")";
        return $this->db->query($sql);        
    }
    
    function searchOutlet($keyword, $kodeKabupaten)
    {
        if($keyword != null)
        {
            return $this->db
                        ->select('kode_outlet as kodeOutlet, nama_outlet as namaOutlet')
                        ->select('alamat, latitude, longitude, telepon')
                        ->select('nama_kelurahan as kelurahan, nama_kecamatan  as kecamatan')
                        ->select('nama_kabupaten as kabupaten, nama_provinsi as provinsi')
                        ->from('ref_cabang_flat')
                        ->like('nama_outlet', $keyword)
                        ->limit(10)
                        ->get()
                        ->result();
        }
        else if($kodeKabupaten != null)
        {
            $cek = $this->db
                        ->select('kode_outlet as kodeOutlet, nama_outlet as namaOutlet')
                        ->select('alamat, latitude, longitude, telepon')
                        ->select('nama_kelurahan as kelurahan, nama_kecamatan  as kecamatan')
                        ->select('nama_kabupaten as kabupaten, nama_provinsi as provinsi')
                        ->from('ref_cabang_flat')
                        ->where('kode_kabupaten',$kodeKabupaten)
                        ->limit(10)
                        ->get()
                        ->result();
            log_message('debug', $this->db->last_query());
            return $cek;

        }

    }

    function getVendorCetakEmas(){
        $imagePath = $this->config->item('emas_image_path');
        $cek =  $this->db->where('status','1')->get('ref_vendor_cetak')->result();
        $result = [];
        foreach($cek as $c){
            $c->thumbnail = $imagePath.$c->thumbnail;
            $result[] = $c;
        }
        return $result;
    }
    
    function getMikroTarifSewa(){
        return $this->db->select('cast(value as double) as jumlah, format(cast(value_2 as double)/100,3) as sewaModal')
                        ->where('variable','mikro_tarif_sewa')
                        ->order_by('jumlah','asc')
                        ->get('config')
                        ->result();
    }

    /**
     * @param  string $variable variable system parameter yang ingin didapatkan 
    */
    function getSystemParameters($variable) {
        $where = [
            'variable' => $variable,
            'type' => 'system_parameters'
        ];        
        $cek = $this->db->where($where)->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value == '1' ? true : false;
        }
        return false; 
    }

    function getParametersGod($variable){
        $where = [
            'variable' => $variable,
            'type' => 'god_parameters'
        ];  
        $cek = $this->db->where($where)->get('config');
        if($cek->num_rows() > 0){
            return $cek->row()->value;
        }
        return 0;
    }
    // Get Data 'apps_parameters'
    function getParametersApps($variable) {
        $where = [
            'variable' => $variable,
            'type' => 'apps_parameters'
        ];  
        $cek = $this->db->where($where)->get('config');
        if($cek->num_rows() > 0){
            return $cek->row()->value;
        }
        return 0;
    }
    
    function getGTEPromo() {
        $where = 'expired >= NOW()';        
        return $this->db->where($where)->get('ref_gte_promo')->result();
    }

    function getBiayaOpenTabemas(){
        $cek = $this->db->where('variable', 'biaya_opentabemas')->get('config');
        if($cek->num_rows() > 0){
            return $cek->row()->value;
        }

        return 100000;
    }

    function getBiayaTitipOpenTabemas()
    {
        $cek = $this->db->where('variable', 'biaya_opentabemas_titip')->get('config');
        if($cek->num_rows() > 0){
            return $cek->row()->value;
        }

        return 30000;
    }
}