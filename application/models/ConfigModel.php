<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfigModel extends CI_Model
{
    public function getConfig()
    {
        return $this->db->get('config')->row();
    }
    
    public function getBiayaPayment($jenisTransaksi, $payment, $productCode, $kodeBank)
    {
        $where = array(
            'jenisTransaksi' => $jenisTransaksi,
            'payment' => $payment,
            'productCode' => $productCode,
            'kodeBank' => $kodeBank
        );   
        $cek = $this->db->where($where)->get('ref_biaya_payment');
        if($cek->num_rows() > 0){
            $jumlah = $cek->row()->jumlah;
            return $jumlah;
        } else {
            return '-';
        }
        
        return 0;
    }

    public function getRealBiayaPayment($jenisTransaksi, $payment, $productCode, $kodeBank)
    {
        $where = array(
            'jenisTransaksi' => $jenisTransaksi,
            'payment' => $payment,
            'productCode' => $productCode,
            'kodeBank' => $kodeBank
        );
        
        $cek = $this->db->where($where)->get('ref_biaya_payment');
        if($cek->num_rows() > 0){
            return $cek->row()->jumlahReal;
        }
        
        return 0;
    }
    
    /**
     * Method untuk mendapatkan minimal gram transfer emas
     */
    function getMinimumTransferEmas(){
        $cek = $this->db->select('cast(value as double) as minimum')
                        ->where('variable','minimum_transfer_emas')
                        ->get('config');
                        
        if($cek->num_rows() > 0){
            $minimum = $cek->row()->minimum;
            return number_format($minimum, 4);
        }else{
            return 1.000;
        }
    }
    
    /**
     * Method untuk mendapatkan minimal gram transfer emas
     */
    function getMinimumCetakEmas(){
        $cek = $this->db->select('cast(value as double) as minimum')
                        ->where('variable','minimum_cetak_emas')
                        ->get('config');
                        
        if($cek->num_rows() > 0){
            $minimum = $cek->row()->minimum;
            return number_format($minimum, 4);
        }else{
            return 1.000;
        }
    }
    
    /**
     * Method untuk mendapatkan minimal gram transfer emas
     */
    function getMinimumBuybackEmas(){
        $cek = $this->db->select('cast(value as double) as minimum')
                        ->where('variable','minimum_cetak_emas')
                        ->get('config');
                        
        if($cek->num_rows() > 0){
            $minimum = $cek->row()->minimum;
            return number_format($minimum, 4);
        }else{
            return 1.000;
        }
    }
    
    function getNamaProduk($jenisTransaksi, $kodeProduct)
    {
        $cek = $this->db->where(array(
            'productCode' => $kodeProduct,
            'jenisTransaksi' => $jenisTransaksi
        ))->get('ref_biaya_payment');
        
        if($cek->num_rows() > 0){
            return $cek->row()->productName;
        }
        
        return "";
    }

    /**
     * Get redaksi tata cara pembayaran berdasarkan jenis pembayaran
     * @param  string $bank   kode bank
     * @param  string $va     nomor va
     * @param  integer $amount jumlah bayar
     * @return array         daftar tata cara bayar
     */
    function getRedaksiPayment($bank, $va = null , $amount = null) 
    {
        $data = [];
        $cek = $this->db->where('kode_bank', $bank)->order_by('index','asc')
                        ->get('ref_step_payment');
        foreach ($cek->result() as $s) {

            // Replace {{noVa}} with $va 
            $desc = str_replace('{{noVa}}', $va, $s->deskripsi);
            // Replace {{nominal}}
            $desc = str_replace('{{nominal}}', $amount, $desc); 
            // Replace paragraph tag
            $desc = str_replace('<p>', '', $desc);
            // Replace closing pragraph tag
            $desc = str_replace('</p>', '', $desc);
            
            $data[] = [
                'title' => $s->judul,
                'description' => $desc 
            ];
        }

        return $data;
    }

    function getMsGcashRefreshToken(){
        $cek = $this->db->where('variable', 'msgcash_refresh_token')->get('config');
        if($cek->num_rows() > 0){
            return $cek->row()->value;
        } else {
            return '';
        }
    }
}


