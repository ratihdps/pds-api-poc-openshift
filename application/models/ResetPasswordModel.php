<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ResetPasswordModel extends CI_Model
{
    function add($data)
    {
        $this->db->insert('reset_password', $data);
        return $this->db->insert_id();
    }

    /**
     * Mengecek apakah permintaan reset password valid. Reset password valid jika
     * dalam waktu 24 jam
     * @param $token
     */
    function isValid($token)
    {
        $where = array(
            'token' => $token,
            'is_valid' => '1'
        );
        $cek = $this->db->where($where)->get('reset_password');
        if($cek->num_rows() > 0){
            //Cek if valid
            $now = new DateTime("now");
            $validUntil = new DateTime($cek->row()->valid_until);

            if($now < $validUntil){
                return true;
            }

            return false;

        }else{
            return false;
        }
    }

    /**
     * Get user id berdasarkan reset password token
     * @param $token
     */
    function getUserId($token)
    {
        return $this->db->where('token', $token)->get('reset_password')->row()->user_AIID;
    }

    function off($token)
    {
        $this->db->where('token', $token)->update('reset_password', array('is_valid'=>'0'));
    }
    
    function isValidPhone($phone)
    {
        $where = array(
            'no_hp' => $phone,
            'status' => '1'
        );
        $cek = $this->db->where($where)
                        ->get('user');
        if($cek->num_rows() > 0){
            return $cek->row();
        }else{
            return false;
        }
    }

}