<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminModel extends CI_Model{
    function getAdminToken($token)
    {
        if($token == null){
            return false;
        }
        
        $cek = $this->db->where('access_token', $token)->get('admin');
        if($cek->num_rows() > 0){
            return $cek->row();
        }
        
        $cek2 = $this->db->where('access_token', $token)->get('ref_user_mikro');
        if($cek2->num_rows() > 0){
            return $cek2->row();
        }
        
        return false;
    }
}