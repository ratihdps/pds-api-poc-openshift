Hi, <?php echo $nama ?>
<br /></br /><br />
Terima kasih telah melakukan transfer G-Cash

<br> <br>

Nota Transaksi

<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Tanggal Transaksi</td>
        <td>:</td>
        <td> <strong><?php $_tglTransaksi = new DateTime($tglTransaksi); echo $_tglTransaksi->format('d-m-Y H:i:s'); ?></strong></td>
    </tr>
    <tr>
        <td>ID Transaksi</td>
        <td>:</td>
        <td> <strong><?php echo $reffSwitching ?></strong></td>
    </tr>
    <tr>
        <td>Reff Biller</td>
        <td>:</td>
        <td> <strong><?php echo $reffBiller ?></strong></td>
    </tr>
    <tr>
        <td>Nama Pengirim</td>
        <td>:</td>
        <td> <strong><?php echo $nama ?></strong></td>
    </tr>
    <tr>
        <td>No G-Cash</td>
        <td>:</td>
        <td> <strong><?php echo $virtualAccount ?></strong></td>
    </tr>
    <tr>
        <td>Bank Pengirim</td>
        <td>:</td>
        <td> <strong>G-Cash <?php echo $vaNamaBank ?></strong></td>
    </tr>
    <tr>
        <td>Nama Penerima</td>
        <td>:</td>
        <td> <strong><?php echo $destVaNama ?></strong></td>
    </tr>
    <tr>
        <td>No Rekening Tujuan</td>
        <td>:</td>
        <td> <strong><?php echo $destVa ?></strong></td>
    </tr>
    <tr>
        <td>Bank Tujuan</td>
        <td>:</td>
        <td> <strong><?php echo $destVaNamaBank ?></strong></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($amount,0,',','.') ?></strong></td>
    </tr>
    
    <tr>
        <td>Biaya Transaksi</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($administrasi,0,',','.') ?></strong></strong></td>
    </tr>
    <tr>
        <td>Status</td>
        <td>:</td>
        <td> <strong>Sukses</strong></strong></td>
    </tr>
</table>

<br /><br />

