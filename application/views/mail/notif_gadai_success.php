Hi, <?php echo $nama ?> 
<br /></br /><br />
Terima Kasih, Pencairan Gadai Anda Berhasil.

<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Tanggal Pengajuan</td>
        <td>:</td>
        <td> <strong><?php echo $tglPengajuan; ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Cair</td>
        <td>:</td>
        <td> <strong><?php echo $tglCair ?></strong></td>
    </tr>
    <tr>
        <td>No Kredit</td>
        <td>:</td>
        <td> <strong><?php echo $noKredit; ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Kredit</td>
        <td>:</td>
        <td> <strong><?php echo $tglKredit; ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Jatuh Tempo</td>
        <td>:</td>
        <td> <strong><?php echo $tglJatuhTempo; ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Lelang</td>
        <td>:</td>
        <td> <strong><?php echo $tglLelang; ?></strong></td>
    </tr>
    <tr>
        <td>Uang Pinjaman</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo $up ?></strong></td>
    </tr>
    <tr>
        <td>Bunga</td>
        <td>:</td>
        <td> <strong><?php echo $bunga ?> %</strong></td>
    </tr>
    <tr>
        <td>Sewa Modal</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo $sewaModal; ?></strong></td>
    </tr>
    <tr>
        <td>Sewa Modal Maksimal</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo $sewaModalMaksimal; ?></strong></td>
    </tr>
    <tr>
        <td>Administrasi</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo $administrasi; ?></strong></td>
    </tr>
    <tr>
        <td>Asuransi</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo $asuransi; ?></strong></td>
    </tr>
    <tr>
        <td>Kode Nama Cabang</td>
        <td>:</td>
        <td> <strong><?php echo $kodeNamaCabang; ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Proses Lelang</td>
        <td>:</td>
        <td> <strong><?php echo $biayaProsesLelang; ?> %</strong></td>
    </tr>
    <tr>
        <td>Biaya Lelang</td>
        <td>:</td>
        <td> <strong><?php echo $biayaLelang; ?> %</strong></td>
    </tr>
    <tr>
        <td>Biaya Admin Bjdpl </td>
        <td>:</td>
        <td> <strong><?php echo $biayaAdmBjpdl; ?> %</strong></td>
    </tr>
    <tr>
        <td>Biaya Bjdpl Max</td>
        <td>:</td>
        <td> <strong><?php echo $biayaBjdplMax; ?> %</strong></td>
    </tr>
    <tr>
        <td>Nama Pimpinan Cabang</td>
        <td>:</td>
        <td> <strong><?php echo $namaPinca; ?></strong></td>
    </tr>
    <tr>
        <td>No ID</td>
        <td>:</td>
        <td> <strong><?php echo $noID; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td> <strong><?php echo $namaNasabah; ?></strong></td>
    </tr>
    <tr>
        <td>Cif</td>
        <td>:</td>
        <td> <strong><?php echo $cif; ?></strong></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td> <strong><?php echo $alamat; ?></strong></td>
    </tr>
    <tr>
        <td>Rubrik</td>
        <td>:</td>
        <td> <strong><?php echo $rubrik; ?></strong></td>
    </tr>

</table>
<br>

Terima Kasih

<br><br>

