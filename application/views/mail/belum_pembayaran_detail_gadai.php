<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pegadaian Digital Service</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            .banner {
                border: 1px solid #0F941A;
                background-color: #5EBD3B;
                padding: 5px;
                text-decoration: none;
                color: white;
            }

            .btn-primary-pegadaian {
                border: 1px solid #0F941A;
                background-color: #5EBD3B;
                color: white;
            }

            .btn-primary-pegadaian:hover{
                background-color: #0F941A;
                color: white !important;
            }
            .greenColor{
                color: #199f56;
                font-weight: 400;
                padding-bottom: 16px;
                border-bottom: 4px solid #dadada;
                margin-bottom: 32px;
            }
            .m-b-34{
                margin-bottom: 34px;
            }
            .width50p{
                width: 50%;
            }
            .text-center {
                text-align: center;
            }
            .underline {
                text-decoration: underline;
            }
            ol.c {
              list-style-type: upper-roman;
              padding-left: 20px;
            }
            .emailOrMobile{
              width: 600px;
              margin: 0 auto;
            }
            .detailGadaiContent{
              width: 300px;
              margin: 0 auto;
              border: 1px solid #CCC;
              padding: 15px;
            }
            @media (max-width: 500px){
              .emailOrMobile{
                width: 100%;
              }
              .detailGadaiContent{
                width: 100%;
              }
            }
        </style>
    </head>
    <body>
    <?php 
      function indonesian_date ($timestamp = '', $date_format = 'l, j F Y H:i') {
        if (trim ($timestamp) == '')
        {
          $timestamp = time ();
        }
        elseif (!ctype_digit ($timestamp))
        {
          $timestamp = strtotime ($timestamp);
        }
        # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace ("/S/", "", $date_format);
        $pattern = array (
          '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
          '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
          '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
          '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
          '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
          '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
          '/April/','/June/','/July/','/August/','/September/','/October/',
          '/November/','/December/',
        );
        $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
          'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
          'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
          'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
          'Oktober','November','Desember',
        );
        $date = date ($date_format, $timestamp);
        $date = preg_replace ($pattern, $replace, $date);
        $date = "{$date}";
        return $date;
      } 
    ?>
    <div class="banner">
        <div class="container">
            <h3>PT PEGADAIAN (Persero) </h3>
              <h4>Cabang/Unit <?php print $kodeNamaCabang ?></h4>
        </div>
    </div>

    <div class="container emailOrMobile">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="margin-top: 60px; border: 0;">
                    <div class="panel-body">
                        <div class="center-block text-center">
                            <img class="center-block" src="http://pds-api.affinitydigitalasia.com/assets/logo.png">
                            <br>
                        </div>
                        <div class="detailGadaiContent">
                          <?php 
                            if($checkPayment->count > 0){
                            }else{
                              print 'Pengajuan ini belum dilakukan pembayaran.';
                            }
                          ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>