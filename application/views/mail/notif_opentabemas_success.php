Hi, <?php echo $nama ?> 
<br></br><br>
Selamat Rekening Tabungan Emas Anda Sudah Aktif.

<br /><br />
<table class="table table-responsive">                                                       
    <tr>
        <td>No Rekening</td>
        <td>:</td>
        <td><strong><?php echo $noRekening ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td><strong><?php echo $nama ?></strong></td>
    </tr>
    <tr>
        <td>CIF</td>
        <td>:</td>
        <td><strong><?php echo $cif != "" ? $cif : "-"  ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</td>
        <td>:</td>
        <td><strong><?php echo $trxId ?></strong></td>
    </tr>
    <tr>
        <td>Saldo</th>
        <td>:</td>
        <td><strong><?php echo $saldo ?></strong></td>
    </tr>    
    <tr>
        <td>Tanggal Buka</td>
        <td>:</td>
        <td><strong><?php echo $tanggalBuka ?></strong></td>
    </tr>    
</table>
<br/><br/>
Anda dapat menghubungi kami dan datang untuk mengambil Buku Tabungan Emas:
<br><br>
<table class="table table-responsive">
    <tr>
        <td>Cabang Kami</td>
        <td>:</td>
        <td><strong><?php echo $namaOutlet ?></strong></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td><strong><?php echo $alamatOutlet ?></strong></td>
    </tr>
    <tr>
        <td>No Telp</td>
        <td>:</td>
        <td>
            <strong>
                <?php echo '<a href="tel:'.$teleponOutlet.'"><span class="fa fa-phone"></span> '.$teleponOutlet.'</a>' ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td>Batas Waktu</td>
        <td>: </td>
        <td><strong><?php echo $batasWaktu ?></strong></td>
    </tr>
</table>

<br /><br />
Terima kasih

