<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <meta name="format-detection" content="telephone=no" />
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
        <title>Pegadaian Digital Service</title>
        <style type="text/css">		
            /* ==> Importing Fonts <== */
            @import url(https://fonts.googleapis.com/css?family=Fredoka+One);
            @import url(https://fonts.googleapis.com/css?family=Quicksand);
            @import url(https://fonts.googleapis.com/css?family=Open+Sans);
            /* ==> Global CSS <== */
            .ReadMsgBody{width:100%;background-color:#ffffff;}
            .ExternalClass{width:100%;background-color:#ffffff;}
            .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
            html{width: 100%;}
            body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0;}
            table{border-spacing:0;border-collapse:collapse;}
            table td{border-collapse:collapse;}
            img{display:block !important;}
            a{text-decoration:none;color:#00A74E;}	

            /* ==> Responsive CSS For Tablets <== */
            @media only screen and (max-width:640px) {
                body{width:auto !important;}
                table[class="tab-1"] {width:450px !important;}
                table[class="tab-2"] {width:47% !important;text-align:left !important;}
                table[class="tab-3"] {width:100% !important;text-align:center !important;}
                img[class="img-1"] {width:100% !important;height:auto !important;}
            }
            /* ==> Responsive CSS For Phones <== */
            @media only screen and (max-width:480px) {
                body { width: auto !important; }
                table[class="tab-1"] {width:290px !important;}
                table[class="tab-2"] {width:100% !important;text-align:left !important;}
                table[class="tab-3"] {width:100% !important;text-align:center !important;}
                img[class="img-1"] {width:100% !important;}
            }
            .table td,
            .table th {
              background-color: #fff !important; }

            .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px; }
            .table > thead > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > th,
            .table > tbody > tr > td,
            .table > tfoot > tr > th,
            .table > tfoot > tr > td {
            padding: 8px;
            line-height: 1.42857;
            vertical-align: top;
            border-top: 1px solid #e7ecf1;
            text-align: inherit; }
            .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #e7ecf1; }
            .table > caption + thead > tr:first-child > th,
            .table > caption + thead > tr:first-child > td,
            .table > colgroup + thead > tr:first-child > th,
            .table > colgroup + thead > tr:first-child > td,
            .table > thead:first-child > tr:first-child > th,
            .table > thead:first-child > tr:first-child > td {
            border-top: 0; }
            .table > tbody + tbody {
            border-top: 2px solid #e7ecf1; }
            .table .table {
            background-color: #fff; }

            .table-striped > tbody > tr:nth-of-type(odd) {
              background-color: #fbfcfd; }
            
            .table-bordered th,
            .table-bordered td {
            border: 1px solid #ddd !important; }

            .btn-download-mulia {
                -moz-box-shadow:inset 0px 1px 0px 0px #caefab;
                -webkit-box-shadow:inset 0px 1px 0px 0px #caefab;
                box-shadow:inset 0px 1px 0px 0px #caefab;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77d42a), color-stop(1, #5cb811));
                background:-moz-linear-gradient(top, #77d42a 5%, #5cb811 100%);
                background:-webkit-linear-gradient(top, #77d42a 5%, #5cb811 100%);
                background:-o-linear-gradient(top, #77d42a 5%, #5cb811 100%);
                background:-ms-linear-gradient(top, #77d42a 5%, #5cb811 100%);
                background:linear-gradient(to bottom, #77d42a 5%, #5cb811 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77d42a', endColorstr='#5cb811',GradientType=0);
                background-color:#77d42a;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;
                border:1px solid #268a16;
                display:inline-block;
                cursor:pointer;
                color:#306108 !important;
                font-family:Arial;
                font-size:15px;
                font-weight:bold;
                padding:6px 24px;
                text-decoration:none;
                text-shadow:0px 1px 0px #aade7c;
                text-decoration: none;
            }
            .btn-download-mulia:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #5cb811), color-stop(1, #77d42a));
                background:-moz-linear-gradient(top, #5cb811 5%, #77d42a 100%);
                background:-webkit-linear-gradient(top, #5cb811 5%, #77d42a 100%);
                background:-o-linear-gradient(top, #5cb811 5%, #77d42a 100%);
                background:-ms-linear-gradient(top, #5cb811 5%, #77d42a 100%);
                background:linear-gradient(to bottom, #5cb811 5%, #77d42a 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cb811', endColorstr='#77d42a',GradientType=0);
                background-color:#5cb811;
                color: white;
                text-decoration: none;
            }
            .btn-download-mulia:active {
                position:relative;
                top:1px;
            }
            .download-mulia {
                text-align: center
            }

        </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">


    </head>
    <body>
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <!-- =======> START ======== -->
            <!--    CUSTOMAR THINKING    -->
            <!-- ======================= -->
            <tr>
                <td align="center">
                    <table class="tab-1" align="center" cellspacing="0" cellpadding="0" width="600">
                        <tr><td height="60"></td></tr>
                        <tr>
                            <td align="">
                                <img src="<?php echo base_url() ?>assets/pegadaian_logo.png" alt="Customer" width="200px">
                            </td>
                        </tr>
                        <tr><td height="25"></td></tr>
                        <tr>
                            <td style="font-family: 'Quicksand', sans-serif; font-weight: 700; letter-spacing: 1px; font-size: 23px; color: #02A84F; border-bottom: 3px solid #DADADA; padding: 8px 5px;">
                                <?php echo $title ?>
                            </td>
                        </tr>
                        <tr><td height="25"></td></tr>
                        <tr>
                            <td  style="font-family: 'open sans', sans-serif; font-size: 12px; color: #7c7c7c;">