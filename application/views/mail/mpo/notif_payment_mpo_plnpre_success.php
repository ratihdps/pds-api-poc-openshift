Hi, <?php echo $nama ?>
<br>
Terima kasih telah melakukan pembelian layanan <?php echo $product ?> .
<br>
<table>
    <tr>
        <td>Id Jurnal </td>
        <td>:</td>
        <td> <strong><?php echo $reffCore; ?></strong></td>
    </tr>
    <tr>
        <td>No Reff </td>
        <td>:</td>
        <td> <strong><?php echo $trxId; ?></strong></td>
    </tr>
    <tr>
        <td>Tgl Transaksi </td>
        <td>:</td>
        <td> <strong><?php echo date('d/m/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Produk </td>
        <td>:</td>
        <td> <strong><?php echo $product; ?></strong></td>
    </tr>
    <tr>
        <td>Nominal </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($idTambahan, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Admin </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($administrasi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>No Meter / ID Pelanggan </td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Pelanggan </td>
        <td>:</td>
        <td> <strong><?php echo $namaPelanggan; ?></strong></td>
    </tr>
    <tr>
        <td>Tarif / Daya </td>
        <td>:</td>
        <td> <strong><?php echo $tarifDaya; ?> VA</strong></td>
    </tr>
    
    <?php if($paymentStatus['code'] == '1'):?>
    <tr>
        <td>No Reff Biller </td>
        <td>:</td>
        <td> <strong><?php echo $reffMpo; ?></strong></td>
    </tr>
    <tr>
        <td>Jumlah KWH </td>
        <td>:</td>
        <td> <strong><?php echo $jumlahKwh; ?></strong></td>
    </tr>
    <tr>
        <td>Token </td>
        <td>:</td>
        <td> <strong><?php echo $token; ?></strong></td>
    </tr>
    <?php endif; ?>
    
    <tr>
        <td colspan="3" >&nbsp;</td>
    </tr>
    
    
    <?php if($paymentStatus['code'] == '1'):?>

    <tr>
        <td colspan="3">Rincian:</td>
    </tr>

    <tr>
        <td>Biaya Admin</td>
        <td>:</td>
        <td>Rp <?php echo number_format($byAdministrasi, 0, ",", "."); ?></td>
    </tr>
    <tr>
        <td>Biaya Materai</td>
        <td>:</td>
        <td>Rp <?php echo number_format($byMaterai, 0, ",", "."); ?></td>
    </tr>
    <tr>
        <td>PPN</td>
        <td>:</td>
        <td>Rp <?php echo number_format($byPpn, 0, ",", "."); ?></td>
    </tr>
    <tr>
        <td>PPJ</td>
        <td>:</td>
        <td>Rp <?php echo number_format($byPpj, 0, ",", "."); ?></td>
    </tr>
    <tr>
        <td>Angsuran</td>
        <td>:</td>
        <td>Rp <?php echo number_format($angsuran, 0, ",", ".") ?></td>
    </tr>
    <tr>
        <td>Biaya Token</td>
        <td>:</td>
        <td>Rp <?php echo number_format($idTambahan-$byPpn-$byPpj-$byMaterai-$angsuran, 0, ",", ".") ?> </td>
    </tr>

    <?php endif; ?>


    <tr>
        <td colspan="3" >&nbsp;</td>
    </tr>

    <!-- PAYMENT METHOD BNI -->
    <?php if ($paymentMethod == 'BNI' 
                || $paymentMethod == 'VA_BCA'
                || $paymentMethod == 'VA_BRI'
                || $paymentMethod == 'VA_PERMATA' 
                || $paymentMethod == 'VA_MANDIRI'): ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td>
            <strong>
                <?php 
                    if($paymentMethod == 'BNI')
                    {
                        echo 'Virtual Account BNI';
                    }
                    else if($paymentMethod == 'VA_BCA')
                    {
                        echo 'Virtual Account BCA';
                    }
                    else if($paymentMethod == 'VA_MANDIRI')
                    {
                        echo 'Virtual Account Mandiri';
                    }
                    else if($paymentMethod == 'VA_PERMATA')
                    {
                        echo 'Virtual Account Permata';
                    }
                    else if($paymentMethod == 'VA_BRI')
                    {
                        echo 'Virtual Account BRI';
                    }
                ?>
            </strong>
        </td>
    </tr>    
    <tr>
        <td>No Rekening VA</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Biaya </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Transfer</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", ".") ?> Include PPN</strong></td>
    </tr>
    <?php endif ?>

    <!-- PAYMENT METHOD MANDIRI -->
    <?php if ($paymentMethod == 'Mandiri'): ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong>Mandiri Click Pay</strong></td>
    </tr>  
    <tr>
        <td>No Rekening VA</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Biaya </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Transfer</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", ".") ?> Include PPN</strong></td>
    </tr>
    <tr>
        <td>Reff Biller:</td>
        <td>:</td>
        <td> <strong><?php echo $reffBiller ?></strong></td>
    </tr>
    <?php endif; ?>

    <!-- PAYMENT METHOD WALLET -->
    <?php if ($paymentMethod == 'WALLET'): ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong>WALLET</strong></td>
    </tr>
    <tr>
        <td>Reff MPO:</td>
        <td>:</td>
        <td> <strong><?php echo $reffMpo ?></strong></td>
    </tr>
    <tr>
        <td>Serial Number:</td>
        <td>:</td>
        <td> <strong><?php echo $serialNumber ?></strong></td>
    </tr>
    <tr>
        <td>SID:</td>
        <td>:</td>
        <td> <strong><?php echo $sid ?></strong></td>
    </tr>
    <?php endif; ?>

    <tr>
        <td>Waktu Pembayaran</td>
        <td>:</td>
        <td> <strong><?php echo date('d/m/Y H:i:s'); ?></strong></td>
    </tr>
    <tr>
        <td>Status </td>
        <td>:</td>
        <td> 
            <strong><?php echo $paymentStatus['label'] ?></strong>. 
            <?php echo $paymentStatus['code'] == '2' ? $paymentStatus['message'] : '' ?>
        </td>
    </tr>

</table>

<br>

<?php echo $mpoFooter ?>


