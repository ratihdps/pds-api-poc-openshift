
Hi, <?php echo $namaNasabah ?>, Selamat Link CIF Anda Berhasil! 
<br /><br />

Berikut merupakan detail data CIF Anda: <br /><br />

<table>
    <tr>
        <td>Tanggal LINK CIF</td>
        <td>:</td>
        <td> <strong><?php $date = new DateTime($tanggalCif); echo $date->format('d/m/Y H:i:s') ?></strong></td>
    </tr>
    <tr>
        <td>CIF</td>
        <td>:</td>
        <td> <strong><?php echo $cif; ?></strong></td>
    </tr>    
</table>
<br />

Terima kasih

