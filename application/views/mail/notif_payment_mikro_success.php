Hi, <?php echo $nama ?> 
<br /></br /><br />
Terima kasih telah melakukan pembayaran.
<br /><br /><br />
Transaksi Anda Berhasil:
<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong><?php echo $payment; ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</td>
        <td>:</td>
        <td> <strong><?php echo $trxId; ?></strong></td>
    </tr>
    <tr>
        <td>Produk</td>
        <td>:</td>
        <td> <strong><?php echo $produk ?></strong></td>
    </tr>
    <tr>
        <td>No Kredit</td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td> <strong><?php echo $namaNasabah; ?></strong></td>
    </tr>
    <tr>
        <td>Angsuran Ke</td>
        <td>:</td>
        <td> <strong><?php echo $angsuranKe ?></strong></td>
    </tr>
    <tr>
        <td>Angsuran</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($angsuran,0,",","."); ?></strong></td>
    </tr>
    <tr>
        <td>Administrasi</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($administrasi,0,",","."); ?></strong></td>
    </tr> 
    <tr>
        <td>Denda</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($denda,0,",","."); ?></strong></td>
    </tr> 
    <tr>
        <td>Biaya Channel</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($biayaTransaksi,0,",","."); ?></strong></td>
    </tr>
    <tr>
        <td>Total</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($total + $biayaTransaksi,0,",","."); ?></strong></td>
    </tr>
</table>
<br><br>

Terima Kasih


<br><br>