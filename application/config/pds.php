<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Admin base URL untuk fetch API dari admin
$config['admin_base_url'] = getenv('ADMIN_BASE_URL');

//Direktori untuk gambar
$config['upload_dir'] = getenv('UPLOAD_DIR');
$config['bank_image_path'] = getenv('BANK_IMAGE_PATH');

//Konfigurasi Email
$config['pds_email_host'] = getenv('PDS_EMAIL_HOST');
$config['pds_email_port'] = getenv('PDS_EMAIL_PORT');
$config['pds_email_username'] = getenv('PDS_EMAIL_USERNAME');
$config['pds_email_password'] = getenv('PDS_EMAIL_PASSWORD');
$config['pds_email_from'] = getenv('PDS_EMAIL_FROM');
$config['pds_email_fromname'] = getenv('PDS_EMAIL_FROMNAME');

//Core API Config
$config['core_password'] = getenv('CORE_PASSWORD');
$config['core_API_URL'] = getenv('CORE_API_URL');
$config['core_VA_API_URL'] = getenv('CORE_VA_API_URL');
$config['core_post_password'] = getenv('CORE_POST_PASSWORD');
$config['core_post_username'] = getenv('CORE_POST_USERNAME');
$config['core_auth_header'] = getenv('CORE_AUTH_HEADER');
$config['core_client_id'] = getenv('CORE_CLIENT_ID');
$config['core_gpoint_client_id'] = getenv('CORE_GPOINT_CLIENT_ID');

//Config Mulia
$config['mulia_image_path'] = getenv('KONTRAK_MULIA_URL');
$config['kontrak_mulia_url'] = getenv('EMAS_IMAGE_PATH');
$config['emas_image_path'] = getenv('MULIA_IMAGE_PATH');
$config['super_admin_base_url'] = getenv('SUPER_ADMIN_BASE_URL');
$config['ekyc_image_path'] = getenv('EKYC_IMAGE_PATH');

//Config API Gadai Efek
$config['gadaiefek_API_URL'] = getenv('GADAIEFEK_API_URL');
$config['gadaiefek_username'] = getenv('GADAIEFEK_USERNAME');
$config['gadaiefek_password'] = getenv('GADAIEFEK_PASSWORD');

// Config API LOS
$config['los_API_URL'] = getenv('LOS_API_URL');
$config['los_username'] = getenv('LOS_USERNAME');
$config['los_password'] = getenv('LOS_PASSWORD');

// Config API Gade Point
$config['gpoint_API_URL'] = getenv('GPOINT_API_URL');
$config['gpoint_auth_username'] = getenv('GPOINT_AUTH_USERNAME');
$config['gpoint_auth_password'] = getenv('GPOINT_AUTH_PASSWORD');
$config['gpoint_username'] = getenv('GPOINT_USERNAME');
$config['gpoint_password'] = getenv('GPOINT_PASSWORD');

// Config API GCASH Microservice
$config['msgcash_API_URL'] = getenv('MSGCASH_API_URL');
$config['msgcash_client_id'] = getenv('MSGCASH_CLIENT_ID');
$config['msgcash_client_secret'] = getenv('MSGCASH_CLIENT_SECRET');
$config['msgcash_grant_type'] = getenv('MSGCASH_GRANT_TYPE');
$config['msgcash_refresh_token'] = getenv('MSGCASH_REFRESH_TOKEN');

// Dukcapil Body
$config['use_fr_dukcapil'] = getenv('USE_FR_DUKCAPIL');
$config['position'] = getenv('POSITION');
$config['template'] = getenv('TEMPLATE');
$config['threshold'] = getenv('THRESHOLD');
$config['transaction_source'] = getenv('TRANSACTIONSOURCE');
$config['type'] = getenv('TYPE');

$config['base_url'] = getenv('BASE_URL');

//setting gosend url
$config['gosend_url'] = getenv('GOSEND_URL');

//setting gosend pass key
$config['gosend_clientid'] = getenv('GOSEND_CLIENTID');

//setting gosend pass key
$config['gosend_passkey'] = getenv('GOSEND_PASSKEY');

// setting midtrans url
$config['midtrans_url'] = getenv('MIDTRANS_URL');

// setting midtrans pass key
$config['midtrans_serverkey'] = getenv('MIDTRANS_SERVERKEY');

// setting endpoint service CoE 
$config['coe_api_url'] = getenv('COE_API_URL');

// config link aja
$config['link_aja_validation'] = ['max' => 2000000,'min' => 10000,'kelipatan' => 1000];
