<?php

require_once APPPATH . 'libraries/JWT.php';

use \Firebase\JWT\JWT;

class Authorization
{
    public static function validateToken($token)
    {
        $CI =& get_instance();
        $CI->load->model('user');
        $key = $CI->config->item('jwt_key');
        $algorithm = $CI->config->item('jwt_algorithm');
        $decoded = null;
        try{
            $result = JWT::decode($token, $key, array($algorithm));
            $accessToken = $result->access_token;
            $agen = null;
            
            if(!isset($result->agen)){
                $agen = 'android';
            }
            
            $agen = $result->agen;
            
            if( 
                ($CI->user->isValidAccessToken($accessToken) && $agen=='android' ) || 
                    ($CI->user->isValidWebAccessToken($accessToken) && $agen=='web')){
                $decoded = $result;
            }else{
                
                $decoded = null;
            }
        }catch (Exception $e){
            $decoded = null;
        }
        return $decoded;
    }

    public static function generateToken($data)
    {
        $CI =& get_instance();
        $key = $CI->config->item('jwt_key');
        $algorithm = $CI->config->item('jwt_algorithm');
        return JWT::encode($data, $key);
    }

    public static function tokenIsExist($headers)
    {
        return (array_key_exists('Authorization', $headers)
            && !empty($headers['Authorization']));
    }

    public static function validateMuliaToken($token)
    {
        $CI =& get_instance();
        $CI->load->model('user');
        $key = $CI->config->item('jwt_key');
        $algorithm = $CI->config->item('jwt_algorithm');
        $decoded = null;
        try{
            $decoded = JWT::decode($token, $key, array($algorithm));            
        }catch (Exception $e){
            $decoded = null;
        }
        return $decoded;
    }
}
