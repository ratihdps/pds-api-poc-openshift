<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegadaian
{
        /**
     * Mentdapatkan token core pegadaian untuk login
     * @return string accesst_token akses token dari pegadaian
     */
    public static function getCoreToken()
    {
        $ci =& get_instance();
        
        $ci->benchmark->mark('auth_core');
        
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        //Get Core Pegadaian Access Token
        $a='grant_type=password&username=test&password=test';
        $tokenurl="http://devpds.pegadaian.co.id:1010/oauth/token";
        $chtoken = curl_init($tokenurl);
        
        //username dan password
        $usr = "aplikasijs" ;
        $pwd = "aplikasi123";
        
        //Put key from the "Send the following to generate Access Token" section under the Production tab on mainapi.net website
        curl_setopt($chtoken, CURLOPT_HTTPHEADER, array(
            'Username: aplikasijs',
            'Authorization: Basic YXBsaWthc2lqczphcGxpa2FzaTEyMw==',
            'Content-Type: application/x-www-form-urlencoded')
        );
        
        curl_setopt($chtoken, CURLOPT_USERPWD, "$usr:$pwd");
        curl_setopt($chtoken, CURLOPT_HEADER, 0);
        curl_setopt($chtoken, CURLOPT_POST, 1);
        curl_setopt($chtoken, CURLOPT_POSTFIELDS, $a);
        curl_setopt($chtoken, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($chtoken, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chtoken, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($chtoken, CURLOPT_RETURNTRANSFER, true);

        $chtoken_result = curl_exec($chtoken);
        curl_close($chtoken);
        $token = ob_get_contents();
        $tokenObj = json_decode($chtoken_result);
        
        $ci->benchmark->mark('auth_core_end');
        
        log_message('info', "Benchmark-Auth-Core ".$ci->benchmark->elapsed_time('auth_core', 'auth_core_end'));
        
        //return string access_token
        return $tokenObj->access_token;
    }
    
    /**
     * Send request ke Pegadaian Core API
     * @param string $path path url request dengan awalan slash
     * @param array $data
     * @param string $operationId untuk keperluan benchmark
     * @return object response dari core
     */
    public static function coreRequest($path, $data, $operationId)
    {
        $ci =& get_instance();
        $ci->benchmark->mark('operation_start');
        
        //authentitaksi ke core dan dapatkan access token
        $tokenChar = self::getCoreToken();               
        $dataSend = json_encode($data);
         

        
        $url="https://devpds.pegadaian.co.id:9090".$path;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer ' . $tokenChar));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataSend);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $ch_result = curl_exec($ch);

        curl_close($ch);
        
        $ci->benchmark->mark('operation_end');       
        log_message('info', "Benchmark-".$operationId."-Core ".$ci->benchmark->elapsed_time('operation_start', 'operation_end'));
        
        return json_decode($ch_result);
    }    
           
    /**
     * Mendapatkan informasi harga emas dari Pegadaian Core
     */
    public static function hargaEmas()
    {   
        $url = '/param/hargaemas/';
        
        $data = array(
            "channelId" => "test",
            "clientId" => "test",
            "flag" => "K"        
        );
        return self::coreRequest($url, $data, 'HargaEmas');
    }
    
     public static function harga()
    {   
        $url = '/param/stl/';
        
        $data = array(
            "channelId" => "6017",
            "clientId" => "9997",
            "flag" => "K"        
        );
        return self::coreRequest($url, $data, 'GadaiSTL');
    }
    
    /*
     * Cek info customer berdasarkan code CIF
     */
    public static function check_cif($cif)
    {
        $url = '/customer/inquiry';
        $data = array(
            "cif" => $cif,
            "channelId" => "test",                
            "clientId" => "test"            
        );
        return self::coreRequest($url, $data, 'CheckCIF');
    }
    
}