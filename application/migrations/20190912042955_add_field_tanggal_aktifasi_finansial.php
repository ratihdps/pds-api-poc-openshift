<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Tanggal_Aktifasi_Finansial extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs

        // define colomn for modify table
        $column = array(
            'tanggal_aktifasi_finansial' => array(
                'type' => 'DATETIME',
                'after' => 'aktifasiTransFinansial'
                )
        );
        // add column
        $this->dbforge->add_column('user',$column);
    }

    public function down()
    {
        // $this->dbforge->drop_column('user', 'tanggal_aktifasi_finansial');
    }
}