## PDS Main API
Main logic dari PDS API. Dioptimalisasi untuk aplikasi PDS versi Android dan IOS.

## Change Log Security & Performance Update (2019-05-06)
1. Fix bug minimum kakakter pada reset password
2. Penambahan sistem lock akun ketika user gagal login 5 kali
3. Limitasi request reset password 30 menit sekali
4. Fix filter request portofolio rekening
5. Optimalisasi method `getUser()` pada models/User.php

## Installasi


Sebelum melakukan merge/pull lakukan update database dengan melakukan eksekusi query di bawah ini.

```sql
ALTER TABLE `user` ADD `next_password_reset` DATETIME NULL DEFAULT NULL AFTER `password`;
ALTER TABLE `user` ADD `is_locked` ENUM('0','1') NULL DEFAULT '0' AFTER `status`, ADD `login_fail_count` TINYINT NOT NULL DEFAULT '0' AFTER `is_locked`;
```


----
----
## Change Log Phase 4
1. Penambahan Metode Bayar Briva
2. Penambahan Fitur Gadai Efek
3. Penambahan Fitur G-Cash
4. Penambahan Fitur Payment method dan disbursment dengan Gcash
5. Penambahan Fitur G-Point
6. Bug fixing


## Instalasi Update Phase 4 (dengan base branch phase-3)

### 1. Update Database

Lakukan update database dengan melakukan eksekusi query di bawah ini pada server basis data 

```sql

INSERT INTO `config` (`id`, `variable`, `value`, `value_2`, `value_3`, `value_4`, `type`, `description`, `last_update`) VALUES (NULL, 'gadai_efek', '0', '', '', '', '', 'Status ketersediaan menu gadai di apk', CURRENT_TIMESTAMP);
INSERT INTO `config` (`id`, `variable`, `value`, `value_2`, `value_3`, `value_4`, `type`, `description`, `last_update`) VALUES (NULL, 'gadaiefek_access_token', '', '', '', '', '', 'Access token untuk API Gadai Efek', CURRENT_TIMESTAMP);
INSERT INTO `config` (`id`, `variable`, `value`, `value_2`, `value_3`, `value_4`, `type`, `description`, `last_update`) VALUES (NULL, 'los_access_token', '', '', '', '', '', 'Access token untuk LOS', CURRENT_TIMESTAMP);
INSERT INTO `config` (`id`, `variable`, `value`, `value_2`, `value_3`, `value_4`, `type`, `description`, `last_update`) VALUES (NULL, 'gpoint_access_token', '', '', '', '', '', 'Access token untuk API Gade Point', CURRENT_TIMESTAMP);
INSERT INTO `config` (`id`, `variable`, `value`, `value_2`, `value_3`, `value_4`, `type`, `description`, `last_update`) VALUES (NULL, 'msgcash_access_token', '', '', '', '', '', 'Akses token untuk API GCASH Microservice', CURRENT_TIMESTAMP);
INSERT INTO `config` (`id`, `variable`, `value`, `value_2`, `value_3`, `value_4`, `type`, `description`, `last_update`) VALUES (NULL, 'msgcash_refresh_token', '', '', '', '', '', 'Refresh token untuk API Gcah Microservice', CURRENT_TIMESTAMP);

CREATE TABLE `gcash` (
  `id` int(11) NOT NULL,
  `user_AIID` int(11) NOT NULL,
  `amount` double DEFAULT NULL,
  `kodeBank` varchar(6) DEFAULT NULL,
  `trxId` varchar(30) NOT NULL,
  `tglExpired` datetime DEFAULT CURRENT_TIMESTAMP,
  `virtualAccount` varchar(30) DEFAULT NULL,
  `vaNumber` varchar(30) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastUpdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `gcash_history` (
  `id` int(11) NOT NULL,
  `user_AIID` int(11) NOT NULL,
  `tipeTransaksi` varchar(2) NOT NULL DEFAULT 'TR',
  `tglTransaksi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `virtualAccount` varchar(30) NOT NULL,
  `kodeBank` varchar(10) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `destVa` varchar(30) DEFAULT NULL,
  `destVaNama` varchar(60) DEFAULT NULL,
  `destVaKodeBank` varchar(10) DEFAULT NULL,
  `destVaNamaBank` varchar(60) DEFAULT NULL,
  `reffBiller` varchar(60) DEFAULT NULL,
  `reffCore` varchar(30) DEFAULT NULL,
  `reffSwitching` varchar(30) DEFAULT NULL,
  `administrasi` double DEFAULT NULL,
  `tglPayment` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `last_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `payment_gcash` (
  `id` int(11) NOT NULL,
  `user_AIID` int(11) DEFAULT NULL,
  `type` char(2) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `administrasi` int(11) DEFAULT NULL,
  `bankCode` varchar(5) DEFAULT NULL,
  `destinationAccountNumber` varchar(30) DEFAULT NULL,
  `destinationAccountName` varchar(60) DEFAULT NULL,
  `destinationAccountBankCode` varchar(5) DEFAULT NULL,
  `reffSwitching` varchar(100) DEFAULT NULL,
  `tglTransaksi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `totalKewajiban` double DEFAULT NULL,
  `vaNumber` varchar(30) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


ALTER TABLE `gcash`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `gcash_history`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `payment_gcash`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `gcash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
ALTER TABLE `gcash_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
ALTER TABLE `payment_gcash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;


ALTER TABLE `transaksi_favorite` CHANGE `tipe` `tipe` ENUM('emas','gadai','mikro','payment','gcash') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;
ALTER TABLE `transaksi_favorite` ADD `kodeBank` VARCHAR(5) NULL DEFAULT NULL AFTER `last_update`, ADD `namaBank` VARCHAR(60) NULL DEFAULT NULL AFTER `kodeBank`;

CREATE TABLE `gadai_efek` (
  `id` int(11) NOT NULL,
  `user_AIID` int(11) NOT NULL,
  `namaNasabah` varchar(60) DEFAULT NULL,
  `kode_booking` varchar(8) NOT NULL,
  `bookingId` varchar(30) NOT NULL,
  `tanggalPengajuan` date DEFAULT NULL,
  `tglTransaksi` date DEFAULT NULL,
  `jenisEfek` char(1) NOT NULL,
  `jaminanEfek` text NOT NULL,
  `tenor` int(3) DEFAULT NULL,
  `totalTaksiran` double DEFAULT NULL,
  `up` double DEFAULT NULL,
  `minUp` double NOT NULL,
  `maxUp` double NOT NULL,
  `idBank` int(11) DEFAULT NULL,
  `noPengajuan` varchar(30) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `noKredit` varchar(30) DEFAULT NULL,
  `noBookingLos` varchar(30) DEFAULT NULL,
  `tglJatuhTempo` date DEFAULT NULL,
  `tglLelang` date DEFAULT NULL,
  `taksiran` double NOT NULL DEFAULT '0',
  `bunga` varchar(5) NOT NULL,
  `sewaModal` double NOT NULL DEFAULT '0',
  `sewaModalMaksimal` double DEFAULT '0',
  `tglCair` datetime DEFAULT CURRENT_TIMESTAMP,
  `tglKredit` date DEFAULT NULL,
  `waktu_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `ref_efek` (
  `id` int(11) NOT NULL DEFAULT '0',
  `kodeEfek` varchar(20) DEFAULT NULL,
  `namaEfek` varchar(255) DEFAULT NULL,
  `closingPrice` int(11) DEFAULT NULL,
  `closingDate` date DEFAULT NULL,
  `hairCut` int(11) DEFAULT NULL,
  `hairCutDate` date DEFAULT NULL,
  `statusGadai` tinyint(1) DEFAULT NULL,
  `jlhEfekBeredar` double DEFAULT NULL,
  `bmpk` double DEFAULT NULL,
  `maxLoanQty` double DEFAULT NULL,
  `usedLoanQty` int(11) DEFAULT NULL,
  `bondRating` varchar(30) DEFAULT NULL,
  `bondMaturityDate` date DEFAULT NULL,
  `satuan` int(11) DEFAULT NULL,
  `status` char(3) DEFAULT NULL,
  `tipeEfek` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `gadai_efek`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `ref_efek`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `gadai_efek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

ALTER TABLE `payment` ADD `gcashIdTujuan` VARCHAR(30) NULL DEFAULT NULL AFTER `norekWalletTujuan`;
ALTER TABLE `gte` ADD `gcashIdTujuan` VARCHAR(30) NULL DEFAULT NULL AFTER `norekBankTujuan`;
INSERT INTO `ref_cabang_flat` (`id`, `kode_outlet`, `nama_outlet`, `alamat`, `telepon`, `latitude`, `longitude`, `kode_keluirahan`, `kode_kecamatan`, `kode_kabupaten`, `kode_provinsi`, `nama_kelurahan`, `nama_kecamatan`, `nama_kabupaten`, `nama_provinsi`) VALUES (NULL, '00050', 'Unit Gadai Efek', 'Jalan Kramat Raya 162 Jakarta Pusat ', '02131555550', '', '', '', '', '', '', '', '', '', '');
ALTER TABLE `payment_mulia` ADD `nilaiTransaksi` DOUBLE NULL DEFAULT NULL AFTER `realTglTransaksi`;

```

### 2. Update Source Code

Setelah melakukan update database, lakukan update source code API ke server API PDS. Update source code bisa dilakukan dengan melakukan `pull` dari branch **phase-4**.
Ikuti langkah-langkah berikut untuk melakukan update source code.
1. Lakukan commit pada repo local
```bash
git add --all
git commit -m "Local Commit"
```
2. Buat branch baru dengan nama phase-4 dan pull branch phase-4 dari repository Gitlab. 

```bash
git checkout -b phase-4
git pull origin phase-4
```

Jika tidak ada konflik, source code API PDS sudah terupdate.

3. Buka file {{RootDirectory}}/application/config/pds.php, kemudian tambahkan dan sesuaikan beberapa konfigurasi berikut sesuai dengan lingkungan production.
```php
//Config API Gadai Efek
$config['gadaiefek_API_URL'] = 'http://103.78.215.82:9442/api/';
$config['gadaiefek_username'] = 'ug1';
$config['gadaiefek_password'] = 'Pegadaian123$';

// Config API LOS
$config['los_API_URL'] = 'http://116.206.196.69/frontlinewsx/';
$config['los_username'] = 'userpds';
$config['los_password'] = '5F605EBF8C1A3B65B1DA724CE3E06F5D855214C4';

// Config API Gade Point
$config['gpoint_API_URL'] = 'https://api-ms.pegadaian.co.id/ms/gpoint/';
$config['gpoint_auth_username'] = 'oauthusername';
$config['gpoint_auth_password'] = 'admin';
$config['gpoint_username'] = 'pds';
$config['gpoint_password'] = 'pds123';

// Config API GCASH Microservice
$config['msgcash_API_URL'] = 'https://api-ms.pegadaian.co.id/ms/gcash/';
$config['msgcash_client_id'] = 'lCdRJTQdOJHlls84WpYEcKxg1rCN35mI';
$config['msgcash_client_secret'] = '2n97FCrzPEhWuksGLy41BeOtJcfhbj0O';
$config['msgcash_grant_type'] = 'refresh_token';
$config['msgcash_refresh_token'] = 'SiFMHsf1WPi2O05Dxtxx21DUl31W6q4u';
```

### 3. Update Scheduler PDS

Login ke server API PDS, kemudian masuk ke mode edit dengan perintah `crontab -e`. Tambahkan baris berikut:
```bash
*/1 * * * * php /home/pds-api/www/index.php Cron getGadaiEfekToken 1
*/1 * * * * php /home/pds-api/www/index.php Cron getLosToken 1
*/1 * * * * php /home/pds-api/www/index.php Cron getGPointToken 1
* */2 * * * php /home/pds-api/www/index.php Cron getEfekList O
* */2 * * * php /home/pds-api/www/index.php Cron getEfekList S
* */1 * * * php /home/pds-api/www/index.php Cron getMsGcashToken 1
```

Setelah melakukan update, silahkan lakukan testing dengan menggunakan APK Prod terbaru. Jika terjadi masalah, segera ganti branch aktif ke **phase-3** dengan cara:
```bash
git checkout phase-3
```


### Step Migration
Run Generate File Migrations
- php vendor/phplucidframe/bare-migration/ci migration:bare "file name migration"
  Ex : php vendor/phplucidframe/bare-migration/ci migration:bare add_table_user

- modify sesuai kebutuhan code yang ada di dalam Dir applications/migrations

- Edit $config['migration_version'] sesuai version timestamp terakhir yang di buat
  Ex: $config['migration_version'] = 20190912034228;

- Run endpoint localhost/migrate di browser untuk menjalankan migration.

